<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Model\booking_user_details;
use App\Model\booking_detail;
use App\Model\booked_room_details;
use App\Model\payment;
use App\Model\admin_customers;
use App\Model\booking_charge;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;


class Managers_Reports extends Controller
{

            public function booked_room_report()
            {
                if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    }              
                else{

                    $property_id = Session::get('manager_outlet');

                    // $date_data = DB::table('rooms')
                    //                         ->select(DB::Raw('MIN(created_at) as min_date, 
                    //                             MAX(created_at) as max_date'))
                    //                         ->where('rooms.property_id','=',$property_id)
                    //                         ->get();

                    $date = date('Y-m-d H:i:s');

                    $date_show = date("l, M-d-Y", strtotime($date));

                    $order_status = DB::table('booking_status')
                                            ->select('id','name')->orderBy('id', 'asc')->get();

                    return view('managers.managers_reports.booked_rooms.list')->with('date',$date)
                                                                   ->with('date_show',$date_show)
                                                                   ->with('order_status',$order_status);
                }
            }  

            public function anyAjaxBookedRoomReportList(Request $request)
            {
                $post_data = $request->all();

                //  print_r($post_data); exit;

                $fr = $request->from;

                $frs = date('Y-m-d');

                //  print_r($frs);exit;


                 if (Session::get('manager_id')) {

                        $property_id = Session::get('manager_outlet');
                        $vendor_id = Session::get('manager_vendor');
                        
                    }                     

                $property_id = Session::get('property_id');
                $vendor_id = Session::get('vendor_id');                

                
                
                $booking_details = DB::table('booked_room')
                                ->select('booked_room.booking_status as order_status','booked_room.*',
                                    'rooms.id','booking_details.booking_status','admin_customers.firstname',
                                    'rooms.room_clean_status','rooms_infos.room_name','booking_status.name')
                                ->leftJoin('rooms','rooms.id','booked_room.room_id')
                                ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                ->leftJoin('booking_details','booking_details.id','=','booked_room.booking_id')
                                ->leftJoin('admin_customers','admin_customers.id','=','booking_details.customer_id')
                                ->leftJoin('booking_status','booking_status.id','booked_room.booking_status')
                                ->where('rooms.property_id','=',$property_id)
                                ->where('booked_room.date','=',$frs)
                                //  ->groupBy('rooms.id','rooms_infos.room_name')
                                ->distinct();
                                
                                //  ->get();
                //  print_r($booking_details);exit;
/*
                $booking_details  = DB::table('rooms')
                                    ->select('rooms.id','rooms_infos.room_name',
                                             'rooms.room_clean_status',
                                             'rooms.check_in_date',
                                             'rooms.check_out_date',
                                             'admin_customers.firstname',
                                             'booked_room.booking_status as order_status',
                                             'booking_status.name',
                                             'booking_status.color_code'
                                            )
                                    ->leftJoin('admin_customers','admin_customers.id','=','rooms.customer_id')
                                    ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                                    ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                    ->leftJoin('booking_info','booking_info.room_id','=','rooms.id')
                                    ->leftJoin('booking_status','booking_status.id','booked_room.booking_status')
                                    ->where('rooms.property_id','=',$property_id)
                                    ->orwhere('booked_room.date','=',$frs)
                                    // ->groupBy('rooms.id','booked_room.check_in_date','order_status',
                                    //           'booked_room.check_out_date','rooms_infos.room_name')
                                    ->distinct('rooms.id');
                                    //  ->get();
*/                                    
/*                                  
                 echo "<pre>";
                 print_r($booking_details);exit;
*/
                return Datatables::of($booking_details)
                ->addColumn('room_clean_status', function ($booking_details) {
      
                        if($booking_details->room_clean_status == 0):
                            $data = trans("messages.Need To Clean / Dirty");
                        elseif($booking_details->room_clean_status == 1):
                            $data = trans("messages.Cleaning Inprogress");  
                        elseif($booking_details->room_clean_status == 2):
                            $data = trans("messages.Clean");                            
                        endif;
                        return $data;
                })
                ->addColumn('order_status', function ($booking_details) {
      
                        if($booking_details->order_status == ""):
                            $data = $booking_details->name;     
                        elseif($booking_details->order_status == 1):
                            $data = $booking_details->name;
                        elseif($booking_details->order_status == 2):
                            $data = $booking_details->name; 
                        elseif($booking_details->order_status == 3):
                            $data = $booking_details->name;
                        elseif($booking_details->order_status == 4):
                            $data =$booking_details->name;                                               
                        elseif($booking_details->order_status == 5):
                            $data = $booking_details->name;                                                
                        endif;                        
                        return $data;
                })                               
                ->addColumn('firstname', function ($booking_details) {
                    return wordwrap(ucfirst($booking_details->firstname),20,'<br>');
                })                
                // ->addColumn('tax_total', function ($booking_details) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$booking_details->tax_total;
                //     }
                //     else {
                //         return $booking_details->tax_total.getCurrency();
                //     }
                // })
                 ->filter(function ($query) use ($request){
//  ->where('booked_room.date','=',$frs)
                    $condition = '1=1';
                    $fr = $request->from;
                    $frs = date('Y-m-d', strtotime($fr));
                    $order_status =  $request->order_status; 

                    $date = date('Y-m-d');
/*
                    $date_show = date("l, M-d-Y", strtotime($date));
                    if(!empty($fr))
                    {   
                            //  $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            //  $condition1 = $condition." and booked_room.date = ".$frs;
                            $query->where('booked_room.date','<=',$frs);                           
                    }
                    else
                    {
                            $query->where('booked_room.date','<=',$date);  
                    }
*/                    
                    if(!empty($order_status))                    
                    {
                        $order_status = Input::get('order_status');
                        $condition2   = $condition." and booking_details.booking_status = ".$order_status;

                        $query->whereRaw($condition2);
                    }
                 })
                ->make(true);
            }


            public function room_report()
            {
                if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }              
                else{

                    $property_id = Session::get('property_id');

                    $date_data = DB::table('rooms')
                                            ->select(DB::Raw('MIN(created_at) as min_date, 
                                                MAX(created_at) as max_date'))
                                            ->where('rooms.property_id','=',$property_id)
                                            ->get();
                    //  dd($date_data);
                    return view('vendors.reports.rooms.list')->with('date_data',$date_data[0]);
                }
            }  


            //Managers room reports
            public function managers_room_report()
            {
                // print_r('expression');exit;
                 if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    }

                else{

                    $property_id = Session::get('manager_outlet');

                    $date_data = DB::table('rooms')
                                            ->select(DB::Raw('MIN(created_at) as min_date, 
                                                MAX(created_at) as max_date'))
                                            ->where('rooms.property_id','=',$property_id)
                                            ->get();
                      // dd($date_data[0]);
                    return view('managers.managers_reports.rooms.list')->with('date_data',$date_data[0]);
                }
            }

            public function anyAjaxRoomReportList(Request $request)
            {
                $post_data = $request->all();

                //  print_r($post_data); exit;

                $property_id = Session::get('property_id');
                
                $booking_details  = DB::table('rooms')
                                    ->select(DB::Raw('count(booking_details.id) as booking_count,
                                                      sum(payments.paid_amount) as payments_amount,
                                                      sum(booking_details.no_of_days) as stay_length'
                                                    ),
                                            'rooms.id',
                                            'room_type.normal_price',
                                            'room_type.discount_price',
                                            'rooms_infos.room_name')
                                    ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                    ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                    ->leftJoin('booking_details','booking_details.rooms','=','rooms.id')
                                    ->leftJoin('payments','payments.booking_id','=','booking_details.id')
                                    ->where('rooms.property_id','=',$property_id)
                                    ->groupBy('rooms.id','rooms_infos.room_name'
                                        ,'room_type.normal_price','room_type.discount_price');
                                    //  ->distinct();
                                    //  ->get();
                                  
                // echo "<pre>";
                // print_r($booking_details);exit;

                return Datatables::of($booking_details)->addColumn('payments_amount', function ($booking_details) {
                    if ($booking_details->payments_amount == "") {
                      return '-';
                    } else if(getCurrencyPosition()->currency_side == 1) {
                        return getCurrency().$booking_details->payments_amount;
                    } else {
                        return $booking_details->payments_amount.getCurrency();
                    }
                })
                ->addColumn('stay_length', function ($booking_details) {
                                    if ($booking_details->stay_length == "") {
                                      return '-';
                                    }
                                    else if($booking_details->stay_length == 1){
                                        return $booking_details->stay_length." Day";
                                    }                                    
                                     else {
                                        return $booking_details->stay_length." Days";
                                    }
                                })                
                // ->addColumn('tax_total', function ($booking_details) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$booking_details->tax_total;
                //     }
                //     else {
                //         return $booking_details->tax_total.getCurrency();
                //     }
                // })
                ->filter(function ($query) use ($request){

                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and rooms.created_at BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                })
                ->make(true);
            }


            // Managers Room Reports
            public function anyAjaxManagersRoomReportList(Request $request)
            {
                $post_data = $request->all();

                //  print_r($post_data); exit;

               $property_id = Session::get('manager_outlet');
                
                $booking_details  = DB::table('rooms')
                                    ->select(DB::Raw('count(booking_details.id) as booking_count,
                                                      sum(payments.paid_amount) as payments_amount,
                                                      sum(booking_details.no_of_days) as stay_length'
                                                    ),
                                            'rooms.id',
                                            'room_type.normal_price',
                                            'room_type.discount_price',
                                            'rooms_infos.room_name')
                                    ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                    ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                    ->leftJoin('booking_details','booking_details.rooms','=','rooms.id')
                                    ->leftJoin('payments','payments.booking_id','=','booking_details.id')
                                    ->where('rooms.property_id','=',$property_id)
                                    ->groupBy('rooms.id','rooms_infos.room_name'
                                        ,'room_type.normal_price','room_type.discount_price');
                                    //  ->distinct();
                                    //  ->get();
                                  
                // echo "<pre>";
                // print_r($booking_details);exit;

                return Datatables::of($booking_details)->addColumn('payments_amount', function ($booking_details) {
                    if ($booking_details->payments_amount == "") {
                      return '-';
                    } else if(getCurrencyPosition()->currency_side == 1) {
                        return getCurrency().$booking_details->payments_amount;
                    } else {
                        return $booking_details->payments_amount.getCurrency();
                    }
                })
                ->addColumn('stay_length', function ($booking_details) {
                                    if ($booking_details->stay_length == "") {
                                      return '-';
                                    }
                                    else if($booking_details->stay_length == 1){
                                        return $booking_details->stay_length." Day";
                                    }                                    
                                     else {
                                        return $booking_details->stay_length." Days";
                                    }
                                })                
                // ->addColumn('tax_total', function ($booking_details) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$booking_details->tax_total;
                //     }
                //     else {
                //         return $booking_details->tax_total.getCurrency();
                //     }
                // })
                ->filter(function ($query) use ($request){

                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and rooms.created_at BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                })
                ->make(true);
            }


            public function manager_trasaction_report(Request $request)
            {
                if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    } 
                else{


                if(count($request->all())>0){  
                       $validation = Validator::make($request->all(), array(
                           'from' => 'required',
                           'to' => 'required',
                       ));
                       // process the validation
                       if ($validation->fails()) { 
                           return Redirect::back()->withInput()->withErrors($validation);
                       }
                       else
                        { 

                            $st = $request->from;
                            $ed = $request->to;

                            $gs = getReportSummary($st,$ed);

                            //      BOOKING COUNT

                            $booking_count_array = array();

                            $j = 0;

                            foreach($gs as $pa)
                            {
                                $date = strtotime($pa['date']).'000';
                                $booking_count_array[][] = $date.','.$pa['booking_count'];
                                $j++;
                            }

                            $booking_count_encoded = json_encode($booking_count_array,JSON_NUMERIC_CHECK);
                            $booking_count_encoded = str_replace("\"", "", $booking_count_encoded);

                            //      CHARGES 

                            $charges_amount_array = array();
                            $i = 0;
                            foreach($gs as $rch)
                            {
                                $date = strtotime($rch['date']).'000';
                                $charges_amount_array[][] = $date.','.$rch['booking_charges'];

                                $i++;   
                            }

                            $charges_amount_encoded = json_encode($charges_amount_array,JSON_NUMERIC_CHECK);
                            $charges_amount_encoded = str_replace("\"", "", $charges_amount_encoded);

                            //  dd($charges_amount_encoded);

                            //      PAYMENTS 

                            $payment_amount_array = array();

                            $j = 0;

                            foreach($gs as $pa)
                            {
                                $date = strtotime($pa['date']).'000';
                                $payment_amount_array[][] = $date.','.$pa['payments'];
                                $j++;
                            }

                            $payment_amount_encoded = json_encode($payment_amount_array,JSON_NUMERIC_CHECK);
                            $payment_amount_encoded = str_replace("\"", "", $payment_amount_encoded);

                            //  dd($payment_amount_encoded);
                        }
                    }

                else
                {

                    $st = date('Y-m-01',strtotime('this month'));
                    $ed = date('Y-m-t',strtotime('this month'));                    

                    $gs = getReportSummary($st,$ed);

                    //   dd($gs);


                    //      BOOKING COUNT

                    $booking_count_array = array();
                    $checked_in_array = array();
                    $pending_array = array();

                    $j = 0;

                    foreach($gs as $pa)
                    {
                        $date = strtotime($pa['date']).'000';
                        $booking_count_array[][] = $date.','.$pa['booking_count'];
                        $checked_in_array[][] = $date.','.$pa['checked_in'];
                        $pending_array[][] = $date.','.$pa['pending'];
                        $j++;
                    }

                    $booking_count_encoded = json_encode($booking_count_array,JSON_NUMERIC_CHECK);
                    $booking_count_encoded = str_replace("\"", "", $booking_count_encoded);
                    $checked_in_encoded = json_encode($checked_in_array,JSON_NUMERIC_CHECK);
                    $checked_in_encoded = str_replace("\"", "", $checked_in_encoded);
                    $pending_encoded = json_encode($pending_array,JSON_NUMERIC_CHECK);
                    $pending_encoded = str_replace("\"", "", $pending_encoded);                                        

                    //      CHARGES 

                    $charges_amount_array = array();
                    $i = 0;
                    foreach($gs as $rch)
                    {
                        $date = strtotime($rch['date']).'000';
                        $charges_amount_array[][] = $date.','.$rch['booking_charges'];

                        $i++;   
                    }

                    $charges_amount_encoded = json_encode($charges_amount_array,JSON_NUMERIC_CHECK);
                    $charges_amount_encoded = str_replace("\"", "", $charges_amount_encoded);

                    //  dd($charges_amount_encoded);

                    //      PAYMENTS 

                    $payment_amount_array = array();

                    $j = 0;

                    foreach($gs as $pa)
                    {
                        $date = strtotime($pa['date']).'000';
                        $payment_amount_array[][] = $date.','.$pa['payments'];
                        $j++;
                    }

                    $payment_amount_encoded = json_encode($payment_amount_array,JSON_NUMERIC_CHECK);
                    $payment_amount_encoded = str_replace("\"", "", $payment_amount_encoded);

                    //  dd($payment_amount_encoded);

                }

                    return view('managers.managers_reports.orders.list')
                                    ->with('booking_count_encoded',$booking_count_encoded)
                                    ->with('charges_amount_encoded',$charges_amount_encoded)
                                    ->with('payment_amount_encoded',$payment_amount_encoded)
                                    //  ->with('checked_in_encoded',$checked_in_encoded)
                                    //  ->with('pending_encoded',$pending_encoded)
                                    ->with('gs',$gs)
                                    ->with('st',$st)
                                    ->with('ed',$ed);
                }
            } 

            /** Report Orderds List for Vendors **/
            public function anyAjaxVendorReportOrderList(Request $request)
            {
                // if(Session::get('vendor_type') == 2)
                // {
                //     $vendor_id = Session::get('staffs_under_by');
                // }
                // else
                // {
                //     $vendor_id = Session::get('vendor_id');
                // }
                $post_data = $request->all();

                $property_id = Session::get('property_id');

                $vendor_id = Session::get('vendor_id');


                $orders  = DB::table('booking_details')
                                    ->select(DB::Raw('sum(payments.paid_amount) as payments_amount,
                                                      sum(booking_charges.price) as booking_charges_amount'
                                                    ),
                                            'booking_details.booking_random_id',
                                            'booking_details.check_out_date',
                                            'booking_details.check_in_date',
                                            'admin_customers.firstname')
                                    ->leftJoin('booking_charges','booking_charges.booking_id','=','booking_details.id')
                                    ->leftJoin('payments','payments.booking_id','=','booking_details.id')
                                    ->leftJoin('booked_room','booked_room.booking_id','booking_details.id')
                                    ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                    ->where('booking_details.outlet_id','=',$property_id)
                                    ->where('booking_details.vendor_id','=',$vendor_id)
                                    ->groupBy('booking_details.id','admin_customers.firstname');
                                    //  ->get();

                                    // print_r($orders);
                                    // exit;

                return Datatables::of($orders)->addColumn('date_start', function ($orders) {
                    $data = date("M-d-Y, l h:i:a", strtotime($orders->check_in_date));
                    return $data;
                })
                ->addColumn('date_end', function ($orders) {
                    $data = date("M-d-Y, l h:i:a", strtotime($orders->check_out_date));
                    return $data;
                })
/*                
                ->addColumn('tax_total', function ($orders) {
                    if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$orders->tax_total;
                    }
                    else {
                        return $orders->tax_total.getCurrency();
                    }
                })
*/                
                ->addColumn('total_charges', function ($orders) {
                    if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$orders->booking_charges_amount;
                    }
                    else {
                        return $orders->booking_charges_amount.getCurrency();
                    }
                })
                ->addColumn('total_payments', function ($orders) {
                    if($orders->payments_amount == "")
                    {
                        return '';
                    }
                    else if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$orders->payments_amount;
                    }
                    else {
                        return $orders->payments_amount.getCurrency();
                    }
                })  
                ->addColumn('due_amount', function ($orders) {

                    $due_amount = $orders->booking_charges_amount - $orders->payments_amount;

                    if($due_amount == 0)
                    {
                        return '';
                    }
                    else if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$due_amount;
                    }
                    else {
                        return $due_amount.getCurrency();
                    }
                })                               
                ->filter(function ($query) use ($request){
                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and booked_room.date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                
                    /*
                    if ($request->has('from') != '' && $request->has('to') != '')
                    {
                        $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                        $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                        $condition1 = $condition." and orders.created_date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";

                        $query->whereRaw($condition1);
                    }
                    if ($request->has('order_status') != '')
                    {
                        $order_status = Input::get('order_status');
                        $condition2   = $condition." and orders.order_status = ".$order_status;

                        $query->whereRaw($condition2);
                    }
                    if ($request->has('group_by'))
                    {
                        $group_by = ($request->get('group_by') != '')?$request->get('group_by'):1;
                        if($group_by == 1)
                            $start_date = " date_trunc('day', orders.created_date) AS date_start, ";
                        else if($group_by == 2)
                            $start_date = " date_trunc('week', orders.created_date) AS date_start, ";
                        else if($group_by == 3)
                            $start_date = " date_trunc('month', orders.created_date) AS date_start, ";
                        else if($group_by == 4)
                            $start_date = " date_trunc('year', orders.created_date) AS date_start, ";
                        $query->selectRaw($start_date.' MAX(orders.created_date) AS date_end')->groupBy('date_start');
                    }
                    */
                })
                ->make(true);
            }

                        /** Report Orderds List for Vendors **/
            public function anyAjaxManagerReportOrderList(Request $request)
            {
                // if(Session::get('vendor_type') == 2)
                // {
                //     $vendor_id = Session::get('staffs_under_by');
                // }
                // else
                // {
                //     $vendor_id = Session::get('vendor_id');
                // }
                $post_data = $request->all();

                $property_id = Session::get('manager_outlet');

                $vendor_id = Session::get('manager_vendor');


                $orders  = DB::table('booking_details')
                                    ->select(DB::Raw('sum(payments.paid_amount) as payments_amount,
                                                      sum(booking_charges.price) as booking_charges_amount'
                                                    ),
                                            'booking_details.booking_random_id',
                                            'booking_details.check_out_date',
                                            'booking_details.check_in_date',
                                            'admin_customers.firstname')
                                    ->leftJoin('booking_charges','booking_charges.booking_id','=','booking_details.id')
                                    ->leftJoin('payments','payments.booking_id','=','booking_details.id')
                                    ->leftJoin('booked_room','booked_room.booking_id','booking_details.id')
                                    ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                    ->where('booking_details.outlet_id','=',$property_id)
                                    ->where('booking_details.vendor_id','=',$vendor_id)
                                    ->groupBy('booking_details.id','admin_customers.firstname');
                                    //  ->get();

                                    // print_r($orders);
                                    // exit;

                return Datatables::of($orders)->addColumn('date_start', function ($orders) {
                    $data = date("M-d-Y, l h:i:a", strtotime($orders->check_in_date));
                    return $data;
                })
                ->addColumn('date_end', function ($orders) {
                    $data = date("M-d-Y, l h:i:a", strtotime($orders->check_out_date));
                    return $data;
                })
/*                
                ->addColumn('tax_total', function ($orders) {
                    if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$orders->tax_total;
                    }
                    else {
                        return $orders->tax_total.getCurrency();
                    }
                })
*/                
                ->addColumn('total_charges', function ($orders) {
                    if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$orders->booking_charges_amount;
                    }
                    else {
                        return $orders->booking_charges_amount.getCurrency();
                    }
                })
                ->addColumn('total_payments', function ($orders) {
                    if($orders->payments_amount == "")
                    {
                        return '';
                    }
                    else if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$orders->payments_amount;
                    }
                    else {
                        return $orders->payments_amount.getCurrency();
                    }
                })  
                ->addColumn('due_amount', function ($orders) {

                    $due_amount = $orders->booking_charges_amount - $orders->payments_amount;

                    if($due_amount == 0)
                    {
                        return '';
                    }
                    else if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$due_amount;
                    }
                    else {
                        return $due_amount.getCurrency();
                    }
                })                               
                ->filter(function ($query) use ($request){
                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and booked_room.date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                
                    /*
                    if ($request->has('from') != '' && $request->has('to') != '')
                    {
                        $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                        $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                        $condition1 = $condition." and orders.created_date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";

                        $query->whereRaw($condition1);
                    }
                    if ($request->has('order_status') != '')
                    {
                        $order_status = Input::get('order_status');
                        $condition2   = $condition." and orders.order_status = ".$order_status;

                        $query->whereRaw($condition2);
                    }
                    if ($request->has('group_by'))
                    {
                        $group_by = ($request->get('group_by') != '')?$request->get('group_by'):1;
                        if($group_by == 1)
                            $start_date = " date_trunc('day', orders.created_date) AS date_start, ";
                        else if($group_by == 2)
                            $start_date = " date_trunc('week', orders.created_date) AS date_start, ";
                        else if($group_by == 3)
                            $start_date = " date_trunc('month', orders.created_date) AS date_start, ";
                        else if($group_by == 4)
                            $start_date = " date_trunc('year', orders.created_date) AS date_start, ";
                        $query->selectRaw($start_date.' MAX(orders.created_date) AS date_end')->groupBy('date_start');
                    }
                    */
                })
                ->make(true);
            }

            public function date_reports($d)
            {
                //  dd($d);

                if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    } 
                else
                {
                
                    $date = $d;

                    //dd($date);

                 if (Session::get('manager_id')) {

                        $property_id = Session::get('manager_outlet');
                        $vendor_id = Session::get('manager_vendor');
                        
                    }                     

                    $property_id = Session::get('property_id');
                    $vendor_id = Session::get('vendor_id');

                    //  dd($vendor_id);

                    $booking_details = DB::table('booking_details')
                                        ->select(DB::RAW('count(booking_details.id) as booking_count'), 'admin_customers.firstname','booking_details.*','booking_info.*','booking_status.*')
                                        ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                                        ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                        ->leftJoin('booking_status','booking_status.id','booking_details.booking_status')
                                        //  ->where('booking_details.vendor_id','=',$vendor_id)
                                        ->where('booking_details.created_date',$d)
                                        ->where('booking_details.outlet_id',$property_id)
                                        ->groupBy('booking_details.id','admin_customers.firstname',
                                            'booking_info.id','booking_status.id')
                                        ->get();

                          //    dd($booking_details);
                    

                    $booking_charges = DB::table('booking_charges')
                                         ->selectRaw('sum(price) as booking_charge')
                                         ->where('created_date',$d)
                                         ->where('property_id',$property_id)
                                         // ->where('created_by',$vendor_id)
                                         ->get();
                    //  dd($booking_charges);

                    $payments = DB::table('payments')
                                         ->selectRaw('sum(paid_amount) as paid_amount')
                                         ->where('created_date',$d)
                                         // ->where('created_by',$vendor_id)
                                         ->where('property_id',$property_id)
                                         ->get();

                    $payment_list = DB::table('payments')
                                    ->select('payments.id as payment_id','payments.active_status as payment_status','payments.*','admin_customers.*','booking_details.booking_random_id')
                                    ->leftJoin('admin_customers','admin_customers.id','payments.customer_id')
                                    ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                    //  ->where('payments.vendor_id','=',$vendor_id)
                                    ->where('payments.property_id','=',$property_id)
                                    ->where('payments.created_date',$d)
                                    ->orderBy('payments.id', 'asc')
                                    ->get(); 

                    //  dd($payment_list);

                    $charges_list = DB::table('booking_charges')
                                                ->select('booking_charges.id as bc_id','booking_charges.notes as bs_notes','booking_charges.active_status as booking_status','booking_charges.*','admin_customers.*','booking_details.booking_random_id')                                       
                                                ->leftJoin('booking_details','booking_details.id','booking_charges.booking_id')
                                                ->leftJoin('admin_customers','admin_customers.id','booking_charges.customer_id')
                                                //  ->where('booking_charges.created_by','=',$vendor_id)
                                                ->where('booking_charges.property_id','=',$property_id)    
                                                ->where('booking_charges.created_date',$d)
                                                ->orderBy('booking_charges.id','asc')
                                                ->get();

                      //    dd($charges_list);


                    return view('managers.managers_reports.orders.show')->with('booking_details',$booking_details)
                                                              ->with('charges_list',$charges_list)
                                                              ->with('payment_list',$payment_list)
                                                              ->with('booking_charges',$booking_charges[0])
                                                              ->with('payments',$payments[0])
                                                              ->with('date',$date);
                }
                //  dd($booking_charges);
            }

            public function outlets_report()
            {

                if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }              
                else{

                    $property_id = Session::get('property_id');

                    $vendor_id = Session::get('vendor_id');

                    $date = date('Y-m-d H:i:s');

                    $date_show = date("l, M-d-Y", strtotime($date));


                    $date_data = DB::table('booking_details')
                                            ->select(DB::Raw('MIN(created_date) as min_date, 
                                                MAX(created_date) as max_date'))
                                            ->get();

                    return view('vendors.reports.outlets.list')->with('date',$date)
                                                                   ->with('date_show',$date_show)
                                                                   ->with('date_data',$date_data[0]);
                                                                   
                }            
            }


            public function anyAjaxOutletsReportList(Request $request)
            {
                $post_data = $request->all();

                //  print_r($post_data); exit;

                $vendor_id = Session::get('vendor_id');
                
                $booking_details  = DB::table('outlets')
                                    ->select(DB::Raw('count(booking_details.id) as booking_count,
                                                      sum(booking_details.charges) as charges_amount,
                                                      sum(booking_details.payments) as payments_amount'
                                                    ),
                                            'outlets.id',
                                            'outlet_infos.outlet_name')
                                    ->leftJoin('outlet_infos','outlet_infos.id','outlets.id')
                                    ->leftJoin('booking_details','booking_details.outlet_id','=','outlets.id')
                                    ->groupBy('outlets.id','outlet_infos.outlet_name')
                                    ->where('outlets.vendor_id','=',$vendor_id)
                                    ->distinct();
                                      //    ->get();

                 // print_r($booking_details);exit;

                //  $oc = DB::table('outlets')->where('vendor_id','=',2)->count();


                                  
                // echo "<pre>";
                // print_r($oc);exit;

                return Datatables::of($booking_details)->addColumn('payments_amount', function ($booking_details) {
                    if ($booking_details->payments_amount == 0) {
                      return '-';
                    } else if(getCurrencyPosition()->currency_side == 1) {
                        return getCurrency()." ".$booking_details->payments_amount;
                    } else {
                        return $booking_details->payments_amount." ".getCurrency();
                    }
                })
                    ->addColumn('charges_amount', function ($booking_details) {
                                        if ($booking_details->charges_amount == "") {
                                          return '-';
                                        } else if(getCurrencyPosition()->currency_side == 1) {
                                            return getCurrency()." ".$booking_details->charges_amount;
                                        } else {
                                            return $booking_details->charges_amount." ".getCurrency();
                                        }
                   })                
                // ->addColumn('tax_total', function ($booking_details) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$booking_details->tax_total;
                //     }
                //     else {
                //         return $booking_details->tax_total.getCurrency();
                //     }
                // })
                ->filter(function ($query) use ($request){

                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and booking_details.created_date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                })
                ->make(true);
            }

            public function staff_report()
            {

                if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }              
                else{

                    $property_id = Session::get('property_id');

                    $vendor_id = Session::get('vendor_id');

                    $date = date('Y-m-d H:i:s');

                    $date_show = date("l, M-d-Y", strtotime($date));


                    $date_data = DB::table('booking_details')
                                            ->select(DB::Raw('MIN(created_date) as min_date, 
                                                MAX(created_date) as max_date'))
                                            ->get();
                    //  dd($outlet_managers);

                    return view('vendors.reports.staff.list')->with('date',$date)
                                                                   ->with('date_show',$date_show)
                                                                   ->with('date_data',$date_data[0]);
                                                                   
                }            
            }


            public function anyAjaxStaffsReportList(Request $request)
            {
                $post_data = $request->all();

                //  print_r($post_data); exit;

                $vendor_id = Session::get('vendor_id');

                $property_id = Session::get('property_id');
                
                $outlet_managers = DB::table('outlet_managers')
                                        ->select('outlet_managers.id','outlet_managers.first_name')
                                        ->leftJoin('vendors_infos','vendors_infos.vendors_view_id','=','outlet_managers.vendor_id')
                                        ->leftJoin('outlets','outlets.id','=','outlet_managers.outlet_id')
                                        ->leftJoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                        ->where('outlet_managers.outlet_id','=',$property_id)
                                        ->where('outlet_managers.vendor_id','=',$vendor_id)
                                        ->orderBy('outlet_managers.id', 'desc');
                                          //    ->get();

                  //    print_r($outlet_managers);exit;

                //  $oc = DB::table('outlets')->where('vendor_id','=',2)->count();


                                  
                // echo "<pre>";
                // print_r($oc);exit;

                return Datatables::of($outlet_managers)->addColumn('payments_amount', function ($outlet_managers) {
                    if ($outlet_managers->payments_amount == 0) {
                      return '-';
                    } else if(getCurrencyPosition()->currency_side == 1) {
                        return getCurrency()." ".$outlet_managers->payments_amount;
                    } else {
                        return $outlet_managers->payments_amount." ".getCurrency();
                    }
                })
                    ->addColumn('charges_amount', function ($outlet_managers) {
                                        if ($outlet_managers->charges_amount == "") {
                                          return '-';
                                        } else if(getCurrencyPosition()->currency_side == 1) {
                                            return getCurrency()." ".$outlet_managers->charges_amount;
                                        } else {
                                            return $outlet_managers->charges_amount." ".getCurrency();
                                        }
                   })                
                // ->addColumn('tax_total', function ($outlet_managers) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$outlet_managers->tax_total;
                //     }
                //     else {
                //         return $outlet_managers->tax_total.getCurrency();
                //     }
                // })
                ->filter(function ($query) use ($request){

                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and outlet_managers.created_date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                })
                ->make(true);
            }                         

}

<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Dingo\Api\Http\Request;
use Dingo\Api\Http\Response;
use Services_Twilio;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Contracts\ArrayableInterface;
use PushNotification;
use Illuminate\Support\Facades\Redirect;
use DB;
use App;
use URL;
use App\Http\Requests;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use App\Model\vendors;
use App\Model\orders;
use App\Model\vendors_infos;
use Illuminate\Support\Facades\Text;
use App\Model\users;
use App\Model\vote_transactions;
use App\Model\api_model;
use Illuminate\Support\Facades\Input;
DB::enableQueryLog();
use Hash;
use App\Model\favorite_vendors;
use App\Model\products;
use App\Model\products_infos;
use App\Model\outlets;
use App\Model\outlet_infos;
use App\Model\stores;
use App\Model\favorite_products;


class Store extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $data) {
		$post_data = $data->all();
        if(isset($post_data['language']) && $post_data['language']!='' && $post_data['language']==2)
       {
           App::setLocale('ar');
       }
       else {
           App::setLocale('en');
       }
        
    }

    /*
     *  get feature store
     */

    public function getfeaturesstore($language_id) {
        $data = array();
        $result = array("response" => array("httpCode" => 400, "status" => false, "data" => $data));
        $orderby = 'vendors.id ASC';
        $query = 'vendors_infos.lang_id = (case when (select count(*) as totalcount from vendors_infos where vendors_infos.lang_id = ' . $language_id . ' and vendors.id = vendors_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
        $query1 = 'outlet_infos.language_id = (case when (select count(language_id) as totalcount from outlet_infos where outlet_infos.language_id = ' . $language_id . ' and outlets.id = outlet_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
        $vendors = Vendors::join('vendors_infos', 'vendors_infos.id', '=', 'vendors.id')
                ->join('outlets', 'outlets.vendor_id', '=', 'vendors.id')
                ->join('outlet_infos', 'outlet_infos.id', '=', 'outlets.id')
                ->select('vendors.id as vendors_id', 'vendors_infos.vendor_name', 'vendors.first_name', 'vendors.last_name', 'vendors.featured_image', 'vendors.logo_image', 'vendors.delivery_time as vendors_delivery_time', 'vendors.category_ids', 'vendors.average_rating as vendors_average_rating', 'outlet_infos.contact_address', 'outlets.id as outlets_id', 'outlets.vendor_id as outlets_vendors_id', 'outlet_infos.outlet_name', 'outlets.delivery_time as outlets_delivery_time', 'outlets.average_rating as outlets_average_rating', 'outlets.url_index')
                ->whereRaw($query)
                ->whereRaw($query1)
                ->where('vendors.active_status', '=', 1)
                ->where('vendors.featured_vendor', '=', 1)
                ->where('outlets.active_status', '=', 1)
                ->orderByRaw($orderby)
                ->get();
        if (count($vendors)) {
            $outlets_list = array();
            $i = 1;
            foreach ($vendors as $key => $datas) {
                $outlets_list[$datas->vendors_id]['vendors_id'] = $datas->vendors_id;
                $outlets_list[$datas->vendors_id]['vendor_name'] = $datas->vendor_name;
                $outlets_list[$datas->vendors_id]['featured_image'] = $datas->featured_image;
                $outlets_list[$datas->vendors_id]['logo_image'] = $datas->logo_image;
                $outlets_list[$datas->vendors_id]['category_ids'] = $datas->category_ids;
                $outlets_list[$datas->vendors_id]['vendors_delivery_time'] = $datas->vendors_delivery_time;
                $outlets_list[$datas->vendors_id]['vendors_average_rating'] = ($datas->vendors_average_rating == null) ? 0 : $datas->vendors_average_rating;
                $outlets_list[$datas->vendors_id]['outlets_id'] = $datas->outlets_id;
                $outlets_list[$datas->vendors_id]['outlets_delivery_time'] = $datas->outlets_delivery_time;
                $outlets_list[$datas->vendors_id]['url_index'] = $datas->url_index;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['vendor_name'] = $datas->vendor_name;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['outlets_id'] = $datas->outlets_id;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['outlets_vendors_id'] = $datas->outlets_vendors_id;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['outletname'] = $datas->outlet_name;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['contact_address'] = $datas->contact_address;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['outlets_delivery_time'] = $datas->outlets_delivery_time;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['outlets_average_rating'] = ($datas->outlets_average_rating == null) ? 0 : $datas->outlets_average_rating;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['url_index'] = $datas->url_index;
                $i ++;
            }
            $result = array("response" => array("httpCode" => 200, "status" => true, 'data' => $outlets_list));
        }
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /*
     * student login
     */

    public function store_list(Request $data) {
        $post_data = $data->all();
        $data = array();
        $result = array("response" => array("httpCode" => 400, "status" => false, "data" => $data));
        $query = 'vendors_infos.lang_id = (case when (select count(*) as totalcount from vendors_infos where vendors_infos.lang_id = ' . $post_data['language'] . ' and vendors.id = vendors_infos.id) > 0 THEN ' . $post_data['language'] . ' ELSE 1 END)';
        $query1 = 'outlet_infos.language_id = (case when (select count(outlet_infos.language_id) as totalcount from outlet_infos where outlet_infos.language_id = ' . $post_data['language'] . ' and outlets.id = outlet_infos.id) > 0 THEN ' . $post_data['language'] . ' ELSE 1 END)';
        $condition = 'vendors.active_status = 1';
        $orderby = 'vendors.id ASC';
        if (isset($post_data['city']) && $post_data['city']) {
            $condition .= ' and outlets.city_id = ' . $post_data['city'];
        }
        if (isset($post_data['location']) && $post_data['location']) {
            $condition .= ' and outlets.location_id = ' . $post_data['location'];
        }
        if (isset($post_data['category_ids']) && $post_data['category_ids']) {
            $c_ids = $post_data['category_ids'];
            //~ $condition .=" and (regexp_split_to_array(category_ids,',')::integer[] @> '{".$c_ids."}'::integer[]  and category_ids !='')";
            $c_ids = explode(",", $c_ids);
            $c_ids = implode($c_ids, "','");
            $c_ids = "'" . $c_ids . "'";
            $condition .= " and vendor_category_mapping.category in($c_ids)";
        }
        $vendors = Vendors::Leftjoin('vendors_infos', 'vendors_infos.id', '=', 'vendors.id')
                ->Leftjoin('outlets', 'outlets.vendor_id', '=', 'vendors.id')
                ->join('vendor_category_mapping', 'vendor_category_mapping.vendor_id', '=', 'vendors.id')
                ->Leftjoin('outlet_infos', 'outlet_infos.id', '=', 'outlets.id')
                ->select('vendors.id as vendors_id', 'vendors_infos.vendor_name', 'vendors.first_name', 'vendors.last_name', 'vendors.featured_image', 'vendors.logo_image', 'vendors.delivery_time as vendors_delivery_time', 'vendors.category_ids', 'vendors.average_rating as vendors_average_rating', 'outlet_infos.contact_address', 'outlets.id as outlets_id', 'outlets.vendor_id as outlets_vendors_id', 'outlet_infos.outlet_name', 'outlets.delivery_time as outlets_delivery_time', 'outlets.average_rating as outlets_average_rating')
                ->whereRaw($query)
                ->whereRaw($query1)
                ->whereRaw($condition)
                ->orderByRaw($orderby)
                ->get();
        if (count($vendors)) {
            $outlets_list = array();
            $i = 1;
            foreach ($vendors as $key => $datas) {
                $outlets_list[$datas->vendors_id]['vendors_id'] = $datas->vendors_id;
                $outlets_list[$datas->vendors_id]['vendor_name'] = $datas->vendor_name;
                $outlets_list[$datas->vendors_id]['featured_image'] = $datas->featured_image;
                $outlets_list[$datas->vendors_id]['logo_image'] = $datas->logo_image;
                $outlets_list[$datas->vendors_id]['category_ids'] = $datas->category_ids;
                $outlets_list[$datas->vendors_id]['vendors_delivery_time'] = $datas->vendors_delivery_time;
                $outlets_list[$datas->vendors_id]['vendors_average_rating'] = ($datas->vendors_average_rating == null) ? 0 : $datas->vendors_average_rating;
                $outlets_list[$datas->vendors_id]['outlets_id'] = $datas->outlets_id;
                $outlets_list[$datas->vendors_id]['outlets_delivery_time'] = $datas->outlets_delivery_time;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['vendor_name'] = $datas->vendor_name;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['outlets_id'] = $datas->outlets_id;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['outlets_vendors_id'] = $datas->outlets_vendors_id;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['outletname'] = $datas->outlet_name;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['contact_address'] = $datas->contact_address;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['outlets_delivery_time'] = $datas->outlets_delivery_time;
                $outlets_list[$datas->vendors_id]['outlets'][$i]['outlets_average_rating'] = ($datas->outlets_average_rating == null) ? 0 : $datas->outlets_average_rating;
                $i ++;
            }
            $result = array("response" => array("httpCode" => 200, "status" => true, 'data' => $outlets_list));
        }
        return json_encode($result);
    }

    /*
     * student login
     */
    public function Store_list_ajax(Request $data)
    { 
        $post_data = $data->all();
        $category_url =  isset($post_data['category_url'])?$post_data['category_url']:'';
        if($post_data['language']==2)
        {
            App::setLocale('ar');
        }
        else {
            App::setLocale('en');
        } 
        $current_lat  = isset($post_data['current_lat'])?$post_data['current_lat']:'';
        $current_long =  isset($post_data['current_long'])?$post_data['current_long']:'';
        //print_r($current_lat);exit;
        $distance = 25*1000;
        $data   = array();
        $result = array("response" => array("httpCode" => 400, "status" => false, "data" =>$data));
        $query  = 'vendors_infos.lang_id = (case when (select count(vendors_infos.lang_id) as totalcount from vendors_infos where vendors_infos.lang_id = '.$post_data['language'].' and vendors.id = vendors_infos.id) > 0 THEN '.$post_data['language'].' ELSE 1 END)';
        $query1  = 'outlet_infos.language_id = (case when (select count(outlet_infos.id) as totalcount1 from outlet_infos where outlet_infos.language_id = '.$post_data['language'].' and outlets.id = outlet_infos.id) > 0 THEN '.$post_data['language'].' ELSE 1 END)';
        $condition = 'vendors.active_status = 1';
        $orderby   = 'vendors.id ASC';
        if(isset($post_data['city']) && $post_data['city'])
        {
           // $condition .=' and cities.url_index = '.$post_data['city'];
            $condition .=" and cities.url_index = '".$post_data['city']."'";
        }
        if(isset($post_data['location']) && $post_data['location'])
        {
            //$condition .=" and zones.url_index = '".$post_data['location']."'";
        }
        if(isset($post_data['category_ids']) && $post_data['category_ids'])
        {
            $c_ids   = $post_data['category_ids'];
            $c_ids   =  explode(",",$c_ids);
            $c_ids   =  implode($c_ids,"','");
            $c_ids   = "'".$c_ids."'"; 
            //$condition .= " and (regexp_split_to_array(category_ids,',')::integer[] @> '{".$c_ids."}'::integer[]  and category_ids !='')";
            $condition .= " and vendor_category_mapping.category in($c_ids)";
        }
        if(isset($post_data['keyword']) && $post_data['keyword'])
        {
            $keyword    = pg_escape_string($post_data['keyword']);
            $condition .= " and vendors_infos.vendor_name ILIKE '%".$keyword."%'";
        }
        if(isset($post_data['sortby']) && $post_data['sortby']=="delivery_time")
        {
            $orderby =' cast (vendors.delivery_time as float) '.$post_data['orderby'];
        }
        if(isset($post_data['sortby']) && $post_data['sortby']=="rating")
        {
            $orderby =' cast(vendors.average_rating as float) '.$post_data['orderby'];
        }
        if( empty($current_lat) && empty($current_long) )
        { 
            $vendors = Vendors::join('vendors_infos','vendors_infos.id','=','vendors.id')
                    ->join('outlets','outlets.vendor_id','=','vendors.id')
                    ->join('outlet_infos','outlet_infos.id','=','outlets.id')
                    ->join('zones','zones.id','=','outlets.location_id')
                    ->join('cities','cities.id','=','outlets.city_id')
                    ->join('vendor_category_mapping','vendor_category_mapping.vendor_id','=','vendors.id')
                    //left join "vendor_category_mapping" on vendor_category_mapping.outlet_id = vendors.id
                    ->select('vendors.id as vendors_id','vendors_infos.vendor_name','vendors.first_name','vendors.last_name','vendors.featured_image','vendors.logo_image','vendors.delivery_time as vendors_delivery_time','vendors.category_ids','vendors.average_rating as vendors_average_rating','outlet_infos.contact_address','outlet_infos.outlet_name','outlets.id as outlets_id','outlets.vendor_id as outlets_vendors_id','outlet_infos.outlet_name','outlets.delivery_time as outlets_delivery_time','outlets.average_rating as outlets_average_rating','outlets.url_index','outlets.category_ids as outlets_category_ids')
                    ->whereRaw($query)
                    ->whereRaw($query1)
                    ->whereRaw($condition)
                    ->where('vendors.featured_vendor','=',1)
                    ->where('outlets.active_status','=','1')
                    ->orderByRaw($orderby)
                    ->get();
        }
        else 
        { 
            $query = 'vendors_infos.lang_id = (case when (select count(vendors_infos.id) as totalcount from vendors_infos where vendors_infos.lang_id = ' . $post_data['language'] . ' and vendors.id = vendors_infos.id) > 0 THEN ' . $post_data['language']. ' ELSE 1 END)';
            $query1 = 'outlet_infos.language_id = (case when (select count(outlet_infos.id) as totalcount from outlet_infos where outlet_infos.language_id = ' . $post_data['language'] . ' and outlets.id = outlet_infos.id) > 0 THEN ' . $post_data['language'] . ' ELSE 1 END)';
            $query2 = 'cities_infos.language_id = (case when (select count(cities_infos.language_id) as totalcount from cities_infos where cities_infos.language_id = '.$post_data['language'].' and cities.id = cities_infos.id) > 0 THEN '.$post_data['language'].' ELSE 1 END)';
            $query3 = 'zones_infos.language_id = (case when (select count(zones_infos.language_id) as totalcount from zones_infos where zones_infos.language_id = ' . $post_data['language'] . ' and zones.id = zones_infos.zone_id) > 0 THEN ' . $post_data['language'] . ' ELSE 1 END)';

            $vendors = DB::select("select vendors.logo_image,vendors.category_ids,outlets.delivery_charges_fixed,outlets.city_id,outlets.location_id,vendors.id as vendors_id,vendors.delivery_time as vendors_delivery_time,vendors.average_rating as vendors_average_rating,vendors.featured_image, outlet_infos.contact_address,outlets.vendor_id as outlets_vendors_id,outlets.id as outlets_id,outlet_infos.outlet_name,outlets.delivery_time as outlets_delivery_time,outlets.average_rating as outlets_average_rating,outlets.category_ids as outlets_category_ids,vendors_infos.vendor_name,vendors.first_name,outlets.url_index,vendors.last_name, outlets.delivery_charges_variation,outlets.minimum_order_amount,outlets.active_status,zones_infos.zone_name,cities_infos.city_name,earth_distance(ll_to_earth(".$current_lat.', '.$current_long."), ll_to_earth(outlets.latitude, outlets.longitude)) as distance  from  vendors  left join outlets on outlets.vendor_id =vendors.id left join outlet_infos on outlets.id = outlet_infos.id left Join cities  on cities.id = vendors.city_id left join cities_infos on cities_infos.id =vendors.city_id left join zones on zones.city_id =vendors.city_id left join zones_infos on zones_infos.zone_id =zones.id left join vendors_infos on vendors_infos.id = vendors.id where earth_box(ll_to_earth(".$current_lat.', '.$current_long.'), '.$distance.") @> ll_to_earth(outlets.latitude, outlets.longitude)and ".$query." and ".$query1." and ".$query2." and ".$query3." and outlets.active_status='1' and vendors.active_status=1 and vendors.featured_vendor='1' order by distance asc");
        }
        if(count($vendors))
        {
            $outlets_list = array();
            $i = 1;
            foreach($vendors as $key => $datas)
            {
                if($datas->outlets_id != 0)
                {
                    $outlets_list[$datas->vendors_id]['vendors_id']     = $datas->vendors_id;
                    $outlets_list[$datas->vendors_id]['vendor_name']    = $datas->vendor_name;
                    $outlets_list[$datas->vendors_id]['featured_image'] = $datas->featured_image;
                    $outlets_list[$datas->vendors_id]['logo_image']     = $datas->logo_image;
                    $outlets_list[$datas->vendors_id]['category_ids']   = $datas->category_ids;
                    $category_name = '';
                    $outlets_list[$datas->vendors_id]['category_ids'] = $category_name;
                    if(!empty($datas->category_ids))
                    {
                        $category_ids = geoutletCategoryLists(explode(',', $datas->category_ids),$post_data['language']);
                        if( count( $category_ids ) > 0 ) {
                            foreach( $category_ids as $cat ) {
                                $category_name .= $cat->category_name.', ';
                            }
                        }
                        $outlets_list[$datas->vendors_id]['vendor_category'] = rtrim($category_name, ', ');
                    }
                    $outlets_list[$datas->vendors_id]['vendors_delivery_time']  = $datas->vendors_delivery_time;
                    $outlets_list[$datas->vendors_id]['vendors_average_rating'] = ($datas->vendors_average_rating==null)?0:$datas->vendors_average_rating;
                    $outlets_list[$datas->vendors_id]['outlets_id'] = $datas->outlets_id;
                    $outlets_list[$datas->vendors_id]['outlet_name'] = $datas->outlet_name; 
                    $distance_km = number_format($datas->distance/1000,1);
                    $outlets_list[$datas->vendors_id]['distance_km'] = $distance_km; 
                    $outlets_list[$datas->vendors_id]['outlets_category_ids'] = $datas->outlets_category_ids;
                    $outlets_list[$datas->vendors_id]['url_index']  = $datas->url_index;
                    $outlets_list[$datas->vendors_id]['outlets_delivery_time']      = $datas->outlets_delivery_time;
                    $outlets_list[$datas->vendors_id]['outlets'][$datas->outlets_id]['vendor_name'] = $datas->vendor_name;
                    $outlets_list[$datas->vendors_id]['outlets'][$datas->outlets_id]['outlets_id']  = $datas->outlets_id;
                    $outlets_list[$datas->vendors_id]['outlets'][$datas->outlets_id]['outlet_name']  = $datas->outlet_name;
                    $outlets_list[$datas->vendors_id]['outlets'] [$datas->outlets_id]['outlets_category_ids']  = $datas->outlets_category_ids;
                    $outlets_list[$datas->vendors_id]['outlets'][$datas->outlets_id]['outlets_vendors_id']     = $datas->outlets_vendors_id;
                    $outlets_list[$datas->vendors_id]['outlets'][$datas->outlets_id]['outletname']             = $datas->outlet_name;
                    $outlets_list[$datas->vendors_id]['outlets'][$datas->outlets_id]['url_index']              = $datas->url_index ;
                    $outlets_list[$datas->vendors_id]['outlets'][$datas->outlets_id]['contact_address']        = $datas->contact_address;
                    $outlets_list[$datas->vendors_id]['outlets'][$datas->outlets_id]['outlets_delivery_time']  = $datas->outlets_delivery_time;
                    $outlets_list[$datas->vendors_id]['outlets'][$datas->outlets_id]['outlets_average_rating'] = ($datas->outlets_average_rating==null)?0:$datas->outlets_average_rating;
                    $outlets_list[$datas->vendors_id]['outlets'][$datas->outlets_id]['distance'] = $datas->distance;
                    $out_category_name = '';
                    $outlets_list[$datas->vendors_id]['outlets'][$datas->outlets_id]['outlets_category'] = $out_category_name;
                    
                    // print_r($distance_km);exit;
                    if(!empty($datas->outlets_category_ids))
                    {
                        $outlet_category_ids = geoutletCategoryLists(explode(',', $datas->outlets_category_ids),$post_data['language']);
                        if( count( $outlet_category_ids ) > 0 ) {
                            foreach( $outlet_category_ids as $out_cat ) {
                                $out_category_name .= $out_cat->category_name.', ';
                              
                            }
                        }
                        $outlets_list[$datas->vendors_id]['outlets'][$datas->outlets_id]['outlets_category'] = rtrim($out_category_name, ', ');
                       
                        $outlets_list[$datas->vendors_id]['outlets'][$datas->outlets_id]['distance_km'] = $distance_km;
                    }
                    
                    $i ++;
                }
            }

            $rateit_js  = url('assets/front/'.Session::get('general')->theme.'/plugins/rateit/src/jquery.rateit.js');
            $rateit_css = url('assets/front/'.Session::get('general')->theme.'/plugins/rateit/src/rateit.css');
            $html       = '<meta  content="text/html; charset=UTF-8" /><script src="'.$rateit_js.'"></script><link href="'.$rateit_css.'" rel="stylesheet">';
            foreach($outlets_list as $outlets_data)
            {
                if($category_url !='')
                {
					$store_url = URL::to('store/info/'.$outlets_data['url_index'].'/'.$category_url);
			    }
			    else{
					$store_url = URL::to('store/info/'.$outlets_data['url_index']);
				}
                if(file_exists(base_path().'/public/assets/admin/base/images/vendors/list/'.$outlets_data['featured_image']))
                {
                    if(count($outlets_data['outlets'])>1)
                    {
                        $url = url('/assets/admin/base/images/vendors/list/'.$outlets_data['featured_image'].'?'.time());
                        $image ='<a href="javascript:;" title="'.$outlets_data['vendor_name'].'" data-toggle="modal" data-target=".bd-example-modal-lg'.$outlets_data['vendors_id'].'" > <img alt="'.$outlets_data['outlet_name'].'" src="'.$url.'" ></a>';
                    }
                    elseif(count($outlets_data['outlets']) == 1) {
                        $url = url('/assets/admin/base/images/vendors/list/'.$outlets_data['featured_image'].'?'.time());
                        $image ='<a href="'.$store_url.'" title="'.$outlets_data['vendor_name'].'"> <img alt="'.$outlets_data['outlet_name'].'" src="'.$url.'" ></a>';
                    }
                }
                else {
                    if(count($outlets_data['outlets'])>1)
                    {
                        $image ='<a href="javascript:;" title="'.$outlets_data['vendor_name'].'" data-toggle="modal" data-target=".bd-example-modal-lg'.$outlets_data['vendors_id'].'" ><img src="{{ URL::asset("assets/admin/base/images/vendors/stores.png") }}" alt="'.$outlets_data['outlet_name'].'"></a>';
                    }
                    elseif(count($outlets_data['outlets']) == 1) {
                        $image ='<a href="'.$store_url.'" title="'.$outlets_data['vendor_name'].'"><img src="{{ URL::asset("assets/admin/base/images/vendors/stores.png") }}" alt="'.$outlets_data['outlet_name'].'"></a>';
                    }
                }

                $outlet_html ='<script>
                                    $(document ).ready(function() { 
                                        $(".close").on("click", function() { 
                                            $("body").removeClass("modal-open");
                                            $(".modal-backdrop").hide();
                                        });
                                    });
                                </script>';
                if(count($outlets_data['outlets'])>1)
                {
                    $outlet_html .='<div class="modal fade store_detials_list bd-example-modal-lg'.$outlets_data['vendors_id'].'" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button><h4 id="myLargeModalLabel" class="modal-title">'.$outlets_data['vendor_name'].'</h4><p class="store_title">'.count($outlets_data['outlets']).' '.trans("messages.Branches available near you.").'</p></div><div class="store_right_items popup_top_sections">';
                    foreach($outlets_data['outlets'] as $outlets)
                    {
						if($category_url !='')
						{
							$outlets_url = URL::to('store/info/'.$outlets['url_index'].'/'.$category_url);
						}
						else{
							$outlets_url = URL::to('store/info/'.$outlets['url_index']);
						}
                        $outlet_html .='<div class="col-md-3 col-sm-3 col-xs-6 padding0"><div class="common_item"><div class="store_itm_img">';
                        if(file_exists(base_path().'/public/assets/admin/base/images/vendors/logos/'.$outlets_data['logo_image']))
                        {
                            $url    = url('/assets/admin/base/images/vendors/logos/'.$outlets_data['logo_image'].'?'.time());
                            $oimage = '<a href="'.$outlets_url.'" title="'.$outlets['outlet_name'].'"> <img alt="'.$outlets['outlet_name'].'"  src="'.$url.'" ></a>';
                        }
                        else{
                            $oimage ='<a href="'.$outlets_url.'" title="'.$outlets['outlet_name'].'"><img src="{{ URL::asset("assets/admin/base/images/vendors/stores.png") }}" alt="'.$outlets['outlet_name'].'"></a>';
                        }
                        $distance2 = '<div class="price_sec"><b>'.$outlets['outlets_delivery_time'].' '.trans("messages.Mins").'</b></div>';
                        if( !empty($current_lat) && !empty($current_long) )
                        {
                            $distance2 = '<div class="price_sec"><b>'.$outlets['distance_km'].' KM</b></div>';
                        }
                        $outlet_html .=$oimage.$distance2.'</div><div class="store_itm_desc">
                        <a href="'.$outlets_url.'" title="'.$outlets['outlet_name'].'">'.$outlets['outlet_name'].'</a><p>'.substr($outlets['outlets_category'],0,85).'</p></div><div class="store_itm_rating"><h2>
                                <div class="rateit" data-rateit-value='.$outlets['outlets_average_rating'].' data-rateit-ispreset="true" data-rateit-readonly="true">  </div>&nbsp'. $outlets['outlets_average_rating'].' </h2></div><div class="store_itm_rating map_location">
                        <a class="location_location"><i class="glyph-icon flaticon-location-pin"></i>'.substr($outlets['contact_address'],0,50).'</a>
                        </div></div></div>';//.' '.trans("messages.Mins")
                    }
                    $outlet_html .='</div></div></div></div>';
                }
                $more ='';
                if(count($outlets_data['outlets'])>1)
                {
                    $count = count($outlets_data['outlets'])-1;
                    $more .= '<a href="javascript:;" class="right_store" title="'.$count.' '.trans("messages.Branches available").'" data-toggle="modal" data-target=".bd-example-modal-lg'.$outlets_data['vendors_id'].'">'.$count.' '.trans("messages.Branches available").'</a>';
                }
                if(count($outlets_data['outlets']) > 0)
                {
                    $distance1 = '<div class="price_sec"><b>'.$outlets_data['vendors_delivery_time'].' '.trans("messages.Mins").'</b></div>';
                    if( !empty($current_lat) && !empty($current_long) )
                    {
                        $distance1 = '<div class="price_sec"><b>'.$outlets_data['distance_km'].' KM</b></div>';
                    }
                    $html .='<div class="col-md-4 col-sm-4 col-xs-6"><div class="common_item"><div class="store_itm_img">'.$image.$distance1.'</div><div class="store_itm_desc"><a href="javascript:;" data-toggle="modal" data-target=".bd-example-modal-lg'.$outlets_data['vendors_id'].'" title="'.$outlets_data['vendor_name'].'">'.$outlets_data['vendor_name'].'</a><p>'.substr($outlets_data['vendor_category'],0,85).'</p></div><div class="store_itm_rating">'. $more.'<h2><a><div class="rateit" data-rateit-value="'.$outlets_data['vendors_average_rating'].'" data-rateit-ispreset="true" data-rateit-readonly="true"></div></a>'.$outlets_data['vendors_average_rating'].'</h2></div></div></div>'.$outlet_html;
                }
            }
            if(isset($post_data['type']) && $post_data['type']=='web')
            {
              if(isset($post_data['filter']) && $post_data['filter'] == 1){
				  
                $result = array("response" => array("httpCode" => 200, "status" => true,'data' => utf8_encode($html)));
			 }
			    else {
				    
				    $result = array("response" => array("httpCode" => 200, "status" => true,'data' => utf8_encode($html)));
				}
            }
            else {
                $result = array("response" => array("httpCode" => 200, "status" => true,'data' => $outlets_list));
            }
            //$result = array("response" => array("httpCode" => 200, "status" => true,'data'=>$outlets_list));
        }
        return json_encode($result,JSON_UNESCAPED_UNICODE);
    }

    /*
     * student login
     */

    public function store_info(Request $data) {
   
        $post_data = $data->all();
        $data = array();
        $rules = [
            'store_id' => ['required'],
            'store_id' => ['numeric'],
            'language' => ['required'],
            'language' => ['numeric'],
        ];
        $category_url = isset($post_data['category_url'])?$post_data['category_url']:'';
        $errors = $result = array();
        $validator = app('validator')->make($post_data, $rules);
       
        $distance = 25*1000;
        if ($validator->fails()) {
            $j = 0;
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[] = is_array($value) ? implode(',', $value) : $value;
            }
            $errors = implode(", \n ", $errors);
            $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => $errors));
        } else {
            $user_id = isset($post_data["user_id"]) ? $post_data["user_id"] : "";
            $language_id = $post_data["language"];
            $store_id = $post_data["store_id"];
            $result = array("response" => array("httpCode" => 400, "status" => false, "data" => $data));
            
            $query = 'vendors_infos.lang_id = (case when (select count(vendors_infos.lang_id) as totalcount from vendors_infos where vendors_infos.lang_id = ' . $language_id . ' and vendors.id = vendors_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
            $query1 = 'outlet_infos.language_id = (case when (select count(outlet_infos.language_id) as totalcount from outlet_infos where outlet_infos.language_id = ' . $language_id . ' and outlets.id = outlet_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
            $query2 = 'zones_infos.language_id = (case when (select count(zones_infos.language_id) as totalcount from zones_infos where zones_infos.language_id = ' . $language_id . ' and zones.id = zones_infos.zone_id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
            $condition = 'vendors.active_status = 1 and vendors.featured_vendor = 1';
            $vendors = Vendors::join('vendors_infos', 'vendors_infos.id', '=', 'vendors.id')
                    ->join('outlets', 'outlets.vendor_id', '=', 'vendors.id')
                    ->join('outlet_infos', 'outlets.id', '=', 'outlet_infos.id')
                     ->join('zones', 'zones.id', '=', 'outlets.location_id')  
                     ->join('zones_infos', 'zones_infos.zone_id', '=', 'zones.id')
                    ->select('vendors.id as vendors_id', 'vendors_infos.vendor_name', 'vendors.first_name', 'vendors.last_name', 'vendors.featured_image', 'vendors.logo_image', 'vendors.delivery_time as vendors_delivery_time', 'vendors.category_ids', 'vendors.average_rating as vendors_average_rating', 'outlet_infos.contact_address as outlets_contact_address', 'outlets.id as outlets_id', 'outlets.vendor_id as outlets_vendors_id', 'outlet_infos.outlet_name', 'outlets.delivery_time as outlets_delivery_time', 'outlets.average_rating as outlets_average_rating', 'outlets.delivery_charges_fixed as outlets_delivery_charges_fixed', 'outlets.pickup_time as outlets_pickup_time', 'outlets.latitude as outlets_latitude', 'outlets.longitude as outlets_longitude', 'outlets.url_index','outlets.contact_phone as outlet_phone_number','outlets.category_ids as outlets_category_ids','zones_infos.zone_name as outlet_location_name')
                    ->whereRaw($query)
                    ->whereRaw($query1) 
                    ->whereRaw($query2)
                    ->whereRaw($query2)
                    ->whereRaw($condition)
                    ->where('outlets.id', '=', $store_id)
                    ->where('outlets.active_status', '=', '1')
                    ->get();
        $category_id = '';
        if($category_url !='')
        {
			$cat_detail = getSubCategoryLists(2,'',$category_url);
			//print_r($cat_detail);exit;
			if(count($cat_detail) > 0)
			{
				$category_id = $cat_detail[0]->id;
			}
		}
		
		//print_r($category_id);exit;
            $pquery = '"products_infos"."lang_id" = (case when (select count(products_infos.lang_id) as totalcount from products_infos where products_infos.lang_id = ' . $language_id . ' and products.id = products_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
            $cquery = '"categories_infos"."language_id" = (case when (select count(categories_infos.language_id) as totalcount from categories_infos where categories_infos.language_id = ' . $language_id . ' and categories.id = categories_infos.category_id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
            $wquery = '"weight_classes_infos"."lang_id" = (case when (select count(weight_classes_infos.lang_id) as totalcount from weight_classes_infos where weight_classes_infos.lang_id = ' . $language_id . ' and weight_classes.id = weight_classes_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
            $products = Products::join('products_infos', 'products.id', '=', 'products_infos.id')
                    ->join('categories', 'categories.id', '=', 'products.category_id')
                    ->join('categories_infos', 'categories_infos.category_id', '=', 'categories.id')
                    ->leftJoin('weight_classes', 'weight_classes.id', '=', 'products.weight_class_id')
                    ->leftJoin('weight_classes_infos', 'weight_classes_infos.id', '=', 'weight_classes.id')
                    ->leftJoin('product_variants', 'products.id', '=', 'product_variants.product_id')
                    ->select('products.id as product_id', 'products.product_url', 'products.product_image', 'products.weight', 'products.original_price', 'products.discount_price', 'products.vendor_id', 'products.outlet_id', 'products_infos.description', 'products_infos.product_name', 'categories_infos.category_name', 'categories.id', 'weight_classes_infos.unit', 'weight_classes_infos.title', 'products.category_id', 'categories.url_key as cat_url',
                        'products.is_multi_price','products.quantity',
                                
                                DB::raw ( '  

                                    array_to_json( 

                                            array_agg( distinct (product_variants ) 
                                             ) 

                                    )  AS variants'
                                 )


                        )
                    ->whereRaw($pquery)
                    ->whereRaw($cquery)
                    //->whereRaw($wquery)
						
                   ->where('products.outlet_id', '=', $store_id)
                    ->where('products.active_status', '=', 1)
                    ->where('products.approval_status', '=', 1)
                    ->orderBy('categories_infos.category_name', 'asc')
                    ->groupBy('products.id','products_infos.description','products_infos.product_name','categories.id','categories_infos.category_name','weight_classes_infos.unit','weight_classes_infos.title','products.quantity')

                    ->get();
          $most_sell_products   = DB::table('orders')
                                    ->select(DB::raw('COUNT(orders_info.item_id)AS product_id_count'),'orders_info.outlet_id','orders_info.item_id as product_id', 'products.product_url', 'products.product_image', 'products.weight', 'products.original_price', 'products.discount_price', 'products.vendor_id', 'products_infos.product_name', 'products_infos.description','weight_classes_infos.unit', 'weight_classes_infos.title','products.is_multi_price','products.quantity',


                                            DB::raw ( '  

                                                array_to_json( 

                                                        array_agg( distinct (product_variants ) 
                                                         ) 

                                                )  AS variants'
                                             )

                                        )
                                    ->join('orders_info','orders.id','=','orders_info.order_id')
                                    ->join('products','products.id','=','orders_info.item_id')
                                    ->join('products_infos','products_infos.id','=','products.id')
                                    ->leftJoin('weight_classes_infos', 'weight_classes_infos.id', '=', 'products.weight_class_id')
                                     ->leftJoin('product_variants', 'products.id', '=', 'product_variants.product_id')
                                    ->groupBy('orders_info.item_id','orders_info.outlet_id','products.id','products_infos.product_name','products_infos.description','weight_classes_infos.unit','weight_classes_infos.title')
                                    ->where('orders_info.outlet_id', "=", $store_id)
                                    ->whereRaw($pquery)
                                    ->where('products.active_status', '=', 1)
                                    ->where('products.approval_status', '=', 1)
                                    ->orderBy('product_id_count', 'desc')
                                    ->limit(10)
                                    ->get();  
            $outlet_products_list = $outlet_products_cate = $outlet_products_category_url = array();
            $categories = array();
            //print_r($products);exit;
            if (count($products)) {
                $i = 1;$current_category_url = '';
                foreach ($products as $pkey => $pdatas) {
                    $outlet_products_list[$pdatas->category_name][$i]['product_id'] = $pdatas->product_id;
                    $outlet_products_list[$pdatas->category_name][$i]['product_name'] = $pdatas->product_name;
                    $outlet_products_list[$pdatas->category_name][$i]['product_url'] = $pdatas->product_url;
                    $outlet_products_list[$pdatas->category_name][$i]['description'] = $pdatas->description;
                    $outlet_products_list[$pdatas->category_name][$i]['product_image'] = $pdatas->product_image;
                    $outlet_products_list[$pdatas->category_name][$i]['weight'] = $pdatas->weight;
                    $outlet_products_list[$pdatas->category_name][$i]['unit'] = $pdatas->unit;
                    $outlet_products_list[$pdatas->category_name][$i]['title'] = $pdatas->title;
                    $outlet_products_list[$pdatas->category_name][$i]['original_price'] = $pdatas->original_price;
                    $outlet_products_list[$pdatas->category_name][$i]['discount_price'] = $pdatas->discount_price;
                    $outlet_products_list[$pdatas->category_name][$i]['vendor_id'] = $pdatas->vendor_id;
                    $outlet_products_list[$pdatas->category_name][$i]['outlet_id'] = $pdatas->outlet_id;
                    $outlet_products_list[$pdatas->category_name][$i]['category_id'] = $pdatas->category_id;
                     $outlet_products_list[$pdatas->category_name][$i]['quantity'] = $pdatas->quantity;



                    $outlet_products_list[$pdatas->category_name][$i]['is_multi_price'] = $pdatas->is_multi_price;

                    if( isset($user_id) && !empty($user_id)  && !$pdatas->is_multi_price )
                    $outlet_products_list[$pdatas->category_name][$i]['product_cart_count'] = $this->get_cart_product_count($user_id, $pdatas->product_id);

                    if( isset($user_id) && !empty($user_id))
                    $outlet_products_list[$pdatas->category_name][$i]['is_fav'] = $this->get_product_fav_count($user_id, $pdatas->product_id);

                
                    if($pdatas->is_multi_price == 1 && !empty($pdatas->variants))
                    {
                        $outlet_products_list[$pdatas->category_name][$i]['variants'] = json_decode($pdatas->variants);
                    }

                    if(isset($user_id) && !empty($user_id) && $pdatas->is_multi_price )
                    $outlet_products_list[$pdatas->category_name][$i]['variants'] = $this->get_multi_cart_product_count($user_id, $outlet_products_list[$pdatas->category_name][$i]['variants']);


                    $outlet_products_cate[$pdatas->category_name] = $pdatas->category_id;
                    if($current_category_url != $pdatas->cat_url)
                    {
                        $current_category_url = $pdatas->cat_url;
                        $outlet_products_category_url[$pdatas->category_name]['category_url_key'] = $current_category_url;
                    }
                    $categories[$pdatas->cat_url]['category_id'] = $pdatas->category_id;
                    $categories[$pdatas->cat_url]['category_name'] = $pdatas->category_name;
                    $categories[$pdatas->cat_url]['url_key'] = $pdatas->cat_url;
                    $i++;
                }
            }
               
            if (count($vendors)) {
                $outlet_info = array();
                foreach ($vendors as $key => $datas) {
                    $outlet_info[$key]['outlets_id'] = $datas->outlets_id;
                    $outlet_info[$key]['vendors_id'] = $datas->vendors_id;
                    $outlet_info[$key]['vendor_name'] = $datas->vendor_name;
                    $outlet_info[$key]['first_name'] = $datas->first_name;
                    $outlet_info[$key]['last_name'] = $datas->last_name;
                    $outlet_info[$key]['featured_image'] = $datas->featured_image;
                    $outlet_info[$key]['logo_image'] = $datas->logo_image;
                     $outlet_info[$key]['category_ids']   = $datas->category_ids;
                  $outlet_info[$key]['vendors_delivery_time'] = $datas->vendors_delivery_time;
                    $outlet_info[$key]['category_ids'] = $datas->category_ids;
                    $outlet_info[$key]['vendors_average_rating'] = ($datas->vendors_average_rating == null) ? 0 : $datas->vendors_average_rating;
                    $outlet_info[$key]['outlets_contact_address'] = $datas->outlets_contact_address;
                     $outlet_info[$key]['outlet_phone_number'] = $datas->outlet_phone_number;
                    $outlet_info[$key]['outlets_vendors_id'] = $datas->outlets_vendors_id;
                    $outlet_info[$key]['outlet_name'] = $datas->outlet_name;
                    $outlet_info[$key]['outlets_delivery_time'] = $datas->outlets_delivery_time;
                    $outlet_info[$key]['outlets_average_rating'] = ($datas->outlets_average_rating == null) ? 0 : $datas->outlets_average_rating;
                    $outlet_info[$key]['outlets_delivery_charges_fixed'] = $datas->outlets_delivery_charges_fixed;
                    $outlet_info[$key]['outlets_pickup_time'] = $datas->outlets_pickup_time;
                    $outlet_info[$key]['outlets_latitude'] = $datas->outlets_latitude;
                    $outlet_info[$key]['outlets_longitude'] = $datas->outlets_longitude;
                    $outlet_info[$key]['outlet_location_name']   = $datas->outlet_location_name;
                    $outlet_info[$key]['url_index'] = $datas->url_index;
                    $outlet_info[$key]['products'] = $outlet_products_list;
                    $outlet_info[$key]['categories'] = $categories;
                    $outlet_info[$key]['categories_url'] = $outlet_products_category_url;
                    $out_category_name = '';
                    $outlet_info[$key]['outlets_category'] = $out_category_name;
                    $outlet_info[$key]['distance']   = $distance;
                    $outlet_info[$key]['outlet_location_name']   = $datas->outlet_location_name;
                    $distance_km = number_format($distance/100,2);
                    if(!empty($datas->outlets_category_ids))
                    {
                        $outlet_category_ids = geoutletCategoryLists(explode(',', $datas->outlets_category_ids),$post_data['language']);
                        if( count( $outlet_category_ids ) > 0 ) {
                            foreach( $outlet_category_ids as $out_cat ) {
                                $out_category_name .= $out_cat->category_name.', ';
                            }
                        }
                        $outlet_info[$key]['outlets_category'] = rtrim($out_category_name, ', ');
                    }
                }


                foreach ($most_sell_products    as $key => $mmo) {

                    if( isset($user_id) && !empty($user_id) && !$mmo->is_multi_price )
                    $most_sell_products[$key]->product_cart_count = $this->get_cart_product_count($user_id, $mmo->product_id);

                    if( isset($user_id) && !empty($user_id))
                    $most_sell_products[$key]->is_fav = $this->get_product_fav_count($user_id, $mmo->product_id);


                    if($mmo->is_multi_price == 1 && !empty($mmo->variants))
                    {
                        $most_sell_products[$key]->variants = json_decode($mmo->variants);
                    }


                    if(isset($user_id) && !empty($user_id) && $mmo->is_multi_price )
                    $most_sell_products[$key]->variants = $this->get_multi_cart_product_count($user_id,  $most_sell_products[$key]->variants);

                }
                $result = array("response" => array("httpCode" => 200, "status" => true ,'data' => $outlet_info,'most_sell_products' => $most_sell_products,'category_url' => $category_url,'category_id' => $category_id ));
            }
        }
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function get_cart_product_count($user_id, $product_id) {

        if ($user_id == "") {
            return 0;
        } else {
            $cart_count = DB::table('cart')
                    ->join('cart_detail', 'cart_detail.cart_id', '=', 'cart.cart_id')
                    ->select('cart_detail.quantity')
                    ->where("cart_detail.product_id", "=", $product_id)
                    ->where("cart.user_id", "=", $user_id)
                    ->get();
            //print_r($cart_count[0]->quantity);exit;
            if (count($cart_count) > 0) {
                return $cart_count[0]->quantity;
            }
            return 0;
        }
    }

    public function get_multi_cart_product_count($user_id, $product_vars) {

        if ($user_id == "") {
            return 0;
        } else {

            foreach( $product_vars as $k=>$var)
            {
                    $cart_count = DB::table('cart')
                    ->join('cart_detail', 'cart_detail.cart_id', '=', 'cart.cart_id')
                    ->select('cart_detail.quantity')
                    ->where("cart_detail.product_id", "=", $var->product_id)
                    ->where("cart.user_id", "=", $user_id)
                    ->where("cart_detail.product_variant_id", $var->variant_id)
                    ->first();

                    $product_vars[$k]->product_cart_count = $cart_count?$cart_count->quantity:0;
            }

            return $product_vars;
        }
    }


    public function get_product_fav_count($user_id, $product_id)
    {
        if ($user_id == "") {
            return 0;
        } else {
            $fav_count = DB::table('favorite_products')
                    ->join('products', 'products.id', '=', 'favorite_products.product_id')
                    ->select(DB::raw('count(favorite_products.id) as fav'))
                    ->where("favorite_products.product_id", "=", $product_id)
                    ->where("favorite_products.user_id", "=", $user_id)
                    ->where("favorite_products.status", "=", true)
                    ->get();

            return $fav_count[0]->fav;
        }

    }


    public function product_list(Request $data) {
        $post_data = $data->all();
        $store_key = $post_data['outlet_key'];
        //print_r($store_key);exit;
        $category_url = $post_data['category_url'];
         $sub_category = $post_data['subcategory'];
       // $pro_category_url = $post_data['pro_category_url'];
        $language_id = $post_data['language'];
        $user_id = isset($post_data["user_id"]) ? $post_data["user_id"] : "";
        $keyword = isset($post_data['keyword']) ? $post_data['keyword'] : '';
        $data = array();
        $result = array("response" => array("httpCode" => 400, "status" => false, "data" => $data));
        $condtion = " products.active_status = 1";
        if ($keyword) {
            $condtion .= " and products_infos.product_name ILIKE '%" . $keyword . "%'";
        }
       /*  $category_id = '';
        if($pro_category_url !='')
        {
			$cat_detail = getSubCategoryLists(2,'',$pro_category_url);
			//print_r($cat_detail);exit;
			if(count($cat_detail) > 0)
			{
				$category_id = $cat_detail[0]->id;
			}
		} */
		//print_r($category_url);exit;
        $pquery = '"products_infos"."lang_id" = (case when (select count(*) as totalcount from products_infos where products_infos.lang_id = ' . $language_id . ' and products.id = products_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
        $cquery = '"categories_infos"."language_id" = (case when (select count(*) as totalcount from categories_infos where categories_infos.language_id = ' . $language_id . ' and categories.id = categories_infos.category_id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
        $wquery = '"weight_classes_infos"."lang_id" = (case when (select count(*) as totalcount from weight_classes_infos where weight_classes_infos.lang_id = ' . $language_id . ' and weight_classes.id = weight_classes_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
        $discount_query = '
                        (case when coalesce(products.original_price,0) = 0 then 0 else 
                            ROUND( (products.original_price - products.discount_price ) /products.original_price *100 )  end 
                        )
                        as product_discount ';

        $products = Products::join('products_infos', 'products.id', '=', 'products_infos.id')
                ->join('categories', 'categories.id', '=', 'products.category_id')
                ->join('categories_infos', 'categories_infos.category_id', '=', 'categories.id')
                ->join('weight_classes', 'weight_classes.id', '=', 'products.weight_class_id')
                ->join('weight_classes_infos', 'weight_classes_infos.id', '=', 'weight_classes.id')
                ->join('outlets', 'outlets.id', '=', 'products.outlet_id')
                ->leftJoin('product_variants', 'products.id', '=', 'product_variants.product_id')
                ->select('products.id as product_id', 'products.product_url','products.vendor_category_id', 'products.product_image', 'products.weight', 'products.original_price', 'products.discount_price', 'products.vendor_id', 'products.outlet_id', 'products_infos.description', 'products_infos.product_name', 'categories_infos.category_name', 'categories.id', 'weight_classes_infos.unit', 'weight_classes_infos.title', 'products.category_id', 'categories.url_key', 'categories.url_key as cat_url', 'outlets.id as outlet_id',
                    'products.quantity','products.weight_class_id','is_multi_price', DB::raw($discount_query) ,
                       DB::raw ( '  array_to_json( array_agg( product_variants order by  product_variants.weight_value desc) )  AS variants'))
                ->whereRaw($pquery)
                ->whereRaw($cquery)
                ->whereRaw($wquery)
                ->where('outlets.url_index', '=', $store_key);

        if ($category_url != 'all' && !$sub_category) {
            $products = $products->where('categories.url_key', '=', $category_url);
        }

        if (isset($sub_category) && $sub_category!='') {
            $products = $products->where('products.sub_category_id', '=', $sub_category);
        } 

        $products = $products->whereRaw($condtion)
                ->where('products.approval_status', '=', 1)
                ->groupBy('products.id','products.product_url','products_infos.product_name',  'products_infos.description', 
                     
                    'categories_infos.category_name','categories.id','weight_classes_infos.unit','weight_classes_infos.title' , 'outlets.id')
                ->orderBy('categories_infos.category_name', 'asc')
                ->get();

               //print_r($products);exit;
        $categories = array();

        foreach ($products as $key => $prod) {

            if(isset($user_id) && !empty($user_id))
            $products[$key]->product_cart_count = $this->get_cart_product_count($user_id, $prod->product_id);


            if(isset($user_id) && !empty($user_id))
            $products[$key]->is_fav = $this->get_product_fav_count($user_id, $prod->product_id);

            $categories['category_name'] = $prod->category_name;
            $categories['url_key'] = $prod->cat_url;

            if($prod->is_multi_price == 1 && !empty($prod->variants) )
            $products[$key]->variants = json_decode($prod->variants);

            if(isset($user_id) && !empty($user_id) && $prod->is_multi_price && !empty($products[$key]->variants))
            $products[$key]->variants  = $this->get_multi_cart_product_count($user_id, $products[$key]->variants);

        }


        //print_r($categories);exit;
        $query = 'vendors_infos.lang_id = (case when (select count(vendors_infos.lang_id) as totalcount from vendors_infos where vendors_infos.lang_id = ' . $language_id . ' and vendors.id = vendors_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
        $query1 = 'outlet_infos.language_id = (case when (select count(outlet_infos.language_id) as totalcount from outlet_infos where outlet_infos.language_id = ' . $language_id . ' and outlets.id = outlet_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
        $condition = 'vendors.active_status = 1 and vendors.featured_vendor = 1';
        $vendors = Vendors::join('vendors_infos', 'vendors_infos.id', '=', 'vendors.id')
                ->join('outlets', 'outlets.vendor_id', '=', 'vendors.id')
                ->join('outlet_infos', 'outlets.id', '=', 'outlet_infos.id')
                ->select('vendors.id as vendors_id', 'vendors_infos.vendor_name', 'vendors.first_name', 'vendors.last_name', 'vendors.featured_image', 'vendors.logo_image', 'vendors.delivery_time as vendors_delivery_time', 'vendors.category_ids', 'vendors.average_rating as vendors_average_rating', 'outlet_infos.contact_address as outlets_contact_address', 'outlets.id as outlets_id', 'outlets.vendor_id as outlets_vendors_id', 'outlet_infos.outlet_name', 'outlets.delivery_time as outlets_delivery_time', 'outlets.average_rating as outlets_average_rating', 'outlets.delivery_charges_fixed as outlets_delivery_charges_fixed', 'outlets.pickup_time as outlets_pickup_time', 'outlets.latitude as outlets_latitude', 'outlets.longitude as outlets_longitude', 'outlets.url_index')
                ->whereRaw($query)
                ->whereRaw($query1)
                ->whereRaw($condition)
                ->where('outlets.url_index', '=', $store_key)
                ->where('outlets.active_status', '=', '1')
                ->get();
        $outlet_info = array();
        if (count($vendors)) {
            foreach ($vendors as $key => $datas) {
                $outlet_info[$key]['outlets_id'] = $datas->outlets_id;
                $outlet_info[$key]['vendors_id'] = $datas->vendors_id;
                $outlet_info[$key]['vendor_name'] = $datas->vendor_name;
                $outlet_info[$key]['first_name'] = $datas->first_name;
                $outlet_info[$key]['last_name'] = $datas->last_name;
                $outlet_info[$key]['featured_image'] = $datas->featured_image;
                $outlet_info[$key]['logo_image'] = $datas->logo_image;
                $outlet_info[$key]['vendors_delivery_time'] = $datas->vendors_delivery_time;
                $outlet_info[$key]['category_ids'] = $datas->category_ids;
                $outlet_info[$key]['vendors_average_rating'] = ($datas->vendors_average_rating == null) ? 0 : $datas->vendors_average_rating;
                $outlet_info[$key]['outlets_contact_address'] = $datas->outlets_contact_address;
                $outlet_info[$key]['outlets_vendors_id'] = $datas->outlets_vendors_id;
                $outlet_info[$key]['outlet_name'] = $datas->outlet_name;
                $outlet_info[$key]['outlets_delivery_time'] = $datas->outlets_delivery_time;
                $outlet_info[$key]['outlets_average_rating'] = ($datas->outlets_average_rating == null) ? 0 : $datas->outlets_average_rating;
                $outlet_info[$key]['outlets_delivery_charges_fixed'] = $datas->outlets_delivery_charges_fixed;
                $outlet_info[$key]['outlets_pickup_time'] = $datas->outlets_pickup_time;
                $outlet_info[$key]['outlets_latitude'] = $datas->outlets_latitude;
                $outlet_info[$key]['outlets_longitude'] = $datas->outlets_longitude;
                $outlet_info[$key]['url_index'] = $datas->url_index;
            }
        }
        $result = array("response" => array("httpCode" => 200, "status" => true, 'data' => $products, 'outlet_info' => $outlet_info, 'categories' => $categories));
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

     public function addto_favourite(Request $data) {
        $post_data = $data->all();
        $data = array();
        $rules = [
            'user_id' => ['required'],
            'vendor_id' => ['required'],
            'token' => ['required'],
        ];
        $errors = $result = array();
        $validator = app('validator')->make($post_data, $rules);
        if ($validator->fails()) {
            $j = 0;
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[] = is_array($value) ? implode(',', $value) : $value;
            }
            $errors = implode(", \n ", $errors);
            $result = array("response" => array("httpCode" => 200, "status" => false, "Message" => $errors));
        } else {

            try {
                $check_auth = JWTAuth::toUser($post_data['token']);
                $ucdata = DB::table('favorite_vendors')
                        ->select('favorite_vendors.id', 'favorite_vendors.status')
                        ->where("favorite_vendors.customer_id", "=", $post_data['user_id'])
                        ->where("favorite_vendors.vendor_id", "=", $post_data['vendor_id'])
                        ->get();
                if (count($ucdata)) {
                    $favourite = Favorite_vendors::find($ucdata[0]->id);
                    $favourite->status = $ucdata[0]->status ? 0 : 1;
                    $favourite->save();
                    $status = $ucdata[0]->status ? 0 : 1;

                     $result = array("response" => array("httpCode" => 200, "Message" => trans("messages.The shop has been added to your favorites list"), "status" => 1));
                    if ($ucdata[0]->status == 1) {
                       $result = array("response" => array("httpCode" => 200, "Message" => trans("messages.The shop has been deleted from your favorites list"), "status" => $status));
                    }
                } else {
                    //echo "asdfa";exit;
                    $favourite = new Favorite_vendors;
                    //print_r($favourite);exit;
                    $favourite->vendor_id = $post_data['vendor_id'];
                    $favourite->customer_id = $post_data['user_id'];
                    $favourite->status = 1;
                    $favourite->save();
                    $result = array("response" => array("httpCode" => 200, "Message" => trans("messages.The shop has been added to your favorites list"), "status" => 1));
                }
            } catch (JWTException $e) {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
            } catch (TokenExpiredException $e) {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
            }
        }
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }
    /* Store information mobile */

    public function store_info_mob(Request $data) {

        $post_data = $data->all();
        $data = array();
        $rules = [
            'language' => ['required', 'numeric'],
            'store_id' => ['required', 'numeric'],
        ];
        $delivery_cost = "";
        $errors = $result = array();
        $validator = app('validator')->make($post_data, $rules);
        if ($validator->fails()) {
            $j = 0;
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[] = is_array($value) ? implode(',', $value) : $value;
            }
            $errors = implode(", \n ", $errors);
            $result = array("response" => array("httpCode" => 400, "Message" => $errors));
        } else {
            $language_id = $post_data['language'];
            $store_id    = $post_data["store_id"];
            $user_id     = isset($post_data["user_id"]) ? $post_data["user_id"] : "";
            $token       = isset($post_data["token"]) ? $post_data["token"] : "";
            $vendor_det  = array();
            $vendors = Outlets::find($store_id);
            if (!count($vendors)) {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Invalid Store")));
                return json_encode($result);
            }
            if ($user_id != '') {
                try {
                    $check_auth = JWTAuth::toUser($post_data['token']);
                } catch (JWTException $e) {
                    $result = array("response" => array("httpCode" => 400, "Message" => trans("messages.Kindly check the user credentials")));
                    return json_encode($result);
                } catch (TokenExpiredException $e) {
                    $result = array("response" => array("httpCode" => 400, "Message" => trans("messages.Kindly check the user credentials")));
                    return json_encode($result);
                }
            }
            $delivery_settings = $this->get_delivery_settings();
			if($delivery_settings->on_off_status == 1)
			{
				if($delivery_settings->delivery_type == 1)
				{
				   $delivery_cost = $delivery_settings->delivery_cost_fixed;
				}
				if($delivery_settings->delivery_type == 2)
				{
				  $delivery_cost = $delivery_settings->flat_delivery_cost;
				}
			}
            $vendor_id = $vendors->vendor_id;
            $vendor_info = Stores::vendor_information($vendor_id);
            $store_info = Stores::store_information($language_id, $store_id);
            $vendor_det['vendor_id']   = $store_info->vendors_id;
            $vendor_det['vendor_name'] = $vendor_info->vendor_name;
            $featured_image = $logo_image = URL::asset('assets/front/'.Session::get("general")->theme.'/images/store_detial.png');
            if (file_exists(base_path() . '/public/assets/admin/base/images/vendors/thumb/detail/' . $store_info->featured_image) && $store_info->featured_image != '') {
                $featured_image = url('/assets/admin/base/images/vendors/thumb/detail/' . $store_info->featured_image);
            }
            if (file_exists(base_path() . '/public/assets/admin/base/images/vendors/logos/' . $store_info->logo_image) && $store_info->logo_image != '') {
                $logo_image = url('/assets/admin/base/images/vendors/logos/' . $store_info->logo_image);
            }
            $category_ids = explode(',', $store_info->category_ids);
            $category_name = '';

            $get_categorys = getVendorCategoryList($vendor_id);
            //print_r($get_categorys);exit;
            foreach ($get_categorys as $category) {
                $category_name .= $category->category_name . ', ';
            }
            //echo  $category_name;exit;
            $vendor_det['featured_image'] = $featured_image;
            $vendor_det['logo_image'] = $logo_image;
            $vendor_det['vendors_delivery_time'] = $store_info->vendors_delivery_time;
            $vendor_det['category_ids'] = rtrim($category_name, ', ');
            $vendor_det['vendors_average_rating'] = ($store_info->vendors_average_rating == null) ? 0 : $store_info->vendors_average_rating;
            $vendor_det['outlets_contact_address'] = $store_info->outlets_contact_address;
            $vendor_det['outlets_id'] = $store_info->outlets_id;
            $vendor_det['outlets_vendors_id'] = $store_info->outlets_vendors_id;
            $vendor_det['outlet_name'] = $store_info->outlet_name;
            $vendor_det['outlets_delivery_time'] = $store_info->outlets_delivery_time;
            $vendor_det['outlets_average_rating'] = ($store_info->outlets_average_rating == null) ? 0 : $store_info->outlets_average_rating;
            $vendor_det['outlets_pickup_time'] = $store_info->outlets_pickup_time;
            $vendor_det['outlets_latitude'] = $store_info->outlets_latitude;
            $vendor_det['minimum_order_amount'] = $store_info->minimum_order_amount;
            $vendor_det['outlet_location_name'] = $store_info->outlet_location_name;
            $vendor_det['outlets_longitude'] = $store_info->outlets_longitude;
            if($delivery_cost != ''){
			$vendor_det['outlets_delivery_charges_fixed'] = $delivery_cost;
               }
			else 
			{
				  $vendor_det['outlets_delivery_charges_fixed'] = $store_info->outlets_delivery_charges_fixed;
				}
          
            $vendor_det['outlets_delivery_charges_variation'] = $store_info->outlets_delivery_charges_variation;
            // print_r($vendor_det);exit;
            $m_categories = getVendorCategoryList($vendor_id);
            $category = array();
            if (count($m_categories) > 0) {
                $c = 0;
                foreach ($m_categories as $main) {
                    $sub_category_list = Stores::get_sub_category_list($main->id);

                    //print_r($sub_category_list); exit;
                    $sub_category = array();
                    if (count($sub_category_list) > 0) {
                        $category[$c]['main_category_id'] = $main->id;
                        $category[$c]['main_category_name'] = $main->category_name;
                        $category[$c]['main_category_url_key'] = $main->url_key;
                        $s = 0;
                        foreach ($sub_category_list as $sub) {
                            $sub_category[$s]['main_category_id'] = $main->id;
                            $sub_category[$s]['sub_category_id'] = $sub->id;
                            
                            $sub_category[$s]['sub_category_name'] = $sub->category_name;
                            $sub_category[$s]['sub_category_url_key'] = $sub->url_key;
                            $category_image = URL::asset('assets/front/tijik/images/no_image.png');
                           // print_r($category_image);exit;
                            if (file_exists(base_path() . '/public/assets/admin/base/images/category/' . $sub->image) && $sub->image != '') {
                                $category_image = url('/assets/admin/base/images/category/' . $sub->image);
                            }
                            
                            $sub_category[$s]['category_image'] = $category_image;

                            $subcat = getSubCategoryListsupdated(1,$sub->id);
                            $sub_category[$s]['child_sub_categories'] = $subcat;
                            $s++;
                            
                        }
                        $category[$c]['sub_category_list'] = $sub_category;
                        $c++;
                    }
                }
            }
            $vendor_det['category_list'] = $category;
            $fstatus = $cart_count = 0;
            if ($user_id) {
                $vendor_fav = Stores::vendor_fav_info($user_id, $store_id);
                $user_cart_info = Stores::user_cart_information($user_id);
                if (count($vendor_fav)) {
                    $fstatus = $vendor_fav->status;
                }
                if (count($user_cart_info)) {
                    $cart_count = $user_cart_info->cart_count;
                }
            }
            $vendor_det['user_favourite'] = $fstatus;
            $vendor_det['cart_count'] = $cart_count;

            $time_interval = $this->get_delivery_time_interval();
            $delivery_slots = $this->get_delivery_slots();
            $date = date('Y-m-d'); //today date
            $weekOfdays = $week = $deliver_slot_array = $u_time = array();
            $datetime = new \DateTime();
            $datetime->modify('+1 day');
            //$listItem = array('<li class="active">', '</li>');
            $i = 0;
            $weekarray = array();
            while (true)
            {        
                if ($i === 7) break;
               // echo $datetime->format('N');exit;
                if ($datetime->format('N') === '7' && $i === 0)
                {
                    $datetime->add(new \DateInterval('P1D'));
                    continue;        
                }
                $weekarray[] = $datetime->format('N');
                $j = $datetime->format('N');
                $datetime->add(new \DateInterval('P1D'));
                $wk_day = date('N', strtotime($date));
                $weekOfdays[$j] = date('d M', strtotime($date));
                $weekOfdays_mob[] = date('d-m-Y', strtotime($date));
                $weekday = date('l', strtotime('+1 day', strtotime($date)));
                
               // $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
                
                
                $deliver_slot_array[$weekday] = "";
                foreach($time_interval as $time)
                {
                    $slot_id = $this->check_value_exist($delivery_slots,$time->id,$j,'time_interval_id','day');
                    if($slot_id != 0) {
                        $deliver_slot_array[$weekday] .= date('g:i a', strtotime($time->start_time)) . ' - ' . date('g:i a', strtotime($time->end_time)) . ",";
                    }
                }
                $week[$j] = date('l',strtotime($date));
                $week_mob[] = date('l',strtotime($date));
                $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));        
                $i++;
            }
            
            
            
            end($deliver_slot_array);
			$key = key($deliver_slot_array); 
			
			
			array_pop($deliver_slot_array);
			//print_r($deliver_slot_array);
			
			$final_delivery_slot[$key] = end($deliver_slot_array);
			
			//array_merge($final_delivery_slot,$final_delivery_slot);
			
			$output =  array_merge($final_delivery_slot,$deliver_slot_array);
			//print_r($output);
		//exit;
           // $last = end($deliver_slot_array);
            //key($last)
		// print_r($deliver_slot_array);exit;
            //print_r($deliver_slot_array);exit;
                        /*$d = $dj = 0;
			$available_s_time = $week_new = array();
			$w_day = '';
			$week_date = $date = date('d-m-Y');
			$avaliable_slot_mob = $this->get_avaliable_slot_mobl();
			foreach($avaliable_slot_mob as $key => $mobile_slot)
			{
				$current_day = date('w') + 1;echo $week_new[$mobile_slot->day];die;
				if($w_day != $week_new[$mobile_slot->day])
				{echo 2111;die;
					if($w_day != '')
						$d++;
					$w_day = $week_new[$mobile_slot->day];
					if($current_day < $mobile_slot->day)
					{
						$min_date  = $mobile_slot->day - $current_day;
						$week_date = date('d-m-Y', strtotime('+'.$min_date.' day', strtotime($date)));
					}
					else if($current_day == $mobile_slot->day) {
						$week_date = $date;
					}
					else if($current_day > $mobile_slot->day) {
						$total_week_day = 7;
						$rem_day = $total_week_day - $current_day;
						$tot_day = $rem_day + $mobile_slot->day;
						$week_date = date('d-m-Y', strtotime('+'.$tot_day.' day', strtotime($date)));
					}
					$available_s_time = array();
					$available_slot_new[$d]['day'] = date('l',strtotime($week_date));
					$dj = 0;
				}echo 11;die;
				$available_s_time[$dj]['week_mob_time'][date('l',strtotime($week_date))][] = date('g:i a', strtotime($mobile_slot->start_time)).' - '.date('g:i a', strtotime($mobile_slot->end_time));
				$available_slot_new[$d]['time']   = $available_s_time;
				$dj++;
			}*/
			//~ $vendor_det['deliver_slot'] = $available_slot_new;
            $timearray = getDaysWeekArray();
            foreach ($timearray as $key1 => $val1) {
                $u_time[$key1] = $this->getOpenTimings($store_id, $val1);
            }
            if (count($u_time)) {
                $ctime = array();
                $k = 0;
                foreach ($u_time as $key3 => $value) {
                    if (isset($value[0])) {
                        $ctime[$k]['id'] = $value[0]->id;
                        $ctime[$k]['created_date'] = $value[0]->created_date;
                        $ctime[$k]['vendor_id'] = $value[0]->vendor_id;
                        $ctime[$k]['day'] = $key3;
                        $ctime[$k]['day_week'] = $value[0]->day_week;
                        $ctime[$k]['start_time'] = date('g:i a', strtotime($value[0]->start_time));
                        $ctime[$k]['end_time'] = date('g:i a', strtotime($value[0]->end_time));
                        $k++;
                    }
                }
            }
            $vendor_det['deliver_slot'] = $output;
            $vendor_det['open_time'] = $ctime;
            
            $result = array("response" => array("httpCode" => 200, "Message" => "Vendor Information", "vendor_detail" => $vendor_det));
        }
        return json_encode($result);
    }
	public function get_avaliable_slot_mobl()
    {
        $available_slots = DB::select('SELECT dts.day,dti.start_time,dti.end_time,dts.id AS slot_id
        FROM delivery_time_slots dts
        LEFT JOIN delivery_time_interval dti ON dti.id = dts.time_interval_id');
        return $available_slots;
    }
    /* To get delivery time interval */

    public function get_delivery_time_interval() {
        $time_interval = DB::table('delivery_time_interval')
                ->select('id', 'start_time', 'end_time')
                ->orderBy('start_time', 'asc')
                ->get();
        return $time_interval;
    }
    public function get_delivery_slots()
    {
        $delivery_slots = DB::table('delivery_time_slots')
                ->select('*')
                ->get();
        return $delivery_slots;
    }

    /* To get store open timings */

    public function getOpenTimings($v_id, $day_week) {
        $time_data = DB::table('opening_timings')
                ->where("vendor_id", $v_id)
                ->where("day_week", $day_week)
                ->orderBy('id', 'asc')
                ->get();
        $time_list = array();
        if (count($time_data) > 0) {
            $time_list = $time_data;
        }
        return $time_list;
    }
    public function check_value_exist($delivery_slots,$interval_id,$day,$key1,$key2)
    {
        foreach ($delivery_slots as $slots)
        {
            if (is_array($slots) && check_value_exist($delivery_slots, $interval_id,$day,$key1,$key2)) return $slots->id;
            
            if (isset($slots->$key1) && $slots->$key1 == $interval_id && isset($slots->$key2) &&$slots->$key2 == $day) return $slots->id;
        }
        return 0;
    }

    /* Store product list based on category and sub category id */
    /* Store information mobile */

    public function store_product(Request $data) {
        $post_data = $data->all();
        $data = array();
        $rules = [
            'language' => ['required', 'numeric'],
            'store_id' => ['required', 'numeric'],
            'outlet_id' => ['required', 'numeric'],
            'category_id' => ['required', 'numeric'],
            'sub_category_id' => ['required', 'numeric'],
        ];
        $errors = $result = array();
        $validator = app('validator')->make($post_data, $rules);
        if ($validator->fails()) {
            $j = 0;
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[] = is_array($value) ? implode(',', $value) : $value;
            }
            $errors = implode(", \n ", $errors);
            $result = array("response" => array("httpCode" => 400, "Message" => $errors));
        } else {
            $language_id = $post_data['language'];
            $store_id = $post_data['store_id'];
            $outlet_id = $post_data['outlet_id'];
            $category_id = $post_data['category_id'];
            $sub_category_id = $post_data['sub_category_id'];
            $vendor_det = array();
            $vendors = Vendors::find($store_id);
            if (!count($vendors)) {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Invalid Store")));
                return json_encode($result);
            }
            $product_list = Stores::product_list($language_id, $store_id, $outlet_id, $category_id, $sub_category_id);
            $product = array();
            if (count($product_list) > 0) {
                $p = 0;
                foreach ($product_list as $pro) {
                    $product[$p]['product_id'] = $pro->product_id;
                    $product[$p]['product_url'] = $pro->product_url;
                    $product[$p]['weight'] = $pro->weight;
                    $product[$p]['original_price'] = $pro->original_price;
                    $product[$p]['discount_price'] = $pro->discount_price;
                    $product[$p]['category_id'] = $pro->id;
                    $product[$p]['unit'] = $pro->unit;
                    $product[$p]['description'] = $pro->description;
                    $product[$p]['title'] = $pro->title;
                    $product[$p]['outlet_name'] = $pro->outlet_name;
                    $product[$p]['average_rating'] = $pro->average_rating;
                    $product_image = URL::asset('assets/front/'.Session::get("general")->theme.'/images/store_detial.png');
                    if (file_exists(base_path() . '/public/assets/admin/base/images/products/list/' . $pro->product_image) && $pro->product_image != '') {
                        $product_image = url('/assets/admin/base/images/products/list/' . $pro->product_image);
                    }
                    $product[$p]['product_image'] = $product_image;
                    $p++;
                }
            }
            $currency_symbol = getCurrency();
            $currency_side = getCurrencyPosition()->currency_side;
            $result = array("response" => array("httpCode" => 200, "Message" => trans("messages.Product list"), "product_list" => $product, "currency_symbol" => $currency_symbol, "currency_side" => $currency_side));
        }
        return json_encode($result);
    }

    /* Store product list based on category and sub category id */
    /* Store information mobile */

    public function store_product_mob(Request $data)
    {
        $post_data = $data->all();
        $data = array();
        $rules = [
            'language' => ['required', 'numeric'],
            'store_id' => ['required', 'numeric'],
            'outlet_id' => ['required', 'numeric'],
            'category_id' => ['required', 'numeric'],
            'sub_category_id' => ['required', 'numeric'],
        ];
        $errors = $result = array();
        $validator = app('validator')->make($post_data, $rules);
        if ($validator->fails()) {
            $j = 0;
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[] = is_array($value) ? implode(',', $value) : $value;
            }
            $errors = implode(", \n ", $errors);
            $result = array("response" => array("httpCode" => 400, "Message" => $errors));
        } else {
            $language_id = $post_data['language'];
            $store_id = $post_data['store_id'];
            $outlet_id = $post_data['outlet_id'];
            $category_id = $post_data['category_id'];
            $sub_category_id = $post_data['sub_category_id'];
			$product_sub_category_id = isset($post_data['product_sub_category_id']) ? $post_data['product_sub_category_id'] : "";
            $product_name = isset($post_data['product_name']) ? $post_data['product_name'] : "";
            $user_id = isset($post_data['user_id']) ? $post_data['user_id'] : "";
            $token = isset($post_data['token']) ? $post_data['token'] : "";
            if ($user_id != '' && $token != '') {
                try {
                    $check_auth = JWTAuth::toUser($post_data['token']);
                } catch (JWTException $e) {
                    $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
                    return json_encode($result);
                } catch (TokenExpiredException $e) {
                    $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
                    return json_encode($result);
                }
            }
            $vendor_det = array();
            $vendors = Vendors::find($store_id);
            if (!count($vendors)) {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Invalid Store")));
                return json_encode($result);
            }
            $product_list = Stores::product_list_mob($language_id, $store_id, $outlet_id, $category_id, $sub_category_id, $product_name, $product_sub_category_id);
            //print_r($product_list);exit;
            $product = array();
            if (count($product_list) > 0) {
                $p = 0;
                foreach ($product_list as $pro) {
                    $product[$p]['product_id'] = $pro->product_id;
                    $product[$p]['product_name'] = $pro->product_name;
                    $product[$p]['product_url'] = $pro->product_url;
                    $product[$p]['weight'] = $pro->weight;
                    $product[$p]['original_price'] = $pro->original_price;
                    $product[$p]['discount_price'] = $pro->discount_price;
                    $product[$p]['category_id'] = $pro->id;
                    $product[$p]['unit'] = $pro->unit;
                    $product[$p]['description'] = $pro->description;
                    $product[$p]['title'] = $pro->title;
                    $product[$p]['outlet_id'] = $pro->outlet_id;
                    $product[$p]['outlet_name'] = $pro->outlet_name;
                    
                    $product[$p]['average_rating'] = ($pro->average_rating == null) ? 0 : $pro->average_rating;
                    $product_image = URL::asset('assets/front/'.Session::get("general")->theme.'/images/store_detial.png');
                    if (file_exists(base_path() . '/public/assets/admin/base/images/products/list/' . $pro->product_image) && $pro->product_image != '') {
                        $product_image = url('/assets/admin/base/images/products/list/' . $pro->product_image);
                    }
                    $product[$p]['product_image'] = $product_image;
                    
					$product_info_image = URL::asset('assets/front/'.Session::get("general")->theme.'/images/store_detial.png');
					if(file_exists(base_path().'/public/assets/admin/base/images/products/detail/'.$pro->product_info_image) && $pro->product_info_image != '') { 
						$product_info_image = url('/assets/admin/base/images/products/detail/' . $pro->product_info_image);
					}
					$product[$p]['product_info_image'] = $product_info_image;

					$product_zoom_image = URL::asset('assets/front/'.Session::get("general")->theme.'/images/store_detial.png');
					if(file_exists(base_path().'/public/assets/admin/base/images/products/zoom/'.$pro->product_zoom_image) && $pro->product_zoom_image != '') { 
						$product_zoom_image = url('/assets/admin/base/images/products/zoom/' . $pro->product_zoom_image);
					}
					$product[$p]['product_zoom_image'] = $product_zoom_image;

                    $cart_count = 0;
                    if ($user_id != '') {
                        $cart_count = $this->get_cart_product_count($user_id, $pro->product_id);
                    }
                    $product[$p]['cart_count'] = $cart_count;
                    $p++;
                }
            }

            //print_r(getCurrency());exit;
            $currency_symbol = getCurrency($language_id);
            $currency_side = getCurrencyPosition()->currency_side;
            $result = array("response" => array("httpCode" => 200, "Message" => trans("messages.Product list"), "product_list" => $product, "currency_symbol" => $currency_symbol, "currency_side" => $currency_side));
        }
        return json_encode($result);
    }

    /* Store product list based on category and sub category id */
    /* Store information mobile */

    public function store_review(Request $data) {
        $post_data = $data->all();
        $data = array();
        $rules = [
            'store_id' => ['required', 'numeric'],
            'outlet_id' => ['required', 'numeric'],
        ];
        $errors = $result = array();
        $validator = app('validator')->make($post_data, $rules);
        if ($validator->fails()) {
            $j = 0;
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[] = is_array($value) ? implode(',', $value) : $value;
            }
            $errors = implode(", \n ", $errors);
            $result = array("response" => array("httpCode" => 400, "Message" => $errors));
        } else {
            $store_id = $post_data['store_id'];
            $outlet_id = $post_data['outlet_id'];
            $vendor_det = array();
            $vendors = Vendors::find($store_id);
            if (!count($vendors)) {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Invalid Store")));
                return json_encode($result);
            }
            $review = array();
            $outlet_reviews = Stores::outlet_reviews($store_id, $outlet_id);
            if (count($outlet_reviews) > 0) {
                $r = 0;
                foreach ($outlet_reviews as $rev) {
                    $review[$r]['review_id'] = $rev->review_id;
                    $review[$r]['title'] = ($rev->title != '') ? $rev->title : '';
                    $review[$r]['comments'] = $rev->comments;
                    $review[$r]['ratings'] = $rev->ratings;
                    $review[$r]['created_date'] = $rev->created_date;
                    $review[$r]['user_id'] = $rev->id;
                    $review[$r]['first_name'] = ($rev->first_name != '') ? ucfirst($rev->first_name) : '';
                    $review[$r]['last_name'] = ($rev->last_name != '') ? ucfirst($rev->last_name) : '';
                    $review[$r]['name'] = $rev->name;
                    $review_image = URL::asset('assets/admin/base/images/a2x.jpg');
                    if (file_exists(base_path() . '/public/assets/admin/base/images/admin/profile/thumb/' . $rev->image) && $rev->image != '') {
                        $review_image = url('/assets/admin/base/images/admin/profile/thumb/' . $rev->image . '?' . time());
                    }
                    $review[$r]['image'] = $review_image;
                    $r++;
                }
            }
            $result = array("response" => array("httpCode" => 200, "Message" => trans("messages.Store review list"), "review_list" => $review));
        }
        return json_encode($result);
    }

    /* Store product list based on category and sub category id */
    /* Store information mobile */

    public function store_list_mob(Request $data) {
        $post_data = $data->all();
        $language = isset($post_data['language']) ? $post_data['language'] : "";
        $category_ids = isset($post_data['category_ids']) ? $post_data['category_ids'] : "";
        $category_url = isset($post_data['category_url']) ? $post_data['category_url'] : "";
        $city = isset($post_data['city']) ? $post_data['city'] : "";
        $location = isset($post_data['location']) ? $post_data['location'] : "";
        $keyword = isset($post_data['keyword']) ? $post_data['keyword'] : "";
        $sortby = isset($post_data['sortby']) ? $post_data['sortby'] : "";
        $orderby = isset($post_data['orderby']) ? $post_data['orderby'] : "desc";
        $stores = array();
        $stores_list = Stores::stores_list($language, $category_ids, $category_url, $city, $location, $keyword, $sortby, $orderby);
        $banners = DB::table('banner_settings')->select('*')->where('banner_type', 2)->where('status', 1)->orderBy('default_banner', 'desc')->get();
        $banner_list = array();
        if (count($banners) > 0) {
            foreach ($banners as $key => $items) {
                $banners[$key]->banner_image_url = url('/assets/admin/base/images/' . $items->banner_image);
            }
        }
        if (count($stores_list) > 0) {
            $s = 0;
            $vendors_id = '';
            foreach ($stores_list as $st) {
                if ($vendors_id != $st->vendors_id) {
                    $vendors_id = $st->vendors_id;
                    $stores[$s]['vendors_id'] = $st->vendors_id;
                    $stores[$s]['vendor_name'] = $st->vendor_name;
                    $stores[$s]['category_ids'] = $st->category_ids;
                    $stores[$s]['contact_address'] = $st->contact_address;
                    $stores[$s]['vendor_description'] = $st->vendor_description;
                    $stores[$s]['vendors_delivery_time'] = $st->vendors_delivery_time;
                    $stores[$s]['delivery_charges_fixed'] = $st->delivery_charges_fixed;
                    $stores[$s]['delivery_cost_variation'] = $st->delivery_cost_variation;
                    $stores[$s]['minimum_order_amount'] = $st->minimum_order_amount;
                    $stores[$s]['vendors_average_rating'] = ($st->vendors_average_rating == null) ? 0 : $st->vendors_average_rating;
                    $category_ids = explode(',', $st->category_ids);
                    $category_name = '';
                    if (count($category_ids) > 0) {
                        foreach ($category_ids as $cate) {
                            $get_category_name = getCategoryListsById($cate);
                            $category_name .= $get_category_name->category_name . ', ';
                        }
                    }
                    $stores[$s]['category_ids'] = rtrim($category_name, ', ');
                    $logo_image = URL::asset('assets/front/'.Session::get("general")->theme.'/images/store_detial.png');
                    if (file_exists(base_path() . '/public/assets/admin/base/images/vendors/list/' . $st->logo_image) && $st->logo_image != '') {
                        $logo_image = url('/assets/admin/base/images/vendors/list/' . $st->logo_image);
                    }
                    $stores[$s]['logo_image'] = $logo_image;
                    $stores[$s]['logo_image'] = $logo_image;
                    $outlet_count = Stores::get_outlet_count($st->vendors_id, $city, $location);
                    if (count($outlet_count) > 0) {
                        $stores[$s]['outlets_count'] = (int) $outlet_count->outlets_count;
                        if ($outlet_count->outlets_count == 1) {
                            $get_outlet_id = Stores::get_outlet_id_by_store($st->vendors_id);
                            $stores[$s]['outlets_id'] = (int) $get_outlet_id->outlets_id;
                        } else {
                            $stores[$s]['outlets_id'] = '';
                        }
                    } else {
                        $stores[$s]['outlets_count'] = 0;
                        $stores[$s]['outlets_id'] = '';
                    }
                    $s++;
                }
            }
        }
        $result = array("response" => array("httpCode" => 200, "Message" => trans("messages.Store list"), "store_list" => $stores, "banners" => $banners));
        return json_encode($result);
    }

    /* outlet list by store id */

    public function store_outlet_list(Request $data) {
        $post_data = $data->all();
        $data = array();
        $rules = [
            'store_id' => ['required', 'numeric'],
            'language' => ['required', 'integer'],
        ];
        $errors = $result = array();
        $validator = app('validator')->make($post_data, $rules);
        if ($validator->fails()) {
            $j = 0;
            foreach ($validator->errors()->messages() as $key => $value) {
                $errors[] = is_array($value) ? implode(',', $value) : $value;
            }
            $errors = implode(", \n ", $errors);
            $result = array("response" => array("httpCode" => 400, "Message" => $errors));
        } else {
            $store_id = $post_data['store_id'];
            $language = $post_data['language'];
            $city = isset($post_data['city']) ? $post_data['city'] : "";
            $location = isset($post_data['location']) ? $post_data['location'] : "";
            $outlet = array();
            $outlet_list = Stores::get_outlet_list($store_id, $language, $city, $location);
            $vendor_name = '';
            if (count($outlet_list) > 0) {
                $o = 0;
                foreach ($outlet_list as $out) {
                    $outlet[$o]['outlets_id'] = $out->outlets_id;
                    $outlet[$o]['outlets_vendors_id'] = $out->outlets_vendors_id;
                    $outlet[$o]['outlet_name'] = $out->outlet_name;
                    $outlet[$o]['contact_address'] = $out->contact_address;
                    $outlet[$o]['outlets_delivery_time'] = $out->outlets_delivery_time;
                    $outlet[$o]['delivery_charges_fixed'] = $out->delivery_charges_fixed; 
                    $outlet[$o]['minimum_order_amount'] = $out->minimum_order_amount;
                    $outlet[$o]['outlets_average_rating'] = ($out->outlets_average_rating == null) ? 0 : $out->outlets_average_rating;
                    $outlet[$o]['delivery_charges_variation'] = $out->delivery_charges_variation;
                     $outlet[$o]['outlet_location_name'] = $out->outlet_location_name;
                    $logo_image = URL::asset('assets/front/'.Session::get("general")->theme.'/images/store_detial.png');
                    if (file_exists(base_path() . '/public/assets/admin/base/images/vendors/logos/' . $out->logo_image) && $out->logo_image != '') {
                        $logo_image = url('/assets/admin/base/images/vendors/logos/' . $out->logo_image);
                    }
                    $outlet[$o]['logo_image'] = $logo_image;
                    $category_ids = explode(',', $out->category_ids);
                    $category_name = '';
                    if (count($category_ids) > 0) {
                        foreach ($category_ids as $cate) {
                            $get_category_name = getCategoryListsById($cate);
                            $category_name .= $get_category_name->category_name . ', ';
                        }
                    }
                    $outlet[$o]['category_ids'] = rtrim($category_name, ', ');
                    $vendor_name = $out->vendor_name;
                    $o++;
                }
            }
            $result = array("response" => array("httpCode" => 200, "Message" => trans("messages.Outlets list"), "outlet_list" => $outlet, "vendor_name" => $vendor_name));
        }
        return json_encode($result);
    }

    /* Store banner list */

    public function store_banner() {
        $banner_list = Api_Model::get_store_banner_list();
        $banner = array();
       
        if (count($banner_list) > 0) {
            $b = 0;
            foreach ($banner_list as $bnr) {
                $banner[$b]['banner_id'] = $bnr->banner_setting_id;
                $banner[$b]['banner_title'] = $bnr->banner_title;
                $banner[$b]['banner_subtitle'] = $bnr->banner_subtitle;
                $banner_image = URL::asset('assets/front/'.Session::get("general")->theme.'/images/store_slid1.png');
                if (file_exists(base_path() . '/public/assets/admin/base/images/banner/' . $bnr->banner_image) && $bnr->banner_image != '') {
                    $banner_image = url('/assets/admin/base/images/banner/' . $bnr->banner_image);
                }
                $banner[$b]['banner_image'] = $banner_image;
                $banner[$b]['banner_link'] = $bnr->banner_link;
                $b++;
            }
        }
        $result = array("response" => array("httpCode" => 200, "Message" => trans("messages.Banner list"), "banner_list" => $banner));
        return json_encode($result);
    }

    public function store_featurelist_mob(Request $data) {
        $post_data = $data->all();
        $language = isset($post_data['language']) ? $post_data['language'] : "";
        $category_ids = isset($post_data['category_ids']) ? $post_data['category_ids'] : "";
        $category_url = isset($post_data['category_url']) ? $post_data['category_url'] : "";
        $city = isset($post_data['city']) ? $post_data['city'] : "";
        $location = isset($post_data['location']) ? $post_data['location'] : "";
        $keyword = isset($post_data['keyword']) ? $post_data['keyword'] : "";
        $sortby = isset($post_data['sortby']) ? $post_data['sortby'] : "";
        $orderby = isset($post_data['orderby']) ? $post_data['orderby'] : "desc";
        $stores = array();
        $stores_list = Stores::feature_stores_list($language, $category_ids, $category_url, $city, $location, $keyword, $sortby, $orderby);
        if (count($stores_list) > 0) {
            //$outlet_count = Stores::get_outlet_count(18);
            //print_r($outlet_count); exit;
            $vendors_id = '';
            $s = 0;
            foreach ($stores_list as $st) {
                if ($vendors_id != $st->vendors_id) {
                    $vendors_id = $st->vendors_id;
                    $stores[$s]['vendors_id'] = $st->vendors_id;
                    $stores[$s]['vendor_name'] = $st->vendor_name;
                    $stores[$s]['category_ids'] = $st->category_ids;
                    $stores[$s]['contact_address'] = $st->contact_address ? $st->contact_address : '';
                    $stores[$s]['vendor_description'] = $st->vendor_description ? $st->vendor_description : '';
                    $stores[$s]['vendors_delivery_time'] = $st->vendors_delivery_time;
                    $stores[$s]['vendors_average_rating'] = ($st->vendors_average_rating == null) ? 0 : $st->vendors_average_rating;
                    $stores[$s]['featured_vendor'] = $st->featured_vendor;
                    $logo_image = URL::asset('assets/front/'.Session::get("general")->theme.'/images/store_detial.png');
                    if (file_exists(base_path() . '/public/assets/admin/base/images/vendors/list/' . $st->logo_image) && $st->logo_image != '') {
                        $logo_image = url('/assets/admin/base/images/vendors/list/' . $st->logo_image);
                    }
                    $stores[$s]['logo_image'] = $logo_image;
                    $outlet_count = Stores::get_outlet_count($st->vendors_id);
                    if (count($outlet_count) > 0) {
                        $stores[$s]['outlets_count'] = (int) $outlet_count->outlets_count;
                        if ($outlet_count->outlets_count == 1) {
                            $get_outlet_id = Stores::get_outlet_id_by_store($st->vendors_id);
                            $stores[$s]['outlets_id'] = (int) $get_outlet_id->outlets_id;
                        } else {
                            $stores[$s]['outlets_id'] = '';
                        }
                    } else {
                        $stores[$s]['outlets_count'] = 0;
                        $stores[$s]['outlets_id'] = '';
                    }
                    $s++;
                }
            }
        }
        $result = array("response" => array("httpCode" => 200, "Message" => trans("messages.Store list"), "store_list" => $stores));
        return json_encode($result);
    }

    public function cart_count(Request $data) {
        $post_data = $data->all();
        $user_id = $post_data['user_id'];
        try {
            $check_auth = JWTAuth::toUser($post_data['token']);
            $user_cart_info = Stores::user_cart_information($user_id);
            $cart_count = 0;
            if (count($user_cart_info)) {
                $cart_count = $user_cart_info->cart_count;
            }
            $result = array("response" => array("httpCode" => 200, "cart_count" => $cart_count));
        } catch (JWTException $e) {
            $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
        } catch (TokenExpiredException $e) {
            $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
        }
        return $result;
    }
    public function product_details(Request $data)
    {
        $post_data   = $data->all();
        $user_id     = isset($post_data['user_id'])?$post_data['user_id']:"";
        $product_url = isset($post_data['product_url'])? $post_data['product_url']: ''; 
        $outlet_url =  isset($post_data['outlet_url'])? $post_data['outlet_url']: '';
        $language_id = isset($post_data['language'])?$post_data['language']: getCurrentLang();

        $result = array("response" => array("httpCode" => 400, "status" => false, "data" => $data));
         //return json_encode($result, JSON_UNESCAPED_UNICODE); 


        $condtion = " products.active_status = 1";
        $pquery = '"products_infos"."lang_id" = (case when (select count(products_infos.lang_id) as totalcount from products_infos where products_infos.lang_id = '.$language_id.' and products.id = products_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $wquery = '"weight_classes_infos"."lang_id" = (case when (select count(weight_classes_infos.lang_id) as totalcount from weight_classes_infos where weight_classes_infos.lang_id = '.$language_id.' and products.weight_class_id = weight_classes_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';

        $discount_query = '
                        (case when coalesce(products.original_price,0) = 0 then 0 else 
                            ROUND( (products.original_price - products.discount_price ) /products.original_price *100 )  end 
                        )
                        as product_discount ';

        $products = Products::join('products_infos', 'products.id', '=', 'products_infos.id')
                ->join('weight_classes_infos', 'weight_classes_infos.id', '=', 'products.weight_class_id')
                ->join('outlets', 'outlets.id', '=', 'products.outlet_id')
                ->join('outlet_infos', 'outlet_infos.id', '=', 'outlets.id')
                ->leftJoin('product_variants', 'product_variants.product_id', '=', 'products.id')
                ->select('products.id as product_id', 'products.product_url','products.product_info_image','products.product_zoom_image','products.product_image', 'products.weight', 'products.original_price', 'products.discount_price', 'products.vendor_id', 'products.outlet_id', 'products_infos.description', 'products_infos.product_name', 'products.category_id', 'products.sub_category_id', 'weight_classes_infos.unit', 'weight_classes_infos.title', 'outlet_infos.outlet_name', 'outlets.url_index as outlet_url_index','products.is_multi_price', 'products.quantity', DB::raw($discount_query) , 
                    DB::raw ( '  array_to_json( array_agg( product_variants order by  product_variants.weight_value desc) )  AS variants')
                    )
                ->whereRaw($pquery)
                ->whereRaw($wquery);

                if(!empty($outlet_url))
                {
                 $products =    $products->where('outlets.url_index', '=', $outlet_url);
                }
                
        $products = $products->where('products.product_url', '=', $product_url)

                             ->groupBy('products.id', 'products.product_url','products.product_info_image','products.product_zoom_image','products.product_image', 'products.weight', 'products.original_price', 'products.discount_price', 'products.vendor_id', 'products.outlet_id', 'products_infos.description', 'products_infos.product_name', 'products.category_id', 'products.sub_category_id', 'weight_classes_infos.unit', 'weight_classes_infos.title', 'outlet_infos.outlet_name', 'outlets.url_index','products.is_multi_price')
                             ->first();

               
        if(count($products) >0)
        {
            $products->product_cart_count = 0;
            if(!empty($user_id))
            {
                $products->product_cart_count = $this->get_cart_product_count($user_id, $products->product_id);        
            }


            if(isset($user_id) && !empty($user_id))
            $products->product_cart_count = $this->get_cart_product_count($user_id, $products->product_id);


            if(isset($user_id) && !empty($user_id))
            $products->is_fav = $this->get_product_fav_count($user_id, $products->product_id);

            if($products->is_multi_price == 1 && !empty($products->variants) )
            $products->variants = json_decode($products->variants);

            if(isset($user_id) && !empty($user_id) && $products->is_multi_price && !empty($products->variants))
            $products->variants  = $this->get_multi_cart_product_count($user_id, $products->variants);




            $result = array("response" => array("httpCode" => 200, "status" => true, 'data' => $products));
        }


        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }
     public function get_delivery_settings()
    {
        $delivery_settings = DB::table('delivery_settings')
        ->first();
        return $delivery_settings;
    }

    public function product_list_filter(Request $data) {
        //
        DB::enableQueryLog();

        $post_data = $data->all();

        $category_url = isset($post_data['category_url'])   ? $post_data['category_url']: '';
        $category_ids  = isset($post_data['category_ids'])    ? $post_data['category_ids'] : '';
        $subcategory_ids = isset($post_data['subcategory_ids'])    ? $post_data['subcategory_ids'] : '';
        $brand_ids        = isset($post_data['brand_ids'])          ? $post_data['brand_ids']       : '';
        $user_id      = isset($post_data['user_id'])        ? $post_data['user_id']     : '';
        $discount     = isset($post_data['discount'])   ? $post_data['discount'] : '';
        $price        = isset($post_data['price'])      ? $post_data['price'] : '';

        $from         = isset($post_data['from'])           ? $post_data['from']        : '';
        $limit        = isset($post_data['limit'])          ? $post_data['limit']       : '';
        $keyword      = isset($post_data['keyword'])        ? $post_data['keyword']     : '';

        $perpage      = isset($post_data['perpage'])        ? $post_data['perpage']     : 6 ;

        $language_id = $post_data['language'];



        $data = array();
        //$result = array("response" => array("httpCode" => 400, "status" => false, "data" => $data));

        if ($keyword != '') {
            $condtion .= " and products_infos.product_name ILIKE '%" . $keyword . "%'";
        }

        $pquery = 'products_infos.lang_id = (
                        case when (
                                    select count(*) as totalcount 
                                    from products_infos 
                                    where 
                                        products_infos.lang_id = ' . $language_id . ' and products.id = products_infos.id) > 0 THEN ' . $language_id . ' 

                                    ELSE 1 END)';

        $cquery = 'categories_infos.language_id = (
                        case when (
                                    select count(*) as totalcount 
                                    from categories_infos 
                                    where 
                                    categories_infos.language_id = ' . $language_id . ' and categories.id = categories_infos.category_id) > 0 THEN ' . $language_id . ' 
                                    ELSE 1 END)';

        $wquery = 'weight_classes_infos.lang_id = (
                        case when (
                                    select count(*) as totalcount 
                                    from weight_classes_infos where weight_classes_infos.lang_id = ' . $language_id . ' and weight_classes.id = weight_classes_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
        $discount_query = '
                                (case when coalesce(products.original_price,0) = 0 then 0 else 
                                    ROUND( (products.original_price - products.discount_price ) /products.original_price *100 )  end 
                                )
                                as product_discount ';

        $products = Products::join('products_infos', 'products.id', '=', 'products_infos.id')
                ->join('categories', 'categories.id', '=', 'products.category_id')
                ->join('categories_infos', 'categories_infos.category_id', '=', 'categories.id')
                ->leftJoin('weight_classes', 'weight_classes.id', '=', 'products.weight_class_id')
                ->leftJoin('weight_classes_infos', 'weight_classes_infos.id', '=', 'weight_classes.id')
                ->join('vendors', 'vendors.id', '=', 'products.vendor_id')
                ->join('outlets', 'outlets.id', '=', 'products.outlet_id')
                ->leftJoin('product_variants', 'products.id', '=', 'product_variants.product_id')
                ->select('products.id as product_id', 'products.product_url','products.vendor_category_id', 'products.product_image', 'products.weight', 'products.original_price', 'products.discount_price', 'products.vendor_id', 'products.outlet_id', 'products_infos.description', 'products_infos.product_name', 'categories_infos.category_name', 'categories.id', 'weight_classes_infos.unit', 'weight_classes_infos.title', 'products.category_id', 'categories.url_key', 'categories.url_key as cat_url', 'outlets.id as outlet_id', 'products.quantity','products.weight_class_id','is_multi_price',

                    DB::raw($discount_query) ,



                    DB::raw ( '  array_to_json( array_agg( product_variants order by  product_variants.weight_value desc) )  AS variants')

                    // DB::raw("

                    //     array_agg( 


                    //     '[' 
                    //         || 

                    //                 product_variants.variant_id || product_variants.weight_class_id ||  
                    //                 product_variants.weight_value  || product_variants.quantity  ||  
                    //                 product_variants.discount_price || product_variants.original_price  ||   
                    //                 product_variants.active_status 

                    //         || 
                    //     ']'


                    //     )  as variants


                    //  ") 


                )

                ->whereRaw($pquery)
                ->whereRaw($cquery)
                //->whereRaw($wquery)
                ->where('vendors.active_status',1)
                ->where('outlets.active_status','1');
                //->where('outlets.url_index', '=', $store_key);

        if ($category_url != '' && $category_url!= 'all') {
            $products = $products->where('categories.url_key', '=', $category_url);
        }

        if ($category_ids != '' && $category_ids!= 'all') {
            $products = $products->whereIn('categories.id', explode(',', $category_ids) );
        }

        if (isset($subcategory_ids) && $subcategory_ids!='') {
            $products = $products->whereIn('products.sub_category_id', explode(',', $subcategory_ids) );
        } 

        if (isset($brand_ids) && $brand_ids != '') {
            $products = $products->whereIn('products.brand_id', explode(',', $brand_ids) );
        }

        if (isset($discount) && $discount != '') {
             // get csv discount
            $products = $products->where(function($query) use ($discount) {

                $discount_query2 = '
                                case when products.original_price = 0 then 0 else 
                                    ROUND( (products.original_price - products.discount_price ) /products.original_price *100 )  end 
                                ';
                $discount_ranges = explode(',', $discount);
                $touch = false;

                foreach ($discount_ranges as $key => $discount) {
                    if(strpos($discount, 'to') !== FALSE){

                        list($lower,$upper) = explode('to', $discount);

                        if($touch)
                        $query->orWhereRaw( '( ' . $discount_query2 . ' BETWEEN ? and ? )', array($lower, $upper) );
                        else
                        $query->whereRaw( '( ' . $discount_query2 . ' BETWEEN ? and ? )', array($lower, $upper) );

                        $touch = true;
                    }
                    else{
                            if(strpos($discount, 'mt') !== FALSE){
                                list($tmp,$upper) = explode('mt', $discount);
                                if($touch)
                                    $query->orWhereRaw( '( ' . $discount_query2 . ' > ? )', array($upper) );
                                else
                                    $query->whereRaw( '( ' . $discount_query2 . ' > ? )', array($upper) );
                            }
                    }
                }
            });
        }


        if (isset($price) && $price != '') {
             // get csv price
            $products = $products->where(function($query) use ($price) {

                $price_ranges = explode(',', $price); // 21to50,51to100
                $touch = false;

                foreach ($price_ranges as $key => $price) {
                   
                    if(strpos($price, 'to') !== FALSE){

                        list($lower,$upper) = explode('to', $price);

                        if($touch)
                            $query->orWhereRaw( '( products.discount_price BETWEEN ? and ? )', array($lower, $upper) );
                        else
                            $query->whereRaw(   '( products.discount_price BETWEEN ? and ? )', array($lower, $upper) );

                        $touch = true;
                    }
                    else{
                            if(strpos($price, 'lt') !== FALSE){

                                list($lower,$upper) = explode('lt', $price);

                                if($touch)
                                $query->orWhereRaw( '( products.discount_price < ? )', array($upper) );
                                else
                                $query->whereRaw( '( products.discount_price < ? )', array($upper) );
                            }

                        $touch = true;
                    }
                }

            });
        }


        $products = $products->where('products.active_status', '=', 1)
                ->where('products.approval_status', '=', 1)
                ->groupBy('products.id', 'products_infos.description', 'products_infos.product_name','categories_infos.category_name','categories.id','weight_classes_infos.unit','weight_classes_infos.title' , 'outlets.id')
                ->orderBy('categories_infos.category_name', 'asc')
                ->orderBy('products.id', 'asc')
                //->get();
                ->paginate($perpage);
                //->get();
               //print_r($products);exit;


        /* DB::getQueryLog() */

        $categories = array();

        foreach ($products as $key => $prod) {

            if(isset($user_id) && !empty($user_id))
            $products[$key]->product_cart_count = $this->get_cart_product_count($user_id, $prod->product_id);


            if(isset($user_id) && !empty($user_id))
            $products[$key]->is_fav = $this->get_product_fav_count($user_id, $prod->product_id);

            $categories['category_name'] = $prod->category_name;
            $categories['url_key'] = $prod->cat_url;

            if($prod->is_multi_price == 1 && !empty($prod->variants) )
            $products[$key]->variants = json_decode($prod->variants);

            if(isset($user_id) && !empty($user_id) && $prod->is_multi_price && !empty($products[$key]->variants))
            $products[$key]->variants  = $this->get_multi_cart_product_count($user_id, $products[$key]->variants);

        }
        //print_r($categories);exit;
        /*
        $query = 'vendors_infos.lang_id = (case when (select count(vendors_infos.lang_id) as totalcount from vendors_infos where vendors_infos.lang_id = ' . $language_id . ' and vendors.id = vendors_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';

        $query1 = 'outlet_infos.language_id = (case when (select count(outlet_infos.language_id) as totalcount from outlet_infos where outlet_infos.language_id = ' . $language_id . ' and outlets.id = outlet_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';

        $condition = 'vendors.active_status = 1 ';//and vendors.featured_vendor = 1';

       $vendors = Vendors::join('vendors_infos', 'vendors_infos.id', '=', 'vendors.id')
                ->join('outlets', 'outlets.vendor_id', '=', 'vendors.id')
                ->join('outlet_infos', 'outlets.id', '=', 'outlet_infos.id')
                ->select('vendors.id as vendors_id', 'vendors_infos.vendor_name', 'vendors.first_name', 'vendors.last_name', 'vendors.featured_image', 'vendors.logo_image', 'vendors.delivery_time as vendors_delivery_time', 'vendors.category_ids', 'vendors.average_rating as vendors_average_rating', 'outlet_infos.contact_address as outlets_contact_address', 'outlets.id as outlets_id', 'outlets.vendor_id as outlets_vendors_id', 'outlet_infos.outlet_name', 'outlets.delivery_time as outlets_delivery_time', 'outlets.average_rating as outlets_average_rating', 'outlets.delivery_charges_fixed as outlets_delivery_charges_fixed', 'outlets.pickup_time as outlets_pickup_time', 'outlets.latitude as outlets_latitude', 'outlets.longitude as outlets_longitude', 'outlets.url_index')
                ->whereRaw($query)
                ->whereRaw($query1)
                ->whereRaw($condition)
                //->where('outlets.url_index', '=', $store_key)
                ->where('outlets.active_status', '=', '1')
                ->get();
        $outlet_info = array(); 
      
        if (count($vendors)) {
            foreach ($vendors as $key => $datas) {
                $outlet_info[$key]['outlets_id'] = $datas->outlets_id;
                $outlet_info[$key]['vendors_id'] = $datas->vendors_id;
                $outlet_info[$key]['vendor_name'] = $datas->vendor_name;
                $outlet_info[$key]['first_name'] = $datas->first_name;
                $outlet_info[$key]['last_name'] = $datas->last_name;
                $outlet_info[$key]['featured_image'] = $datas->featured_image;
                $outlet_info[$key]['logo_image'] = $datas->logo_image;
                $outlet_info[$key]['vendors_delivery_time'] = $datas->vendors_delivery_time;
                $outlet_info[$key]['category_ids'] = $datas->category_ids;
                $outlet_info[$key]['vendors_average_rating'] = ($datas->vendors_average_rating == null) ? 0 : $datas->vendors_average_rating;
                $outlet_info[$key]['outlets_contact_address'] = $datas->outlets_contact_address;
                $outlet_info[$key]['outlets_vendors_id'] = $datas->outlets_vendors_id;
                $outlet_info[$key]['outlet_name'] = $datas->outlet_name;
                $outlet_info[$key]['outlets_delivery_time'] = $datas->outlets_delivery_time;
                $outlet_info[$key]['outlets_average_rating'] = ($datas->outlets_average_rating == null) ? 0 : $datas->outlets_average_rating;
                $outlet_info[$key]['outlets_delivery_charges_fixed'] = $datas->outlets_delivery_charges_fixed;
                $outlet_info[$key]['outlets_pickup_time'] = $datas->outlets_pickup_time;
                $outlet_info[$key]['outlets_latitude'] = $datas->outlets_latitude;
                $outlet_info[$key]['outlets_longitude'] = $datas->outlets_longitude;
                $outlet_info[$key]['url_index'] = $datas->url_index;
            }
        }
        */
        $result =   array("response" =>
                                array(  "httpCode" => 200, 
                                        "status" => true, 
                                        'data' => $products, 
                                        //'outlet_info' => $outlet_info, 
                                        'categories' => $categories,
                                        'query' => DB::getQueryLog(),
                                        'links' => $products->links()
                                )
                    );

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function add_fav_product(Request $data) {
        $post_data = $data->all();
        $data = array();
        $rules = [
        'user_id' => ['required'],
        'product_id' => ['required'],
        'token' => ['required'],
        ];

        $errors = $result = array();
        $validator = app('validator')->make($post_data, $rules);
        if ($validator->fails()) {
            $j = 0;
            foreach ($validator->errors()->messages() as $key => $value) {
            $errors[] = is_array($value) ? implode(',', $value) : $value;
            }
            $errors = implode(", \n ", $errors);
            $result = array("response" => array("httpCode" => 200, "status" => false, "Message" => $errors));
        } else {
            try {
                $check_auth = JWTAuth::toUser($post_data['token']);
                $ucdata = DB::table('favorite_products')
                ->select('favorite_products.id', 'favorite_products.status')
                ->where("favorite_products.user_id", "=", $post_data['user_id'])
                ->where("favorite_products.product_id", "=", $post_data['product_id'])
                ->get();

                if (count($ucdata)) {
                    $favourite = favorite_products::find($ucdata[0]->id);
                    $favourite->status = $ucdata[0]->status ? 0 : 1;
                    $favourite->save();
                    $status = $ucdata[0]->status ? 0 : 1;

                    $result = array("response" => array("httpCode" => 200, "Message" => trans("messages.The product has been added to your favorites list"), "status" => 1));
                    if ($ucdata[0]->status == 1) {
                    $result = array("response" => array("httpCode" => 200, "Message" => trans("messages.The product has been deleted from your favorites list"), "status" => $status));
                    }
                } else {
                    //echo "asdfa";exit;
                    $favourite = new favorite_products;
                    //print_r($favourite);exit;
                    $favourite->user_id = $post_data['user_id'];
                    $favourite->product_id = $post_data['product_id'];
                    $favourite->status = 1;
                    $favourite->save();
                    $result = array("response" => array("httpCode" => 200, "Message" => trans("messages.The product has been added to your favorites list"), "status" => 1));
                }
            } catch (JWTException $e) {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
            } catch (TokenExpiredException $e) {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
            }
        }
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function product_ajax_search(Request $data)
    {
        $post_data = $data->all();
        return $post_data['query'];


    }

    public function great_deals_filter(Request $data) {
    
        DB::enableQueryLog();

        $post_data = $data->all();

        $category_url = isset($post_data['category_url'])   ? $post_data['category_url']: '';
        $category_ids  = isset($post_data['category_ids'])    ? $post_data['category_ids'] : '';
        $subcategory_ids = isset($post_data['subcategory_ids'])    ? $post_data['subcategory_ids'] : '';
        $brand_ids        = isset($post_data['brand_ids'])          ? $post_data['brand_ids']       : '';
        $user_id      = isset($post_data['user_id'])        ? $post_data['user_id']     : '';
        $discount     = isset($post_data['discount'])   ? $post_data['discount'] : '';
        $price        = isset($post_data['price'])      ? $post_data['price'] : '';

        $from         = isset($post_data['from'])           ? $post_data['from']        : '';
        $limit        = isset($post_data['limit'])          ? $post_data['limit']       : '';
        $keyword      = isset($post_data['keyword'])        ? $post_data['keyword']     : '';

        $perpage      = isset($post_data['perpage'])        ? $post_data['perpage']     : 6 ;
        $purpose      = isset($post_data['purpose'])        ? $post_data['purpose']     : '' ;
        $language_id = $post_data['language'];



        $data = array();
        //$result = array("response" => array("httpCode" => 400, "status" => false, "data" => $data));

        if ($keyword != '') {
            $condtion .= " and products_infos.product_name ILIKE '%" . $keyword . "%'";
        }

        $pquery = 'products_infos.lang_id = (
                        case when (
                                    select count(*) as totalcount 
                                    from products_infos 
                                    where 
                                        products_infos.lang_id = ' . $language_id . ' and products.id = products_infos.id) > 0 THEN ' . $language_id . ' 

                                    ELSE 1 END)';

        $cquery = 'categories_infos.language_id = (
                        case when (
                                    select count(*) as totalcount 
                                    from categories_infos 
                                    where 
                                    categories_infos.language_id = ' . $language_id . ' and categories.id = categories_infos.category_id) > 0 THEN ' . $language_id . ' 
                                    ELSE 1 END)';

        $wquery = 'weight_classes_infos.lang_id = (
                        case when (
                                    select count(*) as totalcount 
                                    from weight_classes_infos where weight_classes_infos.lang_id = ' . $language_id . ' and weight_classes.id = weight_classes_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
        $discount_query = '
                                (case when coalesce(products.original_price,0) = 0 then 0 else 
                                    ROUND( (products.original_price - products.discount_price ) /products.original_price *100 )  end 
                                )
                                as product_discount ';

        $products = Products::join('products_infos', 'products.id', '=', 'products_infos.id')
                ->join('categories', 'categories.id', '=', 'products.category_id')
                ->join('categories_infos', 'categories_infos.category_id', '=', 'categories.id')
                ->leftJoin('weight_classes', 'weight_classes.id', '=', 'products.weight_class_id')
                ->leftJoin('weight_classes_infos', 'weight_classes_infos.id', '=', 'weight_classes.id')
                ->join('vendors', 'vendors.id', '=', 'products.vendor_id')
                ->join('outlets', 'outlets.id', '=', 'products.outlet_id')
                ->leftJoin('product_variants', 'products.id', '=', 'product_variants.product_id')
                ->select('products.id as product_id', 'products.product_url','products.vendor_category_id', 'products.product_image', 'products.weight', 'products.original_price', 'products.discount_price', 'products.vendor_id', 'products.outlet_id', 'products_infos.description', 'products_infos.product_name', 
                    'categories_infos.category_name', 'categories.id', 'weight_classes_infos.unit', 'weight_classes_infos.title', 'products.category_id', 'categories.url_key', 'categories.url_key as cat_url', 'outlets.id as outlet_id', 'products.quantity','products.weight_class_id','is_multi_price',

                    DB::raw($discount_query) ,



                    DB::raw ( '  array_to_json( array_agg( product_variants order by  product_variants.weight_value desc) )  AS variants')


                )

                ->whereRaw($pquery)
                ->whereRaw($cquery)
                //->whereRaw($wquery)
                ->where('vendors.active_status',1)
                ->where('outlets.active_status','1');



        if ($category_url != '' && $category_url!= 'all') {
            $products = $products->where('categories.url_key', '=', $category_url);
        }

        if ($category_ids != '' && $category_ids!= 'all') {
            $products = $products->whereIn('categories.id', explode(',', $category_ids) );
        }

        if (isset($subcategory_ids) && $subcategory_ids!='') {
            $products = $products->whereIn('products.sub_category_id', explode(',', $subcategory_ids) );
        } 

        if (isset($brand_ids) && $brand_ids != '') {
            $products = $products->whereIn('products.brand_id', explode(',', $brand_ids) );
        }

        if (isset($discount) && $discount != '') {
             // get csv discount
            $products = $products->where(function($query) use ($discount) {

                $discount_query2 = '
                                case when products.original_price = 0 then 0 else 
                                    ROUND( (products.original_price - products.discount_price ) /products.original_price *100 )  end 
                                ';
                $discount_ranges = explode(',', $discount);
                $touch = false;

                foreach ($discount_ranges as $key => $discount) {
                    if(strpos($discount, 'to') !== FALSE){

                        list($lower,$upper) = explode('to', $discount);

                        if($touch)
                        $query->orWhereRaw( '( ' . $discount_query2 . ' BETWEEN ? and ? )', array($lower, $upper) );
                        else
                        $query->whereRaw( '( ' . $discount_query2 . ' BETWEEN ? and ? )', array($lower, $upper) );

                        $touch = true;
                    }
                    else{
                            if(strpos($discount, 'mt') !== FALSE){
                                list($tmp,$upper) = explode('mt', $discount);
                                if($touch)
                                    $query->orWhereRaw( '( ' . $discount_query2 . ' > ? )', array($upper) );
                                else
                                    $query->whereRaw( '( ' . $discount_query2 . ' > ? )', array($upper) );
                            }
                    }
                }
            });
        }


        if (isset($price) && $price != '') {
             // get csv price
            $products = $products->where(function($query) use ($price) {

                $price_ranges = explode(',', $price); // 21to50,51to100
                $touch = false;

                foreach ($price_ranges as $key => $price) {
                   
                    if(strpos($price, 'to') !== FALSE){

                        list($lower,$upper) = explode('to', $price);

                        if($touch)
                            $query->orWhereRaw( '( products.discount_price BETWEEN ? and ? )', array($lower, $upper) );
                        else
                            $query->whereRaw(   '( products.discount_price BETWEEN ? and ? )', array($lower, $upper) );

                        $touch = true;
                    }
                    else{
                            if(strpos($price, 'lt') !== FALSE){

                                list($lower,$upper) = explode('lt', $price);

                                if($touch)
                                $query->orWhereRaw( '( products.discount_price < ? )', array($upper) );
                                else
                                $query->whereRaw( '( products.discount_price < ? )', array($upper) );
                            }

                        $touch = true;
                    }
                }

            });
        }


        $products = $products->where('products.active_status', '=', 1)
                ->where('products.approval_status', '=', 1)
                ->groupBy('products.id', 'products_infos.description', 'products_infos.product_name','categories_infos.category_name','categories.id','weight_classes_infos.unit','weight_classes_infos.title' , 'outlets.id');
                //->orderBy('categories_infos.category_name', 'asc');


            $products = $products->whereRaw('products.discount_price < products.original_price')
            ->orderBy('product_discount','desc');
                //->where('outlets.url_index', '=', $store_key);
        
                //->orderBy('products.id', 'asc')
                //->get();
        
        $products = $products->paginate($perpage);
                //->get();
               //print_r($products);exit;


        /* DB::getQueryLog() */

        $categories = array();

        foreach ($products as $key => $prod) {

            if(isset($user_id) && !empty($user_id))
            $products[$key]->product_cart_count = $this->get_cart_product_count($user_id, $prod->product_id);


            if(isset($user_id) && !empty($user_id))
            $products[$key]->is_fav = $this->get_product_fav_count($user_id, $prod->product_id);

 

            if($prod->is_multi_price == 1 && !empty($prod->variants) )
            $products[$key]->variants = json_decode($prod->variants);

            if(isset($user_id) && !empty($user_id) && $prod->is_multi_price && !empty($products[$key]->variants))
            $products[$key]->variants  = $this->get_multi_cart_product_count($user_id, $products[$key]->variants);

        }
        //print_r($categories);exit;
        /*
        $query = 'vendors_infos.lang_id = (case when (select count(vendors_infos.lang_id) as totalcount from vendors_infos where vendors_infos.lang_id = ' . $language_id . ' and vendors.id = vendors_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';

        $query1 = 'outlet_infos.language_id = (case when (select count(outlet_infos.language_id) as totalcount from outlet_infos where outlet_infos.language_id = ' . $language_id . ' and outlets.id = outlet_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';

        $condition = 'vendors.active_status = 1 ';//and vendors.featured_vendor = 1';

       $vendors = Vendors::join('vendors_infos', 'vendors_infos.id', '=', 'vendors.id')
                ->join('outlets', 'outlets.vendor_id', '=', 'vendors.id')
                ->join('outlet_infos', 'outlets.id', '=', 'outlet_infos.id')
                ->select('vendors.id as vendors_id', 'vendors_infos.vendor_name', 'vendors.first_name', 'vendors.last_name', 'vendors.featured_image', 'vendors.logo_image', 'vendors.delivery_time as vendors_delivery_time', 'vendors.category_ids', 'vendors.average_rating as vendors_average_rating', 'outlet_infos.contact_address as outlets_contact_address', 'outlets.id as outlets_id', 'outlets.vendor_id as outlets_vendors_id', 'outlet_infos.outlet_name', 'outlets.delivery_time as outlets_delivery_time', 'outlets.average_rating as outlets_average_rating', 'outlets.delivery_charges_fixed as outlets_delivery_charges_fixed', 'outlets.pickup_time as outlets_pickup_time', 'outlets.latitude as outlets_latitude', 'outlets.longitude as outlets_longitude', 'outlets.url_index')
                ->whereRaw($query)
                ->whereRaw($query1)
                ->whereRaw($condition)
                //->where('outlets.url_index', '=', $store_key)
                ->where('outlets.active_status', '=', '1')
                ->get();
        $outlet_info = array(); 
      
        if (count($vendors)) {
            foreach ($vendors as $key => $datas) {
                $outlet_info[$key]['outlets_id'] = $datas->outlets_id;
                $outlet_info[$key]['vendors_id'] = $datas->vendors_id;
                $outlet_info[$key]['vendor_name'] = $datas->vendor_name;
                $outlet_info[$key]['first_name'] = $datas->first_name;
                $outlet_info[$key]['last_name'] = $datas->last_name;
                $outlet_info[$key]['featured_image'] = $datas->featured_image;
                $outlet_info[$key]['logo_image'] = $datas->logo_image;
                $outlet_info[$key]['vendors_delivery_time'] = $datas->vendors_delivery_time;
                $outlet_info[$key]['category_ids'] = $datas->category_ids;
                $outlet_info[$key]['vendors_average_rating'] = ($datas->vendors_average_rating == null) ? 0 : $datas->vendors_average_rating;
                $outlet_info[$key]['outlets_contact_address'] = $datas->outlets_contact_address;
                $outlet_info[$key]['outlets_vendors_id'] = $datas->outlets_vendors_id;
                $outlet_info[$key]['outlet_name'] = $datas->outlet_name;
                $outlet_info[$key]['outlets_delivery_time'] = $datas->outlets_delivery_time;
                $outlet_info[$key]['outlets_average_rating'] = ($datas->outlets_average_rating == null) ? 0 : $datas->outlets_average_rating;
                $outlet_info[$key]['outlets_delivery_charges_fixed'] = $datas->outlets_delivery_charges_fixed;
                $outlet_info[$key]['outlets_pickup_time'] = $datas->outlets_pickup_time;
                $outlet_info[$key]['outlets_latitude'] = $datas->outlets_latitude;
                $outlet_info[$key]['outlets_longitude'] = $datas->outlets_longitude;
                $outlet_info[$key]['url_index'] = $datas->url_index;
            }
        }
        */
        $result =   array("response" =>
                                array(  "httpCode" => 200, 
                                        "status" => true, 
                                        'data' => $products, 
                                        //'outlet_info' => $outlet_info, 
                                        'categories' => $categories,
                                        'query' => DB::getQueryLog(),
                                        'links' => $products->links()
                                )
                    );

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }




    public function most_popular_filter(Request $data) {
    

        DB::enableQueryLog();

        $post_data = $data->all();
 
        $brand_ids        = isset($post_data['brand_ids'])          ? $post_data['brand_ids']       : '';
        $user_id      = isset($post_data['user_id'])        ? $post_data['user_id']     : '';
        $discount     = isset($post_data['discount'])   ? $post_data['discount'] : '';
        $price        = isset($post_data['price'])      ? $post_data['price'] : '';

        $from         = isset($post_data['from'])           ? $post_data['from']        : '';
        $limit        = isset($post_data['limit'])          ? $post_data['limit']       : '';
        $keyword      = isset($post_data['keyword'])        ? $post_data['keyword']     : '';

        $perpage      = isset($post_data['perpage'])        ? $post_data['perpage']     : 6 ;

        $language_id = isset($post_data['language'])        ? $post_data['language']     :  getCurrentLang();



        $data = array();
        //$result = array("response" => array("httpCode" => 400, "status" => false, "data" => $data));

        $pquery = ' products_infos.lang_id = (
                        case when (
                                    select count(*) as totalcount 
                                    from products_infos 
                                    where 
                                        products_infos.lang_id = ' . $language_id . ' and products.id = products_infos.id) > 0 THEN ' . $language_id . ' 

                                    ELSE 1 END)';



        $wquery = 'weight_classes_infos.lang_id = (
                        case when (
                                    select count(*) as totalcount 
                                    from weight_classes_infos where weight_classes_infos.lang_id = ' . $language_id . ' and weight_classes.id = weight_classes_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
        $discount_query = '
                                (case when coalesce(products.original_price,0) = 0 then 0 else 
                                    ROUND( (products.original_price - products.discount_price ) /products.original_price *100 )  end 
                                )
                                as product_discount ';


//////////////////////////////////////////////////////////////////////////////


        $products   = DB::table('orders')

                                ->select( 

                                        DB::raw('SUM(orders_info.item_unit) AS sold_count'),
                                        'orders_info.outlet_id',
                                        'orders_info.vendor_id as vendor_id',
                                        'orders_info.item_id as product_id', 
                                        'products.id as product_id',
                                        'products.product_url', 
                                        'products.product_image', 
                                        'products.weight', 
                                        'products.original_price', 
                                        'products.discount_price', 
                                        //'products.vendor_id',
                                        'products.quantity', 
                                        'products_infos.product_name', 
                                        'products_infos.description',
                                        'weight_classes_infos.unit', 
                                        'weight_classes_infos.title',
                                        'products.weight_class_id',
                                        'products.is_multi_price',
                                
                                        DB::raw ('  

                                            array_to_json( 

                                                    array_agg( distinct (product_variants ) 
                                                     ) 

                                            )  AS variants'
                                         ),

                                        DB::raw($discount_query)
                                

                                )

/*
SELECT products.name, SUM(orders.quantity) AS sum_quantity
FROM orders left join products ON ( orders.product_id = products.id )
GROUP BY orders.product_id
ORDER BY sum_quantity DESC;
*/
                                ->join('orders_info'                ,'orders.id',                '=',           'orders_info.order_id')
                                ->join('products'                   ,'products.id',              '=',           'orders_info.item_id')
                                ->join('products_infos'             ,'products_infos.id',        '=',           'products.id')
                                ->leftJoin('weight_classes', 'weight_classes.id',                '=',           'products.weight_class_id')
                                ->leftJoin('weight_classes_infos'   , 'weight_classes_infos.id', '=',           'weight_classes.id')
                                ->join('vendors'                    , 'vendors.id',              '=',           'products.vendor_id')
                                ->join('outlets'                    , 'outlets.id',              '=',           'products.outlet_id')
                                ->leftJoin('product_variants'       , 'products.id',             '=',           'product_variants.product_id')
                                ->whereRaw($pquery)
                                ->where('products.active_status', '=', 1)
                                ->where('products.approval_status', '=', 1);

                                if(isset($vendors) && count($vendors))
                                {
                                    $products = $products->whereIn('orders_info.vendor_id', $vendors);
                                }
                                
                            // 'orders_info.item_unit')

                            //->where('orders.outlet_id', "=", $store_id)
 

////////////////////////////////////////////////////////////////

                $products = $products
                            ->where('vendors.active_status',1)
                            ->where('outlets.active_status','1');


        if (isset($brand_ids) && $brand_ids != '') {
            $products = $products->whereIn(     'products.brand_id',     explode(',', $brand_ids)       );
        }

        if (isset($discount) && $discount != '') {
             // get csv discount
            $products = $products->where(function($query) use ($discount) {

                $discount_query2 = '
                                        case when products.original_price = 0 then 0 else 
                                        ROUND( (products.original_price - products.discount_price ) /products.original_price *100 )  end 
                                    ';
                $discount_ranges = explode(',', $discount);
                $touch = false;

                foreach ($discount_ranges as $key => $discount) {
                    if(strpos($discount, 'to') !== FALSE){

                        list($lower,$upper) = explode('to', $discount);

                        if($touch)
                            $query->orWhereRaw( '( ' . $discount_query2 . ' BETWEEN ? and ? )', array($lower, $upper) );
                        else
                            $query->whereRaw( '( ' . $discount_query2 . ' BETWEEN ? and ? )', array($lower, $upper) );

                        $touch = true;
                    }
                    else{
                            if(strpos($discount, 'mt') !== FALSE){
                                list($tmp,$upper) = explode('mt', $discount);
                                if($touch)
                                    $query->orWhereRaw( '( ' . $discount_query2 . ' > ? )', array($upper) );
                                else
                                    $query->whereRaw( '( ' . $discount_query2 . ' > ? )', array($upper) );
                            }
                    }
                }
            });
        }


        if (isset($price) && $price != '') {
                 // get csv price
                $products = $products->where(function($query) use ($price) {

                    $price_ranges = explode(',', $price); // 21to50,51to100
                    $touch = false;

                    foreach ($price_ranges as $key => $price) {
                       
                        if(strpos($price, 'to') !== FALSE){

                            list($lower,$upper) = explode('to', $price);

                            if($touch)
                                $query->orWhereRaw( '( products.discount_price BETWEEN ? and ? )', array($lower, $upper) );
                            else
                                $query->whereRaw(   '( products.discount_price BETWEEN ? and ? )', array($lower, $upper) );

                            $touch = true;
                        }
                        else{
                                if(strpos($price, 'lt') !== FALSE){

                                    list($lower,$upper) = explode('lt', $price);

                                    if($touch)
                                    $query->orWhereRaw( '( products.discount_price < ? )', array($upper) );
                                    else
                                    $query->whereRaw( '( products.discount_price < ? )', array($upper) );
                                }

                            $touch = true;
                        }
                    }

                });
        }


        $products = $products->where('products.active_status', '=', 1)
                    ->where('products.approval_status', '=', 1)
                    ->groupBy('orders_info.item_id',
                                'orders_info.outlet_id',
                                'orders_info.vendor_id',
                                'products.id',
                                'products_infos.product_name',
                                'products_infos.description',
                                'weight_classes_infos.unit',
                                'weight_classes_infos.title'
                            );


        $most_sold_products = $products->orderBy('sold_count', 'desc')
                                    ->paginate($perpage);


    


        /* DB::getQueryLog() */

        $categories = array();

        $products = $most_sold_products;


        foreach ($products as $key => $prod) {

            if(isset($user_id) && !empty($user_id))
            $products[$key]->product_cart_count = $this->get_cart_product_count($user_id, $prod->product_id);


            if(isset($user_id) && !empty($user_id))
            $products[$key]->is_fav = $this->get_product_fav_count($user_id, $prod->product_id);



            if($prod->is_multi_price == 1 && !empty($prod->variants) )
            $products[$key]->variants = json_decode($prod->variants);

            if(isset($user_id) && !empty($user_id) && $prod->is_multi_price && !empty($products[$key]->variants))
            $products[$key]->variants  = $this->get_multi_cart_product_count($user_id, $products[$key]->variants);

        }

        // echo '<pre>';

        // print_r(  $products );

        // exit;

 
        $result =   array("response" =>
                                array(  "httpCode" => 200, 
                                        "status" => true, 
                                        'data' => $products
                                )
                    );

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    public function new_arrivals_filter(Request $data) {
    

   $post_data = $data->all();

        $category_url = isset($post_data['category_url'])   ? $post_data['category_url']: '';
        $category_ids  = isset($post_data['category_ids'])    ? $post_data['category_ids'] : '';
        $subcategory_ids = isset($post_data['subcategory_ids'])    ? $post_data['subcategory_ids'] : '';
        $brand_ids        = isset($post_data['brand_ids'])          ? $post_data['brand_ids']       : '';
        $user_id      = isset($post_data['user_id'])        ? $post_data['user_id']     : '';
        $discount     = isset($post_data['discount'])   ? $post_data['discount'] : '';
        $price        = isset($post_data['price'])      ? $post_data['price'] : '';

        $from         = isset($post_data['from'])           ? $post_data['from']        : '';
        $limit        = isset($post_data['limit'])          ? $post_data['limit']       : '';
        $keyword      = isset($post_data['keyword'])        ? $post_data['keyword']     : '';

        $perpage      = isset($post_data['perpage'])        ? $post_data['perpage']     : 6 ;
        $purpose      = isset($post_data['purpose'])        ? $post_data['purpose']     : '' ;
        $language_id = isset($post_data['language'])        ? $post_data['language']     :  getCurrentLang();


        $todays         = date("Y-m-d");
 

        $data = array();
        //$result = array("response" => array("httpCode" => 400, "status" => false, "data" => $data));

        if ($keyword != '') {
            $condtion .= " and products_infos.product_name ILIKE '%" . $keyword . "%'";
        }

        $pquery = 'products_infos.lang_id = (
                        case when (
                                    select count(*) as totalcount 
                                    from products_infos 
                                    where 
                                        products_infos.lang_id = ' . $language_id . ' and products.id = products_infos.id) > 0 THEN ' . $language_id . ' 

                                    ELSE 1 END)';

        $cquery = 'categories_infos.language_id = (
                        case when (
                                    select count(*) as totalcount 
                                    from categories_infos 
                                    where 
                                    categories_infos.language_id = ' . $language_id . ' and categories.id = categories_infos.category_id) > 0 THEN ' . $language_id . ' 
                                    ELSE 1 END)';

        $wquery = 'weight_classes_infos.lang_id = (
                        case when (
                                    select count(*) as totalcount 
                                    from weight_classes_infos where weight_classes_infos.lang_id = ' . $language_id . ' and weight_classes.id = weight_classes_infos.id) > 0 THEN ' . $language_id . ' ELSE 1 END)';
        $discount_query = '
                                (case when coalesce(products.original_price,0) = 0 then 0 else 
                                    ROUND( (products.original_price - products.discount_price ) /products.original_price *100 )  end 
                                )
                                as product_discount ';

        $products = Products::join('products_infos', 'products.id', '=', 'products_infos.id')
                ->join('categories', 'categories.id', '=', 'products.category_id')
                ->join('categories_infos', 'categories_infos.category_id', '=', 'categories.id')
                ->leftJoin('weight_classes', 'weight_classes.id', '=', 'products.weight_class_id')
                ->leftJoin('weight_classes_infos', 'weight_classes_infos.id', '=', 'weight_classes.id')
                ->join('vendors', 'vendors.id', '=', 'products.vendor_id')
                ->join('outlets', 'outlets.id', '=', 'products.outlet_id')
                ->leftJoin('product_variants', 'products.id', '=', 'product_variants.product_id')
                ->select('products.id as product_id', 'products.product_url','products.vendor_category_id', 'products.product_image', 'products.weight', 'products.original_price', 'products.discount_price', 'products.vendor_id', 'products.outlet_id', 'products_infos.description', 'products_infos.product_name', 
                    'categories_infos.category_name', 'categories.id', 'weight_classes_infos.unit', 'weight_classes_infos.title', 'products.category_id', 'categories.url_key', 'categories.url_key as cat_url', 'outlets.id as outlet_id', 'products.quantity','products.weight_class_id','is_multi_price',

                    DB::raw($discount_query) ,



                    DB::raw ( '  array_to_json( array_agg( product_variants order by  product_variants.weight_value desc) )  AS variants')


                )

                ->whereRaw($pquery)
                ->whereRaw($cquery)
                //->whereRaw($wquery)
                ->where('vendors.active_status',1)
                ->where('outlets.active_status','1');



        if ($category_url != '' && $category_url!= 'all') {
            $products = $products->where('categories.url_key', '=', $category_url);
        }

        if ($category_ids != '' && $category_ids!= 'all') {
            $products = $products->whereIn('categories.id', explode(',', $category_ids) );
        }

        if (isset($subcategory_ids) && $subcategory_ids!='') {
            $products = $products->whereIn('products.sub_category_id', explode(',', $subcategory_ids) );
        } 

        if (isset($brand_ids) && $brand_ids != '') {
            $products = $products->whereIn('products.brand_id', explode(',', $brand_ids) );
        }

        if (isset($discount) && $discount != '') {
             // get csv discount
            $products = $products->where(function($query) use ($discount) {

                $discount_query2 = '
                                case when products.original_price = 0 then 0 else 
                                    ROUND( (products.original_price - products.discount_price ) /products.original_price *100 )  end 
                                ';
                $discount_ranges = explode(',', $discount);
                $touch = false;

                foreach ($discount_ranges as $key => $discount) {
                    if(strpos($discount, 'to') !== FALSE){

                        list($lower,$upper) = explode('to', $discount);

                        if($touch)
                        $query->orWhereRaw( '( ' . $discount_query2 . ' BETWEEN ? and ? )', array($lower, $upper) );
                        else
                        $query->whereRaw( '( ' . $discount_query2 . ' BETWEEN ? and ? )', array($lower, $upper) );

                        $touch = true;
                    }
                    else{
                            if(strpos($discount, 'mt') !== FALSE){
                                list($tmp,$upper) = explode('mt', $discount);
                                if($touch)
                                    $query->orWhereRaw( '( ' . $discount_query2 . ' > ? )', array($upper) );
                                else
                                    $query->whereRaw( '( ' . $discount_query2 . ' > ? )', array($upper) );
                            }
                    }
                }
            });
        }


        if (isset($price) && $price != '') {
             // get csv price
            $products = $products->where(function($query) use ($price) {

                $price_ranges = explode(',', $price); // 21to50,51to100
                $touch = false;

                foreach ($price_ranges as $key => $price) {
                   
                    if(strpos($price, 'to') !== FALSE){

                        list($lower,$upper) = explode('to', $price);

                        if($touch)
                            $query->orWhereRaw( '( products.discount_price BETWEEN ? and ? )', array($lower, $upper) );
                        else
                            $query->whereRaw(   '( products.discount_price BETWEEN ? and ? )', array($lower, $upper) );

                        $touch = true;
                    }
                    else{
                            if(strpos($price, 'lt') !== FALSE){

                                list($lower,$upper) = explode('lt', $price);

                                if($touch)
                                $query->orWhereRaw( '( products.discount_price < ? )', array($upper) );
                                else
                                $query->whereRaw( '( products.discount_price < ? )', array($upper) );
                            }

                        $touch = true;
                    }
                }

            });
        }


        $products = $products->where('products.active_status', '=', 1)
                ->where('products.approval_status', '=', 1)
                ->groupBy('products.id', 'products_infos.description', 'products_infos.product_name','categories_infos.category_name','categories.id','weight_classes_infos.unit','weight_classes_infos.title' , 'outlets.id');
                //->orderBy('categories_infos.category_name', 'asc');


            $products = $products->whereRaw('products.discount_price < products.original_price')
                        ->orderBy('product_discount','desc');
                //->where('outlets.url_index', '=', $store_key);
        
                //->orderBy('products.id', 'asc')
                //->get();
 
        $products = $products->whereDate(  'products.created_date', '=' , $todays )->paginate($perpage);
                //->get();
               //print_r($products);exit;


        /* DB::getQueryLog() */

        $categories = array();

        foreach ($products as $key => $prod) {

            if(isset($user_id) && !empty($user_id))
            $products[$key]->product_cart_count = $this->get_cart_product_count($user_id, $prod->product_id);


            if(isset($user_id) && !empty($user_id))
            $products[$key]->is_fav = $this->get_product_fav_count($user_id, $prod->product_id);



            if($prod->is_multi_price == 1 && !empty($prod->variants) )
            $products[$key]->variants = json_decode($prod->variants);

            if(isset($user_id) && !empty($user_id) && $prod->is_multi_price && !empty($products[$key]->variants))
            $products[$key]->variants  = $this->get_multi_cart_product_count($user_id, $products[$key]->variants);

        }

        // echo '<pre>';

        // print_r(  $products );

        // exit;

 
        $result =   array("response" =>
                                array(  "httpCode" => 200, 
                                        "status" => true, 
                                        'data' => $products
                                )
                    );

        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;

use Yajra\Datatables\Datatables;
use URL;
use App\Model\vendors;
use App\Model\vendors_view;
use App\Model\vendors_infos;
use App\Model\users;
use App\Model\outlets;
use App\Model\outlet_infos;
use App\Model\delivery_timings;
use App\Model\opening_timings;
use App\Model\settings;
use App\Model\outlet_managers;
use Illuminate\Support\Facades\Text;
class Staffs extends Controller
{
                const VENDORS_REGISTER_EMAIL_TEMPLATE = 4;
                const MANAGER_WELCOME_EMAIL_TEMPLATE = 11;
                const MANAGER_SIGNUP_EMAIL_TEMPLATE = 12;
                /**
                 * Create a new controller instance.
                 *
                 * @return void
                 */
                public function __construct()
                {
                    // $this->middleware('auth');
                    $this->site_name = isset(getAppConfig()->site_name)?ucfirst(getAppConfig()->site_name):'';
                    SEOMeta::setTitle($this->site_name);
                    SEOMeta::setDescription($this->site_name);
                    SEOMeta::addKeyword($this->site_name);
                    OpenGraph::setTitle($this->site_name);
                    OpenGraph::setDescription($this->site_name);
                    OpenGraph::setUrl($this->site_name);
                    Twitter::setTitle($this->site_name);
                    Twitter::setSite('@'.$this->site_name);
                    App::setLocale('en');
                }
                /**
                 * Show the application dashboard.
                 * @return \Illuminate\Http\Response
                 */
                public function staffs()
                {
                    
                    $id=Session::get('vendor_id');
                        if (!$id)
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendors/staffs'))
                        {
                        return view('errors.405');
                        }
                        //  print_r('expression');exit;
                        return view('vendors.staffs.list');
                   /* }*/
                }
                /**
                 * Create the specified vendor in view.
                 * @param  int  $id
                 * @return Response
                 */
                
                public function staffs_create()
                {
                   // print_r('expression');exit;
                    $id=Session::get('vendor_id');
                        if (!$id)
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendors/create_staffs'))
                        {
                        return view('errors.405');
                        }
                        else 

                        //Get countries data
                        $countries = getCountryLists();
                        //Get the categories data with type vendor
                        $categories= getCategoryLists(2);
                        return view('vendors.staffs.create')->with('countries', $countries)->with('categories', $categories);
                   /* }*/
                }

                /**
                 * Add the specified vendor in storage.
                 * @param  int  $id
                 * @return Response
                 */
                public function staffs_store(Request $data)
                {
                    //  print_r($data->all());exit;
                    $id=Session::get('vendor_id');
                        if (!$id)
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendors/staffs'))
                        {
                        return view('errors.405');
                        }

                    $fields['first_name'] = Input::get('first_name');
                    $fields['last_name'] = Input::get('last_name');
                    $fields['email'] = Input::get('email');
                    $fields['user_password'] = Input::get('user_password');
                    //$fields['user_password_confirm'] = Input::get('user_password_confirm');
                    $fields['mobile'] = Input::get('mobile');
                    //$fields['phone_number'] = Input::get('phone_number');
                    $fields['date_of_birth'] = Input::get('date_of_birth');
                    $fields['gender'] = Input::get('gender');
                    //$fields['outlet_name'] = Input::get('outlet_name');
                    $fields['city'] = Input::get('city');
                    $fields['postal_code'] = Input::get('postal_code');
                    $fields['address'] = Input::get('address');
                    $fields['image'] = Input::get('image');

                    $rules = array(
                        'first_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:3|max:32',
                        'last_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:1|max:32',
                        'email' => 'required|email|max:255|unique:vendors_view,email',
                        //'mobile' => 'required',
                        'user_password'=>'required',
                        'mobile'=>'required',
                        //'date_of_birth'=>'required',
                        'gender'=>'required',
                        //'outlet_name'=>'required',
                        'city'=>'required',
                        'postal_code'=>'required',
                        'address'=>'required',
                        
                        /*
                            'Mobile Number' => 'required|regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/',
                        
                        'contact_address' => 'required',
                        */
                        //'featured_vendor' => 'required',
                        //'active_status' => 'required',
                        
                        //'logo' => 'required|mimes:png,jpg,jpeg,bmp',
                        //'image' => 'required|mimes:png,jpg,jpeg,bmp|max:2024'
                    );
                         

                    $validation = Validator::make($fields, $rules);    
                    // process the validation
                   // print_r($data->all());//exit;
                        if ($validation->fails())
                        { 
                            return Redirect::back()->withErrors($validation)->withInput();
                        }  
                    else {
                        //Store the data here with database
                        try{
                    //print_r("ihi");exit;
                            $users = new Users;

                            $users->name = $_POST['first_name'];
                            $users->first_name = $_POST['first_name'];
                            $users->last_name = $_POST['last_name'];
                            $users->email = $_POST['email'];
                            $users->password = md5($_POST['user_password']);
                            $users->mobile_number = $_POST['mobile'];
                            $users->country_id = 1;      /*  1-> Malaysia    $_POST['country'];   */  
                            $users->city_id = $_POST['city'];
                            $users->is_verified = $_POST['is_verified'];
                            //$users->image = $_POST['image'];
                            $users->created_date = date('Y-m-d H:i:s');
                            //$users->updated_date = date('Y-m-d H:i:s');
                            // $users->created_by = Auth::user()->id;
                            $users->created_by = Session::get('vendor_id');

                            $users->save();

                            $this->vendor_insert($users,$_POST,$data);
                     
                            Session::flash('message', trans('messages.User has been added successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('vendors/staffs');
                    }
                }
                
                public function vendor_insert($user_detail,$post,$data) 
                {
                            // print_r($data->all());
                            // exit;
                            $Vendors = new Vendors;

                            $Vendors->user_id = $user_detail->id;
                           // $Vendors->property_id = Input::get('outlet_name');
                            $Vendors->postal_code = $_POST['postal_code'];
                            $Vendors->user_address = $_POST['address'];
                            $Vendors->gender = $_POST['gender'];
                            $Vendors->date_of_birth = $_POST['date_of_birth'];
                            $Vendors->created_vendor_id = Session::get('vendor_id');
                            $Vendors->vendor_name = Session::get('vendor_name');
                            // $Vendors->phone_number = $_POST['phone_number'];
                                 //$Vendors->contact_email = $_POST['contact_email'];
                                 //$Vendors->contact_address = $_POST['contact_address'];
                               //  $Vendors->updated_date = date('Y-m-d H:i:s');
                            $Vendors->active_status = isset($_POST['active_status'])?$_POST['active_status']:0;
                            // $Vendors->featured_vendor = isset($_POST['featured_vendor'])?$_POST['featured_vendor']:0;
                            // $Vendors->latitude = $_POST['latitude'];
                            // $Vendors->longitude = $_POST['longitude'];
                           // $Vendors->vendor_key =  strtoupper(substr($vendor_name[1],0,3));
                            $Vendors->original_password = $_POST['user_password'];
    
                            $Vendors->vendor_type = 2;
                            $Vendors->save();

                            if(isset($_FILES['image']['name']) && $_FILES['image']['name']!='')
                           {

                                $imageName = strtolower($Vendors->user_id . '.' . $data->file('image')->getClientOriginalExtension());
                                $data->file('image')->move(
                                    base_path() . '/public/assets/admin/base/images/vendors/staffs/logos/', $imageName
                                );
                                $destinationPath1 = url('/assets/admin/base/images/vendors/staffs/logos/'.$imageName.'');
                                Image::make( $destinationPath1 )->fit(203, 103)->save(base_path() .'/public/assets/admin/base/images/vendors/staffs/logos/'.$imageName)->destroy();
                                $Vendors->logo_image=$imageName;
                                $Vendors->save();
                            }
                            /*

                            $imageName = strtolower($Vendors->id . '.' . $data->file('featured_image')->getClientOriginalExtension());
                            $data->file('featured_image')->move(
                                base_path() . '/public/assets/admin/base/images/vendors/', $imageName
                            );
                            $destinationPath2 = url('/assets/admin/base/images/vendors/'.$imageName.'');
                            $size=getImageResize('VENDOR');
                            Image::make( $destinationPath2 )->fit($size['LIST_WIDTH'], $size['LIST_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/vendors/list/'.$imageName)->destroy();
                            
                            Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/vendors/detail/'.$imageName)->destroy();
                            
                            Image::make( $destinationPath2 )->fit($size['THUMB_WIDTH'], $size['THUMB_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/vendors/thumb/'.$imageName)->destroy();
                            
                            Image::make( $destinationPath2 )->fit($size['DETAIL_WIDTH'], $size['DETAIL_HEIGHT'])->save(base_path() .'/public/assets/admin/base/images/vendors/thumb/detail/'.$imageName)->destroy();
                            $Vendors->featured_image=$imageName;
                            $Vendors->save();   
                            */
                            // $this->vendor_save_after($user_detail,$Vendors,$_POST,1);             

                }  
                public function staffs_edit($id)
                {
                     // print_r($id);exit;
                    //$id=Session::get('vendor_id');
                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        if(!has_staff_permission('vendors/edit_staffs'))
                        {
                        return view('errors.405');
                        }
                        //Get vendor details
                        $vendors = Vendors_view::find($id);
                        // echo '<pre>';
                        // print_r($vendors);exit;
                        if(!count($vendors)){
                             Session::flash('message', 'Invalid User Details'); 
                             return Redirect::to('vendors/staffs');
                        }
                        //Get the vendors information
                        $info = new Vendors_infos;
                        // echo '<pre>';
                        // print_r($info);exit;
                        //Get countries data
                       // $countries = getCountryLists();
                        //Get the categories data with type vendor
                        //$categories= getCategoryLists(2);
                        
                        return view('vendors.staffs.edit')/*->with('countries', $countries)->with('categories', $categories)*/->with('datas', $vendors)->with('infomodel', $info);
                   /* } */
                }

                /**
                 * Update the specified vendor in storage.
                 * @param  int  $id
                 * @return Response
                 */
                public function staffs_update(Request $data, $id)
                {
                // $id=Session::get('vendor_id');
                    // dd('AK');
                   // print_r($id);exit;
                    if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }
                    if(!has_staff_permission('vendors/staffs'))
                    {
                    return view('errors.405');
                    }                   

                    $fields['first_name'] = Input::get('first_name');
                    $fields['last_name'] = Input::get('last_name');
                    $fields['mobile'] = Input::get('mobile');
                    $fields['gender'] = Input::get('gender');
                    //$fields['outlet_name'] = Input::get('outlet_name');
                    $fields['city'] = Input::get('city');
                    $fields['postal_code'] = Input::get('postal_code');
                    $fields['address'] = Input::get('address');

                    $rules = array(
                        'first_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:3|max:32',
                        'last_name' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|min:1|max:32',
                        'mobile'=>'nullable|numeric',
                        'gender'=>'required',
                        //'outlet_name'=>'required',
                        'city'=>'required',
                        'postal_code'=>'required',
                        'address'=>'required',

                    );
                    
                    $validator = Validator::make($fields, $rules); 
                       
                    // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput();
                    } else {
                        try{
                       
                            $users = Users::find($id);

                            $users->name = Input::get('first_name');
                            $users->first_name = Input::get('first_name');
                            $users->last_name = Input::get('last_name');
                           /* if(Input::get('user_password') != ""){
                                $users->password = md5(Input::get('user_password'));
                            }*/
                            if(Input::get('mobile') != ""){
                                $users->mobile_number = Input::get('mobile');
                            }
                            $users->mobile_number = Input::get('mobile');
                            $users->country_id = 1;      /*  1-> Malaysia    $_POST['country'];   */ 
                            $users->city_id = Input::get('city');

                            // $users->updated_date = date('Y-m-d H:i:s');
                            $users->save();


                            $this->vendor_insert_update($users->id,$data);

                            // $this->vendor_save_after_infos($users->id,$users,$_POST,1);
                                      
                            Session::flash('message', trans('messages.User has been updated successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('vendors/staffs');
                    }
                }


                public function vendor_insert_update($abc,$data)
                {

                    // $vendor_name = $_POST['vendor_name'];
                    // print_r($data->all());exit;
                    //print_r($id);die;
                    $Vendors = Vendors::where('user_id',$abc)
                                ->update([
                                   // 'property_id'   => $data['outlet_name'],
                                    'postal_code'  => $data['postal_code'],
                                    'user_address'=> $data['address'],
                                    'active_status'  => isset($data['active_status'])?$data['active_status']:0,
                                    'gender'       => $data['gender'],
                                    'date_of_birth'      => $data['date_of_birth'],
                                    'original_password'       => $data['user_password'],
                                    'vendor_type' => 2,
                                    'vendor_name' => Session::get('vendor_name'),
                                    'created_vendor_id' => Session::get('vendor_id'),
                                    ]);
                                    

                    if(isset($_FILES['image']['name']) && $_FILES['image']['name']!=''){
                                //get last insert id
                                $imageName = strtolower($abc . '.' . $data->file('image')->getClientOriginalExtension());

                                $data->file('image')->move(
                                    base_path() . '/public/assets/admin/base/images/vendors/staffs/logos/', $imageName
                                );
                                $destinationPath1 = url('/assets/admin/base/images/vendors/staffs/logos/'.$imageName.'');
                                Image::make( $destinationPath1 )->fit(253, 133)->save(base_path() .'/public/assets/admin/base/images/vendors/staffs/logos/'.$imageName)->destroy();

                                $Vendors = Vendors::where('user_id',$abc)->update(['logo_image' => $imageName]);
                            }                    
                }
                

               /**
                 * Display the specified vendor.
                 * @param  int  $id
                 * @return Response
                 */
                public function staffs_show($id)
                {
                    // print_r('expression');//exit;
                    if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }
                    if(!has_staff_permission('vendors/staffs_details'))
                    {
                    return view('errors.405');
                    }
                    // print_r('expression123');//exit;
                    //Get vendor details
                    $vendors = Vendors_view::find($id);
                    if(!count($vendors)){
                        Session::flash('message', 'Invalid User Details'); 
                        return Redirect::to('vendors/staffs');    
                    }
                     // print_r($vendors);exit;
                    //Get the vendors information
                    $info = new Vendors_infos;
                    //Get countries data
                    $countries = getCountryLists();
                    //Get the categories data with type vendor
                    $categories= getCategoryLists(2);
                    
                    return view('vendors.staffs.show')->with('countries', $countries)->with('categories', $categories)->with('data', $vendors)->with('infomodel', $info);
                }
                   /**
                 * Delete the specified vendor in storage.
                 * @param  int  $id
                 * @return Response
                 */
                public function staffs_destroy($id)
                {

                    $data = Vendors::where('user_id',$id)->first();
                    if(!count($data)){
                        Session::flash('message', 'Invalid Vendor Details'); 
                        return Redirect::to('vendors/staffs');    
                    }
                    if(!has_staff_permission('vendors/delete_staffs'))
                    {
                    return view('errors.405');
                    }
                    $vendors = Vendors_view::find($id);
                    if(!count($vendors)){
                        Session::flash('message', 'Invalid User Details'); 
                        return Redirect::to('vendors/staffs');    
                    }
                    //$data->delete();
                    //Update delete status while deleting
                    $data->active_status = 2;
                    $data->save();
                    Session::flash('message', trans('messages.User has been deleted successfully!'));
                    return Redirect::to('vendors/staffs');
                }

                /**
                 * Process datatables ajax request.
                 * @return \Illuminate\Http\JsonResponse
                 */
                public function anyAjaxStaffsBranchmanager()
                {
                    $vendor_id = Session::get('vendor_id');
                        //  print_r($id); exit;

                        if(Session::get('vendor_type') == 2 )
                            {
                                $vendor_id = Session::get('created_vendor_id');
                                //  print_r($id); exit;
                            } 
                    //$query = '"vendors_infos"."lang_id" = (case when (select count(id) as totalcount from vendors_infos where vendors_infos.lang_id = '.getAdminCurrentLang().' and vendors_view.id = vendors_infos.vendors_view_id) > 0 THEN '.getAdminCurrentLang().' ELSE 1 END)';
                    $vendors = DB::table('vendors_view')
                    //->leftJoin('outlets','outlets.id','=','vendors_view.property_id')
                    //->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                     ->select('vendors_view.id', 'vendors_view.first_name', 'vendors_view.last_name','vendors_view.email', 'vendors_view.mobile_number', 'vendors_view.created_date', 'vendors_view.active_status'/*,'outlet_infos.outlet_name'*/)
                                ->where('vendors_view.vendor_type','=',2)
                                //  ->whereRaw($query)
                                ->orderBy('vendors_view.created_date', 'desc')
                                ->get();
                            // print_r($vendors);exit;
                    return Datatables::of($vendors)->addColumn('action', function ($vendors) {
                            if(has_staff_permission('vendors/edit_staffs/'))
                            {
                            $html='<div class="btn-group">
                                <a href="'.URL::to("vendors/edit_staffs/".$vendors->id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu xs pull-right" role="menu">
                                    <li><a href="'.URL::to("vendors/staffs_details/".$vendors->id).'" class="view-'.$vendors->id.'" title="'.trans("messages.View").'"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;'.@trans("messages.View").'</a></li>
                                    <li><a href="'.URL::to("vendors/delete_staffs/".$vendors->id).'" class="delete-'.$vendors->id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                </ul>
                            </div>
                            <script type="text/javascript">
                                $( document ).ready(function() {
                                    $(".delete-'.$vendors->id.'").on("click", function(){
                                        return confirm("'.trans("messages.Are you sure want to delete?").'");
                                    });
                                });
                            </script>';
                            return $html;
                        }
                    })
 

                    ->addColumn('active_status', function ($vendors) {
                        if($vendors->active_status==0):
                            $data = '<span class="label label-warning">'.trans("messages.Inactive").'</span>';
                        elseif($vendors->active_status==1):
                            $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                        elseif($vendors->active_status==2):
                            $data = '<span class="label label-danger">'.trans("messages.Delete").'</span>';
                        endif;
                        return $data;
                    })
                    ->make(true);
                }
 
}

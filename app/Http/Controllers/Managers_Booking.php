<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Model\booking_user_details;
use App\Model\booking_detail;
use App\Model\booked_room_details;
use App\Model\payment;
use App\Model\admin_customers;
use App\Model\booking_charge;
use App\Model\booking_infos;
use App\Model\rooms_infos;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;


class Managers_Booking extends Controller
{

                
                public function new_booking()
                {

                    if (!Session::get('manager_id')) {

                        return redirect()->guest('managers/login');

                    }
                    else{

                        $room_type = DB::table('room_type')
                                    ->select('room_type.*','room_type_infos.*')
                                    ->leftJoin('room_type_infos','room_type_infos.id','=','room_type.id')
                                    ->where('default_status', 1)
                                    ->where('property_id',Session::get('manager_outlet'))
                                    ->orderBy('room_type', 'asc')
                                    ->get();

                        $room = DB::table('rooms')
                                    ->select('rooms.*','rooms_infos.*')
                                    ->leftJoin('rooms_infos','rooms_infos.id','=','rooms.id')
                                    ->where('rooms.property_id',Session::get('manager_outlet'))
                                    ->where('default_status',1)
                                    ->orderBy('room_name','asc')
                                    ->get();
                        //  print_r($room); exit;

                        $order_status = DB::table('booking_status')
                                            ->select('id','name')->orderBy('id', 'asc')->get();                                    

                        return view('managers.booking.create')->with('room_type',$room_type)
                                                              ->with('room',$room)
                                                              ->with('order_status',$order_status);
                    }
                }


                public function booking_user(Request $data)
                {
                            //echo "<pre>";

                              //    dd($data->all());
                    //  dd($data->child_count);
                    //                     print_r($data->all());
                    //         var_dump($data->all());
                    // exit;
  //    dd($data->all());

                    $fields['booking_type']    = Input::get('booking_type');
                    $fields['booking_source']  = Input::get('booking_source');
                    $fields['guest_check']     = Input::get('guest_check');
                    $fields['adult_count']     = Input::get('adult_count');
                    $fields['child_count']     = Input::get('child_count');
                    $fields['description']     = Input::get('description');                    
                    $fields['image']           = Input::file('image');
                    $fields['guest_name']      = Input::get('guest_name');
                    $fields['mobile']          = Input::get('mobile');
                    $fields['email']           = Input::get('email');
                    $fields['address']         = Input::get('address');



                    $rules = array(
                        
                        'booking_type'   => 'required',
                        'booking_source' => 'required',
                        //  'guest_check'    => 'required',
                        'adult_count'    => 'required',
                        'child_count'    => 'required',
                        'email'          => 'required',
                        // 'guest_name'     => 'required',
                        // 'mobile'         => 'required|numeric',
                        // 'address'        => 'required',
                        //  'city'           => 'required',
                    );
  
                    $country_short = Input::get('country_short');

                    $validator = Validator::make($fields, $rules);    
                            // process the validation
                    if ($validator->fails())
                    { 
                        return Redirect::back()->withErrors($validator)->withInput()->with('country_short',$country_short);
                    } 
                    else {
                        try{

                              //    dd('Ak');
                             // dd($data->all());

                    $rooms = $_POST['room'];

                    $free_room_name = rooms_infos::where('id',$rooms)->pluck('room_name')->toArray();
                    
                    $roo_nm = implode(',', $free_room_name);                     

                    //  $user_id = Input::get('user_id');

                    $mail_id = Input::get('email');

                    //  print_r($user_id); exit;
                    //  dd($data->all());

                    $user_query = Admin_customers::where('email',$mail_id)->first();

                    // print_r($user_query->id);
                    // exit;

                     // echo "<pre>";

                     // print_r($user_query); exit;

                     /*

                    if(count($data)>0){
                        $data->firstname = Input::get('guest_name');
                        $data->email = Input::get('email');
                        $data->mobile_number = Input::get('mobile');
                        $data->country_code = Input::get('country_code');
                        $data->address = Input::get('address');
                        $data->save();
                    }

                    else
                    {
                                         
                        $customers = new Admin_customers;

                        $customers->firstname = Input::get('guest_name');
                        $customers->email = Input::get('email');
                        $customers->mobile_number = Input::get('mobile');
                        $customers->country_code = Input::get('country_code');
                        $customers->address = Input::get('address');
                        $customers->status = 1;
                        $customers->is_verified = 1;
                        $customers->save();
                    }

                    */

//  print_r($data->id); exit;
//  print_r($customers->id);exit;

                    $cout_date = date('Y-m-d',strtotime(Input::get('check_out'). ' - 1 day'));
                    // print_r($cout_date); 
                    // print_r($data->all());
                    // exit;

                    $cin = strtotime(Input::get('check_in'));

                    $cout =strtotime($cout_date);                    

                    // print_r($cout);
                    // exit;

//                      foreach ($rooms as $key => $value) {

                    $booking_random_id = getRandomBookingNumber(6);

                    //  $invoice_id = "INV".$booking_random_id;

                    //  $this->downloadinvoice($invoice_id,Session::get('vendor_id'));

                    // dd($invoice_id);

                            $booking_detail = new booking_detail;

                            $booking_detail->booking_status =  Input::get('booking_type');
                            $booking_detail->booking_source = Input::get('booking_source');
                            $booking_detail->adult_count = Input::get('adult_count');
                            $booking_detail->child_count = Input::get('child_count');
                            $booking_detail->check_in_date = Input::get('check_in');
                            $booking_detail->check_out_date = Input::get('check_out');
                            //  $booking_detail->check_out_date = $cout_date;
                            $booking_detail->no_of_days = Input::get('nights');
                            $booking_detail->room_type = Input::get('room_type_id');
                            $booking_detail->rooms = $rooms;
                            $booking_detail->room_name = $roo_nm;
                            $booking_detail->notes = Input::get('notes');
                            $booking_detail->booking_random_id = $booking_random_id;
                            $booking_detail->created_date = date('Y-m-d H:i:s');
                            $booking_detail->room_booked_date = date('Y-m-d');
                            $booking_detail->charges = Input::get('price');
                            $booking_detail->original_price = Input::get('price');
                            $booking_detail->room_type_cost = Input::get('price');
                            $booking_detail->payments = 0;
                            $booking_detail->booking_type = 2;

                            //  $booking_detail->date = date("Y-m-d", $seconds);
                            // if($user_id!="")
                            // {
                            //     $booking_detail->customer_id = $data->id;    
                            // }
                            // else
                            // {
                            //     $booking_detail->customer_id = $customers->id; 
                            // }

                            $booking_detail->customer_id = $user_query->id;
                            
                            $booking_detail->created_by = Session::get('manager_id');
                            $booking_detail->manager_id = Session::get('manager_id');
                            $booking_detail->outlet_id = Session::get('manager_outlet');
                            $booking_detail->vendor_id = Session::get('manager_vendor');

                            $booking_detail->save(); 

                            //  STORING ROOMS ID IN BOOKING INFOS TABLE

                            $infos = new booking_infos;
                            $infos->room_id = $rooms;
                            $infos->booking_id = $booking_detail->id;
                            $infos->save();


                            $charges = new booking_charge;

                            //  $charges->room_id = $rooms;
                            $charges->room_type = Input::get('room_type_id');
                            $charges->price = Input::get('price');
                            $charges->charge_type = 1;
                            $charges->booking_id = $booking_detail->id;
                            $charges->customer_id = $user_query->id;
                            $charges->created_by = Session::get('manager_id');
                            $charges->created_date = date('Y-m-d H:i:s');
                            $charges->notes = Input::get('notes');
                            $charges->active_status = 1;
                            $charges->property_id = Session::get('manager_outlet');
                            $charges->save();
    

                            /*
                                $booking_ud = new booking_user_details;

                                $booking_ud->guest_name =  Input::get('guest_name');
                                $booking_ud->email = Input::get('email');
                                $booking_ud->customer_type = Input::get('customer_type');
                                $booking_ud->country_code = Input::get('country_code');
                                $booking_ud->mobile_number = Input::get('mobile');
                                $booking_ud->created_date = date("Y-m-d H:i:s");
                                $booking_ud->created_by = Session::get('vendor_id');
                                $booking_ud->address = Input::get('address');
                                $booking_ud->city_id = Input::get('city');
                                $booking_ud->postal_code = Input::get('postal_code');
                                $booking_ud->booking_id = $booking_detail->id;
                                $booking_ud->vendor_id = Session::get('vendor_id');
                                $booking_ud->outlet_id = Session::get('property_id');

                                $booking_ud->save();

                            */

                    for ($seconds=$cin; $seconds<=$cout; $seconds+=86400)
                        {
                            $brd = new booked_room_details;

                            $brd->check_in_date  = Input::get('check_in');
                            //  $brd->check_out_date = Input::get('check_out');
                            $brd->check_out_date = $cout_date;
                            $brd->no_of_days     = Input::get('nights');
                            $brd->room_id        = $rooms;
                            $brd->booking_id     = $booking_detail->id;
                            $brd->date = date("Y-m-d", $seconds);
                            $brd->availablity_status = 1;
                            $brd->booking_status = Input::get('booking_type');
                            $brd->customer_id = $user_query->id;

                            $brd->save();
                        }

                    //  }   FOREACH

                    $users = DB::table('rooms')
                                        //  ->whereIn('rooms.id', $rooms)
                                        ->where('rooms.id',$rooms)
                                        ->update([
                                            'room_clean_status' => 0,
                                            'availability_status' => 1,
                                            'check_in_date' => Input::get('check_in'),
                                            'check_out_date' => Input::get('check_out'),
                                            'customer_id' => $user_query->id,
                                        ]);

                            Session::flash('message', trans('messages.Booking has been added successfully'));
                        }catch(Exception $e) {
                                Log::Instance()->add(Log::ERROR, $e);
                        }
                        return Redirect::to('managers/dashboard');                    
                    }
                }
                           /**
                             * Process datatables ajax request.
                             *
                             * @return \Illuminate\Http\JsonResponse
                             */
                public function anyAjaxPaymentmanager()
                            {
         
                                $vendor_id = Session::get('vendor_id');
                                

                                if(Session::get('vendor_type') == 2 )
                                    {
                                        $vendor_id = Session::get('created_vendor_id');
                                    } 

                                $payment = DB::table('payments')
                                                ->select('payments.id as payment_id','payments.active_status as payment_status','payments.*','admin_customers.*')
                                                ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                                //  ->where('payments.booking_id','=',$id)
                                                ->leftJoin('admin_customers','admin_customers.id','payments.customer_id')
                                                ->where('payments.vendor_id','=',$vendor_id)
                                                ->orderBy('payments.id', 'desc')
                                                ->get();
                                  //    print_r($payment);exit;

                                return Datatables::of($payment) /*->addColumn('action', function ($payment) {
                                    if(has_staff_permission('vendor/edit_outlet_manager')){
                                        $html='<div class="btn-group"><a href="'.URL::to("booking/editPayment/".$payment->payment_id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                                                    <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu xs pull-right" role="menu">
                                                    <li><a href="'.URL::to("booking/deletePayment/".$payment->payment_id).'" class="delete-'.$payment->payment_id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                                                    </ul>
                                                </div><script type="text/javascript">
                                                $( document ).ready(function() {
                                                $(".delete-'.$payment->payment_id.'").on("click", function(){
                                                     return confirm("'.trans("messages.Are you sure want to delete?").'");
                                                });});</script>';
                                    
                                        return $html;
                                        }
                                    })
                                    */
                            ->addColumn('payment_status', function ($payment) {
                                if($payment->payment_status==0):
                                    $data = '<span class="label label-warning">'.trans("messages.Inactive").'</span>';
                                elseif($payment->payment_status==1):
                                    $data = '<span class="label label-success">'.trans("messages.Active").'</span>';
                                elseif($payment->payment_status==2):
                                    $data = '<span class="label label-danger">'.trans("messages.Delete").'</span>';
                                endif;
                                return $data;
                            })
                            ->addColumn('paid_type', function ($payment) {
                                $data = '-';
                                if($payment->paid_type==1):
                                     $data = trans("messages.Cash Payment");
                                endif;
                                return $data;                                                              
                                })                                
                            ->make(true);
                            }
                public function showBooking($id)
                {
                //  dd($id);
                    if (!Session::get('manager_id')) {

                        return redirect()->guest('managers/login');

                    }
                    else
                        {
                           
                        $vendor_id = Session::get('manager_vendor');
                           
                        $vendor_orders = DB::table('booking_details')
                                            ->select('admin_customers.id as cust_id','booking_details.id as bid','booking_details.*','admin_customers.*','booking_status.*','booking_info.*')
                                            ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                            ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                                            ->leftJoin('rooms','rooms.id','booking_info.room_id')
                                            ->leftJoin('booking_status','booking_status.id','booking_details.booking_status')
                                            ->where('booking_details.vendor_id',$vendor_id)
                                            //  ->where('booking_details.outlet_id',Session::get('property_id'))
                                            ->where('booking_details.id','=',$id)
                                            ->first();
                        //  dd($vendor_orders);

                        $payment = DB::table('payments')
                                        ->select('payments.id')
                                        ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                        ->where('payments.booking_id','=',$id)
                                        //  ->paginate(1)
                                        ->get();


                        $payment_list = DB::table('payments')
                                        ->select('payments.id as payment_id','payments.active_status as payment_status','payments.*','admin_customers.*')
                                        ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                        ->where('payments.booking_id','=',$id)
                                        ->leftJoin('admin_customers','admin_customers.id','payments.customer_id')
                                        ->where('payments.vendor_id','=',$vendor_id)
                                        ->orderBy('payments.id', 'asc')
                                        ->get(); 

                        //  Multiple Rooms Makes the Duplicate Issues , So Booking_details.rooms are used

                        $charges_list = DB::table('booking_charges')
                                                    ->select('booking_charges.id as bc_id','booking_charges.notes as bc_notes','booking_charges.active_status as booking_status','booking_charges.*','booking_details.*','admin_customers.*'/*, 'rooms_infos.room_name', 'booking_info.*'*/)
                                                    ->leftJoin('booking_details','booking_details.id','booking_charges.booking_id')
                                                    //  ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                                                    ->leftJoin('admin_customers','admin_customers.id','booking_charges.customer_id')
                                                    //  ->leftJoin('rooms_infos','rooms_infos.id','booking_info.room_id')
                                                    ->where('booking_charges.booking_id','=',$id)
                                                    ->orderBy('booking_charges.id','asc')
                                                    ->get();
                            //  dd($charges_list);

                        $charges_count = DB::table('booking_charges')
                                                    ->select(DB::raw('sum(price) as price'))
                                                    ->leftJoin('booking_details','booking_details.id','booking_charges.booking_id')
                                                    ->where('booking_charges.booking_id','=',$id)
                                                    ->get();

                        $payment_count = DB::table('payments')
                                                   ->select(DB::raw('sum(paid_amount) as paid_amount'))
                                                   ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                                   ->where('payments.booking_id','=',$id)
                                                   ->get();
                        $order_status = DB::table('booking_status')
                                                   ->select('id','name')
                                                   ->orderBy('id', 'asc')
                                                   ->get();

                        $order_history = DB::select('SELECT ol.order_comments,ol.booking_status,log_time,booking_status.name as status_name,booking_status.color_code as color_code
                            FROM orders_log ol
                            left join booking_status on booking_status.id = ol.booking_status
                            where ol.order_id = ? ORDER BY ol.id',array($id));

                        return view('managers.booking.show')->with('data', $vendor_orders)
                                                            ->with('payment',$payment)
                                                            ->with('payment_list',$payment_list)
                                                            ->with('charges_list',$charges_list)
                                                            ->with('charges_count',$charges_count[0])
                                                            ->with('payment_count',$payment_count[0])
                                                            ->with('order_status',$order_status)
                                                            ->with('order_history',$order_history);
                        }                    
                }

                public function storePayment()
                {
                        $bid = $_POST['booking_id'];

                    //  print_r($bid); exit;

                        $booking_details = DB::table('booking_details')
                                             ->where('id',$bid)
                                             ->increment('payments', $_POST['amount']);

                        //  print_r($booking_details); exit;

                        $payment= new payment;

                        $vendor_id = Session::get('vendor_id');

                        $payment->booking_id =  $_POST['booking_id'];
                        $payment->customer_id = $_POST['customer_name'];
                        $payment->created_date = date("Y-m-d H:i:s");
                        $payment->active_status =  1;
                        $payment->created_by =  Session::get('manager_id');
                        $payment->paid_type = $_POST['paid_type'];
                        $payment->paid_amount = $_POST['amount'];
                        $payment->description = $_POST['description'];
                        $payment->property_id = Session::get('manager_vendor');
                        $payment->vendor_id = Session::get('manager_vendor');
                        $payment->room_id = $_POST['room_id'];
                        $payment->room_type = $_POST['room_type_id'];                        
                        $payment->save();

                        echo 1; exit;
                }

                public function storeCharges()
                {
                        $bid = $_POST['booking_id'];

                    //  print_r($bid); exit;

                        $charges = new booking_charge;

                        $vendor_id = Session::get('manager_vendor');

                        $charges->booking_id =  $_POST['booking_id'];
                        $charges->customer_id = $_POST['customer_name_charges'];
                        $charges->created_date = date("Y-m-d H:i:s");
                        $charges->active_status =  1;
                        $charges->created_by =  $vendor_id;
                        $charges->charge_type = $_POST['charge_type'];
                        $charges->manager_id = Session::get('manager_id');
                        $charges->property_id = Session::get('manager_outlet');
                        $charges->price = $_POST['charges_amount'];
                        $charges->notes = $_POST['notes'];
                        $charges->room_id = $_POST['room_id'];
                        $charges->room_type = $_POST['room_type_id'];
                        $charges->save();

                        $booking_details = DB::table('booking_details')
                                             ->where('id',$bid)
                                             ->increment('charges', $_POST['charges_amount']);

                        echo 1; exit;
                }
                public function payment_destroy($id)
                {
                    //  dd($id);
                        if (!Session::get('vendor_id')) {
                            return redirect()->guest('vendors/login');
                        }
                        $data = Payment::find($id);
                        if(!count($data))
                        {
                            Session::flash('message', 'Invalid Payment Details'); 
                            return Redirect::to('vendor/editbooking/19');    
                        }
                        $data->active_status = 2 ;
                        $data->save();
                        Session::flash('message', trans('messages.Payment has been deleted successfully!'));
                        return Redirect::to('vendor/editbooking/19');

                }

                public function editBooking($id)
                {
                    //  dd($id);
                        if (!Session::get('vendor_id'))
                        {
                            return redirect()->guest('vendors/login');
                        }
                        /*
                            if(!has_staff_permission('vendor/outlet_details'))
                            {
                            return view('errors.405');
                            }
                        */
                        else
                        {
                           
                        $vendor_id = Session::get('vendor_id');
                      
                        if (Session::get('vendor_type') == 2 ){

                            $vendor_id = Session::get('created_vendor_id');

                        }                            
                        $property_id = Session::get('property_id');                        
                        $vendor_orders = DB::table('booking_details')
                                            ->select('booking_details.id as bid','booking_details.*','admin_customers.*')
                                            ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                            ->leftJoin('rooms','rooms.id','booking_details.rooms')
                                            ->where('booking_details.vendor_id',$vendor_id)
                                            ->where('booking_details.outlet_id',Session::get('property_id'))
                                            ->where('booking_details.id','=',$id)
                                            ->first();
                        //  dd($vendor_orders);

                        $payment = DB::table('payments')
                                        ->select('payments.id')
                                        ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                        ->where('payments.booking_id','=',$id)
                                        ->get();

                        $room_type = DB::table('room_type')
                                    ->select('room_type.*','room_type_infos.*')
                                    ->leftJoin('room_type_infos','room_type_infos.id','=','room_type.id')
                                    ->where('default_status', 1)
                                    ->where('property_id',Session::get('property_id'))
                                    ->orderBy('room_type', 'asc')
                                    ->get();                                        

                              //    dd($payment);

                        return view('managers.booking.edit')->with('data', $vendor_orders)
                                                           ->with('payment',$payment)
                                                           ->with('room_type',$room_type);
                                                           
                    }                    
                }

                public function autocomplete(Request $request)
                {
                    //  $data = DB::table('admin_customers')

                    $data = Admin_customers::select("email as name")
                                            ->where("email","LIKE","%{$request->input('query')}%")
                                            ->get();

                    return response()->json($data);
                }

                public function newUser(Request $request)
                {


                    $customers = new Admin_customers;

                    $customers->firstname = Input::get('guest_name');
                    $customers->email = Input::get('email');
                    $customers->mobile_number = Input::get('mobile');
                    $customers->country_code = Input::get('country_code');
                    $customers->address = Input::get('address');
                    $customers->status = 1;
                    $customers->is_verified = 1;
                    $customers->save();

                    echo 1; exit;
                }
                
/*                
    function getRoomBookingList($check_in_date)
    {
        //  Get the rooms data
        //  print_r($check_in_date);
        $date = str_replace('/', '/', $check_in_date);
        echo $nd = date('Y-m-d', strtotime($date));        
        
        $all_rooms = DB::table('rooms')
                        ->select('rooms.id','rooms_infos.room_name')
                        ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                        ->where('property_id','=',Session::get('property_id'))  
                        ->get();

                        print_r($all_rooms);
        
        $arr = array();
        $i = 0;

        foreach ($all_rooms as $key) {
            
            $arr[$i] = $key->id;
              $i++;
        }

        print_r($arr);//    exit;

        $rooms=DB::table('rooms')
            ->select('rooms.id as rid','booked_room.id' ,'rooms_infos.room_name')
            ->leftJoin('rooms_infos','rooms_infos.id','=','rooms.id')
            ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
            ->where('booked_room.check_out_date','>',$nd)
            ->get();

            print_r($rooms);

        if(!empty($rooms))
        {
            
            $emt2 = array();
            $i = 0;

            foreach($rooms as $r)
            {
                $emt2[$i] = $r->rid;
                $i++;
            }

            print_r($emt2); //  exit;


            $result=array_diff($arr,$emt2);
            
            print_r($result);

$users = DB::table('rooms')
                    ->whereIn('id', $result)
                    ->get();
            print_r($users);//  exit;


        }

        $rooms_list=array();
        if(count($rooms)>0){
            $rooms_list = $rooms;
        }
        //  print_r($rooms_list);exit;
        return $rooms_list;




                        $vendor_orders = DB::table('booking_details')
                                            ->select('booking_details.*','booking_user_details.*')
                                            ->leftJoin('booking_user_details','booking_user_details.booking_id','booking_details.id')
                                            ->leftJoin('rooms','rooms.id','booking_details.rooms')
                                            ->where('booking_details.vendor_id',$vendor_id)
                                            ->where('booking_details.outlet_id',Session::get('property_id'))
                                            ->groupBy('booking_details.rooms')
                                            ->get();

                          //    dd($vendor_orders);

                          

                        $vendors_orders_aray = array();
                        $i = 0;
                        foreach($vendor_orders as $vendor_order)
                        {
                            if($vendor_order->date!="")
                            {

                                $cin_date = date('Y-m-d',strtotime($vendor_order->date));

                                //  $cout_date = date('Y-m-d',strtotime($vendor_order->date. ' + 1 day'));

                                //  $cout_date->modify('+1 day');

                                //  dd($date);
                                $vendors_orders_aray[$i]['id'] = $vendor_order->id;
                                $vendors_orders_aray[$i]['title'] = $vendor_order->guest_name;
                                $vendors_orders_aray[$i]['start'] = $cin_date /*    .'T'.'00:00:00' ; 
                                //  $vendors_orders_aray[$i]['end'] = $cin_date /* .'T'.'00:00:00' ;
                                $vendors_orders_aray[$i]['notes'] = $vendor_order->rooms;

                    

                                $i++;
                            }

                            //$vendors_orders_aray['end'] = $vendor_order->date.'T1'.$vendor_order->date;
                        }

                        $vendor_orders_encoded = json_encode($vendors_orders_aray,true);


    }

*/
}

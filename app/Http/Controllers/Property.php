<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use DB;
use App\Model\contactus;
use App\Model\users;
use App\Model\settings;
use App\Model\emailsettings;
use App\Model\cms;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use MetaTag;
use Mail;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use App\Model\outlets;
use Illuminate\Support\Facades\Input;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ServerErrorResponseException;
use App\Model\api;
use App\Model\Api_model;
use App\Model\rooms;
use Socialite;

class Property extends Controller
{
    public function __construct()
    {
            //    print_r(getAppConfig()->site_name);exit;
        $this->client = new Client([
            // Base URI is used with relative requests
            'base_uri' => url('/'),
            // You can set any number of default request options.
            'timeout'  => 3000.0,
        ]);
        $this->theme = Session::get("general")->theme;
        $this->api = New Api;
    }

    /*
    *Properties listing page based on cities and categories also both ajax and pagination here
    */
    public function index($city_category,$url_index, Request $post_request)
    {



        $site_settings = getSettingsLists();

        SEOMeta::setTitle($site_settings->meta_title);
        SEOMeta::setDescription($site_settings->site_description);
        SEOMeta::addKeyword($site_settings->meta_keywords);
        OpenGraph::setTitle($site_settings->meta_title);
        OpenGraph::setDescription($site_settings->site_description);
        OpenGraph::setUrl(URL::to('/'));
        Twitter::setTitle($site_settings->meta_title);
        Twitter::setSite($site_settings->site_description);
        
        //  dd($url_index);
        $api = New Api;
        $method = "GET";
        $data = array();

        $guest_count = 1;
        $room_count = 1;
        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d', strtotime(' +1 day'));        
        /*
        *If the request is from cities list city based properties 
        */
        if($city_category=='city') {


            $url = url("/api/list-hotels/city/".$url_index);
            $response = $api->call_api($data,$url,$method);


            $outlets =  DB::table('outlets')
                            ->leftJoin('cities','cities.id','=','outlets.city_id')
                            ->join('room_type','room_type.property_id','=','outlets.id')
                            ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id') 
                            ->select('outlets.id')
                            ->where('cities.url_index',$url_index)
                            ->groupBy('outlets.id')
                            ->get();


            $oid = $outlets->pluck('id')->toArray();

            // dd($oid);

            $query = DB::table('rooms')
                        ->leftjoin('room_type','room_type.id','=','rooms.room_type_id')
                        ->whereIN('rooms.property_id',$oid)
                        ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id')
                        ->groupBy('room_type.id');
                      
            $room_type = $query->get();
             //        dd($room_type);

            $rt = $room_type->pluck('id');             

            // dd($rt);

            $rtr = rooms::whereIN('room_type_id',$rt)
                        ->pluck('id')->toArray();

             // dd($rtr);
            //  print_r($rtr);

            $rtrs = rooms::whereIN('room_type_id',$rt)
                        ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                        ->whereBetween('booked_room.date',array($startDate,$endDate))
                        ->pluck('rooms.id')->toArray();

           //   dd($rtrs);
            $result=array_diff($rtr,$rtrs);
            
            //  dd($result);

            $users = DB::table('rooms')
                                ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id as rt')
                                ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                //  ->leftJoin('outlets','outlets.id','rooms.property_id')
                                ->whereIn('rooms.id', $result)
                                ->groupBy('room_type.id')
                                ->get();

               //   dd($users);

            $romco = $users->where('rooms_count','>=',$room_count)->pluck('rt')->toArray();

                 //   return $romco;

            $out =  DB::table('outlets')
                     ->whereIN('room_type.id',$romco)
                     ->leftjoin('room_type','room_type.property_id','=','outlets.id')
                     ->distinct('oid')
                     ->pluck('outlets.id as oid')
                     ->toArray();

            //  dd($out);
/*
            $outlets = DB::table('cities')
            ->join('outlets','outlets.city_id','=','cities.id')
            ->leftjoin('cities_infos','cities_infos.id','=','cities.id') 
            ->join('outlet_price_category_mapping','outlet_price_category_mapping.outlet_id','=','outlets.id')
            ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
            ->leftjoin('zones_infos','zones_infos.zone_id','=','outlets.location_id')            
            ->join('room_type','room_type.property_id','=','outlets.id')
            ->leftjoin('outlet_reviews','outlet_reviews.outlet_id','=','outlets.id')
            ->select(DB::Raw('min(room_type.discount_price) as discount_price'),
                DB::Raw('min(room_type.normal_price) as normal_price'),
                DB::Raw('count(outlet_reviews.id) as or_count'),
                DB::Raw('avg(outlet_reviews.ratings) as or_rates'),
                'outlet_infos.outlet_name','cities_infos.city_name','outlets.outlet_image',
                'outlet_infos.contact_address','outlets.url_index','cities_infos.city_name',
                'zones_infos.zone_name','outlets.id')
            ->where('cities.url_index',$url_index)
            ->groupBy('cities_infos.city_name','zones_infos.zone_name')
            ->groupBy('outlets.url_index')
            ->groupBy('outlet_infos.outlet_name')
            ->groupBy('outlets.outlet_image','outlets.id')
            ->groupBy('outlet_infos.contact_address','cities_infos.city_name','zones_infos.zone_name')
            //  ->paginate(2);
            ->get();
*/
            $outlets =  DB::table('outlets')
                            ->leftJoin('cities','cities.id','=','outlets.city_id')
                            ->leftjoin('cities_infos','cities_infos.id','=','cities.id')
                            ->leftjoin('zones_infos','zones_infos.zone_id','=','outlets.location_id')
                            ->leftJoin('outlet_reviews','outlet_reviews.outlet_id','=','outlets.id')
                            ->join('room_type','room_type.property_id','=','outlets.id')
                            ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                            ->select(DB::Raw('avg(outlet_reviews.ratings) as avg_out_rev'),'outlets.id','outlet_infos.outlet_name','cities_infos.city_name',
                                'outlets.outlet_image','outlet_infos.contact_address','outlets.url_index',
                                'cities_infos.city_name','zones_infos.zone_name')
                            ->whereIN('outlets.id',$out)
                            ->groupBy('outlets.id')
                            ->groupBy('outlets.url_index','outlet_infos.contact_address')
                            ->groupBy('outlet_infos.outlet_name')
                            ->groupBy('outlets.outlet_image','outlets.id')                            
                            ->groupBy('cities_infos.city_name','zones_infos.zone_name')
                            ->orderBy('avg_out_rev','asc')
                            ->paginate(2);
                            //  ->get();

              // dd($outlets);

            $priv = $outlets->pluck('id');

            $orp = outlets::whereIN('id',$priv)->pluck('rd')->toArray();

            if(count($orp) > 0)
            {
                $min_value = min($orp);
                $max_value = max($orp); 
            }
            else
            {
                $min_value = 0;
                $max_value = 1000;
            }

            /*
            *If the request is from ajax take the details and pass it into external view
            */
            if ($post_request->ajax()) {
                $data = $post_request->all();

                        //  dd($data);

               if(!empty($data['data']['pricing'])){
                    $pricing = explode(',',$data['data']['pricing']);
               }
               
               if(!empty($data['data']['amenities'])){
                    $amenities = explode(',',$data['data']['amenities']);
               }

               if(!empty($data['data']['accomodation'])){
                    $accomodation = explode(',',$data['data']['accomodation']);
               }

               if(!empty($data['data']['areas'])){
                    $areas = explode(',',$data['data']['areas']);
               }

               if(!empty($data['data']['rating'])){
                    $rating = explode(',',$data['data']['rating']);
               }

               if(!empty($data['data']['radio_price'])){
                    $radio_price = $data['data']['radio_price'];
               }

               if(!empty($data['data']['radio_popular'])){
                    $radio_popular = $data['data']['radio_popular'];
               }
               
               if(!empty($data['data']['radio_dist'])){
                    $radio_dist = $data['data']['radio_dist'];
               }

               if(!empty($data['data']['radio_rating'])){
                    $radio_rating = $data['data']['radio_rating'];
               }

               if(!empty($data['data']['slider_val'])){
                    $slider_val = explode(',',$data['data']['slider_val']);
               }                    


                //  dd($rating);

                $city_property = DB::table('outlets')
                    ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                    ->Join('cities','cities.id','=','outlets.city_id');


                if(isset($radio_price)){
                    $city_property->orderby('outlets.rd','asc');
                }

                if(isset($radio_rating)){
                    //  dd($radio_rating);
                    $city_property->orderby('outlets.average_rating','desc');
                }

                if(isset($rating)){
                    $city_property->whereIn('outlets.average_rating',$rating);
                }

                if(isset($slider_val)){
                    $city_property->whereBetween('outlets.rd',$slider_val);
                }

                if(isset($pricing)){
                    $city_property->leftjoin('outlet_price_category_mapping','outlet_price_category_mapping.outlet_id','=','outlets.id');
                }
                if(isset($amenities)){
                    $city_property->leftjoin('outlet_amenity_mapping','outlet_amenity_mapping.outlet_id','=','outlets.id');
                }
                if(isset($accomodation)){
                    $city_property->leftjoin('outlet_accomodation_mapping','outlet_accomodation_mapping.outlet_id','=','outlets.id');
                }
                if(isset($areas)){
                    $city_property->leftjoin('zones','zones.id','=','outlets.location_id');
                }
                $city_property->where('cities.url_index',$url_index);
                if(isset($pricing)){
                    $city_property->whereIn('outlet_price_category_mapping.outlet_price_category',$pricing);
                }


                if(isset($amenities)){
                    $city_property->whereIn('outlet_amenity_mapping.amenity_id',$amenities);
                }
                if(isset($accomodation)){
                    $city_property->whereIn('outlet_accomodation_mapping.accomodation_id',$accomodation);
                }
                if(isset($areas)){
                    $city_property->whereIn('zones.id', $areas);
                }

               
                $outlets = $city_property->select(
                                DB::Raw('min(room_type.discount_price) as discount_price'),
                                DB::Raw('min(room_type.normal_price) as normal_price'),                   
                        'outlets.url_index','outlet_infos.outlet_name',
                        'outlets.outlet_image','outlet_infos.contact_address',
                        'cities_infos.city_name','zones_infos.zone_name','outlets.id')
                    ->join('room_type','room_type.property_id','=','outlets.id')
                    ->leftjoin('outlet_reviews','outlet_reviews.outlet_id','=','outlets.id')
                    ->leftjoin('cities_infos','cities_infos.id','=','cities.id')  
                    ->leftjoin('zones_infos','zones_infos.zone_id','=','outlets.location_id')                    
                    ->groupBy('outlet_infos.outlet_name')
                    ->groupBy('outlets.outlet_image')
                    ->groupBy('outlets.url_index')
                    ->groupBy('outlet_infos.contact_address','outlets.id')
                    ->groupBy('cities_infos.city_name','zones_infos.zone_name')
                    //  ->distinct('outlet_infos.outlet_name')
                    //  ->where('or_rates','<=',4)
                    ->paginate(10);
                    //  ->get();

                      //    dd($outlets);
            
                return view('front.'.$this->theme.'.ajax_property_list', ['properties' => $outlets])->render(); 
            }

            // echo "<pre>";
            // print_r($response);exit;
            /*
            *If the request is from get take the details and pass it into main properties view
            */
            $pricing = $response->response->pricing;
            $accomodation = $response->response->accomodation;
            $amenities = $response->response->amenities;
            $areas = $response->response->areas;
            $city = $response->response->city_name;
            $avg_outlet_reviews = $response->response->avg_outlet_reviews;

            // echo "<pre>";
            // print_r($avg_outlet_reviews);exit;    

            //  print_r(getAppConfig()->site_name);exit;        
            
            return view('front.'.$this->theme.'.property_list')->with('properties',$outlets)
                    ->with('pricing',$pricing)->with('category_index','city')->with('url_index',$url_index)->with('accomodation',$accomodation)->with('amenities',$amenities)->with('areas',$areas)
                      ->with('cities',$city)->with('avg_outlet_reviews',$avg_outlet_reviews)
                      ->with('min_value',$min_value)->with('max_value',$max_value);
         /*
         *If the request is from categories list categories based properties 
         */             
        } else if($city_category=='category') {

            $url = url("/api/list-hotels/category/".$url_index);
            $response = $api->call_api($data,$url,$method);

              //    dd($response);

            $outlets =  DB::table('outlets')
            ->Join('outlet_place_category_mapping','outlet_place_category_mapping.outlet_id','=','outlets.id')
            ->join('room_type','room_type.property_id','=','outlets.id')
            ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id') 
            ->leftjoin('outlet_reviews','outlet_reviews.outlet_id','=','outlets.id')
            ->leftjoin('categories','outlet_place_category_mapping.outlet_place_category','=',DB::raw('CAST("categories"."id" AS TEXT)'))
            ->leftjoin('categories_infos','categories_infos.category_id','=','categories.id')
            ->select('outlets.id')
            ->where('categories.url_key','=',$url_index)
            ->where('categories.category_type',7)
            ->groupBy('outlets.id')
            ->get();


            $oid = $outlets->pluck('id')->toArray();

            //  dd($oid);

            $query = DB::table('rooms')
                        ->leftjoin('room_type','room_type.id','=','rooms.room_type_id')
                        ->whereIN('rooms.property_id',$oid)
                        ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id')
                        ->groupBy('room_type.id');
                      
            $room_type = $query->get();
                     // dd($room_type);

            $rt = $room_type->pluck('id');             

             // dd($rt);

            $rtr = rooms::whereIN('room_type_id',$rt)
                        ->pluck('id')->toArray();

             // dd($rtr);
            //  print_r($rtr);

            $rtrs = rooms::whereIN('room_type_id',$rt)
                        ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                        ->whereBetween('booked_room.date',array($startDate,$endDate))
                        ->pluck('rooms.id')->toArray();

           //   dd($rtrs);
            $result=array_diff($rtr,$rtrs);
            
            //  dd($result);

            $users = DB::table('rooms')
                                ->select(DB::RAW('count(rooms.id) as rooms_count'),'room_type.id as rt')
                                ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                //  ->leftJoin('outlets','outlets.id','rooms.property_id')
                                ->whereIn('rooms.id', $result)
                                ->groupBy('room_type.id')
                                ->get();

               //   dd($users);

            $romco = $users->where('rooms_count','>=',$room_count)->pluck('rt')->toArray();

                 //   return $romco;

            $out =  DB::table('outlets')
                     ->whereIN('room_type.id',$romco)
                     ->leftjoin('room_type','room_type.property_id','=','outlets.id')
                     ->distinct('oid')
                     ->pluck('outlets.id as oid')
                     ->toArray();
            //  dd($out);

            $outlets =  DB::table('outlets')
            ->Join('outlet_place_category_mapping','outlet_place_category_mapping.outlet_id','=','outlets.id')
            ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
            ->leftJoin('outlet_reviews','outlet_reviews.outlet_id','=','outlets.id')
            ->leftjoin('cities_infos','cities_infos.id','=','outlets.city_id')
            ->leftjoin('zones_infos','zones_infos.zone_id','=','outlets.location_id')
            ->leftjoin('categories','outlet_place_category_mapping.outlet_place_category','=',DB::raw('CAST("categories"."id" AS TEXT)'))
            ->leftjoin('categories_infos','categories_infos.category_id','=','categories.id')
            ->select(
                DB::Raw('avg(outlet_reviews.ratings) as avg_out_rev'),
                'outlets.url_index','outlet_infos.outlet_name','outlets.outlet_image','outlets.id',
                'outlet_infos.contact_address','cities_infos.city_name','zones_infos.zone_name',
                'categories_infos.category_name')
            ->where('categories.url_key','=',$url_index)
            ->where('categories.category_type',7)
            ->whereIN('outlets.id',$out)
            ->groupBy('outlet_infos.outlet_name')
            ->groupBy('outlets.url_index')
            ->groupBy('outlets.outlet_image','outlets.id','categories_infos.category_name')
            ->groupBy('outlet_infos.contact_address','cities_infos.city_name','zones_infos.zone_name')
            ->orderBy('avg_out_rev','asc')
            ->paginate(2);
            //  ->get();

            //  dd($outlets);

            $priv = $outlets->pluck('id');

            $orp = outlets::whereIN('id',$priv)->pluck('rd')->toArray();

            if(count($orp) > 0)
            {
                $min_value = min($orp);
                $max_value = max($orp); 
            }
            else
            {
                $min_value = 0;
                $max_value = 1000;
            }

            /*
            *If the request is from ajax take the details and pass it into external view
            */
            if ($post_request->ajax()) {
                $data = $post_request->all();

                  //    dd($data);
                if(!empty($data['data']['cities'])){
                    $cities = explode(',',$data['data']['cities']);
               }

               if(!empty($data['data']['pricing'])){
                    $pricing = explode(',',$data['data']['pricing']);
               }
               
               if(!empty($data['data']['amenities'])){
                    $amenities = explode(',',$data['data']['amenities']);
               }

               if(!empty($data['data']['accomodation'])){
                    $accomodation = explode(',',$data['data']['accomodation']);
               }

               if(!empty($data['data']['rating'])){
                    $rating = explode(',',$data['data']['rating']);
               }       

               if(!empty($data['data']['slider_val'])){
                    $slider_val = explode(',',$data['data']['slider_val']);
               }                    

               if(!empty($data['data']['radio_price'])){
                    $radio_price = $data['data']['radio_price'];
               }

               if(!empty($data['data']['radio_popular'])){
                    $radio_popular = $data['data']['radio_popular'];
               }
               
               if(!empty($data['data']['radio_dist'])){
                    $radio_dist = $data['data']['radio_dist'];
               }

               if(!empty($data['data']['radio_rating'])){
                    $radio_rating = $data['data']['radio_rating'];
               }

               //   dd($slider_val);

                $url_index = DB::table('categories')->where('url_key',$url_index)->where('category_type',7)->select('id','category_type')->get();
                $category_id = (count($url_index)>0) ? $url_index[0]->id:0;
            
                if($category_id != 0){

                    //  dd($out);
                    $category_property = DB::table('outlets')
                                    ->leftjoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                    ->Join('outlet_place_category_mapping','outlet_place_category_mapping.outlet_id','=','outlets.id')
                                    ->whereIn('outlets.id',$out);
                        
                          if(isset($pricing)){
                            $category_property->leftjoin('outlet_price_category_mapping','outlet_price_category_mapping.outlet_id','=','outlets.id');
                          }

                          if(isset($amenities)){
                            $category_property->leftjoin('outlet_amenity_mapping','outlet_amenity_mapping.outlet_id','=','outlets.id');
                          }
                          if(isset($accomodation)){
                            $category_property->leftjoin('outlet_accomodation_mapping','outlet_accomodation_mapping.outlet_id','=','outlets.id');
                          }
                          if(isset($cities)){
                            $category_property->leftjoin('cities','cities.id','=','outlets.city_id');
                          }
                          $category_property->where('outlet_place_category_mapping.outlet_place_category',
                            $category_id);
                          if(isset($pricing)){
                            $category_property->whereIn('outlet_price_category_mapping.outlet_price_category',$pricing);
                          }

                          if(isset($radio_price)){
                                $category_property->orderby('outlets.rd','asc');
                            }

                          if(isset($radio_rating)){
                                $category_property->orderby('outlets.average_rating','desc');
                            }                           

                          if(isset($rating)){
                                $category_property->whereIn('outlets.average_rating',$rating);
                            }

                          if(isset($slider_val)){
                                $category_property->whereBetween('outlets.rd',$slider_val);
                            }

                          if(isset($amenities)){
                            $category_property->whereIn('outlet_amenity_mapping.amenity_id',$amenities);
                          }

                          if(isset($accomodation)){
                            $category_property->whereIn('outlet_accomodation_mapping.accomodation_id',$accomodation);
                          }
                          if(isset($cities)){
                            $category_property->whereIn('cities.id', $cities);
                          }
                    $outlets = $category_property->select(DB::Raw('count(outlet_reviews.id) as or_count'),
                                DB::Raw('avg(outlet_reviews.ratings) as or_rates'),
                                DB::Raw('min(room_type.discount_price) as discount_price'),
                                DB::Raw('min(room_type.normal_price) as normal_price'),
                                'outlets.url_index','outlet_infos.outlet_name','outlets.outlet_image','outlets.id',
                                'outlet_infos.contact_address','cities_infos.city_name','zones_infos.zone_name')
                            ->Join('room_type','room_type.property_id','=','outlets.id')
                            ->leftjoin('cities_infos','cities_infos.id','=','outlets.city_id')
                            ->leftjoin('zones_infos','zones_infos.zone_id','=','outlets.location_id')
                            ->leftjoin('outlet_reviews','outlet_reviews.outlet_id','=','outlets.id')
                            ->groupBy('outlet_infos.outlet_name')
                            ->groupBy('outlets.outlet_image')
                            ->groupBy('outlets.url_index','cities_infos.city_name','zones_infos.zone_name')
                            ->groupBy('outlet_infos.contact_address','outlets.id')
                            ->paginate(2);
                            //  ->get();
                    //  dd($outlets);
                    return view('front.'.$this->theme.'.ajax_property_list', ['properties' => $outlets])->render();
                }
            }
             /*
            *If the request is from get take the details and pass it into main properties view
            */
            $pricing = $response->response->pricing;
            $accomodation = $response->response->accomodation;
            $amenities = $response->response->amenities;
            $cities = $response->response->city_details;
            $avg_outlet_reviews = $response->response->avg_outlet_reviews;

            //  $outlets = $response->response->properties;

            return view('front.'.$this->theme.'.property_list')->with('properties',$outlets)
                    ->with('pricing',$pricing)->with('category_index','category')->with('url_index',$url_index)
                    ->with('accomodation',$accomodation)->with('amenities',$amenities)
                    ->with('city_details',$cities)->with('avg_outlet_reviews',$avg_outlet_reviews)
                    ->with('min_value',$min_value)->with('max_value',$max_value);
        }        
    }
}
?>
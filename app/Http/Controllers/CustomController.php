<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Hash;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;
use App\Model\fund_requests as FundRequest;


class CustomController extends Controller
{
    
     public function __construct(){
       
     }

     public function fundRequestIndex(){
       if (!Session::get('vendor_id'))
        {
            return redirect()->guest('vendors/login');
        }

        return view('vendors.fund_requests.list');
     }

     public function anyAjaxfundRequest()
     {
        if (!Session::get('vendor_id'))
        {
            return redirect()->guest('vendors/login');
        }
         $vendor_id = Session::get('vendor_id');
         $fund_request = DB::table('fund_requests')
                             ->where('vendor_id',$vendor_id)
                             ->select('id', 'vendor_id','reference_no','request_amount',
                              'request_status','created_at')
                             ->get();    
                    return Datatables::of($fund_request)
                            ->addColumn('request_status', function ($fund_request) {
                                $data = '-';
                                if($fund_request->request_status == 0):
                                    $data = '<span class="label label-primary">Pending</span>';
                                elseif($fund_request->request_status == 1):
                                    $data = '<span class="label label-primary">Approved</span>';
                                elseif($fund_request->request_status == 2):
                                    $data = '<span class="label label-primary">Rejected</span>';
                                endif;                                    
                                return $data;
                            })
                            ->make(true);                                                
     } 

     public function createfundRequest(){
        if (!Session::get('vendor_id'))
        {
            return redirect()->guest('vendors/login');
        }   
            return view('vendors.fund_requests.create');
    }

   public function storefundRequest(Request $request){
        if(!Session::get('vendor_id'))
        {
            return redirect()->guest('vendors/login');
        }
        
        $vendor_id = Session::get('vendor_id'); 
        $vendor_details = DB::table('vendors')->where('id', $vendor_id)
                              ->select('current_balance')->first();
        $amount = $vendor_details->current_balance;
        if($amount <= 0){
          $errors = array('You can\'t send the fund request.');
          return Redirect::back()->withErrors($errors)->withInput();
        }

        $min_req = getAppConfig()->min_fund_request;
        $max_req = getAppConfig()->max_fund_request;

        $minimum_amount = ($amount * $min_req)/100;
        $maximun_amount = ($amount * $max_req)/100;

        $validation = Validator::make($request->all(),array(
            'request_amount' => 'required|integer|min:'.$minimum_amount.'|max:'.$maximun_amount.'',
        ));

        if ($validation->fails()) {
           return Redirect::back()->withErrors($validation)->withInput();
        } else { 
                //echo 'here';die;
                $fund_request = new FundRequest;
                $fund_request->vendor_id = $vendor_id;
                $fund_request->reference_no = getRandomNumber(9);
                $fund_request->request_amount = Input::get('request_amount');
                $fund_request->request_status = 0;
                $fund_request->save();

            // redirect
            Session::flash('message', trans('messages.Fund Request Details has been submitted successfully!'));
            return Redirect::to('vendors/fund_requests');
       }
    }

    public function adminFundRequest()
    {
        if (Auth::guest())
        {
            return redirect()->guest('admin/login');
        }
        return view('admin.fund_requests.list');
    }


     public function anyAjaxAdminfunds()
     {
        if (Auth::guest())
        {
            return redirect()->guest('admin/login');
        }
         $vendor_id = Session::get('vendor_id');
         $fund_request = DB::table('fund_requests')
                            ->Join('vendors_view','vendors_view.id','fund_requests.vendor_id')
                             ->select('vendors_view.first_name','vendors_view.last_name','fund_requests.id', 'fund_requests.vendor_id','fund_requests.reference_no','fund_requests.request_amount','fund_requests.request_status',
                              'fund_requests.created_at')
                             ->get();    
                    return Datatables::of($fund_request)
                            ->addColumn('vendor_name', function ($fund_request) {
                                $data = '-';
                                if($fund_request->first_name != "" && $fund_request->last_name != ""):
                                    $data = $fund_request->first_name.' '.$fund_request->last_name;
                                elseif($fund_request->first_name != ""):
                                    $data = $fund_request->first_name;
                                endif;
                                return $data;
                            })
                            ->addColumn('request_status', function ($fund_request) {
                                $data = '-';
                                if($fund_request->request_status == 0):
                                    $data = ' <button class="btn btn-xs btn-info approve_request" value="'.$fund_request->id.'">Approve</button>
                                       <button class="btn btn-xs btn-warning reject_request" value="'.$fund_request->id.'">Reject</button>';
                                elseif($fund_request->request_status == 1):
                                    $data = '<span class="label label-success">Approved</span>';
                                elseif($fund_request->request_status == 2):
                                    $data = '<span class="label label-danger">Rejected</span>';
                                endif;                                    
                                return $data;
                            })
                            ->make(true);                                                
     } 

    public function updatefundRequest(Request $request){
        $fund_id = $request->fund_id;
        $fund_request = FundRequest::where('id',$fund_id)->first();
        return View('admin.fund_requests.fund_payment_list',compact('fund_request'))->render();
    }

    public function updatePaymentMethod(Request $request,$id){
        $fund_id = $request->id;
        $fund_request = FundRequest::find($fund_id)->first();

        $validation = Validator::make($request->all(),array(
            'payment_method' => 'required',
        ));
        if ($validation->fails()) {
           return Redirect::back()->withErrors($validation)->withInput();
        } else { 
                $fund_request = FundRequest::find($id);
                $fund_request->payment_method = Input::get('payment_method');
                $fund_request->request_status = 1;
                $fund_request->save();
             // redirect
            Session::flash('message', trans('messages.Fund Request has been approved successfully!'));
            return Redirect::to('admin/fund_requests');
       }
    }

    public function rejectPaymentRequest(Request $request){
        $reject_id = $request->reject_id;

        $fund_request = FundRequest::find($reject_id);
        $fund_request->request_status = 2;
        $fund_request->save();

        Session::flash('message', trans('messages.Fund Request has been rejected successfully!'));
        echo json_encode(array('success'=>'true'));
    }
}
                                         
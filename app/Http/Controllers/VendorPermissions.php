<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Support\Facades\Text;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use Closure;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use URL;
use App\Model\notifications;
use App\Model\role;
use App\Model\roles_users;
use App\Model\permission_menu;
use App\Model\roles_permission;



class VendorPermissions extends Controller
{
                /**
                 * Create a new controller instance.
                 *
                 * @return void
                 */
                public function __construct()
                {
            		$this->site_name = isset(getAppConfig()->site_name)?ucfirst(getAppConfig()->site_name):'';
                    App::setLocale('en');
                }

                /**
                 * Show the application dashboard.
                 *
                 * @return \Illuminate\Http\Response
                 */
                public function index()
                {
                    if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }
                    else {
                        return view('vendors.roles.list');
                    }
                }


                public function role_create()
                {
/*
                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
*/                    
                     //echo 1;
                     //exit;
                        $roleUser= new role;

                        $vendor_id = Session::get('vendor_id');

                        $roleUser->role_name =  strtolower($_POST['role_name']);
                        $roleUser->created_by =  $vendor_id;
                        $roleUser->created_date = date("Y-m-d H:i:s");
                        $roleUser->active_status =  1;
                        $roleUser->save();

                    $role_id=$roleUser->id;
                        
                        if($role_id){
                        if(isset($_POST['nodes'])){
                         $nodes=$_POST['nodes'];   
                        }else{
                        $nodes;
                        }

                        if(count($nodes)>0) {

                            foreach ($nodes as $node) {
                                # code...

                                    $permission_menu= permission_menu::find($node);
                                    $roles_permission= new roles_permission;
                                    $roles_permission->role_id =  $role_id;
                                    $roles_permission->menu_id =  $permission_menu->id;
                                    $roles_permission->menu_key = $permission_menu->menu_key;
                                    $roles_permission->created_at =  date("Y-m-d H:i:s");;
                                    $roles_permission->save();
}
                            echo 1;
                            exit;
                        }


                    }else{

                        echo -1;exit;
                    }

                }



                 public function role_update()
                {
/*                    
                    if (Auth::guest())
                    {
                        return redirect()->guest('admin/login');
                    }
*/                    
                   //   echo 1;
                    $roleId= $_POST['role_id'];
                     // echo ($roleId);

                    DB::table('roles_permission')->where('role_id', $roleId)->delete();

                    $roleUser = Role::find($roleId);
                    //  print_r($roleUser);exit;
                    $roleUser->role_name =  strtolower($_POST['role_name']);
                    $roleUser->created_by =  Session::get('vendor_id');
                    $roleUser->updated_date = date("Y-m-d H:i:s");
                    $roleUser->active_status =  1;
                    $roleUser->save();

                    $role_id=$roleId;


                        
                        if($role_id){
                        if(isset($_POST['nodes'])){
                         $nodes=$_POST['nodes'];   
                        }else{
                        $nodes;
                        }
                        // if(count($nodes)>0){
                        //     print_r("ak");
                        //     exit;
                        // }
                        if(count($nodes)>0) {

                            foreach ($nodes as $node) {
                                # code...

                                    $permission_menu = permission_menu::find($node);
                                    $roles_permission = new roles_permission;
                                    $roles_permission->role_id =  $role_id;
                                    $roles_permission->menu_id =  $permission_menu->id;
                                    $roles_permission->menu_key = $permission_menu->menu_key;
                                    $roles_permission->created_at =  date("Y-m-d H:i:s");
                                    $roles_permission->save();




                            }
                            echo 1;
                            exit;
                        }


                    }else{

                        echo -1;exit;
                    }

                }
               

}

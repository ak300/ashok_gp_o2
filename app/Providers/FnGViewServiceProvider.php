<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Session;
use App\Model\api;
use App\Model\Api_model;

class FnGViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot()
    {

        view()->composer('*', function ($view)
        {
             

            $api = New Api;

        	   //--------------
        	$language = getCurrentLang();

        	$user_id = Session::has('user_id')? Session::get('user_id'): '';
            $cart_items = '';
            $total = '';
            $sub_total = '';
            $min_order_amount = '';
            $diff =  '';
            $language = getCurrentLang();

            if(!empty($user_id))
            {

                $token = Session::get('token');
               
                $user_array = array("user_id" => $user_id,"token"=>$token,"language" =>$language);
                $method = "POST";
                $data = array('form_params' => $user_array);
                $response = $api->call_api($data,'api/get_cart',$method);
               
                if($response->response->httpCode == 400)
                {
                    $cart_items = array();
                }
                else
                {
                    $cart_items = $response->response->cart_items;
                    $total = $response->response->total;
                    $sub_total = $response->response->sub_total;
                    //$tax = $response->response->tax;
                    //$delivery_cost = $response->response->delivery_cost;
                    //$tax_amount = $response->response->tax_amount;
                    $min_order_amount = getSettings('min_order_amount');

                    $diff = $min_order_amount -  $sub_total;
                }           
            }

// echo '..................................';
// echo '<pre>';
// print_r($cart_items);
// exit;
            $view
            ->with("cart_items",$cart_items)
            ->with("total",$total)
            ->with("sub_total",$sub_total)
            ->with("language",$language)
            ->with("sess_user_id", $user_id)
            ->with("min_order_amount", $min_order_amount)
            ->with("diff", $diff);

	//------------
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;


class cart_model extends Model 
{    

	public $timestamps  = false;
    protected $table = 'cart';
    protected $primaryKey = 'cart_id';
	public static function cart_items($language_id,$user_id)
	{

		// get original model
		$fetchMode = DB::getFetchMode();
		// set mode to custom
		DB::setFetchMode(\PDO::FETCH_ASSOC);
		// get data
 

		$query = 'pi.lang_id = (case when (select count(*) as totalcount from products_infos where products_infos.lang_id = '.$language_id.' and p.id = products_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';
		$vquery = 'vi.lang_id = (case when (select count(*) as totalcount from vendors_infos where vendors_infos.lang_id = '.$language_id.' and vn.id = vendors_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';

		$oquery = 'outin.language_id = (case when (select count(*) as totalcount from outlet_infos where outlet_infos.language_id = '.$language_id.' and out.id = outlet_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';

		$wquery = 'weight.lang_id = (case when (select count(*) as totalcount from weight_classes_infos where weight_classes_infos.lang_id = '.$language_id.' and weight.id = weight_classes_infos.id) > 0 THEN '.$language_id.' ELSE 1 END)';

		$cart_items = DB::select('SELECT 


			c.cart_id,c.user_id,c.cart_status,
			cd.store_id,cd.outlet_id,

			pi.product_name,pi.description,
			p.is_multi_price,
			p.id as product_id,
			p.category_id,
			p.sub_category_id,
			p.outlet_id,
			p.vendor_id as store_id,
			p.product_image,
			p.stock_status,

			

			p.original_price as original_price,
			p.discount_price as discount_price,
			p.weight_class_id as weight_class_id,
			p.weight as weight,
			p.quantity as product_qty,
				
				
 

			vi.vendor_name,
			out.minimum_order_amount,
			out.delivery_charges_fixed,
			out.url_index,
			out.delivery_charges_variation,
			out.service_tax,cd.quantity,
			out.url_index,
			out.latitude,out.longitude,
			cd.cart_detail_id,
			vn.vendor_key, weight.unit, weight.title, vi.vendor_name, outin.outlet_name,

			cd.created_at

									FROM cart c
									LEFT JOIN cart_detail cd ON c.cart_id = cd.cart_id
									 JOIN products p ON p.id = cd.product_id
									LEFT JOIN products_infos PI ON pi.id = p.id
									
									LEFT JOIN outlets OUT ON out.id = cd.outlet_id
									LEFT JOIN outlet_infos OUTIN ON out.id = outin.id
									LEFT JOIN vendors vn ON out.vendor_id = vn.id
									LEFT JOIN vendors_infos VI ON vi.id = vn.id
									Left join weight_classes ON weight_classes.id = p.weight_class_id
									Left join weight_classes_infos weight ON weight.id =weight_classes.id
									where coalesce(cd.product_variant_id,0) = 0 AND '.$query.' AND '.$vquery.' AND '.$wquery.' AND '.$oquery.' AND c.user_id = ? ORDER BY cd.created_at' , array($user_id));




		// return $cart_items;


		/****** for variant *******/



		$variant_cart_items = DB::select('SELECT 


			c.cart_id,c.user_id,c.cart_status,
			cd.store_id,cd.outlet_id,cd.product_variant_id as variant_id,

			pi.product_name,pi.description,
			p.is_multi_price,
			p.id as product_id,
			p.category_id,
			p.sub_category_id,
			p.outlet_id,
			p.vendor_id as store_id,
			p.product_image,
			p.stock_status,

			

			pv.original_price as original_price,
			pv.discount_price as discount_price,
			pv.weight_class_id as weight_class_id,
			pv.weight_value as weight,
			pv.quantity as product_qty,
				
				
 

			vi.vendor_name,
			out.minimum_order_amount,
			out.delivery_charges_fixed,
			out.url_index,
			out.delivery_charges_variation,
			out.service_tax,cd.quantity,
			out.url_index,
			out.latitude,out.longitude,
			cd.cart_detail_id,
			vn.vendor_key, weight.unit, weight.title, vi.vendor_name, outin.outlet_name,

			cd.created_at

									FROM cart c
									LEFT JOIN cart_detail cd ON c.cart_id = cd.cart_id
									 JOIN products p ON p.id = cd.product_id
									LEFT JOIN products_infos PI ON pi.id = p.id
									JOIN product_variants pv ON cd.product_variant_id = pv.variant_id
									LEFT JOIN outlets OUT ON out.id = cd.outlet_id
									LEFT JOIN outlet_infos OUTIN ON out.id = outin.id
									LEFT JOIN vendors vn ON out.vendor_id = vn.id
									LEFT JOIN vendors_infos VI ON vi.id = vn.id
									Left join weight_classes ON weight_classes.id = pv.weight_class_id
									Left join weight_classes_infos weight ON weight.id =weight_classes.id
									where coalesce(cd.product_variant_id,0) != 0 AND '.$query.' AND '.$vquery.' AND '.$wquery.' AND '.$oquery.' AND c.user_id = ? ORDER BY cd.created_at' , array($user_id));

		// restore mode the original
		DB::setFetchMode($fetchMode);

		$output = [];

		if( count($cart_items) > 0 ){
			foreach ($cart_items as $key => $value) {
				$output[] =  $value;
			}
		}

		if( count($variant_cart_items) > 0 ){
			foreach ($variant_cart_items as $key => $value) {
				$value['product_name'] = $value['product_name'] . ' (' . $value['weight'] . ' ' . $value['unit'] . ')' ;
				$output[] =  $value;
			}
		}

		array_multisort(array_column($output, 'created_at'), $output);


		if( count($output) > 0 ){
			foreach ($output as $key => $value) {
				$output[$key] =  (object)$value;
			}
		}

		return $output;
	}
}

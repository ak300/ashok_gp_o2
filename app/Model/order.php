<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class order extends Model
{
	public $timestamps  = false;
	protected $table = 'orders';
    
	public static function outlet_details_by_order($order_id, $language = '')
	{
		if(empty($language))
			$language = getCurrentLang();
		$query = '"outlet_infos"."language_id" = (case when (select count(outlet_infos.language_id) as totalcount from outlet_infos where outlet_infos.language_id = '.$language.' and outlets.id = outlet_infos.id) > 0 THEN '.$language.' ELSE 1 END)';
		$outlet_data = DB::table('orders')
						->select('outlets.id as outlet_id','outlet_infos.outlet_name','orders.order_key_formated')
						->join('orders_info','orders.id','=','orders_info.order_id')
					
						->join('outlets','outlets.id','=','orders_info.outlet_id')
						->join('outlet_infos','outlet_infos.id','=','outlets.id')
						->whereRaw($query)
						->where('orders.id',$order_id)
						->first();
		//print_r($user_data);exit;
		return $outlet_data;
	}

	public static function get_driver_current_location($order_id)
	{
		$driver_current_location = DB::table('orders')
									->select('driver_orders.driver_id','driver_track_location.latitude as driver_latitude','driver_track_location.longitude as driver_longitude','drivers.first_name as driver_first_name','drivers.last_name as driver_last_name','drivers.mobile_number as driver_mobile_number','user_address.latitude as user_latitude','user_address.longtitude as user_longtitude','drivers.profile_image as driver_profile_image','orders.total_amount','orders.delivery_instructions','orders.delivery_date','delivery_time_interval.start_time','delivery_time_interval.end_time','outlets.delivery_time as outlet_delivery_minites','orders.id as order_id')
									->join('driver_orders','driver_orders.order_id','=','orders.id')
									->join('drivers','drivers.id','=','driver_orders.driver_id')
									->join('driver_track_location','driver_track_location.driver_id','=','driver_orders.driver_id')
									->join('delivery_time_slots','delivery_time_slots.id','=','orders.delivery_slot')
									->join('delivery_time_interval','delivery_time_interval.id','=','delivery_time_slots.time_interval_id')
									->join('outlets','outlets.id','=','orders.outlet_id')
									->leftjoin('user_address','user_address.id','=','orders.delivery_address')
									->where('driver_orders.order_id',$order_id)
									->orderby('driver_track_location.id','desc')->first();
		return $driver_current_location;
	}
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class vendors_view extends Model
{
	public $timestamps  = false;
	public $table  = 'vendors_view';
    //public $primarykey = 'id';
}

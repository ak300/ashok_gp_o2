<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use DB;


class sms_templates extends Model 
{    
	
   public $timestamps  = false;
   protected $table = 'sms_templates';
   
}

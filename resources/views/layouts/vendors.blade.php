<!DOCTYPE html>
<html lang="en">
    @include('includes.vendors.head')
	<body>
	    <header>
	        @include('includes.vendors.header')
	    </header>
	    <section>
	        <div class="mainwrapper">
	        	<?php 
	        		$vendor_id = Session::get('vendor_id');
	        		$currentplan = getCurrentPlanStatus($vendor_id);

	        		//	dd($currentplan);
	        		if(count($currentplan)>0){
	        		$expired_date = $currentplan[0]->subscription_end_date;
	        		$today = date('Y-m-d');
	        		$expiry_first_day = date('Y-m-d', strtotime($today. ' + 1 day'));
	        		$expiry_end_day  = date('Y-m-d', strtotime($today. ' + 2 days'));
	        		if(($today == $expired_date) || $expiry_first_day == $expired_date ||
	        			$expiry_end_day == $expired_date) {
	        		if($today == $expired_date)
	        			$msg = "Your trial will expire by today";
	        		else if($expiry_first_day == $expired_date)
	        			$msg = "Your trial will expire in 2 days";
	        		else if($expiry_end_day == $expired_date)
	        			$msg = "Your trial will expire in 3 days";
	        	?>
	        		<div class="alert alert-danger trial_period" style="margin-left:250px;display: block;">{{ $msg }}.To avoid service interruption, <a href="{{ URL::to('vendors/subscription_packages') }}">please click here</a>
	        	</div>
	        	<?php }
	        		}?>
				<aside>
				@include('includes.vendors.sidebar')
				</aside>
				<?php 
				if(!Request::is('vendors/subscription_packages')){
				//	$vendor_plan_status = 1;
				$vendor_plan_status = $currentplan[0]->plan_status;
				if($vendor_plan_status == 0 ){	
				?>
					<div class="modal fade in" id="subscription_update" data-backdrop="static" data-keyboard="false" style="display: block; padding-right: 15px;">
					  <div class="modal-dialog modal-lg">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h4 class="modal-title">
					        	Subscription Notice
					        </h4>        
					      </div>
					      <div class="modal-body">
					      	Dear customer, 
						    <p class="message">Your account in Otel2go has been expired.Please subscribe any plan to continue the service.</p>
					      </div>
					      <div class="modal-footer">
					        <a class="btn btn-success" href="{{ URL::to('vendors/subscription_packages') }}">Subscribe Plan</a>
					      </div>   
					    </div> 
					  </div><!-- /.modal-dialog --> 
					</div><!-- /.modal -->
				<?php }} ?>
				<div class="mainpanel">
					<main>
						@yield('content')
					</main>
				</div><!-- mainpanel -->
	        </div><!-- mainwrapper -->
	    </section>
	</body>
</html>
<script>
	$(document).ready(function(){
		$('#subscription_update').modal();
	});
</script>
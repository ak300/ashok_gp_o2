<!DOCTYPE html>
<html lang="en">
 @include('includes.front.otel2go.head')
  <body class="ht-body ht-tm-default-theme">
    <header>
      @include('includes.front.otel2go.header')
      @include('includes.front.otel2go.listing_header')
    </header>
      @yield('content')
    <footer>
      @include('includes.front.otel2go.footer')
    </footer>
  </body>
</html>

@extends('layouts.managers')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/highcharts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/funnel.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/exporting.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/data.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/drilldown.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/fullcalendar.print.min.css') }}" rel='stylesheet' media='print' />

<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/moment.min.js') }}"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.min.css" rel='stylesheet' />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.0.0/fullcalendar.js"></script>


{{-- <link href="{{ URL::asset('assets/admin/base/css/fullcalendar.css') }}" rel='stylesheet' media='print' />
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/fullcalendar.min.js') }}"></script> --}}

<script src="{{ URL::asset('assets/admin/base/js/tooltip.js') }}"></script>
<script src="{{ URL::asset('assets/admin/base/js/popover.js') }}"></script>
<style>
container:'body';
</style>

@if (Session::has('message'))
	<div class="admin_sucess_common">
		<div class="admin_sucess">
			<div class="alert alert-info">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>{{ Session::get('message') }}
			</div>
		</div>
	</div>
@endif

<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
			<i class="fa fa-home"></i>
		</div>
		<div class="media-body">
			<ul class="breadcrumb">
				<li><a href="{{ URL::to('managers/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Managers')</a></li>
				<li>@lang('messages.Booking')</li>
			</ul>
			<h4>@lang('messages.Booking')</h4>
		</div>
	</div><!-- media -->
</div><!-- pageheader -->

<div class="contentpanel">

<div class="col-md-3">
	<a href="{{ URL::to('managers/booking') }}" class="btn btn-primary">@lang('messages.Create New Booking')</a>
</div>
<br><br>



<div class="col-md-12">

<div class="calender_view">

{{ csrf_field() }}

<div id="calendar"></div>
    <script>
    $(document).ready(function(){
	
		$(".fc-right").hide();
		$('#calendar').fullCalendar({


			header: { left: 'prev,next',center: 'title',right: 'agendaDay,month,agendaWeek'},  editable:true,allDay: false,


   			events:<?php echo $vendor_booking; ?>	,


  				eventClick: function (event, jsEvent, view) {

					window.location.href = '<?php echo URL::to("managers/showbooking");?>'+'/'+event.id;
				},
/*
			eventRender: function(event, element, view)
			{
				element.popover({
				            title: 'Name - ' + event.title +', Booked Room - '+ event.notes,
				            placement:'top',
				            html:true,
				            container:'body',
				            //	trigger: 'manual',
				            content:moment(event.start).format('MMM Do') 	+ ' to ' + moment(event.end).format('MMM Do')	,

                        });

			},

			*/

        eventMouseover: function (data, event, view) {

            tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#ffffff;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;">' + 'Booking ID: '+ ': ' + data.random_id + '<br/>' + 'Customer Name: ' + ': ' + data.title + '</br>' + 'Booked Room: ' + ': ' + data.notes + '</br>' + 'Date: ' + ': ' + moment(data.start).format('MMM Do') + ' to ' + moment(data.end_date).format('MMM Do') + '</div>';


            $("body").append(tooltip);
            $(this).mouseover(function (e) {
                $(this).css('z-index', 10000);
                $('.tooltiptopicevent').fadeIn('500');
                $('.tooltiptopicevent').fadeTo('10', 1.9);
            }).mousemove(function (e) {
                $('.tooltiptopicevent').css('top', e.pageY + 10);
                $('.tooltiptopicevent').css('left', e.pageX + 20);
            });

        },		
        eventMouseout: function (data, event, view) {
            $(this).css('z-index', 8);

            $('.tooltiptopicevent').remove();

        },        	
		
    		//	weekends: false;	SAT & SUN REMOVED

			dayClick: function(date, jsEvent, view) {
			   
			    var minDate = moment();

				var check = (moment(date).format("YYYY-MM-DD HH:MM:SS"));
				var today_date = (moment(minDate).format("YYYY-MM-DD HH:MM:SS"));			    
			}
  		});



		
    });
   
</script>

<div id="dialog" title="My dialog" style="display:none">

<form>
<fieldset>
	<label for="Id">Id</label>
	<input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all">
	<label for="Id">Title</label>
	<input type="text" name="title" id="title" class="text ui-widget-content ui-corner-all">
</fieldset>
</form>
</div>

<div id="calendarModal" class="modal fade">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
            <h4 id="modalTitle" class="modal-title"></h4>
        </div>
        <div id="modalBody" class="modal-body">
			 Start: <span id="startTime"></span><br>
    End: <span id="endTime"></span><br><br>
    <p id="eventInfo"></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>



		</div>

</div>
</div><!-- row -->
@endsection

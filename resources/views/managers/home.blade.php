@extends('layouts.managers')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/highcharts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/funnel.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/exporting.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/data.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/drilldown.js') }}"></script>
@if (Session::has('message'))
    <div class="admin_sucess_common">
        <div class="admin_sucess">
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>{{ Session::get('message') }}
            </div>
        </div>
    </div>
@endif

<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-home"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href="{{ URL::to('managers/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Restaurant Managers')</a></li>
                <li>@lang('messages.Dashboard')</li>
            </ul>
            <h4>@lang('messages.Dashboard')</h4>
        </div>
    </div><!-- media -->
</div><!-- pageheader -->

<div class="contentpanel">


<?php /*
   <a href="<?php echo URL::to("vendor/amenities"); */?><?php /*">
       <div class="col-md-3">
           <div class="panel panel-dark panel_orange noborder">
               <div class="panel-heading noborder">
                   <div class="panel-btns">
                   </div><!-- panel-btns -->
                   <div class="panel-icon"><i class="fa fa-credit-card"></i></div>
                   <div class="media-body">
                       <h5 class="md-title nomargin">@lang('messages.Amenities')</h5>
                       <h1 class="mt5"><?php echo count($amenities); */ ?><?php /*</h1>
                   </div><!-- media-body -->
               </div><!-- panel-body -->
           </div><!-- panel -->
       </div><!-- col-md-4 -->
   </a>
*/ ?>   

   <a href="<?php echo URL::to("managers/accomodation");?>">
       <div class="col-md-3" style="display: none;">
           <div class="panel panel-dark panel_orange noborder">
               <div class="panel-heading noborder">
                   <div class="panel-btns">
                   </div><!-- panel-btns -->
                   <div class="panel-icon"><i class="fa fa-home"></i></div>
                   <div class="media-body">
                       <h5 class="md-title nomargin">@lang('messages.Accomodation')</h5>
                       <h1 class="mt5"><?php echo count($accomodation_type); ?></h1>
                   </div><!-- media-body -->
               </div><!-- panel-body -->
           </div><!-- panel -->
       </div><!-- col-md-4 -->
   </a>

   <a href="<?php echo URL::to("managers/room_type");?>">
       <div class="col-md-3">
           <div class="panel panel-dark panel_orange noborder">
               <div class="panel-heading noborder">
                   <div class="panel-btns">
                   </div><!-- panel-btns -->
                   <div class="panel-icon"><i class="fa fa-home"></i></div>
                   <div class="media-body">
                       <h5 class="md-title nomargin">@lang('messages.Room Type')</h5>
                       <h1 class="mt5"><?php echo count($room_type); ?></h1>
                   </div><!-- media-body -->
               </div><!-- panel-body -->
           </div><!-- panel -->
       </div><!-- col-md-4 -->
   </a>

   <a href="<?php echo URL::to("managers/rooms");?>">
       <div class="col-md-3">
           <div class="panel panel-dark panel_orange noborder">
               <div class="panel-heading noborder">
                   <div class="panel-btns">
                   </div><!-- panel-btns -->
                   <div class="panel-icon"><i class="fa fa-home"></i></div>
                   <div class="media-body">
                       <h5 class="md-title nomargin">@lang('messages.Rooms')</h5>
                       <h1 class="mt5"><?php echo count($rooms); ?></h1>
                   </div><!-- media-body -->
               </div><!-- panel-body -->
           </div><!-- panel -->
       </div><!-- col-md-4 -->
   </a>

    <a href="<?php echo URL::to("managers/house_keeping");?>">
        <div class="col-md-3">
            <div class="panel panel-dark panel_orange noborder">
                <div class="panel-heading noborder">
                    <div class="panel-btns">
                        <!-- panel-btns -->
                    </div>
                    <div class="panel-icon"><i class="fa fa-users"></i></div>
                    <div class="media-body">
                        <h5 class="md-title nomargin">@lang('messages.Housekeeping Staffs')</h5>
                        <h1 class="mt5"><?php echo count($housekeeping_staffs); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </a>

    <a href="<?php echo URL::to("managers/housekeeping_tasks");?>">
        <div class="col-md-3">
            <div class="panel panel-dark panel_orange noborder">
                <div class="panel-heading noborder">
                    <div class="panel-btns">
                        <!-- panel-btns -->
                    </div>
                    <div class="panel-icon"><i class="fa fa-tasks"></i></div>
                    <div class="media-body">
                        <h5 class="md-title nomargin">@lang('messages.Housekeeping Tasks')</h5>
                        <h1 class="mt5"><?php echo count($housekeeping_tasks); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </a>

    <div class="admin_dasbord_home">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>@lang('messages.Detail')</th>
                    <th>@lang('messages.Today')</th>
                    <th>@lang('messages.This week')</th>
                    <th>@lang('messages.This month')</th>
                    <th>@lang('messages.This Year')</th>
                    <th>@lang('messages.Total')</th>
                </tr>
            </thead>
            <tbody>

                    <tr style="display: none;">
                        <td>@lang('messages.Accomodation')</td>
                        <td><h3><?php echo $accomodation_type_query[0]->day_count; ?></h3></td>
                        <td><h3><?php echo $accomodation_type_query[0]->week_count; ?></h3></td>
                        <td><h3><?php echo $accomodation_type_query[0]->month_count; ?></h3></td>
                        <td><h3><?php echo $accomodation_type_query[0]->year_count; ?></h3></td>
                        <td><h3><?php echo $accomodation_type_query[0]->total_count; ?></h3></td>
                    </tr>
                    <tr>
                        <td>@lang('messages.Room Type')</td>
                        <td><h3><?php echo $room_type_query[0]->day_count; ?></h3></td>
                        <td><h3><?php echo $room_type_query[0]->week_count; ?></h3></td>
                        <td><h3><?php echo $room_type_query[0]->month_count; ?></h3></td>
                        <td><h3><?php echo $room_type_query[0]->year_count; ?></h3></td>
                        <td><h3><?php echo $room_type_query[0]->total_count; ?></h3></td>
                    </tr>
                    <tr>
                        <td>@lang('messages.Rooms')</td>
                        <td><h3><?php echo $rooms_query[0]->day_count; ?></h3></td>
                        <td><h3><?php echo $rooms_query[0]->week_count; ?></h3></td>
                        <td><h3><?php echo $rooms_query[0]->month_count; ?></h3></td>
                        <td><h3><?php echo $rooms_query[0]->year_count; ?></h3></td>
                        <td><h3><?php echo $rooms_query[0]->total_count; ?></h3></td>
                    </tr>

                    <tr>
                        <td>@lang('messages.Housekeeping Staffs')</td>
                        <td><h3><?php echo  $housekeeping_staffs_query[0]->day_count; ?></h3></td>
                        <td><h3><?php echo $housekeeping_staffs_query[0]->week_count; ?></h3></td>
                        <td><h3><?php echo $housekeeping_staffs_query[0]->month_count; ?></h3></td>
                        <td><h3><?php echo $housekeeping_staffs_query[0]->year_count; ?></h3></td>
                        <td><h3><?php echo $housekeeping_staffs_query[0]->total_count; ?></h3></td>
                    </tr>
                    <tr>
                        <td>@lang('messages.Housekeeping Tasks')</td>
                        <td><h3><?php echo  $housekeeping_tasks_query[0]->day_count; ?></h3></td>
                        <td><h3><?php echo $housekeeping_tasks_query[0]->week_count; ?></h3></td>
                        <td><h3><?php echo $housekeeping_tasks_query[0]->month_count; ?></h3></td>
                        <td><h3><?php echo $housekeeping_tasks_query[0]->year_count; ?></h3></td>
                        <td><h3><?php echo $housekeeping_tasks_query[0]->total_count; ?></h3></td>
                    </tr>
                
             </tbody> 
        </table>
    </div>
    

<div class="col-md-12">
  {{ csrf_field() }}
  <div id="container2" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
  <div id="container" style="height: 400px; min-width: 600px"></div>
  <div style="display: none;" id="container3" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>

    <script src="{{ URL::asset('assets/admin/base/js/highstock.js') }}"></script>
    <script src="{{ URL::asset('assets/admin/base/js/exporting.js') }}"></script>
</div><!-- row -->
<script>
  $(function() {
    <?php  if(count(json_decode($vendor_booking_encoded,1))> 0){ ?>
    $.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=aapl-c.json&callback=?', function(data) {
      var datas = <?php echo $vendor_booking_encoded; ?>;
      var vendors_orderscount_count_encoded = <?php echo $vendors_orderscount_count_encoded; ?>;
      // Create the chart
      window.chart = new Highcharts.StockChart({
        chart: {
          renderTo: 'container'
        },

        rangeSelector: {
            allButtonsEnabled:true,
            inputEnabled:false,
            buttons: [
            {
                type: 'month',
                count: 3,
                text: 'Day',
                dataGrouping: {
                    forced: true,
                    units: [['day', [1]]]
                }
            }, {
                type: 'year',
                count: 1,
                text: 'Week',
                dataGrouping: {
                    forced: true,
                    units: [['week', [1]]]
                }
            }, {
                type: 'all',
                text: 'Month',
                dataGrouping: {
                    forced: true,
                    units: [['month', [1]]]
                }
            }, 
            {
          type: 'year',
          text: 'Year',
          dataGrouping: {
                    forced: true,
                    units: [['year', [1]]]
                  }
      }, 
      {
          type: 'all',
          text: 'All',
          dataGrouping: {
                    forced: true,
                    units: [['year', [1]]]
                  }
      }
      ],
          selected: 1,
          inputDateFormat: '%Y-%m-%d'
        },

        title: {
          text: 'Booking Summary'
        },

        series: [{
          name: 'Amount',
          data: datas,
          tooltip: {
            valueDecimals: 2
          }},
          {
          name: 'Booking',
          data: vendors_orderscount_count_encoded,
          tooltip: {
            valueDecimals: 0
          }}],


      }, function(chart) {

        // apply the date pickers
        setTimeout(function() {
          $('input.highcharts-range-selector', $('#' + chart.options.chart.renderTo)).datepicker()
        }, 0)
      });
    });
    <?php }else{ ?>
          $('#container').hide();
    <?php } ?>


    // Set the datepicker's date format
    $.datepicker.setDefaults({
      dateFormat: 'yy-mm-dd',
      onSelect: function(dateText) {
        this.onchange();
        this.onblur();
      }
    });

  });

<?php if($months_data_check > 0){ ?>

  Highcharts.chart('container2', {
    chart: {
      type: 'column',
    }, 
     rangeSelector: {
        inputEnabled:false,
                enabled: true,
            
                selected: 1,
        inputDateFormat: '%Y-%m-%d'
            },
    title: {
      text: 'Monthly Bookings'
    },
    xAxis: {
      categories: <?php echo $months_encoded;?>,
      crosshair: true
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Booking statuses'
      }
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    colors: <?php echo $colors; ?>,
    series: [{
      name: 'Total Booking',
      data: <?php echo $total_sales;?>

    }, {
      name: 'Walk In',
      data: <?php echo $total_walkin;?>

    },{
      name: 'Checked In',
      data: <?php echo $total_confirmed;?>

    },{
      name: 'Checked Out',
      data: <?php echo $total_complete;?>

    }, 
    /*{
      name: 'Missed',
      data: <?php /* echo $total_missed; */ ?>

    }, {
      name: 'Initiated',
       data: <?php /* echo $total_initiated; */ ?>

    }, 

    */
    {
      name: 'Cancelled',
      data: <?php echo $total_canceled;?>

    },{
      name: 'Pending',
      data: <?php echo $total_pending;?>

    },{
      name: 'Denied',
      data: <?php echo $total_denied;?>

    }]
  });
<?php }else{ ?>


      $('#container2').hide();

  <?php  }?>

<?php  if(count(json_decode($staff_count_encoded,1))> 0){ ?>

Highcharts.chart('container3', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Staff Count'
    },
    xAxis: {
    categories: <?php echo $months_encoded;?>,
        crosshair: true
    },
    yAxis: {
        min: 0,
        max: 20,
        title: {
            text: 'Staff Count'
        }
    },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr>' +
      '<td style="padding:0"><b>{point.y} Staff</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
    plotOptions: {
        column: {
            //  pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Months',
        data: <?php  echo $staff_count_encoded; ?>

    }]
});

<?php } else  { ?>

   $('#container3').hide();

<?php } ?>
</script>
    
</div><!-- row -->
@endsection

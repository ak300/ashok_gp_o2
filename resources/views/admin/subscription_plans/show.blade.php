@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/switch/js/bootstrap-switch.min.js') }}"></script> 
<link href="{{ URL::asset('assets/admin/base/plugins/switch/css/bootstrap3/bootstrap-switch.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<div class="row">	
	<div class="col-md-12 ">
		<!-- Nav tabs -->
		<div class="pageheader">
			<div class="media">
				<div class="pageicon pull-left">
					<i class="fa fa-home"></i>
				</div>
				<div class="media-body">
					<ul class="breadcrumb">
						<li><a href="{{ URL::to('admin/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Admin')</a></li>
						<li>@lang('messages.Subscription Packages')</li>
					</ul>
					<h4>@lang('messages.View Subscription Packages')</h4>
				</div>
			</div><!-- media -->
		</div><!-- pageheader -->

		<div class="contentpanel">
			@if (count($errors) > 0) 
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif <?php /* */ ?>
			<div class="buttons_block pull-right">
				<div class="btn-group mr10">
					<a class="btn btn-primary tip" href="{{ URL::to('admin/subscription_packages/edit/' . $data[0]->id . '') }}" title="Edit" >@lang('messages.Edit')</a>
				</div>
			</div>
			<ul class="nav nav-tabs"></ul>
			<div class="tab-content mb30">
				<div class="tab-pane active" id="home3">
					<!-- Task Name -->
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Plan Name') :</label>
						<div class="col-sm-3">
							{{ $data[0]->subscription_plan_name }}
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Description') :</label>
						<div class="col-sm-3">
							{{ $data[0]->description }}
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Price') :</label>
						<div class="col-sm-6">
							{{ $data[0]->subscription_plan_price }}
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Discount Price') :</label>
						<div class="col-sm-6">
							{{ $data[0]->subscription_discount_price }}
						</div>
					</div>
					<div class="form-group">
						<label for="index" class="col-sm-3 control-label"> @lang('messages.Duration') :</label>
						<div class="col-sm-6">
							{{ $data[0]->subscription_plan_duration }} @lang('messages.Days')
						</div>
					</div>
					<div class="form-group">
						<label for="index" class="col-sm-3 control-label"> @lang('messages.Total Properties') :</label>
						<div class="col-sm-6">
							{{ $data[0]->total_properties }}
						</div>
					</div>
					<div class="form-group">
						<label for="index" class="col-sm-3 control-label"> @lang('messages.Rooms Per Property') :</label>
						<div class="col-sm-6">
							{{ $data[0]->total_rooms }} 
						</div>
					</div>
					<div class="form-group">
						<label for="index" class="col-sm-3 control-label"> @lang('messages.User Limit') :</label>
						<div class="col-sm-6">
							{{ $data[0]->total_users }} 
						</div>
					</div>
					<div class="form-group">
						<label for="index" class="col-sm-3 control-label"> @lang('messages.Package Modules') :</label>
						<div class="col-sm-6">
							@foreach($packages as $key => $val)
								<p>
									@foreach($allpackages as $keys => $values)
										@if($values->id == $val['package_id'])
											{{ $values->package_name }}
										@endif
									@endforeach
								</p>	
							@endforeach
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Created Date') :</label>
						<div class="col-sm-6">
							{{ $data[0]->created_at }}
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Updated Date') :</label>
						<div class="col-sm-6">
							{{ $data[0]->updated_at }}
						</div>
					</div>
		            <div class="form-group">
                                <label  class="col-sm-3 control-label">@lang('messages.Status')</label>
                                <div class="col-sm-6">
                                    <?php $checked = ""; 
                                          if($data[0]->subscription_plan_status == 1){
                                                $checked ="checked=checked";
                                          }
                                    ?>
                                    <input type="checkbox" class="toggle" name="status" data-size="small" <?php echo $checked;?> readonly data-on-text="@lang('messages.Yes')" data-off-text="@lang('messages.No')" data-off-color="danger" data-on-color="success" style="visibility:hidden;" value="1" />
                                </div>
                        </div>
					 <div class="panel-footer">
					<button type="button"  onclick="window.location='{{ url('admin/subscription_packages') }}'" class="btn btn-primary mr5">@lang('messages.Cancel')</button>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection



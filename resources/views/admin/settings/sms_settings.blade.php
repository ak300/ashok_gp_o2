@extends('layouts.admin')
@section('content')
<!-- Nav tabs -->
<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-home"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href="{{ URL::to('admin/dashboard') }}"><i class="glyphicon glyphicon-home"></i>{{ trans('messages.Admin') }}</a></li>
                <li>@lang('messages.Settings')</li>
            </ul>
            <h4>@lang('messages.SMS')</h4>
        </div>
    </div><!-- media -->
</div><!-- pageheader -->
<div class="contentpanel">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li><?php echo trans('messages.'.$error); ?> </li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('message'))
        <div class="admin_sucess_common">
            <div class="admin_sucess">
                <div class="alert alert-info success"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('message') }}</div>
            </div>
        </div>
    @endif
    <?php $settings_id=0; if(isset($settings->id)&& $settings->id!=''){ $settings_id=$settings->id; } ?>
    <div class="col-md-12">
        <div class="row panel panel-default">
            <div class="grid simple">
                <div id="general" class="panel-heading">
                    <h4 class="panel-title">@lang('messages.SMS')</h4>
                    <p>@lang('messages.SMS settings')</p>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <ul class="nav nav-tabs"></ul>
                {!!Form::open(array('url' => ['admin/settings/updatesms', $settings_id], 'method' => 'post','class'=>'tab-form attribute_form','id'=>'local_edit_form'));!!}
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="form-label">@lang('messages. SMS Account ID/API Key') <span class="asterisk">*</span></label>
                            <div class="controls">
                                <input class="form-control" maxlength="60" type="text" value="{{$settings->sms_account_id}}" required name="sms_account_id">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">@lang('messages. SMS Account Token/Secret Key') <span class="asterisk">*</span></label>
                            <div class="controls">
                                <input class="form-control" type="text" maxlength="60" value="{{ $settings->sms_account_token }}" required name="sms_account_token">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">@lang('messages. SMS Sender Number') <span class="asterisk">*</span></label>
                            <div class="controls">
                                <input class="form-control" type="tel"  maxlength="60" value="{{ $settings->sms_sender_number }}" required name="sms_sender_number">
                            </div>
                       </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary mr5" title="@lang('Update')">@lang('messages.Update')</button>
                        <button type="reset" title="@lang('Cancel')" class="btn btn-default" onclick="window.location='{{ url('admin/dashboard') }}'">@lang('messages.Cancel')</button>
                    </div>
                {!!Form::close();!!} 
            </div>
        </div>
    </div>
</div>
@endsection


@extends('layouts.vendors')
@section('content')
<script src="{{url('assets/front/'.Session::get('general')->theme.'/plugins/rateit/src/jquery.rateit.js')}}"></script>
<link href="{{url('assets/front/'.Session::get('general')->theme.'/plugins/rateit/src/rateit.css')}}" rel="stylesheet">

<!-- Nav tabs -->
<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-home"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
                <li><a href="{{ URL::to('vendors/customer_feedback') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Customer Feedback')</a></li>
            </ul>
            <h4>@lang('messages.View Customer Feedback')</h4>
        </div>
    </div><!-- media -->
</div><!-- pageheader -->

<div class="contentpanel">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li><?php echo trans('messages.'.$error); ?> </li>
                @endforeach
            </ul>
        </div>
    @endif
    <ul class="nav nav-tabs"></ul>
    <div class="tab-content mb30">
        <div class="tab-pane active" id="home3">
            <div class="btn_edit_group_new">
                <div class="btn_edit_group">
                    <div class="buttons_block pull-right">
                        <div class="btn-group mr5">
                            <a class="btn btn-primary tip" href="{{ URL::to('vendors/customer_feedback') }}" title="@lang('messages.Back')" >@lang('messages.Back')</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-group_form_new">
                    <label for="index" class="col-sm-2 control-label"> @lang('messages.Comments')</label>
                    <div class="col-sm-8"><?php echo ucfirst($review->comments); ?></div>
                </div>
            </div>
<?php /*            
            <div class="form-group">
                <div class="form-group_form_new">
                    <label for="index" class="col-sm-2 control-label"> @lang('messages.Rating')</label>
                    <div class="col-sm-10"><div class="rateit" data-rateit-value="<?php echo $review->ratings; */ ?><?php /*" data-rateit-ispreset="true" data-rateit-readonly="true"></div> 
                    </div>
                </div>
            </div>
*/ ?>            
            <div class="form-group">
                <div class="form-group_form_new">
                    <label for="index" class="col-sm-2 control-label"> @lang('messages.Posted by')</label>
                    <div class="col-sm-10"><?php echo ucfirst($review->user_name); ?></div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-group_form_new">
                    <label for="index" class="col-sm-2 control-label"> @lang('messages.Posted to')</label>
                    <div class="col-sm-10"><?php echo ucfirst($review->outlet_name); ?></div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-group_form_new">
                    <label for="index" class="col-sm-2 control-label"> @lang('messages.Posted date')</label>
                    <div class="col-sm-10"><?php echo date('d - M - Y h:i A' , strtotime($review->review_posted_date)); ?></div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-group_form_new">
                    <label for="index" class="col-sm-2 control-label"> @lang('messages.Status') :</label>
                    <div class="col-sm-10">
                        <?php if($review->active_status==0):
                            $data = '<span  class="label label-danger">'.trans("messages.Inactive").'</span>';
                        elseif($review->active_status==1):
                            $data = '<span  class="label label-success">'.trans("messages.Active").'</span>';
                        endif;echo $data; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.vendors')
@section('content')
 <link href="{{ URL::asset('assets/admin/base/css/toastr.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
			<i class="fa fa-home"></i>
		</div>
		<div class="media-body">
			<ul class="breadcrumb">
				<li><a href="{{url('vendors/dashboard')}}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendor')</a></li>
				<li>@lang('messages.Change Password')</li>
			</ul>
			<h4>@lang('messages.Change Password')</h4>
		</div>
	</div><!-- media -->
</div><!-- pageheader -->

@if (Session::has('message'))
    <div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>{{ Session::get('message') }}
	</div>
@endif
<div class="contentpanel">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('messages.Change Password')</div>
                <div class="panel-body">
                        {!!Form::open(array('method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'otp_number' ,'onsubmit'=>'return reset_password()'));!!}
                        <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('messages.Old Password')</label>
                            <div class="col-md-8">
                                <input type="password" class="form-control" name="old_password" required />
                                @if ($errors->has('old_password'))
                                    <span class="help-block">
                                        <strong><?php echo trans('messages.'.$errors->first('old_password')); ?></strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('messages.New Password')</label>
                            <div class="col-md-8">
                                <input type="password" id="password" class="form-control" name="password" required />
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong><?php echo trans('messages.'.$errors->first('password')); ?></strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('messages.Confirm Password')</label>
                            <div class="col-md-8">
                                <input type="password" class="form-control" name="password_confirmation" required />
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong><?php echo trans('messages.'.$errors->first('password_confirmation'));?></strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" id="reset-password" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-btn fa-envelope"></i> @lang('messages.Reset Password')</button>
                        </div>
                        
					{!!Form::close();!!} 

                  <!-- Modal 
                  <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                
                      <div class="modal-content">
                        <div class="otp-modal modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title" align="center">Please Enter the OTP Sent to your mobile</h4>
                        </div>
                        {!!Form::open(array('method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'verify_otp' ,'onsubmit'=>'return verify_otp()'));!!}
                        <div class="modal-body">
                            <div class="wrapper" >
                                <input type="hidden" name="verify_otp_id" id="verify_otp_id" value="2">
                                <input type="text" name="otp_number" id="otp" value="" placeholder="Enter the OTP" required class="form-control">
                                <button type="submit" id="submit_otp" class="btn btn-primary">  @lang('messages.Verify OTP')
                                    </button>
                            </div>
                            
                        </div>
                    {!!Form::close();!!} 
                      </div>
                    </div>
                  </div>
                   Modal ends-->

                  <!-- verify modal -->
                   <div class="modal fade verifycode" id="verifycode" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="singup_info">
                                <div class="sign_up verifycode">
                                   <hr> 
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 15px;">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                                  <div class="signup_inner_containt">
                                    <div class="signup_image">
                                      <h4 align="center">SIGNUP VERIFICATION CODE</h4>
                                      <i class="signup_line"></i>
                                    </div>
                                    <hr>
                                    {!!Form::open(array('method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'verify_otp' ,'onsubmit'=>'return verify_otp()'));!!}
                                    <div >
                                      <p class="" style="margin-left: 60px;">We sent OTP verification code to your mobile number.
                                        Please enter the code.
                                      </p>
                                    </div>
                                      <div class="col-md-12 input-group verify_input">
                                        <input type="hidden" name="verify_otp_id" id="verify_otp_id" value="0">

                                        <div class="form-group">
                                             <label class="col-md-3 col-md-offset-1 control-label">Enter the OTP Code:</label>
                                             <div class="col-md-6">
                                                <input type="text" name="otp_number" class="form-control" required placeholder="OTP" aria-label="Recipient's username" aria-describedby="basic-addon2" >
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                          <button class="btn btn-primary btn-outline-secondary confirm" type="submit" id="submit_otp" style="margin-left:60px">Confirm</button>
                                            <a class="resend btn btn-warning" onclick ="resend_otp();" href="#">Resend OTP</a>
                                        </div>
                                      </div>                               
                                    
                                     
                                    {!!Form::close();!!}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                  <!-- verify modal ends-->
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/toastr.min.js') }}"></script>
 <script type="text/javascript">
 function reset_password(){
    $this = $("#reset-password");
    $this.html('Loading....');
    var value = $("#otp_number").serializeArray();
    var o_url = '/vendors/password_data';
    token = $('input[name=_token]').val();

    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        headers: {'X-CSRF-TOKEN': token},
        url: o_url,
        data: value,
        datatype: 'JSON',
        success: function(resp){
            data = resp;
            if(data.httpCode == 200)
            {  
                $this.html('Reset Password');
                toastr.success(data.errors);
                $('#verify_otp_id').val(data.user_id);
                $('#verifycode').modal('show');

            } else if(data.httpCode == 400){
             $this.html('Reset Password');   
             $.each(resp.errors,function(key,val){
                    toastr.error(val);
                });
            }
                
        }
    });
    return false;
}

 function verify_otp(){
    $this = $("#submit_otp");
    $this.html('Verifying....');
    var value = $("#verify_otp").serializeArray();
    var pwd = $('#password').val(); // This value is from 1st form
    value.push({ name: "password", value: pwd }); // Pushing the 1st form value 'password' in 2nd form ---->
    var o_url = '/vendors/verify_otp_password';
    token = $('input[name=_token]').val();

    // AJAX Code To Submit Form.
    $.ajax({
        type: "POST",
        headers: {'X-CSRF-TOKEN': token},
        url: o_url,
        data: value,
        datatype: 'JSON',
        success: function(resp){
            data = resp;
            if(data.httpCode == 200)
            {  
                $this.html('Verify OTP');
                toastr.success(data.errors);
                $('#verify_otp').trigger('reset');
                location.reload();
            } else if(data.httpCode == 400){
             $this.html('Verify OTP');   
             $.each(resp.errors,function(key,val){
                    toastr.error(val);
                });
            }   
        }
    });
    return false;
}
function resend_otp(){
    var data = $('#verify_otp').serializeArray();
    var o_url = '/resend_otp_changepassword';

    $.ajax({
        type: "POST",
        headers: {'X-CSRF-TOKEN': token},
        url: o_url,
        data: data,
        datatype: 'JSON',
        success: function(resp){
            data = resp;
            if(data.httpCode == 200)
            {  
                toastr.success(data.errors);
            }
            else
            {
                toastr.error(data.errors);
            }
        }
    });
    return false;
}
</script>
@endsection


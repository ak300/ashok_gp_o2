@extends('layouts.vendors')
@section('content')
 <script type="text/javascript" src="{{ URL::asset('assets/js/media/picturecut/jquery.picture.cut.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery-ui-1.10.3.min.js') }}"></script>
 <script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/switch/js/bootstrap-switch.min.js') }}"></script> 
 <link href="{{ URL::asset('assets/admin/base/plugins/switch/css/bootstrap3/bootstrap-switch.min.css') }}" media="all" rel="stylesheet" type="text/css" /> 
  <link href="{{ URL::asset('assets/admin/base/css/jquery-ui-1.10.3.css') }}" media="all" rel="stylesheet" type="text/css" /> 
  <script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/select2.min.js') }}"></script>
 <link href="{{ URL::asset('assets/admin/base/css/select2.css') }}" media="all" rel="stylesheet" type="text/css" />
<!-- Nav tabs -->
<div class="pageheader">
<div class="media">
	<div class="pageicon pull-left">
		<i class="fa fa-home"></i>
	</div>
	<div class="media-body">
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('admin/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
			<li>@lang('messages.Fund Requests')</li>
		</ul>
		<h4>@lang('messages.Fund Requests')</h4>
	</div>
</div><!-- media -->
</div><!-- pageheader -->

<div class="contentpanel">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
			<ul>
				@foreach ($errors->all() as $error)
					<li><?php echo trans('messages.'.$error); ?> </li>
				@endforeach
			</ul>
		</div>
		@endif
		<ul class="nav nav-tabs"></ul>
       {!!Form::open(array('url' => 'create_fund_requests', 'method' => 'post','class'=>'tab-form attribute_form','id'=>'fund_request','files' => true));!!} 

	<div class="tab-content mb30">
		<div class="tab-pane active" id="home3">
			 <div class="form-group">
				<label class="col-sm-2 control-label">@lang('messages.Enter the amount')</label>
				<div class="col-sm-6">
				  <input type="text"  name="request_amount" class="form-control" 
				  value="">
				</div>
			</div>			
       </div>
			<div class="panel-footer">
				<button class="btn btn-primary mr5" title="Save">@lang('messages.Save')</button>
				<button type="reset" class="btn btn-default" onclick="window.location='{{ url('vendor/events') }}'"  title="Cancel">@lang('messages.Cancel')</button>
			</div>
        </div>
		{!!Form::close();!!}
		</div>
@endsection

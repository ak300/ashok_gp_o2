<?php if( Request::is('/') || Request::is('vendor-signup')) {  ?>
      <div class="header_ourt header_bussiness">
<?php  } else { ?>
      <div class="header_ourt header_options listing_header" style="display:none;">
<?php  }  ?>

<link rel="shortcut icon" href="<?php echo url('/assets/front/'.Session::get("general")->theme.'/images/favicon/'.Session::get("general")->favicon.''); ?>"> 

    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
      
        <div class="container">
          <div class="row">
            <div class="col col-sm col-lg col-xl">
              <div class="brand_logo">

                <!--  LANDING PAGE -->
                <?php if( Request::is('/') || Request::is('vendor-signup')) {  ?>

                <a href="{{  url('/') }}" title="Otel2go">
                     <img src="{{ URL::asset('assets/front/otel2go/images/business_logo.png') }}" title="{{ ucfirst(getAppConfig()->site_name) }}" alt="logo"/>
                </a>

                <?php  } else { ?>

                <a href="{{  url('/book-now') }}" title="Otel2go">
                     <img src="<?php echo url('/assets/front/'.Session::get("general")->theme.'/images/logo/159_81/'.Session::get("general")->logo.'?'.time()); ?>"  title="{{ ucfirst(getAppConfig()->site_name) }}" alt="logo">
                </a>

                <?php  }  ?>
              </div>
            </div>
            <div class="col col-sm col-lg col-xl">
              <div class="navbar_right">
                <nav class="navbar navbar-expand-sm bg-light">
                  <!-- Links -->
                  <a href="#" id="click_menus">
                  </a>
            <ul class="navbar-nav">

              <!--  LANDING PAGE -->
            <?php if( Request::is('/') || Request::is('vendor-signup')) {  ?>
              <li>
                <a href="{{ url('vendors/login') }}" title="@lang('messages.Login')" target="_blank">@lang('messages.Login')</a>
              </li>
              <li>
                <a href="{{ url('vendor-signup') }}"  title="@lang('messages.Signup')">@lang('messages.Signup')</a>
              </li>

              <li>
                <a href="{{ url('book-now') }}" class="book_now_bussines" title="@lang('messages.Book Now')">@lang('messages.Book Now')</a>
              </li>                              

            <?php  } else { ?>

              <?php if(Session::has('user_id')) { ?>
              <li><a title="{{Session::get('user_name')}}" id="open_drop_hed" href="{{ URL::to('/profile') }}" title="@lang('messages.Login')"><i class="glyph-icon flaticon-social"></i><?php echo ucfirst(str_limit(Session::get('user_name'), 10)); ?></a>
                <div class="after_login_drop">
                      <ul>
                          <li class="{{ Request::is('orders') ? 'active' : '' }}"><a href="{{url('/orders')}}" title="@lang('messages.My bookings')">@lang('messages.My bookings')</a></li>
                          <li class="{{ Request::is('wallet') ? 'active' : '' }}"><a href="{{url('/wallet')}}" title="@lang('messages.My Wallet')">@lang('messages.My Wallet')</a></li>
                          <li class="{{ Request::is('refer_friends*') ? 'active' : '' }}"><a href="{{url('refer_friends')}}" title="@lang('messages.Refer Friends')">@lang('messages.Refer Friends')</a></li> 
                          <li class="{{ Request::is('logout*') ? 'active' : '' }}"><a href="{{url('logout')}}" title="@lang('messages.Logout')">@lang('messages.Logout')</a></li>
                      </ul>
                </div>    
                </li>
                <?php } ?>              
              <li class="nav-item">
                <a class="nav-link" href="{{ URL::to('/cms/help') }}" title="@lang('messages.Help')">Help</a>
              </li>
              <?php 
              if(!Session::has('user_id')) {?>
               <li class="nav-item">
                <a href="javascript:;" class="nav-link" title="@lang('messages.Login')" data-toggle="modal" data-target=".login">@lang('messages.Login')</a>
              </li>
              <li class="nav-item">
                <a href="javascript:;" class="nav-link" title="@lang('messages.Signup')" data-toggle="modal" data-target=".signup">@lang('messages.Signup')</a>
              </li>
              <?php } } ?>
            </ul>
                  <div class="responsive_menu" id="hiddle_menus" style="display: none;">
                    <ul class="nav">
                      <li>
                        <a href="#">Help
                        </a>
                      </li>
                      <li>
                        <a href="#">Login
                        </a>
                      </li>
                      <li>
                        <a href="#">Sign up
                        </a>
                      </li>
                    </ul>
                  </div>
                </nav>
              </div>
            </div>
          </div>

  
<!-- RESPONSIVE --> 
  
  <div id="navigation" class="navigation">
          <nav id="nav">
          <ul class="navbar-nav">
              <?php if(Session::has('user_id')) { ?>
              <li><a title="{{Session::get('user_name')}} " id="open_drop_hed" href="{{ URL::to('/profile') }}" title="@lang('messages.Login')"><i class="glyph-icon flaticon-social"></i><?php echo ucfirst(str_limit(Session::get('user_name'), 10)); ?></a>
               
                      <ul>
                          <li class="{{ Request::is('bookings*') ? 'active' : '' }}"><a href="{{url('/orders')}}" title="@lang('messages.My bookings')">@lang('messages.My bookings')</a></li>    
                          <li class="{{ Request::is('logout*') ? 'active' : '' }}"><a href="{{url('logout')}}" title="@lang('messages.Logout')">@lang('messages.Logout')</a></li>
                      </ul>
                 
                </li>
                <?php } ?>
              <li>
                <a class="nav-link" href="{{ URL::to('/cms/help') }}" title="@lang('messages.Help')">Help</a>
              </li>
              <?php 
              if(!Session::has('user_id')) {?>
               <li>
                <a href="javascript:;" class="nav-link" title="@lang('messages.Login')" data-toggle="modal" data-target=".login">@lang('messages.Login')</a>
              </li>
              <li>
                <a href="javascript:;" class="nav-link" title="@lang('messages.Signup')" data-toggle="modal" data-target=".signup">@lang('messages.Signup')</a>
              </li>
              <?php } ?>
            </ul>
          </nav>
  </div>

<!-- RESPONSIVE -->

        </div>
      </div>
	  
	  
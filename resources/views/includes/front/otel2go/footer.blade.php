@include('front.'.Session::get("general")->theme.'.login')
@include('front.'.Session::get("general")->theme.'.user_details')
@include('front.'.Session::get("general")->theme.'.verify_otp')
@include('front.'.Session::get("general")->theme.'.forgot_password')
@include('front.'.Session::get("general")->theme.'.signup')
<script src="{{ URL::asset('assets/front/otel2go/js/select2.min.js') }}">  </script>
<footer>
 <div class="container">
      <div class="row">
        <div class="col-sm">

          <?php if( Request::is('/') || Request::is('vendor-signup')) {  ?>

            <a href="{{  url('/') }}">
            <img src="<?php echo url('/assets/front/'.Session::get("general")->theme.'/images/logo/159_81/'.Session::get("general")->logo.'?'.time()); ?>"  title="{{ ucfirst(getAppConfig()->site_name) }}" alt="logo">
            </a>

          <?php } else { ?>

            <a href="{{  url('/book-now') }}">
            <img src="<?php echo url('/assets/front/'.Session::get("general")->theme.'/images/logo/159_81/'.Session::get("general")->logo.'?'.time()); ?>"  title="{{ ucfirst(getAppConfig()->site_name) }}" alt="logo">
            </a>

          <?php } ?>


          <ul class="list-inline">
             <?php   $settings = getSettingsDetails(); 
              // echo "<pre>"; print_r($settings); exit;
             ?>
             <br/>
                  <li>
                    <h6>For Hoteliers and Travelers</h6>
                    <h6 style="display: none;">@lang('messages.Made in Malaysia For Hoteliers and Travelers')</h6>
                  </li>
                  <h2>Contact us</h2>
                  <li> 
                    <?php if(isset($settings->telephone) && ($settings->telephone !="")) { ?>    
                         <label><i class="glyph-icon flaticon-call-answer"></i>&nbsp;&nbsp;<?php echo $settings->telephone; ?></label>
                    <?php } ?> <?php /*&nbsp;+038062-3632 */ ?>
                  </li>
                  <li> 
                    <?php if(isset($settings->email) && ($settings->email !="")) { ?>
                          <a href="mailto:<?php echo $settings->email; ?>" title="<?php echo $settings->email; ?>"><i class="glyph-icon flaticon-close-envelope"></i>&nbsp;&nbsp;<?php echo $settings->email; ?></a>
                    <?php } ?>  
                  </li>
                  <li> 
                    <?php if(isset($settings->telephone) && ($settings->telephone !="")) { ?>
                          <lable><i class="glyph-icon flaticon-whatsapp">&nbsp;&nbsp;<?php echo $settings->telephone; ?></lable></i>
                    <?php } ?>
                    <?php /* <i class="glyph-icon flaticon-whatsapp">
                    </i>&nbsp; 038062-3632 */ ?>
              </li>
          </ul>
        </div>
        <?php $site_settings = getSettingsLists(); ?>
        <div class="col-sm">
          <h2> {{ $site_settings->site_name }}
          </h2>
          <ul class="list-inline">
            <li>
              <a href="{{ URL::to('/cms/about-us') }}" title="@lang('messages.About Us')">@lang('messages.About Us')
              </a>
            </li>
            <li>
              <a href="{{ URL::to('/contact-us') }}" title="@lang('messages.Contact Us')">@lang('messages.Contact Us')
              </a>
            </li>
            <li style="display: none;">
              <a href="{{ URL::to('/cms/careers') }}" title="@lang('messages.Careers')">@lang('messages.Careers')
              </a>
            </li>
             <li>
              <a href="{{ URL::to('/blog') }}" title="@lang('messages.Blog')">@lang('messages.Blog')
              </a>
            </li>
            <li>
              <a href="{{ URL::to('/cms/press') }}" title="@lang('messages.Press')">@lang('messages.Press')
              </a>
            </li>
            <li>
              <a href="{{ URL::to('/cms/privacy-policy  ') }}" title="@lang('messages.Privacy policy')">@lang('messages.Privacy policy')
              </a>
            </li>
            <li style="display: none;">
              <a href="{{ URL::to('/cms/help') }}" title="@lang('messages.Help')">@lang('messages.Help')
              </a>
            </li>
            <li>
              <a href="{{ URL::to('/cms/terms-and-conditions') }}" title="@lang('messages.Terms and Conditions')">
                @lang('messages.Terms and Conditions')
              </a>
            </li>             
          </ul>
        </div>
        <div class="col-sm">
          <h2>Company Partners
          </h2>
          <ul class="list-inline">
                <?php   $top_cities = getTopCities(6);  ?>
                  @if($top_cities)
                    @foreach($top_cities as $c_key => $c_val)
                      <li>
                          <a href="{{ URL::to('list-hotels/city/'.$c_val->url_index) }}">
                            Hotel in {{  $c_val->city_name }}
                          </a>
                      </li>
                    @endforeach
                  @endif          
          </ul>          
          
        </div>
         <div class="col-sm">
         <div class="social_share">
             <h2>Follow us</h2>
             <a class="facebook" href="<?php echo Session::get("social")->facebook_page; ?>" target="_blank"><i class="glyph-icon flaticon-facebook-logo-button"></i></a>
             <a class="twitter" href="<?php echo Session::get("social")->twitter_page; ?>" target="_blank"><i class="glyph-icon flaticon-twitter-logo-button"></i> </a>
             <a class="insta" href="<?php echo Session::get("social")->instagram_page; ?>" target="_blank"><i class="glyph-icon flaticon-instagram-logo"></i> </a>
        </div>
        </div>
    </div>
    </div>
    <div class="copy_rights">
      <p> {{ $site_settings->copyrights }}
      </p>
    </div>
	</footer>
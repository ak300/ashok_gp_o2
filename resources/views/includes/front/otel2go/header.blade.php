<div class="header_ourt front_header" style="display:none;">
  <div class="container">
    <div class="row">
      <div class="col col-sm-6 col-lg col-xl">
        <div class="brand_logo">
          <a href="{{  url('/book-now') }}">
          <img src="{{ URL::asset('assets/front/otel2go/images/otel2go.png') }}" alt="" title="{{ ucfirst(getAppConfig()->site_name) }}">
          </a>
        </div>
      </div>
      <div class="col col-sm-6 col-lg col-xl">
        <div class="navbar_right">
          <nav class="navbar navbar-expand-sm bg-light">
            <!-- Links -->
            <ul class="navbar-nav disply_hidden">
              <?php if(Session::has('user_id')) { ?>
              <li><a title="{{Session::get('user_name')}} " id="open_drop_hed" href="{{ URL::to('/profile') }}" title="@lang('messages.Login')"><i class="glyph-icon flaticon-social"></i><?php echo ucfirst(str_limit(Session::get('user_name'), 10)); ?></a>
                <div class="after_login_drop">
                      <ul>
                          <li class="{{ Request::is('bookings*') ? 'active' : '' }}"><a href="{{url('/orders')}}" title="@lang('messages.My bookings')">@lang('messages.My bookings')</a></li>    
                          <li class="{{ Request::is('logout*') ? 'active' : '' }}"><a href="{{url('logout')}}" title="@lang('messages.Logout')">@lang('messages.Logout')</a></li>
                      </ul>
                </div>    
                </li>
                <?php } ?>
              <li>
                <a  href="{{ URL::to('/cms/help') }}" title="@lang('messages.Help')">Help</a>
              </li>
              <?php 
              if(!Session::has('user_id')) {?>
               <li>
                <a href="javascript:;"  title="@lang('messages.Login')" data-toggle="modal" data-target=".login">@lang('messages.Login')</a>
              </li>
              <li>
                <a href="javascript:;"  title="@lang('messages.Signup')" data-toggle="modal" data-target=".signup">@lang('messages.Signup')</a>
              </li>
              <?php } ?>
            </ul>
          </nav>
		  <div id="button-box" class="button-box">
          <button type="button" id="toggle-nav" class="toggle-nav">MENU<span>&#9776;</span></button>
        </div>
        </div>
      </div>
    </div>
	
<!-- RESPONSIVE -->	
	
	<div id="navigation" class="navigation">
          <nav id="nav">
          <ul class="navbar-nav">
              <?php if(Session::has('user_id')) { ?>
              <li><a title="{{Session::get('user_name')}} " id="open_drop_hed" href="{{ URL::to('/profile') }}" title="@lang('messages.Login')"><i class="glyph-icon flaticon-social"></i><?php echo ucfirst(str_limit(Session::get('user_name'), 10)); ?></a>
               
                      <ul>
                          <li class="{{ Request::is('bookings*') ? 'active' : '' }}"><a href="{{url('/orders')}}" title="@lang('messages.My bookings')">@lang('messages.My bookings')</a></li>    
                          <li class="{{ Request::is('logout*') ? 'active' : '' }}"><a href="{{url('logout')}}" title="@lang('messages.Logout')">@lang('messages.Logout')</a></li>
                      </ul>
                 
                </li>
                <?php } ?>
              <li>
                <a class="nav-link" href="{{ URL::to('/cms/help') }}" title="@lang('messages.Help')">Help</a>
              </li>
              <?php 
              if(!Session::has('user_id')) {?>
               <li>
                <a href="javascript:;" class="nav-link" title="@lang('messages.Login')" data-toggle="modal" data-target=".login">@lang('messages.Login')</a>
              </li>
              <li>
                <a href="javascript:;" class="nav-link" title="@lang('messages.Signup')" data-toggle="modal" data-target=".signup">@lang('messages.Signup')</a>
              </li>
              <?php } ?>
            </ul>
          </nav>
  </div>
        
	
	
  </div>
</div>


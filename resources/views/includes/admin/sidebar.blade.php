<div class="leftpanel">
	<div class="media profile-left">
		<a class="pull-left profile-thumb">
			<?php if(file_exists(public_path('/assets/admin/base/images/admin/profile/'.Auth::user()->image)) && Auth::user()->image != '') { ?>
				<img src="<?php echo url('/assets/admin/base/images/admin/profile/'.Auth::user()->image.'?'.time()); ?>" class="img-circle">
			<?php } else{  ?>
				<img src=" {{ URL::asset('assets/admin/base/images/a2x.jpg') }} " class="img-circle">
			<?php } ?>
		</a>
		<div class="media-body">
			<h4 class="media-heading"><?php echo Auth::user()->name; ?></h4>
			<small class="text-muted"> <a href="{{ url('admin/editprofile/'.Auth::id()) }}" title="Edit Profile">@lang('messages.Edit Profile')</a> </small>
		</div>
	</div><!-- media -->
	<h5 class="leftpanel-title"></h5>
	<ul class="nav nav-pills nav-stacked">

		<li class="{{ Request::is('admin/dashboard*') ? 'active' : '' }}" ><a href="{{ URL::to('admin/dashboard') }}"><i class="fa  fa-home"></i> <span>@lang('messages.Dashboard')</span></a></li>

        <?php if(has_permission('vendors/vendors') || has_permission('vendors/outlets') || has_permission('vendors/outlet_managers')){ ?>
			<li class="parent {{ Request::is('vendors/*') ? 'active' : ''}}"><a  href="javascript:;"><i class="fa fa-rocket"></i> <span>@lang('messages.Vendors')</span></a>
				<ul class="children">
					<?php if(has_permission('vendors/vendors')){ ?> 
						<li class="{{ (Request::is('vendors/vendors*') || Request::is('vendors/create_vendor') || Request::is('vendors/edit_vendor/*') || Request::is('vendors/vendor_details/*')) ? 'active' : '' }}" ><a  href="{{ URL::to('vendors/vendors') }}">@lang('messages.Vendors')</a></li>
					<?php } ?>			


					<?php if(has_permission('vendors/outlets')){ 	?>
						<li class="{{ (Request::is('vendors/outlets*') || Request::is('vendors/create_outlet') || Request::is('vendors/edit_outlet/*') || Request::is('vendors/outlet_details/*'))? 'active' : '' }}" ><a  href="{{ URL::to('vendors/outlets') }}">@lang('messages.Oulets')</a></li>
					<?php 	} 	?>
					<?php if(has_permission('vendors/outlet_managers')){ 	?>
						<li class="{{ (Request::is('vendors/outlet_managers*') || Request::is('vendors/create_outlet_managers') || Request::is('vendors/edit_outlet_manager/*')) ? 'active' : '' }}" ><a  href="{{ URL::to('vendors/outlet_managers') }}">@lang('messages.Oulet Managers')</a></li>
					<?php 	} 	?>
				
				</ul>
			</li>
		<?php } ?>	

		<?php if(has_permission('admin/orders/index')){ ?>
			<li class="{{ Request::is('admin/orders/index*') ? 'active' : '' || Request::is('admin/showbooking/*') ? 'active' : ''}}"><a href="{{ URL::to('admin/orders/index') }}"><i class="fa fa-database"></i> <span>@lang('messages.Booking Management')</span></a></li>
		<?php }  ?>	

        <?php if(has_permission('reports/order') || has_permission('reports/returns') || has_permission('reports/user') || has_permission('reports/vendors')){ ?>
            <li class="parent {{ (Request::is('reports/*')) ? 'active' : ''}}"><a  href="javascript:;"><i class="fa fa-bar-chart-o"></i> <span>@lang('messages.Reports & Analytics')</span></a>
                <ul class="children">
					<?php if(has_permission('admin/reports')){ ?>
			            <li style="display: none;" class="{{ Request::is('admin/reports*') ? 'active' : '' }}"><a href="{{ URL::to('admin/reports') }}"><span>@lang('messages.Reports')</span></a></li>
					<?php } ?>                	
                    <?php if(has_permission('reports/order')){ ?>
                        <li class="{{ Request::is('reports/order') ? 'active' : '' }}" ><a  href="{{ URL::to('reports/order') }}">@lang('messages.Booking')</a></li>
                    <?php } ?>
                    <?php if(has_permission('reports/transactions')){ ?>
                        <li class="{{ Request::is('reports/transactions') ? 'active' : '' }}" ><a  href="{{ URL::to('reports/transactions') }}">@lang('messages.Transactions')</a></li>
                    <?php } ?>
                    <?php if(has_permission('reports/statistics')){ ?>
                        <li class="{{ Request::is('reports/statistics') ? 'active' : '' }}" ><a  href="{{ URL::to('reports/statistics') }}">@lang('messages.Statistics')</a></li>
                    <?php } ?>                                     
                    <?php if(has_permission('reports/user')){ ?>
                        <li class="{{ Request::is('reports/user') ? 'active' : '' }}" ><a  href="{{ URL::to('reports/user') }}">@lang('messages.Customers')</a></li>
                    <?php } ?>
                    <?php if(has_permission('reports/vendor')){ ?>
                        <li class="{{ Request::is('reports/vendor') ? 'active' : '' }}" ><a  href="{{ URL::to('reports/vendor') }}">@lang('messages.Vendors')</a></li>
                    <?php } ?>      
                    <?php if(has_permission('reports/outlets')){ ?>
                        <li style="display: none;" class="{{ Request::is('reports/outlets') ? 'active' : '' }}" ><a  href="{{ URL::to('reports/outlets') }}">@lang('messages.Property')</a></li>
                    <?php } ?>
                    <?php if(has_permission('reports/coupons')){ ?>
                        <li style="display: none;" class="{{ Request::is('reports/coupons') ? 'active' : '' }}" ><a  href="{{ URL::to('reports/coupons') }}">@lang('messages.Coupons')</a></li>
                    <?php } ?>
                    <?php if(has_permission('reports/products')){ ?>
                        <li style="display: none;" class="{{ Request::is('reports/products') ? 'active' : '' }}" ><a  href="{{ URL::to('reports/products') }}">@lang('messages.Products')</a></li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>			

		<?php if(has_permission('admin/subscription')){ ?>
			<li class="parent {{ Request::is('admin/subscription*') ? 'active' : '' }}"><a  href="javascript:;"><i class="fa fa-bell"></i> <span>@lang('messages.Subscriptions')</span></a>
				<ul class="children">
					<?php if(has_permission('admin/subscription_packages')){ ?>
					<li class="{{ Request::is('admin/subscription_packages*') ? 'active' : '' }}" ><a  href="{{ URL::to('admin/subscription_packages') }}">@lang('messages.Subscription Packages')</a></li>
					<?php } ?>
					<?php if(has_permission('admin/subscription_transactions')){ ?>
					<li class="{{ Request::is('admin/subscription_transactions*') ? 'active' : '' }}" ><a  href="{{ URL::to('admin/subscription_transactions') }}">@lang('messages.Subscription Transactions')</a></li>
					<?php } ?>
				</ul>
			</li>
		<?php } ?>

		<?php if(has_permission('system/permission')||has_permission('permission/users')){ ?>
			<li class="parent {{ Request::is('system/permission*') ? 'active' : '' || Request::is('permission/users*') ? 'active' : '' || Request::is('permission*') ? 'active' : '' }}"><a  href="javascript:;"><i class="fa fa-desktop"></i> <span>@lang('messages.Permission')</span></a>
				<ul class="children">
					<?php if(has_permission('system/permission')){ ?>
					<li class="{{ Request::is('system/permission*') ? 'active' : '' }}" ><a  href="{{ URL::to('system/permission') }}">@lang('messages.Roles')</a></li>
					<?php } ?>
					<?php if(has_permission('permission/users')){ ?>
					<li class="{{ Request::is('permission/users*') ? 'active' : '' || Request::is('permission*') ? 'active' : '' }}" ><a  href="{{ URL::to('permission/users') }}">@lang('messages.Users')</a></li>
					<?php } ?>
				</ul>
			</li>
		<?php } ?>

		<?php if(has_permission('admin/users/index') || has_permission('admin/users/groups')) { ?>
			<li  class="parent {{ Request::is('admin/users*') ? 'active' : ''|| Request::is('admin/user*') ? 'active' : '' || Request::is('admin/groups*') ? 'active' : '' || Request::is('admin/addresstype*') ? 'active' : '' || Request::is('admin/customers*') ? 'active' : ''}}" ><a  href="javascript:;"><i class="fa fa-users"></i> <span>@lang('messages.Users')</span></a>
				<ul class="children">
					<?php if(has_permission('admin/users/groups')){ ?>
						<li class="{{ Request::is('admin/users/groups*') ? 'active' : '' || Request::is('admin/groups*') ? 'active' : '' }}" ><a  href="{{ URL::to('admin/users/groups') }}">@lang('messages.Groups')</a></li>
					<?php } ?>
					<?php /* if(has_permission('admin/users/addresstype')){ ?>
                        <li class="{{ Request::is('admin/users/addresstype*') ? 'active' : ''}}" ><a  href="{{ URL::to('admin/users/addresstype') }}">@lang('messages.Address Type')</a></li>
                    <?php } */?>
					<?php if(has_permission('admin/users/index')){ ?>
						<li class="{{ Request::is('admin/users/index*') ? 'active' : '' || Request::is('admin/users/create*') ? 'active' : '' || Request::is('admin/users/edit*') ? 'active' : '' }}" ><a  href="{{ URL::to('admin/users/index') }}">@lang('messages.Manage Users')</a></li>
					<?php } ?>
					<?php if(has_permission('admin/customers')){ ?>
						<li  class="{{ Request::is('admin/customers*') ? 'active' : '' }}"><a href="{{ URL::to('admin/customers') }}"><span>@lang('messages.Customers')</span></a></li>
					 <?php } ?>
				</ul>
			</li>
		<?php } ?>

		<?php if(has_permission('admin/settings/general')){ ?>
			<li class="parent {{ Request::is('admin/settings*') ? 'active' : '' || Request::is('admin/payment*') ? 'active' : '' }}">
				<a  href="javascript:;"><i class="fa fa-anchor"></i> <span>@lang('messages.Settings')</span></a>
				<ul class="children">
					<li class="{{ Request::is('admin/settings/general*') ? 'active' : '' }}" ><a  href="{{ URL::to('admin/settings/general') }}">@lang('messages.General')</a></li>
					<li class="{{ Request::is('admin/settings/local*') ? 'active' : '' }}"><a href="{{ URL::to('admin/settings/local') }}">@lang('messages.Local')</a></li>
					<li class="{{ Request::is('admin/settings/email*') ? 'active' : '' }}"><a href="{{ URL::to('admin/settings/email') }}">@lang('messages.Email')</a></li>
					<li class="{{ Request::is('admin/settings/socialmedia*') ? 'active' : '' }}"><a href="{{ URL::to('admin/settings/socialmedia') }}">@lang('messages.Social Media')</a></li>
					<li class="{{ Request::is('admin/settings/sms*') ? 'active' : '' }}"><a href="{{ URL::to('admin/settings/sms') }}">@lang('messages.SMS')</a></li>
					<li class="{{ Request::is('admin/settings/image*') ? 'active' : '' }}"><a href="{{ URL::to('admin/settings/image') }}">@lang('messages.Image')</a></li>
					<?php if(has_permission('admin/payment')){ ?>
					<li class="{{ Request::is('admin/payment*') ? 'active' : '' }}"><a href="{{ URL::to('admin/payment/settings') }}">@lang('messages.Payment')</a></li>	
					<?php } ?>		 	
				</ul>
			</li>
		<?php } ?>

		<?php if(has_permission('admin/localisation/country')||has_permission('admin/localisation/zones')||has_permission('admin/localisation/city')||has_permission('admin/localisation/language')||has_permission('admin/localisation/currency')){ ?>
			<li class="parent {{ Request::is('admin/localisation*') ? 'active' : '' || Request::is('admin/localisation/country*') ? 'active' : '' || Request::is('admin/localisation/language*') ? 'active' : '' || Request::is('admin/city*') ? 'active' : ''|| Request::is('admin/currency*') ? 'active' : '' || Request::is('admin/zones*') ? 'active' : '' || Request::is('admin/booking_status*') ? 'active' : ''}} ">
						<a  href="javascript:void();"><i class="fa fa-file-text-o"></i> <span>@lang('messages.Localisation')</span></a>
						<ul class="sub children">
							
<?php 				
/*
							<li class="{{ Request::is('admin/localisation/country*') ? 'active' : '' || Request::is('admin/country*') ? 'active' : ''  }}" ><a href="{{ URL::to('admin/localisation/country') }}">@lang('messages.Countries')</a></li>
*/							
?>							<?php  if(has_permission('admin/localisation/city'))  { ?>
							<li class="{{ Request::is('admin/localisation/city*') ? 'active' : '' || Request::is('admin/city*') ? 'active' : ''  }}"><a href="{{ URL::to('admin/localisation/city') }}">@lang('messages.Cities')</a></li>
							<?php } ?>
				<?php { ?>
							<?php  if(has_permission('admin/localisation/zones'))  { ?>
							<li class="{{ Request::is('admin/localisation/zones*') ? 'active' : '' || Request::is('admin/zones*') ? 'active' : ''  }}"><a href="{{ URL::to('admin/localisation/zones') }}">@lang('messages.Zones')</a></li>
							<?php } ?>
							<?php  if(has_permission('admin/localisation/language'))  { ?>
							<li class="{{ Request::is('admin/localisation/language*') ? 'active' : '' || Request::is('admin/language*') ? 'active' : ''  }}"><a  href="{{ URL::to('admin/localisation/language') }}">@lang('messages.Languages')</a></li>
							<?php } ?>
							<?php  if(has_permission('admin/booking_status'))  {  ?>
							<li class="{{ Request::is('admin/booking_status*') ? 'active' : '' }}"><a  href="{{ URL::to('admin/booking_status') }}">@lang('messages.Booking Status')</a></li>
							<?php   }  ?>
							<?php   if(has_permission('admin/localisation/currency'))  { ?>
							<li class="{{ Request::is('admin/localisation/currency*') ? 'active' : '' || Request::is('admin/currency*') ? 'active' : ''  }}" ><a href="{{ URL::to('admin/localisation/currency') }}">@lang('messages.Currencies')</a></li>
							<?php }  ?>
						</ul>
					</li>
				<?php } ?>	

		<?php } ?>

        <?php if(has_permission('admin/reviews')){ ?>
			<li  class="{{ Request::is('admin/reviews*') ? 'active' : '' }}" ><a href="{{ URL::to('admin/reviews') }}"><i class="glyphicon  glyphicon-star"></i> <span>@lang('messages.Reviews')</span></a></li>
		<?php } ?>
	
		<?php if(has_permission('admin/coupons')){ ?>
		<li  class="{{ Request::is('admin/coupons*') ? 'active' : '' }}" ><a href="{{ URL::to('admin/coupons') }}"><i class="fa fa-tags"></i> <span>@lang('messages.Coupons')</span></a></li>
		<?php } ?>

		<?php if(has_permission('admin/category')){ ?>
			<li class="{{ Request::is('admin/category*') ? 'active' : '' }}" ><a href="{{ URL::to('admin/category') }}"><i class="fa  fa-tasks"></i> <span>@lang('messages.Category')</span></a></li>
		<?php  } ?>		

		 <?php if(has_permission('admin/template/subjects') || has_permission('admin/templates/email')){ ?>
		 <li class="parent {{ Request::is('admin/templates*') ? 'active' : '' || Request::is('admin/sms_template*') ? 'active' : '' }}"><a  href="javascript:;"><i class="fa fa-envelope-o"></i> <span>@lang('messages.Notification Templates')</span></a>
			<ul class="children">
				 <?php if(has_permission('admin/templates/email')){ ?>
				<li class="{{ Request::is('admin/templates/email*') ? 'active' : '' || Request::is('admin/templates*') ? 'active' : '' }}" ><a  href="{{ URL::to('admin/templates/email') }}">@lang('messages.Email Templates')</a></li>
				<?php } ?>
				<?php if(has_permission('admin/sms_template')){ ?>
				<li class="{{ Request::is('admin/sms_template*') ? 'active' : '' || Request::is('admin/sms_template/create*') ? 'active' : '' }}" ><a  href="{{ URL::to('admin/sms_template') }}">@lang('messages.SMS Templates')</a></li>
				<?php  }  ?>
			</ul>
		</li>
		<?php } ?>

        <?php if(has_permission('admin/newsletter')){ ?>
			<li  class="{{ Request::is('admin/newsletter*') ? 'active' : '' }}" ><a href="{{ URL::to('admin/newsletter') }}"><i class="fa fa-reply-all"></i> <span>@lang('messages.Newsletter')</span></a></li>
        <?php } ?>
        
		<?php if(has_permission('admin/notifications')){ ?>
			<li class="parent {{ Request::is('admin/notifications*') ? 'active' : '' || Request::is('admin/email-notifications*') ? 'active' : '' || Request::is('admin/push-notifications*') ? 'active' : '' || Request::is('admin/SMS-notifications*') ? 'active' : '' }}">
			<a href="javascript:;"><i class="fa fa-desktop"></i><span>@lang('messages.Notifications')</span></a>

				<ul class="children">
					<?php if(has_permission('admin/email-notifications')){ ?>
						<li class="{{ Request::is('admin/notifications**') ? 'active' : '' }}" ><a href="{{ URL::to('admin/notifications') }}"><i class="fa fa-volume-down"></i> <span>@lang('messages.Notifications')</span></a></li>
					<?php } ?>
				</ul>
				<ul class="children">
					<?php if(has_permission('admin/email-notifications')){ ?>
						<li class="{{ Request::is('admin/email-notifications*') ? 'active' : '' }}" ><a href="{{ URL::to('admin/email-notifications') }}"><i class="fa fa-envelope-square"></i> <span>@lang('Email notifications')</span></a></li>
					<?php } ?>
				</ul>
				<ul class="children">
					<?php if(has_permission('admin/push-notifications')){ ?>
						<li class="{{ Request::is('admin/push-notifications*') ? 'active' : '' }}" ><a href="{{ URL::to('admin/push-notifications') }}"><i class="fa fa-bell"></i> <span>@lang('Push notifications')</span></a></li>
					<?php } ?>
				</ul>

				<ul class="children">
					<?php if(has_permission('admin/SMS-notifications')){ ?>
						<li  style="display: none;" class="{{ Request::is('admin/SMS-notifications*') ? 'active' : '' }}" ><a href="{{ URL::to('admin/SMS-notifications') }}"><i class="fa fa-bell"></i> <span>@lang('SMS notifications')</span></a></li>
					<?php } ?>
				</ul>
			</li>
		<?php } ?>	

		<?php if(has_permission('admin/subscribers')){ ?>
			<li  class="{{ Request::is('admin/subscribers*') ? 'active' : '' }}" ><a href="{{ URL::to('admin/subscribers') }}"><i class="fa fa-child"></i> <span>@lang('messages.Subscribers')</span></a></li>
        <?php } ?>			

		<?php if(has_permission('admin/customer_referrals')){ ?>
			<li class="{{ Request::is('admin/customer_referrals') ? 'active' : ''}}"><a href="{{ URL::to('admin/customer_referrals') }}"><i class="fa fa-users"></i> <span>@lang('messages.Customer Referrals')</span></a></li>
		<?php } ?>

		<?php if(has_permission('admin/fund_requests')){ ?>
			<li class="{{ Request::is('admin/fund_requests*') ? 'active' : '' }}" ><a href="{{ URL::to('admin/fund_requests') }}"><i class="fa fa-comment"> </i> <span>@lang('messages.Fund Requests')</span></a></li>
		<?php } ?>	

		<?php if(has_permission('admin/cms')){ ?>
            <li class="{{ Request::is('admin/cms*') ? 'active' : '' }}"><a href="{{ URL::to('admin/cms') }}"><i class="fa fa-file-text-o"></i> <span>@lang('messages.Cms')</span></a></li>
		<?php } ?>

		<?php if(has_permission('admin/blog')){ ?>
			<li class="{{ Request::is('admin/blog*') ? 'active' : '' }}"><a href="{{ URL::to('admin/blog') }}"><i class="fa fa-file-text-o"></i> <span>@lang('messages.Blog')</span></a></li>
		<?php }  ?>		

		<?php if(has_permission('admin/banners')){ ?>
				<li  class="{{ Request::is('admin/banners*') ? 'active' : '' || Request::is('admin/banner*') ? 'active' : '' }}" ><a href="{{ URL::to('admin/banners') }}"><i class="fa  fa-file-image-o"></i> <span>@lang('messages.Banners')</span></a></li>
		 <?php } ?>		

	</ul>
	<footer>
        @include('includes.admin.footer')
    </footer>
</div><!-- leftpanel -->

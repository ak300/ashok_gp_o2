@extends('layouts.front')
@section('content')
<?php //echo $referral_id;?>
<link href="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/css/intlTelInput.css');?>" media="all" rel="stylesheet" type="text/css" />

 <section class="store_item_list">
            <div class="container">
            <div class="cms_pages">
            <div class="cms_titl">
            	<h1>Sign Up</h1>
            </div>
          
            		{!!Form::open(array('method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'referral_signin' ,'onsubmit'=>'return referral_signin()'));!!}
		 <div class="tab-content mb30">
                    <div class="tab-pane active" id="home3">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Email') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="email" required placeholder="@lang('messages.Email')" class="form-control" value="{!! old('email') !!}" />
                            </div>
                        </div>

                        <div class="form-group">
                                <label class="col-sm-2 control-label">Mobile Number<span class="asterisk">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text" name="mobile" required id="mobile_number" value="{{ old('mobile')}}" placeholder="Mobile Number" class="form-control">
                                    <input id="phone_hidden" type="hidden" name="country_code" value="0">
                                </div>
                                    <input type="hidden" name="country_short" id="country_short" 
                                value="<?php echo 
                                    (Session::has('country_short'))?Session::get('country_short'):'my';
                                    ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Password') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input type="password" name="password" required maxlength="50" placeholder="@lang('messages.Password')" class="form-control" value="{!! old('password') !!}" />
                                </div>
                            </div>
                        </div>


                         <div class="form-group">
                                    <label class="checkmark_button">
                                        <input type="checkbox" required name="terms_condition" value="1" class="form-control">I agree the<span> 
                                        <a href="{{  URL::to('cms/terms-and-conditions') }}">Terms &amp; Conditions</a> </span> of Otel2go
                                        <span class="checkmark"></span>
                                    </label>
                          </div>

                        <div class="button_signup">
			                <div class="row">
			                  <div class="col-sm-6 col">
			                  	<input type="hidden" name="referral_code" value="{{ $referral_id }}">
			                  	<input type="hidden" name="user_id" value="{{ $user_id }}">
			                    <button type="submit" id="signup_btn" class="btn signup_btn signinbtn">Sign Up
			                    </button>
			                  </div>
			                </div>
			            </div>
                    </div>
				</div>                  
		{!!Form::close();!!}
            </div>
            </div>
                
        </section>
<script>
    $('.listing_header').show();

	function referral_signin(){   
		//To set country code in hidden input 
		var countryData = $("#mobile_number").intlTelInput("getSelectedCountryData");
		$("#phone_hidden").val('+'+countryData.dialCode);

		$this = $("#signup_btn");
		$this.html('Loading....');
		data = $("#referral_signin").serializeArray();
		data.push({ name: "login_type", value: "1" });
		var c_url = '/signup_user';
		token = $('input[name=_token]').val();
		$.ajax({
			url: c_url,
			headers: {'X-CSRF-TOKEN': token},
			data: data,
			type: 'POST',
			datatype: 'JSON',
			success: function (resp)
			{
				if(resp.httpCode == 200)
				{  
					toastr.success(resp.Message);
					$('#verify_otp_id').val(resp.last_id);
					$('#verifycode').modal('show');
					$('#referral_signin').trigger('reset');
					$this.html('Sign Up');
				}
				else
				{
					if(resp.httpCode == 400)
					{
						$.each(resp.Message,function(key,val){
							toastr.warning(val);
						});
					} else {
						toastr.warning(resp.Message);
					}
					$this.html('Sign Up');
				}
			},
			error:function(resp)
			{
				$this.button('reset');
				console.log('out--'+resp); 
				return false;
			}
		});
		return false;
	}

$( document ).ready(function() {
    $("#mobile_number").intlTelInput("setCountry",'my');
    setTimeout(function() {
            $('.alert').fadeOut('fast');
    }, 7500);
    
    $('.BDC_CaptchaImageDiv a').addClass('test');
    $('.test').css('visibility', 'hidden');
    $('#ContactCaptcha_SoundIcon').css('visibility', 'hidden');
             
});

$("#mobile_number").intlTelInput({
    initialCountry: "auto",
    separateDialCode: true,
    allowDropdown: true,
    onlyCountries: ['in','my'],
    utilsScript: "<?php echo URL::asset('/assets/front/'.Session::get("general")->theme.'/js/utils.js'); ?>" // just for formatting/placeholders etc
});
</script>        
@endsection
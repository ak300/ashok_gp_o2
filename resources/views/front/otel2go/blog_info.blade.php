      @extends('layouts.front')
      @section('content')
		<?php  
			if(file_exists(base_path().'/public/assets/admin/base/images/blog/914_649/'.$blog->image))
			{
				$image_url = url('/assets/admin/base/images/blog/914_649/'.$blog->image.'');
			} 
			else
			{ 
				$image_url =  URL::asset('assets/admin/base/images/blog_det_bg.jpg');
			} 
		?>
	<input type="hidden" class="getiamge" value="<?php echo $image_url; ?>">
    <section class="banner_sections_inner blog_det">
		<img src="{{$image_url}}" title="Blog image">
    </section>
    <section class="error_sections">
		<div class="container">
			<div class="blog_det_infor">
				<p>{{ ucfirst($blog->title) }}</p>
				<?php echo $blog->content; ?>
			</div>
		</div>
	</section>
<script>
      $('.listing_header').show();
</script>
<script>	
$( document ).ready(function() {
	var inputC = $('input.getiamge').val();
	$('.blog_detials_bg').css('background', 'url(' + inputC + ')no-repeat');
	//$('.banner_sections_inner').css('background', 'url(' + inputC + ')no-repeat');

});
</script>
@endsection

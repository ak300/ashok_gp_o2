@extends('layouts.front')
@section('content')

<div class="checkout">
      <section class="check_out_bg">
        <div class="container">
          <div class="booking_cancelled">
            <div class="sms_send">
              <span class="user_width_user">
                <img src="{{ URL::asset('assets/admin/base/images/vendors/logos/'.$vendors[0]->logo_image) }}" alt="Vendor img">
              </span>
              <h4>Otel2go Captain
                {{  $vendors[0]->name  }}
                 contact details will be shared with you via 
                <br/>
                SMS on the day of check in.
              </h4>
            </div>
            <hr>
            <div class="your_orders_sucess">
              <h2 class="booking_title">
                <i class="glyph-icon flaticon-tick-inside-circle">
                </i>Your Booking is Secured
              </h2>
              <p >Thank you for booking with us
              </p>
              <a href="#" class="btn btn-success" style="display: none;"> 
                <i class="glyph-icon flaticon-cloud-storage-download">
                </i>Invoice
              </a>
            </div>
            <div class="booking_cancelled_common">
              <div class="booking_info_top">
                <div class="row">
                  <div class="col col-sm col-lg booking_isd">
                    <label>Booking ID
                    </label>
                    <br/>
                    <b>{{ $booking_details->booking_random_id }}
                    </b>
                  </div>
                  <div class="col col-sm col-lg booking_isd">
                    <?php $dat = date("D, d M Y",strtotime($booking_details->bc_date)); 
                          $ti = date("H.I A",strtotime($booking_details->bc_date)); ?>
                    <p>Booked by {{  $vendors[0]->name  }}   on {{ $dat }} at {{ $ti }} IST
                    </p>
                  </div>
                </div>
              </div>
              <hr>
              <div class="hotel_booking_info">
                <div class="row">
                  <div class="col-md-2 col-sm-2 col-xs-12">
                    <div class="hotel_info_img">
                      <img class="hotel_image" src="{{ URL::asset('assets/admin/base/images/vendors/property_image/list/'.$properties->outlet_image) }}" alt="">
                    </div>
                  </div>
                  <div class="col-md-10 col-sm-10 col-xs-10 padding_left0">
                    <div class="hotels_info_img">
                      <h1>{{  $properties->outlet_name  }}
                      </h1>
                      <p>
                        <i class="glyph-icon flaticon-facebook-placeholder-for-locate-places-on-maps">
                        </i>
                        {{ isset($properties->contact_address)?$properties->contact_address:"" }}
                      </p>
                      <label>
                        <strong>Directions 
                        </strong>: {{ $properties->directions }}
                      </label>
                      <h4>
                        <strong>Landmark 
                        </strong>: {{ $properties->landmark }}
                      </h4>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <div class="date_info_list">
                <div class="row">
                  <div class="col-md-7 col-sm-7 col-lg-7">
                    <div class="row">
                      <div class="col">
                        <div class="date_list_midd">
                          <?php 
                              $ci1 = date("M d",strtotime($booking_details->check_in_date));
                              $ci2 = date("D",strtotime($booking_details->check_in_date));
                              $co1 = date("M d",strtotime($booking_details->check_out_date));
                              $co2 = date("D",strtotime($booking_details->check_out_date));
                          ?>
                          <h3>{{ $ci1 }}
                          </h3>
                          <p>{{ $ci2 }}, 11.00 PM
                          </p>
                        </div>
                      </div>
                      <div class="col">
                        <div class="date_list_night">
                          <i class="glyph-icon flaticon-moon-phase-outline">
                          </i>
                          <br/>
                          <label>{{ $booking_details->no_of_days }} Night
                          </label>
                        </div>
                      </div>
                      <div class="col">
                        <div class="date_list_midd border-right">
                          <h3>{{ $co1 }}
                          </h3>
                          <p>{{ $co2 }}, 11.00 PM
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5 col-sm-5 col-lg-5">
                    <div class="row">
                      <div class="col">
                        <h3 class="room_title">{{ $booking_details->adult_count }} Guest
                        </h3>
                      </div>
                      <div class="col">
                        <h3 class="room_title">{{ $room_count }}  Room
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="date_info_list">
                <div class="row">
                  <div class="col col-sm col-xs">
                    <div class="booking_info_ammounts">
                      <label>Primary Guest
                      </label>
                      <h3>{{ $booking_details->firstname }}
                      </h3>
                    </div>
                  </div>
                  <div class="col col-sm col-xs">
                    <div class="booking_info_ammounts">
                      <label>Mobile Number
                      </label>
                      <h3>{{ $booking_details->country_code."".$booking_details->mobile_number }}
                      </h3>
                    </div>
                  </div>
                  <div class="col col-sm col-xs">
                    <div class="booking_info_ammounts">
                      <label>Email ID
                      </label>
                      <h3>{{ $booking_details->email }}
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <div class="date_info_bookings">
                <ul>
                  <li>
                    <div class="row">
                      <div class="col-sm-6 col-md-6 col-xs-6">
                        <div class="inner_book_list">
                          <h3>Booking Amount
                          </h3>
                        </div>
                      </div>
                      <div class="col-sm-4 col-md-4 col-xs-4">
                        <div class="inner_book_list">

                          <h4>{{ $booking_details->room_type_cost }} MYR x {{ $room_count }} Room x {{ $booking_details->no_of_days }} Nights
                          </h4>
                        </div>
                      </div>
                      <div class="col-sm-2 col-md-2 col-xs-2">
                        <div class="inner_book_list aligin_right">
                          <h3>{{ $booking_details->original_price }} MYR
                          </h3>
                        </div>
                      </div>
                    </div>
                  </li>
                  @if($booking_details->coupon_amount > 0)
                  <li>
                    <div class="row">
                      <div class="col-sm-6 col-md-6 col-xs-6">
                        <div class="inner_book_list">
                          <h3>Coupon Amount
                          </h3>
                        </div>
                      </div>
                      <div class="col-sm-4 col-md-4 col-xs-4">
                        <div class="inner_book_list">
                          <h4>Coupon Applied
                          </h4>
                        </div>
                      </div>
                      <div class="col-sm-2 col-md-2 col-xs-2">
                        <div class="inner_book_list aligin_right">
                          <h3> - {{ $booking_details->coupon_amount }} MYR
                          </h3>
                        </div>
                      </div>
                    </div>
                  </li>
                  @endif
                </ul>
                <hr>
                <div class="row">
                  <div class="col-sm-6 col-md-6 col-xs-6">
                    <div class="inner_book_list">
                      <h3>Total Payable Amount
                        <br/>
                        <span>(All Inclusive)
                        </span>
                      </h3>
                    </div>
                  </div>
                  <div class="col-sm-4 col-md-4 col-xs-4">
                    <div class="inner_book_list paid_info">
                      <h4>Amount to be Paid at 
                        the Hotel
                      </h4>
                    </div>
                  </div>
                  <div class="col-sm-2 col-md-2 col-xs-2">
                    <div class="inner_book_list info_listing">
                      <h3>{{ $booking_details->bc_charges }} MYR
                      </h3>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="smothing_canceld">

                  <?php $booking_id = encrypt($booking_details->bid); ?>
                  <b>Something is not right? 
                    <a href="{{ URL::to('cancel_order/'.$booking_id )}}" title="Cancel Booking" id="cancel_booking">Cancel Booking 
                    </a>or call +60 58579858673
                  </b>
                </div>
                <hr>
                <div class="rules cancel_pay">
                  <h1>Hotel Rules
                  </h1>
                  <ul class="list-inline">
                    <li>
                      <span>-
                      </span> {{ $properties->hotel_rules }}
                    </li>
                  </ul>
                </div>
                <hr>
                <div class="rules cancel_pay">
                  <h1>Early Check In/Late Check Out
                  </h1>
                  <ul class="list-inline">
                    <li>
                      <span>-
                      </span> {{ $properties->cancellation_policy }}
                    </li>
                  </ul>
                </div>
              </div>
              <div class="space1">
              </div>
            </div>
          </div>
          <div class="space">
          </div>
        </div>
      </section>
</div>


<script type="text/javascript">

$( document ).ready(function() {
          $("#cancel_booking").on("click", function(){
              return confirm("'.trans("messages.Are you sure want to delete?").'");
          });
});

</script>


    <script>
      $('.listing_header').show();
    </script>
    
@endsection
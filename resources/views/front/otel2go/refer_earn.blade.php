@extends('layouts.front')
@section('content')
  <meta property="og:image" content="https://static.wixstatic.com/media/56a444_9273e80a60684dc8b38e56025059f356%7Emv2_d_3200_1800_s_2.png" />
	<div class="container">
		<h1>Referral Code : {{ $data->customer_unique_id }}</h1>
		<div class="row">
			<div class="col-sm-2">
				<div class="fb-share-button" data-href="https://www.otel2go.com/referral_signup/{{ $data->customer_unique_id }}" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.otel2go.com/book-now?referral_id=rt4567" class="fb-xfbml-parse-ignore">Share on Facebook</a></div>
			</div>
			<div class="col-sm-2">
				<a href="http://twitter.com/share?url=https://192.168.1.19:1006/referral_signup/{{ $data->customer_unique_id }};size=l&amp;count=none" target="_blank">Tweet</a>
			</div>
			<div class="col-sm-2">
				<div class="fb-share-button" data-href="https://www.otel2go.com/book-now?referral_id={{ $data->customer_unique_id }}" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.otel2go.com/book-now?referral_id=rt4567" class="fb-xfbml-parse-ignore">Share on Facebook</a></div>
			</div>
			<?php /* 
			<a target="_blank" title="Share on LinkedIn"
			     href="http://www.linkedin.com/shareArticle?mini=true&url=otel2go.com">Click LinkedIn
			</a>
			<button class="fb_click btn">FB</ > */ ?>
		</div>
	</div>
<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.0';
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
</script> 
<script>
        $('.listing_header').show();
        $(document).ready(function(){
			var descript = 'My Description';
			var mainUrl = 'https://192.168.1.19:1006';
			var imgUrl = "<?php 
							echo '192.168.1.19:1006/assets/front/otel2go/images/logo/159_81/logo.png'; 
						  ?>";
			var referral_id = "<?php echo $data->customer_unique_id;?>";
			var shareLink = mainUrl + encodeURIComponent('/book-now?referral_id='+referral_id);
			var fbShareLink = shareLink + '&picture=' + imgUrl + '&description=' + descript;
			var twShareLink = 'text=' + descript + '&url=' + shareLink;
		
			$(".my-btn .facebook").off("tap click").on("tap click",function(){
			  var fbpopup = window.open("https://www.facebook.com/sharer/sharer.php?u=" + fbShareLink, "pop", "width=600, height=400, scrollbars=no");
			  return false;
			});
			/*$('.fb_click').on('click',function(){
				 var fbpopup = window.open("https://www.facebook.com/sharer/sharer.php?u=" + fbShareLink, "pop", "width=600, height=400, scrollbars=no");
			  	return false;
			}); */
			$(".my-btn .twitter").off("tap click").on("tap click",function(){
			  var twpopup = window.open("http://twitter.com/intent/tweet?" + twShareLink , "pop", "width=600, height=400, scrollbars=no");
			  return false;
			});  
		});
</script>	
<?php /*
<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
<script type="IN/Share" data-url="www.otel2go.com/referral_signup/<?php echo $data->customer_unique_id;?>"></script> */ ?>
@endsection

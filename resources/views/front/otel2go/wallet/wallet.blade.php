@extends('layouts.front')
@section('content')
<?php $general = Session::get("general");$currency_side = getCurrencyPosition()->currency_side;$currency_symbol = getCurrency();?>
<section class="store_item_list">
    <div class="container">
        <div class="cms_pages">
            <h2 class="wallet_title">@lang('messages.Wallet')</h2>
            <div class="cms_pages_walet">
                <div class="waleet_host_sec">
				    <div class="sec_info_bot">
						<div class="col-md-8 col-sm-8 col-xs-8 padding_left0">
							<p>@lang('messages.Your wallet balance')</p>
							<?php $wallet_amount = !empty($wallet_list->user_wallet_amount)?$wallet_list->user_wallet_amount:0;?>
							@if($currency_side == 1)
								<b>{{$currency_symbol." ".$wallet_amount}}</b><br/>
							@else
								<b>{{$wallet_amount." ".$currency_symbol}}</b><br/>
							@endif
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4 pading_right0">
							<a class="btn btn-default" data-toggle="modal" data-target="#myModal"> <i class="glyph-icon flaticon-plus"></i>@lang('messages.Add credit')</a>
						</div>
					</div>
                    <label>@lang('messages.Wallet history')</label>
                    <ul class="tabs">
                        <li class="tab-link current col-md-6 col-sm-6" data-tab="tab-1">@lang('messages.ADDED')</li>
                        <li class="tab-link col-md-6 col-sm-6" data-tab="tab-2">@lang('messages.DEDUCTED')</li>
                    </ul>
					<span class="border-bottom"></span>
                    <div id="tab-1" class="tab-content current">
                        <ul class="list_added">
							@if(count($wallet_list->credit_wallet_list) > 0)
								@foreach($wallet_list->credit_wallet_list as $credit)
									<li>
										<div class="col-md-9 col-sm-9">
											<h3>@lang('messages.Added to :site account',['site' => ucfirst($general->site_name)])</h3>
											<h6>{{$credit->created_date}}</h6>
										</div>
										<div class="col-md-3 col-sm-3">
											@if($currency_side == 1)
												<b>{{$currency_symbol.$credit->total_amount}}</b><br/>
											@else
												<b>{{$credit->total_amount.$currency_symbol}}</b><br/>
											@endif
										</div>
									</li>
								@endforeach
							@else
								<li><h3>@lang('messages.Wallet credited amount not found')</h3></li>
							@endif
                        </ul>
                    </div>
                    <div id="tab-2" class="tab-content">
                        <ul class="list_added">
							@if(count($wallet_list->debit_wallet_list) > 0)
								@foreach($wallet_list->debit_wallet_list as $debit)
									<li>
										<div class="col-md-9 col-sm-9">
											<h3>@lang('messages.Deducted to :site account',['site' => ucfirst($general->site_name)])</h3>
											<h6>{{$debit->created_date}}</h6>
											<h3><a title="@lang('messages.View')" href="{{url('/order-info/'.encrypt($debit->order_id)) }}">@lang('messages.View')</a></h3>
										</div>
										<div class="col-md-3 col-sm-3">
											@if($currency_side == 1)
												<b>{{$currency_symbol.$debit->amount}}</b><br/>
											@else
												<b>{{$debit->amount.$currency_symbol}}</b><br/>
											@endif
										</div>
									</li>
								@endforeach
							@else
								<li><h3>@lang('messages.Wallet debitted amount not found')</h3></li>
							@endif
                        </ul>
                    </div>
                    <span class="bottom_space_cont"></span>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade wallet_model_popup" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">@lang('messages.Add money')</h4>
                </div>
                {!!Form::open(array('url' => 'add-wallet', 'method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'wallet_form'));!!}
					<div class="modal-body">
						<div class="form-group">
						<div class="inner_counts_bg">
							<label class="col-sm-4 control-label">@lang('messages.Wallet amount') <span class="asterisk">*</span></label>
							<div class="col-sm-8">
								<input type="text" name="wallet_amount" id="wallet_amount" value="" maxlength="10" placeholder="@lang('messages.Wallet amount')" class="form-control"  />
							</div>
						</div>
						</div>
					</div>
					<div class="form-group Loading_Img" style="display:none;">
						<div class="col-sm-4">
							<i class="fa fa-spinner fa-spin fa-3x"></i><strong style="margin-left: 3px;">@lang('messages.Processing...')</strong>
						</div>
					</div>
					<div class="modal-footer Submit_button">
						<button type="button" class="btn btn-default" data-dismiss="modal">@lang('messages.Cancel')</button>
						<button class="btn btn-primary" id="proceed">@lang('messages.Proceed')</button>
					</div>
				{!!Form::close();!!}
            </div>
        </div>
    </div>
</section>
<script>

    $('.listing_header').show();
    
    $(document).ready(function(){
    	$('ul.tabs li').click(function(){
    		var tab_id = $(this).attr('data-tab');

    		$('ul.tabs li').removeClass('current');
    		$('.tab-content').removeClass('current');

    		$(this).addClass('current');
    		$("#"+tab_id).addClass('current');
    	})
    })
    $(window).load(function(){
        $('form').preventDoubleSubmission();
    });
	$(document).on("submit", "#wallet_form", function(evt){
		var wallet_amount = $('#wallet_amount').val();
		$('.Loading_Img').show();
        $('#proceed').hide();
		if(wallet_amount == '') {
			$('.Loading_Img').hide();
			$('#proceed').show();
			toastr.error('<?php echo trans('messages.The wallet amount field is required.');?>');
			return false;
		} else if(!$.isNumeric(wallet_amount)) {
			$('.Loading_Img').hide();
			$('#proceed').show();
			toastr.error('<?php echo trans('messages.The wallet amount must be a number.');?>');
			return false;
		}
		$( "#wallet_form" ).submit();
        return false;
	});
</script>
@endsection

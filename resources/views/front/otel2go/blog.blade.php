      @extends('layouts.front')
      @section('content')
   <!-- <section class="banner_sections_inner blog">
<div class="container">
<div class="inner_banner_info">
<h2>@lang('messages.Blog')</h2>
<p>@lang('messages.Where everything on demand is discussed')</p>
</div>
</div>
    </section>-->
    <section class="error_sections">
<div class="container">
<div class="row">

<div class="cat_drop_sec">
<div class="col-md-6" style="display:none;">
<div class="select_content">
<div class="col-md-10 padding0">
 <select  name="category" class="js-example-disabled-results" id="category" >
	 <option value="">@lang('messages.Category')</option>
	 <?php /*if(count($category)) { ?>
		 <?php foreach($category as $cat){ ?>
			<option <?php if(Input::get('filter')==$cat->url_key){ ?> selected="selected" <?php } ?>  value="<?php echo $cat->url_key; ?>"><?php echo $cat->category_name; ?></option>
         <?php } ?>
     <?php } */?>

 </select>
											</div>
</div>
</div>

</div>
<div class="bolg_listing">

	@if (count($blog) > 0 )
		@foreach($blog as $key => $value)
				<div class="col-md-6">
				<div class="blog_list_in">
				<a title="{{ ucfirst($value->title) }}" href="{{ URL::to('/blog/info/' . $value->url_index . '') }}">{{ str_limit($value->title.',', 50) }}</a>
				<div class="blog_list_img">
				<a title="{{ ucfirst($value->title) }}" href="{{ URL::to('/blog/info/' . $value->url_index . '') }}">
                        <?php  if(file_exists(base_path().'/public/assets/admin/base/images/blog/list/'.$value->image)) { ?>
								<img   alt="{{ ucfirst($value->title) }}"  src="<?php echo url('/assets/admin/base/images/blog/list/'.$value->image.''); ?>" >
							<?php } else{  ?>
									<img src="{{ URL::asset('assets/admin/base/images/blog/blog.png') }}" alt="{{ ucfirst($value->title) }}">
							<?php } ?>
                        </a>
				</div>
				<p>{{ str_limit($value->short_notes , 250) }}</p>
				<a href="{{ URL::to('/blog/info/' . $value->url_index . '') }}" title="@lang('messages.Continue Reading')" class="continue_butt"> <span>→</span> @lang('messages.Continue Reading')</a>
				</div>
				</div>
		@endforeach
	@else
		<div class="blog_no_img">
	<img src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/images/blog.png');?>" alt="">
	@lang('<p>No data found.</p>')

	</div>
	@endif
</div>

</div>
</div>
<script>
      $('.listing_header').show();
</script>
@endsection

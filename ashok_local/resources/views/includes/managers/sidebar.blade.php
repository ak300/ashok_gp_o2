<div class="leftpanel">
    <div class="media profile-left">
        <?php $manager_id = Session::get('manager_id'); ?>
        <a class="pull-left profile-thumb">   
           <?php if(file_exists(public_path('/assets/admin/base/images/staff/thumb/'.$manager_id)))  { ?>
			  
                <img src="<?php echo url('/assets/admin/base/images/staff/'.$manager_id.'?'.time()); ?>" class="img-circle">
            <?php } else{  ?>
                <img src=" {{ URL::asset('assets/admin/base/images/a2x.jpg') }} " class="img-circle">
            <?php } ?>
        </a>
        <div class="media-body">
            <h4 class="media-heading"><?php echo ucfirst(Session::get('manager_name'));?></h4>
            <small class="text-muted"> <a href="{{ url('managers/editprofile') }}" title="Edit Profile">@lang('messages.Edit Profile')</a> </small>
        </div>
    </div><!-- media -->
    <ul class="nav nav-pills nav-stacked">

        @if(Session::get('staff_type') == 1)
<?php /*
        <li class="{{ Request::is('managers/dashboard*') ? 'active' : '' }}" ><a href="{{ URL::to('managers/dashboard') }}"><i class="fa  fa-home"></i> <span>@lang('messages.Dashboard')</span></a></li>
*/ ?>
        <li class="parent {{ Request::is('managers/orders/index*') ? 'active' : '' || Request::is('managers/dashboard*') ? 'active' : '' || Request::is('managers/showbooking*') ? 'active' : ''}}"><a  href="javascript:;"><i class="fa fa-home"></i> <span>@lang('messages.Booking Management')</span></a>
                <ul class="children">
                    
                    <li class="{{ Request::is('managers/orders/index*') ? 'active' : '' }}" ><a  href="{{ URL::to('managers/orders/index') }}">@lang('messages.Booking Management')</a></li>
                    
                    <li class="{{ Request::is('managers/dashboard*') ? 'active' : '' || Request::is('managers/showbooking*') ? 'active' : '' }}" ><a href="{{ URL::to('managers/dashboard') }}"><span>@lang('messages.Booking Calendar')</span></a></li>                    
                </ul>
        </li>
        

        <li class="parent {{ Request::is('managers/room_type*') ? 'active' : '' || Request::is('managers/rooms*') ? 'active' : '' }}"><a  href="javascript:;"><i class="fa fa-building"></i> <span>@lang('messages.Rooms Management')</span></a>
            <ul class="children">
                <?php /*if(has_permission('system/permission')) { */ ?>
                <li class="{{ Request::is('managers/room_type*') ? 'active' : '' }}" ><a  href="{{ URL::to('managers/room_type') }}">@lang('messages.Room Types')</a></li>
                <?php /*} */?>
                <?php /*if(has_permission('permission/users')){*/ ?>
                <li class="{{ Request::is('managers/rooms') ? 'active' : ''}}" ><a  href="{{ URL::to('managers/rooms') }}">@lang('messages.Rooms')</a></li>
                <?php /*}*/ ?>
            </ul>
        </li>

        <li class="parent {{ (Request::is('reports/*')) ? 'active' : '' || Request::is('managers/reports_analysis') ? 'active' : '' || Request::is('managers/reports/order') ? 'active' : '' || Request::is('managers/room/reports') ? 'active' : '' || Request::is('managers/reports/room') ? 'active' : '' || Request::is('managers/reports/room') ? 'active' : '' || Request::is('manager/reports/order') ? 'active' : '' || Request::is('manager/reports/room') ? 'active' : '' }}"><a  href="javascript:;"><i class="fa fa-bar-chart-o"></i> <span>@lang('messages.Reports & Analytics')</span></a>
                <ul class="children">

                        <li class="{{ Request::is('managers/reports_analysis*') ? 'active' : '' }}"><a href="{{ URL::to('managers/reports_analysis') }}"><span>@lang('messages.Reports & Analysis')</span></a></li>

                        <li class="{{ Request::is('manager/reports/order') ? 'active' : '' }}" ><a  href="{{ URL::to('manager/reports/order') }}">@lang('messages.Transactions')</a></li>

                        <li class="{{ Request::is('manager/reports/room') ? 'active' : '' }}" ><a  href="{{ URL::to('manager/reports/room') }}">@lang('messages.Rooms')</a></li> 

                        <li style="display: none;" class="{{ Request::is('vendor/reports/room') ? 'active' : '' }}" ><a  href="{{ URL::to('vendor/reports/room') }}">@lang('messages.Sales')</a></li>   

                        <li style="display: none;" class="{{ Request::is('reports/coupons') ? 'active' : '' }}" ><a  href="{{ URL::to('reports/coupons') }}">@lang('messages.Coupons')</a></li>

                        <li style="display: none;" class="{{ Request::is('reports/products') ? 'active' : '' }}" ><a  href="{{ URL::to('reports/products') }}">@lang('messages.Products')</a></li>
                    
                </ul>
        </li>
        
        @endif

        @if(Session::get('staff_type') == 2)
            <li class="parent {{ Request::is('managers/expenses*') ? 'active' : '' || Request::is('managers/accounts') ? 'active' : '' }}"><a href="{{ URL::to('managers/expenses') }}"><i class="fa fa-building"></i> <span>@lang('messages.Accounts Management')</span></a>

                <ul class="children">
                    <li class="{{ Request::is('managers/expenses*') ? 'active' : '' }}"><a href="{{ URL::to('managers/expenses') }}"><span>@lang('messages.Expenses')</span></a></li>   
                    
                    <li class="{{ Request::is('managers/accounts*') ? 'active' : '' }}" ><a  href="{{ URL::to('managers/accounts') }}">@lang('messages.Accounts')</a></li>
                     
                </ul>
            </li> 

            <li class="parent {{ Request::is('managers/house_keeping*') ? 'active' : '' || Request::is('managers/housekeeping_tasks*') ? 'active' : ''}}"><a  href="javascript:;"><i class="fa fa-users"></i> <span>@lang('messages.Housekeeping Staffs')</span></a>
                <ul class="children">
                    <?php /*if(has_permission('system/permission')) { */ ?>
                    <li class="{{ Request::is('managers/house_keeping*') ? 'active' : '' }}" ><a  href="{{ URL::to('managers/house_keeping') }}">@lang('messages.Housekeeping Staffs')</a></li>
                    <?php /*} */?>
                    <?php /*if(has_permission('permission/users')){*/ ?>
                    <li class="{{ Request::is('managers/housekeeping_tasks*') ? 'active' : ''}}" ><a  href="{{ URL::to('managers/housekeeping_tasks') }}">@lang('messages.Housekeeping Tasks')</a></li>
                    <?php /*}*/ ?>
                </ul>
            </li>                
        @endif  


        <li style="display:none;" class="{{ Request::is('managers/accomodation*') ? 'active' : '' }}"><a href="{{ URL::to('managers/accomodation') }}"><i class="fa fa-home"></i> <span>@lang('messages.Accomodation')</span></a></li>

       <?php /* <li class="{{ Request::is('managers/products*') ? 'active' : '' }}"><a href="{{ URL::to('managers/products') }}"><i class="fa fa-cubes"></i> <span>@lang('messages.Products')</span></a></li>
        <li class="{{ Request::is('managers/reviews*') ? 'active' : '' }}" ><a href="{{ URL::to('managers/reviews') }}"><i class="glyphicon  glyphicon-star"></i> <span>@lang('messages.Reviews')</span></a></li>
        <li class="parent {{ (Request::is('managers/return_orders*') || Request::is('managers/orders*') || Request::is('managers/request_amount/*')) ? 'active' : ''}}"><a  href="#"><i class="fa fa-database"></i> <span>@lang('messages.Sales')</span></a>
            <ul class="children">
                <li class="{{ Request::is('managers/orders*') ? 'active' : '' }}" ><a  href="{{ URL::to('managers/orders/index') }}">@lang('messages.Orders')</a></li>
                <?php /*<li class="{{ Request::is('managers/return_orders*') ? 'active' : '' }}" ><a  href="{{ URL::to('managers/return_orders') }}">@lang('messages.Return Orders')</a></li>
            </ul>
        </li>
     <?php /*   <li class="parent {{ (Request::is('managers/report_return_orders*') || Request::is('managers/report_orders*') || Request::is('managers/report_products*')) ? 'active' : ''}}"><a  href="#"><i class="fa fa-bar-chart-o"></i> <span>@lang('messages.Reports & Analytics')</span></a>
            <ul class="children">
                <li class="{{ Request::is('managers/report_orders*') ? 'active' : '' }}" ><a  href="{{ URL::to('managers/report_orders') }}">@lang('messages.Orders')</a></li>
                <?php /*<li class="{{ Request::is('managers/report_return_orders*') ? 'active' : '' }}" ><a  href="{{ URL::to('managers/report_return_orders') }}">@lang('messages.Return Orders')</a></li>
                <li class="{{ Request::is('managers/report_products*') ? 'active' : '' }}" ><a  href="{{ URL::to('managers/report_products') }}">@lang('messages.Products')</a></li>
            </ul>
        </li>
        <li class="{{ Request::is('managers/notifications*') ? 'active' : '' }}" ><a href="{{ URL::to('managers/notifications') }}"><i class="fa fa-bell"></i> <span>@lang('messages.Notifications')</span></a></li>
        */ ?>
    </ul>
    <footer>
        @include('includes.managers.footer')
    </footer>
</div><!-- leftpanel -->

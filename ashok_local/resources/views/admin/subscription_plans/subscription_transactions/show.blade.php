@extends('layouts.admin')
@section('content')
<div class="row">	
	<div class="col-md-12 ">
		<!-- Nav tabs -->
		<div class="pageheader">
			<div class="media">
				<div class="pageicon pull-left">
					<i class="fa fa-home"></i>
				</div>
				<div class="media-body">
					<ul class="breadcrumb">
						<li><a href="{{ URL::to('admin/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Admin')</a></li>
						<li>@lang('messages.Subscription Transactions')</li>
					</ul>
					<h4>@lang('messages.View Subscription Transaction')</h4>
				</div>
			</div><!-- media -->
		</div><!-- pageheader -->

		<div class="contentpanel">
			<ul class="nav nav-tabs"></ul>
			<div class="tab-content mb30">
				<div class="tab-pane active" id="home3">
					<!-- Task Name -->
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Vendor Name') :</label>
						<div class="col-sm-3">
							{{ isset($data[0]->vendor_name)?$data[0]->vendor_name:'--' }}
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Plan Name') :</label>
						<div class="col-sm-3">
							{{ isset($data[0]->plan_name)?$data[0]->plan_name:'--' }}
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Transaction Id') :</label>
						<div class="col-sm-3">
							{{ isset($data[0]->transaction_id)?$data[0]->transaction_id:'--' }}
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Auth Id') :</label>
						<div class="col-sm-3">
							{{ isset($data[0]->auth_id)?$data[0]->auth_id:'--' }}
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Reference No') :</label>
						<div class="col-sm-3">
							{{ isset($data[0]->reference_no)?$data[0]->reference_no:'--' }}
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Price') :</label>
						<div class="col-sm-3">
							{{ isset($data[0]->plan_price)?$data[0]->plan_price:'--' }}
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Discount Price') :</label>
						<div class="col-sm-3">
							{{ isset($data[0]->plan_discount_price)?$data[0]->plan_discount_price:'--' }}
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Duration') :</label>
						<div class="col-sm-3">
							{{ isset($data[0]->plan_duration)?$data[0]->plan_duration:'--' }} @lang('messages.Days')
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Payment Type') :</label>
						<div class="col-sm-6">
							@if(isset($data[0]->payment_type))
							 	@if($data[0]->payment_type == 1) 
							 		 @lang('messages.Offline/Cash') 
							 	@elseif($data[0]->payment_type == 2) 
							 		 @lang('messages.Online') 
							 	@endif
						 	@endif
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Created Date') :</label>
						<div class="col-sm-6">
							{{ isset($data[0]->created_at)?$data[0]->created_at:'--' }}
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Payment Status') :</label>
						<div class="col-sm-6">
							@if(isset($data[0]->payment_status))
							 	@if($data[0]->payment_status == 0) 
							 		 @lang('messages.Incompleted') 
							 	@elseif($data[0]->payment_status == 1) 
							 		 @lang('messages.Completed') 
							 	@endif
						 	@endif
						</div>
					</div>
					 <div class="panel-footer">
					<button type="button"  onclick="window.location='{{ url('admin/subscription_transactions') }}'" class="btn btn-primary mr5">@lang('messages.Cancel')</button>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection



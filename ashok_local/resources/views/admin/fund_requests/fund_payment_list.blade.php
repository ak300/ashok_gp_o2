<div class="row">
	<div class="col-sm-12">
		<div class="row">
			{!!Form::open(array('url' => ['update_fund_payment', $fund_request['id']], 'method' => 'post','class'=>'tab-form attribute_form','id'=>'fund_request_form','files' => true));!!} 
				<div class="row">
					<div class="col-sm-6">
						<label class="control-label"><h5>Fleet Owner</h5></label>
					</div>
					<div class="col-sm-6">
						<?php 
						$vendor_id = $fund_request['vendor_id'];
						$vendor_details = getVendorName($vendor_id);
						$vendor_name = isset($vendor_details[0]->vendor_name)?$vendor_details[0]->vendor_name:'-';
						?>
						<label><h5>{{ $vendor_name }}
						</h5></label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<label class="control-label"><h5>Reference No</h5></label>
					</div>
					<div class="col-sm-6">
						<label><h5>
						{!! isset($fund_request['reference_no'])?$fund_request['reference_no']:'--' !!}
						</h5></label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<label class="control-label"><h5>Request Amount</h5></label>
					</div>
					<div class="col-sm-6">
						<label><h5>
							{!! isset($fund_request['request_amount'])?$fund_request['request_amount']:'--' !!}
						</h5></label>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<label class="control-label"><h5>Payment Method</h5></label>
					</div>
					<div class="col-sm-6">
						<select class="form-control" name="payment_method" id="payment_method">
							<option value="">@lang('messages.Select Payment Method')</option>
							<option value="1">@lang('messages.Cash')</option>
							<option value="2">@lang('messages.Cheque')</option>
							<option value="3">@lang('messages.Bank Transfer')</option>	
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<button class="btn btn-primary mr10" type="submit">@lang('messages.Save')</button>
					</div>
				</div>
				{!!Form::close();!!} 
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#fund_request_form').on('submit',function(e){
		e.preventDefault();
		var payment_method = $('#payment_method').val();
		if(payment_method == ""){
			alert('please select the payment method.');
		} else {
			$(this).unbind().submit();
		}
	});
</script>
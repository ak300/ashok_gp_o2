@extends('layouts.admin')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/datatables2.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-datetimepicker.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/datatables2.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/css/bootstrap-datetimepicker.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/plugins/export/buttons.dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/select2.min.js') }}"></script>
<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-home"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href="{{ URL::to('admin/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Admin')</a></li>
                <li>@lang('messages.Reports')</li>
            </ul>
            <h4>@lang('messages.Booking Reports')</h4>
        </div>
    </div><!-- media -->
</div><!-- pageheader -->
<div class="contentpanel">
    @if (Session::has('message'))
        <div class="admin_sucess_common">
            <div class="admin_sucess">
                <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('message') }}</div>
            </div>
        </div>
    @endif

    {!!Form::open(array('method' => 'POST','class'=>'tab-form attribute_form','id'=>'reports_order_form','files' => true));!!}
        <div class="form-group">
            <div class="col-md-6 padding0">
                <label class="col-sm-3 control-label padding_left0">@lang('messages.Date Start')</label>
                <div class="col-sm-9">
                    <input type="text"  name="from" value="{{   $date_data->min_date    }}" autocomplete="off" id="datepicker" placeholder="mm/dd/yyyy"  class="form-control"  />
                </div>
            </div>
            <div class="col-md-6 padding0">
                <label class="col-sm-3 control-label">@lang('messages.Date End')</label>
                <div class="col-sm-9">
                    <input type="text"  name="to" value="{{   $date_data->max_date    }}"  autocomplete="off" id="datepicker1"  placeholder="mm/dd/yyyy"  class="form-control"  />
                </div>
            </div>
        </div>       

        <div class="form-group">
            <button type="submit" class="btn btn-primary mr5" title="@lang('messages.Search')">@lang('messages.Search')</button>
        </div>
    {!!Form::close();!!}

    <div class="contentpanel">

    <div class="col-md-12">
        {{ csrf_field() }}
        <div id="container" style="height: 400px; min-width: 600px"></div>

    <script src="{{ URL::asset('assets/admin/base/js/highstock.js') }}"></script>
    <script src="{{ URL::asset('assets/admin/base/js/exporting.js') }}"></script>
    </div><!-- row -->
<script type="text/javascript">

<?php  if(count(json_decode($payment_amount_encoded,1))> 0){ ?>

Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Booking'
    },
    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
            day: '%b %e'
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Booking'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr>' +
            '<td style="padding:0"><b>Booking Count - {point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            //  pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
    {
        name: 'Payments',
        data: <?php  echo $payment_amount_encoded; ?>

    },    
    ]
});

<?php   } else  {   ?>

     $('#container').hide();

<?php   }   ?>    

</script>    
    </div>

    <div class="vender_scroll_sec">
        <table id="ReportsOrdersTable" class="table table-striped table-bordered responsive">
            <thead>
                <tr class="headings">
                    <th>@lang('messages.Date Start')</th> 
                    <th>@lang('messages.Date End')</th>
                    <?php   /*  <th>@lang('messages.No. Orders')</th>
                    <th>@lang('messages.No. Products')</th>
                    <th>@lang('messages.Tax')</th>  */  ?>
                    <th>@lang('messages.Customer Name')</th>
                    <th>@lang('messages.Booking Type')</th>
                    <th>@lang('messages.Total Amount')</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="empty-text" colspan="6" style="background-color: #fff!important;">
                        <div class="list-empty-text"> @lang('messages.No records found.') </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<script> 
    $(window).load(function(){
        $('#order_status').select2();
        $('#group_by').select2();
        // $('#datepicker').val('03/21/2018 11:12 AM');
        // $('#datepicker1').val('03/22/2018 11:12 AM');
        $('#datepicker').datetimepicker();
        $('#datepicker1').datetimepicker({
            useCurrent: false //Important! See issue #1075
        });
        $("#datepicker").on("dp.change", function (e) {
            $('#datepicker1').data("DateTimePicker").minDate(e.date);
        });
        $("#datepicker1").on("dp.change", function (e) {
            //  $('#datepicker').data("DateTimePicker").maxDate(e.date);
        });
    });
    $(function()
    {
        var oTable = $('#ReportsOrdersTable').DataTable({
        bFilter: false,
        dom: 'lBfrtip',
        buttons: [
            {
                extend: 'collection',
                text: 'Export',
                title: 'orders_reports',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ],
        processing: true,
        serverSide: true,
        responsive: true,
        autoWidth:true,
        ajax: {
            url: '{{ URL::to('reports/report-order-list') }}',
            type: 'POST',
            data: function (d) {
                d.from         = $('input[name=from]').val();
                d.to           = $('input[name=to]').val();
                //  d.group_by     = $('#group_by').val();
                //  d.order_status = $('#order_status').val();
            },
            headers:{
                'X-CSRF-TOKEN': $('input[name=_token]').val()
            }
        },
        order: [],
           columnDefs: [ {
               targets  : 'no-sort',
               orderable: false,
           }],
        "order": [[ 0, "desc" ]],
        columns: [
                { data: 'check_in_date', name: 'check_in_date'},
                { data: 'check_out_date', name: 'check_out_date'},
                <?php   /*  { data: 'orders_count', name: 'orders_count' }, 
                { data: 'quantity_count', name: 'quantity_count' },
                { data: 'tax_total', name: 'tax_total' },   */  ?>
                { data: 'firstname', name: 'firstname' },
                { data: 'booking_status', name: 'booking_status' },
                { data: 'booking_charges_amount', name: 'booking_charges_amount' },

            ],
        
        });
        $('#reports_order_form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
    });
	$('.dropdown-toggle').dropdown();
</script>

@endsection

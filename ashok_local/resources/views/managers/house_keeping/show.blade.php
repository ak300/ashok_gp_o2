@extends('layouts.vendors')
@section('content')
<div class="row">	
	<div class="col-md-12 ">
		<!-- Nav tabs -->
		<div class="pageheader">
			<div class="media">
				<div class="pageicon pull-left">
					<i class="fa fa-home"></i>
				</div>
				<div class="media-body">
					<ul class="breadcrumb">
						<li><a href="#"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
						<li>@lang('messages.Housekeeping Staffs')</li>
					</ul>
					<h4>@lang('messages.View Housekeeping Staffs')</h4>
				</div>
			</div><!-- media -->
		</div><!-- pageheader -->

		<div class="contentpanel">
			@if (count($errors) > 0) 
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<div class="buttons_block pull-right">
				<div class="btn-group mr10">
					<a class="btn btn-primary tip" href="{{ URL::to('vendor/house_keeping/edit/' . $data['id'] . '') }}" title="Edit" >@lang('messages.Edit')</a>
				</div>
			</div>
			<ul class="nav nav-tabs"></ul>
			<div class="tab-content mb30">
				<div class="tab-pane active" id="home3">
					<!-- Task Name -->
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Housekeeping Staff Name') :</label>
						<div class="col-sm-3">
							<?php echo $data['firstname'].' '.$data['lastname']; ?>
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Gender') :</label>
						<div class="col-sm-3">
							@if($data['gender'] == 'M')
								@lang('messages.Male')
							@else
								@lang('messages.FeMale')
							@endif
						</div>
					</div>
					<div class="form-group">
						<label for="title" class="col-sm-3 control-label"> @lang('messages.Date of birth') :</label>
						<div class="col-sm-3">
							<?php echo $data['date_of_birth']; ?>
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Mobile Number') :</label>
						<div class="col-sm-6">
							<?php echo $data['mobile_number']; ?>
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Address') :</label>
						<div class="col-sm-6">
							<?php echo $data['address']; ?>
						</div>
					</div>
					<div class="form-group">
						<label for="index" class="col-sm-3 control-label"> @lang('messages.Outlet Name') :</label>
						<div class="col-sm-6">
							 <?php $outlet_list = get_outlet_list(Session::get('vendor_id')); ?>
                            @if(count($outlet_list) > 0)
                                @foreach($outlet_list as $ot)
                                     @if($data['outlet_id'] == $ot->id) 
                                     	{{ ucfirst($ot->outlet_name) }}
                                     @endif 
                                @endforeach
                            @endif
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Created Date') :</label>
						<div class="col-sm-6">
							<?php echo $data['created_at']; ?>
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-3 control-label"> @lang('messages.Updated Date') :</label>
						<div class="col-sm-6">
							<?php echo $data['updated_at']; ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">@lang('messages.Image') :</label>
						<div class="col-sm-6">
							<?php if($data['image']){ ?>
							<img src="<?php echo url('/assets/admin/base/images/house_keepers/'.$data['image'].'');?>" name="image" class="img-thumbnail">
							<?php } ?>
						</div>	
					</div>
					 <div class="panel-footer">
					<button type="button"  onclick="window.location='{{ url('vendor/house_keeping') }}'" class="btn btn-primary mr5">@lang('messages.Cancel')</button>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection



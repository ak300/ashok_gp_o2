@extends('layouts.vendors')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/switch/js/bootstrap-switch.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/plugins/switch/css/bootstrap3/bootstrap-switch.min.css') }}" media="all" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/tinymce4.1/tinymce.min.js') }}"></script>
<div class="row">	
	<div class="col-md-12 ">
<!-- Nav tabs -->
<div class="pageheader">
<div class="media">
	<div class="pageicon pull-left">
		<i class="fa fa-home"></i>
	</div>
	<div class="media-body">
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
			<li>@lang('messages.Booking')</li>
		</ul>
		<h4>@lang('messages.Edit Booking') - {{$data->id}}</h4>
	</div>
</div><!-- media -->
</div><!-- pageheader -->

<div class="contentpanel">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
			<ul>
				@foreach ($errors->all() as $error)
			<li><?php echo trans('messages.'.$error); ?> </li>
				@endforeach
			</ul>
		</div>
		@endif
<ul class="nav nav-tabs"></ul>

       {!!Form::open(array('url' => ['updatebooking', $data->id], 'method' => 'post','class'=>'tab-form attribute_form','id'=>'blog_form','files' => true));!!} 
	<div class="tab-content mb30">
	<div class="tab-pane active" id="home3">
		
         <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Booking type') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                              <select id="booking_type" name="booking_type" class="form-control" >
                                <option value="">@lang('messages.Select Booking Type')</option>
                                <?php  $old =old('booking_type'); $value= $data->booking_type; if($old){  $value=$old; } ?>
                                @if (count(getBookingTypes()) > 0)
                                    @foreach (getBookingTypes() as $key => $type)
                                        <option value="{{ $key }}" <?php echo ($value==$key)?'selected="selected"':''; ?> ><?php echo trans('messages.'.$type); ?></option>
                                    @endforeach
                                @endif
                              </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Booking Source') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                              <select id="booking_source" name="booking_source" class="form-control" >
                                <?php /*    <option value="">@lang('messages.Select Booking Source')</option>   */  ?>
                                @if (count(getBookingSources()) > 0)
                                    @foreach (getBookingSources() as $key => $type)
                                        <option value="{{ $key }}" <?php echo ($data->booking_source==$key)?'selected="selected"':''; ?> ><?php echo trans('messages.'.$type); ?></option>
                                    @endforeach
                                @endif
                              </select>
                            </div>
                        </div>
                        <?php /*
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Customer Type')</label>
                            <div class="col-sm-6">
                              <select id="customer_type" name="customer_type" class="form-control" >
                                <?php   /*  <option value="">@lang('messages.Select Customer Type')</option>    */  ?>
                                <?php /*
                                @if (count(getCustomerTypes()) > 0)
                                    @foreach (getCustomerTypes() as $key => $type)
                                        <option value="{{ $key }}" <?php echo ($data->customer_type==$key)?'selected="selected"':''; */	?><?php /* ><?php echo $type; */	?><?php /*	</option>
                                    @endforeach
                                @endif
                              </select>
                            </div>
                        </div>
                        */
                        ?> 
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Email') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="email" id="email" maxlength="100" placeholder="@lang('messages.Email')" class="form-control guest_email" value="{!! $data->email !!}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Guest Name') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <?php   /*
                                <div class="checkbox">
                                    <label class="checkbox"><input type="checkbox" id="guest" name="guest_check"></label>
                                </div>
                                */  ?>
                                <input type="text" id="guest_name" name="guest_name" maxlength="50" placeholder="@lang('messages.Guest')" class="form-control" value="{!! $data->firstname !!}" />
                                
                            </div>
                        </div>

                       
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Mobile') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                              <input type="text" id="mobile" name="mobile" maxlength="12" placeholder="@lang('messages.Mobile')" class="form-control" value="{!! $data->mobile_number !!}" />
                              <input id="phone_hidden" type="hidden" name="country_code" value="0">
                              <span class="help-block">@lang('messages.Add Phone number(s) in comma seperated. <br>For example: 9750550341,9791239324')</span>
                            </div>
                                <input type="hidden" name="country_short" id="country_short" value="<?php echo (Session::has('country_short')) ? 
                                Session::get('country_short'):'my'; ?>">                            
                        </div>                        

                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Address') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="address" maxlength="100" placeholder="@lang('messages.Address')" class="form-control" value="{!! $data->address !!}" id="address"/>
                            </div>
                        </div>

                        <input type="hidden" name="user_id" id="user_id" value="">
<?php   /*
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.City') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <select name="city" id="city_id" class="form-control" >
                                    <?php   /*  <option value="">@lang('messages.Select City')</option> */  ?>
                                    <?php /*
                                        @foreach(getCityList() as $list)
                                            <option name="city" value="{{$list->id}}" <?php echo (old('city')==$list->id)?'selected="selected"':''; */  ?><?php /* >{{$list->city_name}}</option>
                                        @endforeach
                                    }<?php /*
                                </select>
                            </div>
                       </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Postal Code')</label>
                            <div class="col-sm-6">
                              <input type="text" name="postal_code" maxlength="12" placeholder="@lang('messages.Postal Code')" class="form-control" value="{!! old('postal_code') !!}" />
                            </div>
                        </div> 
*/  ?>                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Adult Count') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                              <select id="adult_count" name="adult_count" class="form-control" onchange="checkin()">
                              	<?php  $old =old('adult_count'); $value= $data->adult_count; if($old){  $value=$old; } ?>
                                <option value="">@lang('messages.Select Adult Count')</option>
                                @if (count(getAdultcount()) > 0)
                                    @foreach (getAdultcount() as $key => $type)
                                        <option value="{{ $key }}" <?php echo ($value==$key)?'selected="selected"':''; ?> ><?php echo $type; ?></option>
                                    @endforeach
                                @endif
                              </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Child Count') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                              <select id="child_count" name="child_count" class="form-control" onchange="checkin()">
                                <option value="">@lang('messages.Select Child Count')</option>
                                <?php  $old =old('child_count'); $value= $data->child_count; if($old){  $value=$old; } ?>
                                @if (count(getChildCount()) > 0)
                                    @foreach (getChildCount() as $key => $type)
                                        <option value="{{ $key }}" <?php echo ($value==$key)?'selected="selected"':''; ?> ><?php echo $type; ?></option>
                                    @endforeach
                                @endif
                              </select>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Room Types <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <select class="form-control" name="room_type_id" id="room_type" onchange="checkin()">
                                <option value="">Select Room Type</option>
								<?php  $old =old('room_type_id'); $room= $data->room_type; if($old){  $value=$old; } ?>
                                    <?php foreach($room_type as $value) {  ?>
                                        <option value="{!! $value->id !!}" <?php echo ($room==$value->id)?'selected="selected"':'';  ?>>{!! $value->room_type !!}</option>
                                    <?php }  ?>
                        
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Check-in date') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="check_in" autocomplete="off" value="{!! $data->check_in_date !!}" placeholder="mm/dd/yyyy" id="check_in" onchange="checkin()">
                                    <span class="input-group-addon datepicker-trigger"><i class="glyphicon glyphicon-calendar" id="check_in"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Check-out date') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="check_out" autocomplete="off" value="{!! $data->check_out_date !!}" placeholder="mm/dd/yyyy" id="check_out">
                                    <span class="input-group-addon datepicker-triggere"><i class="glyphicon glyphicon-calendar" id="check_out"></i></span>
                                </div>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.No Of Nights')</label>
                            <div class="col-sm-6">
                                <input type="text" name="nights" id="days" maxlength="50" placeholder="@lang('messages.No Of Nights')" class="form-control" value="{!! $data->no_of_days !!}" readonly="" />
                            </div>
                        </div>                        

                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Rooms <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <select id = "room"  name="room[]" data-placeholder="@lang('messages.Select Rooms')" multiple class="width300">
                            <?php 
                            /*
                                if(!empty(old('room_type_id'))){ 
                                $rooms = getRoomList(old('room_type_id'));
                              */      
                            ?>
                            <?php /* $old =old('room'); $cate=array(); if($old){  $cate=$old; } */ ?>
                                    
                                 
                                <?php  /*   }  */   ?>
                                    
                                </select>
                            </div>
                        </div>  

		
       </div>
		<div class="panel-footer">
		<button class="btn btn-primary mr5" title="Update">@lang('messages.Update')</button>
		<button type="reset" title="Cancel" class="btn btn-default" onclick="window.location='{{ url('admin/blog') }}'">@lang('messages.Cancel')</button>
		</div>
        </div>
      
 {!!Form::close();!!} 
</div></div></div>
<script type="text/javascript">
$(document).ready(function(){
	$('#room').select2(); 
});
</script>
<script type="text/javascript">
</script>
@endsection


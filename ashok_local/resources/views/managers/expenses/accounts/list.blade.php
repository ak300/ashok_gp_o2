@extends('layouts.managers')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/datatables2.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-datetimepicker.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/datatables2.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/css/bootstrap-datetimepicker.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/plugins/export/buttons.dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/select2.min.js') }}"></script>
<div class="pageheader">
    <div class="media">
        <div class="pageicon pull-left">
            <i class="fa fa-home"></i>
        </div>
        <div class="media-body">
            <ul class="breadcrumb">
                <li><a href="{{ URL::to('managers/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Managers')</a></li>
                <li>@lang('messages.Accounts')</li>
            </ul>
            <h4>@lang('messages.Accounts')</h4>
        </div>
    </div><!-- media -->
</div><!-- pageheader -->
<div class="contentpanel">
    @if (Session::has('message'))
        <div class="admin_sucess_common">
            <div class="admin_sucess">
                <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('message') }}</div>
            </div>
        </div>
    @endif

    {!!Form::open(array('method' => 'POST','class'=>'tab-form attribute_form','id'=>'reports_order_form','files' => true));!!}
        <div class="form-group">
            <div class="col-md-6 padding0">
                <label class="col-sm-3 control-label padding_left0">@lang('messages.Date Start')</label>
                <div class="col-sm-9">
                    <input type="text"  name="from" value="{{   $date_data->min_date }}" autocomplete="off" id="datepicker" placeholder="mm/dd/yyyy"  class="form-control"  />
                </div>
            </div>
            <div class="col-md-6 padding0">
                <label class="col-sm-3 control-label">@lang('messages.Date End')</label>
                <div class="col-sm-9">
                    <input type="text"  name="to" value="{{   $date_data->max_date }}"  autocomplete="off" id="datepicker1"  placeholder="mm/dd/yyyy"  class="form-control"  />
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 padding0">
                <label class="col-sm-3 control-label padding_left0">@lang('messages.Booking Status')</label>
                <div class="col-sm-8">
                    <select name="order_status" id="order_status"  class="select2-offscreen"  style="width:100%;">
                        @if(count($order_status) > 0)
                            <option value="">@lang('messages.All')</option>
                            @foreach($order_status as $list)
                                <option value="{{$list->id}}" <?php echo (Input::get('order_status')==$list->id)?'selected="selected"':''; ?> >{{$list->name}}</option>
                            @endforeach
                        @else
                            <option value="">@lang('messages.No order status found')</option>
                        @endif
                    </select>
                </div>
            </div> 
            <div class="col-md-6 padding0">
                <label class="col-sm-3 control-label">@lang('messages.Group By')</label>
                <div class="col-sm-9">
                    <select name="group_by" id="group_by" class="select2-offscreen" style="width:100%;">
                        <option value="1" <?php echo (Input::get('group_by') == 1)?'selected="selected"':''; ?>>@lang('messages.Days')</option>
                        <option value="2" <?php echo (Input::get('group_by') == 2)?'selected="selected"':''; ?>>@lang('messages.Weeks')</option>
                        <option value="3" <?php echo (Input::get('group_by') == 3)?'selected="selected"':''; ?>>@lang('messages.Months')</option>
                        <option value="4" <?php echo (Input::get('group_by') == 4)?'selected="selected"':''; ?>>@lang('messages.Years')</option>
                </select>
            </div>                      
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary mr5" title="@lang('messages.Save')" id="expenses_btn">@lang('messages.Search')</button>
        </div>
    {!!Form::close();!!}
    <div class="vender_scroll_sec">
        <table id="ReportsOrdersTable" class="table table-striped table-bordered responsive">
            <thead>
                <tr class="headings">
                    <th>@lang('messages.Booking ID')</th>
                    <th>@lang('messages.Customer Name')</th>
                    <th>@lang('messages.Date Start')</th> 
                    <th>@lang('messages.Date End')</th>
<?php   /*               <th>@lang('messages.Booking Type')</th>                    
                    <th>@lang('messages.No. Products')</th>
                    <th>@lang('messages.Tax')</th> */   ?>
                    <th>@lang('messages.Total Charges')</th>
                    <th>@lang('messages.Total Payments')</th>
                    <th>@lang('messages.Due Amount')</th>
                    <th>@lang('messages.Balance')</th>
                    <th>@lang('messages.Actions')</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="empty-text" colspan="6" style="background-color: #fff!important;">
                        <div class="list-empty-text"> @lang('messages.No records found.') </div>
                    </td>
                </tr>
            </tbody>
            <tbody>

                <?php $expense = getExpensesData($date_data->min_date,$date_data->max_date);?>
                @foreach($sum_data as $sd)
                <tr>
                    
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><h4>Total</h4></td>

                    <!--   TOTAL CHARGES -->

                        @if(getCurrencyPosition()->currency_side == 1)
                        
                            <td><h5>{{   getCurrency()." ".$sd->cp   }}</h5></td>
                        
                        @else
                            <td><h5>{{   $sd->cp." ".getCurrency()   }}</h5></td>
                        
                        @endif



                    <!--   TOTAL PAYMENTS -->

                        @if(getCurrencyPosition()->currency_side == 1)
                        
                            <td><h5>{{   getCurrency()." ".$sd->pp   }}</h5></td>
                        
                        @else 
                            <td><h5>{{   $sd->pp." ".getCurrency()   }}</h5></td>
                        
                        @endif

                        <?php  

                                $ch = $sd->cp; 
                                $py = $sd->pp; 

                                if($ch >= $py){
                                    $due_amount = $ch - $py;
                                }
                                else{
                                    $due_amount = "-";
                                }

                                if($ch >= $py)
                                {
                                   $bal = "-";
                                }
                                elseif($ch < $py){
                                    $bal = $py - $ch;
                                    if(getCurrencyPosition()->currency_side == 1)
                                    {                                    
                                            $bal = getCurrency()." ".$bal;
                                    }
                                    else{
                                            $bal = $bal." ".getCurrency();
                                    }
                                    
                                }

                        ?>

                    <!--   DUE AMOUNT -->

                        @if(getCurrencyPosition()->currency_side == 1)
                        
                            <td><h5>{{   getCurrency()." ".$due_amount   }}</h5></td>
                        
                        @else 
                            <td><h5>{{   $due_amount." ".getCurrency()   }}</h5></td>
                        
                        @endif

                    <!--   BALANCE -->

                    <td><h5>{{   $bal   }}</h5></td>
                             
                    <td></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script> 
    $(window).load(function(){
        $('#order_status').select2();
        $('#group_by').select2();
        $('#datepicker').datetimepicker({
            showClear: true,
        });
        $('#datepicker1').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            showClear: true,
        });
        $("#datepicker").on("dp.change", function (e) {
            $('#datepicker1').data("DateTimePicker").minDate(e.date);
        });
        $("#datepicker1").on("dp.change", function (e) {
            //  $('#datepicker').data("DateTimePicker").maxDate(e.date);
        });
        /*
        $('#datepicker').datetimepicker({
            maxDate: new Date(),
            sideBySide: true,
            useCurrent: false
        });
        $('#datepicker1').datetimepicker({
            maxDate: new Date(),
            sideBySide: true,
            useCurrent: false
        });*/
    });
    $(function()
    {
        var oTable = $('#ReportsOrdersTable').DataTable({
        bFilter: false,
        dom: 'lBfrtip',
        buttons: [
            {
                extend: 'collection',
                text: 'Export',
                title: 'vendor_orders_reports',
                buttons: [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ],
        processing: true,
        serverSide: true,
        responsive: true,
        autoWidth:true,
        ajax: {
            url: '{{ URL::to('managers/accounts/booking_list') }}',
            type: 'POST',
            data: function (d) {
                d.from         = $('input[name=from]').val();
                d.to           = $('input[name=to]').val();
                d.group_by     = $('#group_by').val();
                d.order_status = $('#order_status').val();
            },
            headers:{
                'X-CSRF-TOKEN': $('input[name=_token]').val()
            }
        },
        order: [],
           columnDefs: [ {
               targets  : 'no-sort',
               orderable: false,
           }],
        "order": [[ 0, "desc" ]],
        columns: [
                { data: 'booking_random_id', name:'booking_random_id'},
                { data: 'firstname', name: 'firstname' },
                { data: 'date_start', name: 'date_start'},
                { data: 'date_end', name: 'date_end'},
               // { data: 'booking_status', name: 'booking_status' },
                
                //  { data: 'quantity_count', name: 'quantity_count' },
                //  { data: 'tax_total', name: 'tax_total' },
                { data: 'total_charges', name: 'total_charges' },
                { data: 'total_payments', name: 'total_payments' },
                { data: 'due_amount', name: 'due_amount'},
                { data: 'balance', name: 'balance'},
                { data: 'action', name: 'action'},
            ],
        
        });
        $('#reports_order_form').on('submit', function(e) {
            oTable.draw();
            e.preventDefault();
        });
    });
    $('.dropdown-toggle').dropdown();
</script>

<script type="text/javascript">

// $("#expenses_btn").on('click',function(){
//     //  alert("Ak");

//     expenseData();
// })


// function expenseData()  
// { 

// alert('AK');
//         var from,to;


//         token = $("input[name=_token]").val();
//         url = '{{   url('expense_data')    }}';

//         from = $('input[name=from]').val();
//         to   = $('input[name=to]').val();

//         if($from && $to == "")
//         {
//             from = {{ $date_data->min_date }};
//             to = {{ $date_data->max_date }};
//         }

//         data = {from:from,to:to};

//         $.ajax({

//             url : url,
//             headers: {'X-CSRF-TOKEN': token},
//             data: data,
//             type: 'POST',
//             datatype: 'JSON',
//             success: function (resp) {

//                             if(resp.data!='')
//                             {
//                                 alert('Ak');
//                                 //  $("#one_night_price").val(resp.data.discount_price);
//                                 //  $("#price").val(resp.data.discount_price);
                                
//                             }
//                             else
//                             {
//                                 toastr.error('No data');

//                                 alert('Ak');
//                             }

//                             // var days = $("#days").val();

//                             // //  alert(days);

//                             // var onp = $("#one_night_price").val();

//                             // //  alert(onp);

//                             // var op = days * onp ;

//                             // //  alert(op);
//                             // $("#price").val(op);
//                             // $("#price").attr('readonly',true);
//                         }
//         });            

// }


</script>

@endsection

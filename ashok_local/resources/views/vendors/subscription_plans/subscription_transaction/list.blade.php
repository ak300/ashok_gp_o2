@extends('layouts.vendors')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/buttons.html5.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/plugins/export/buttons.dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<div class="pageheader">
<div class="media">
	<div class="pageicon pull-left">
		<i class="fa fa-home"></i>
	</div>
	<div class="media-body">
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
			<li>@lang('messages.Subscription Transactions')</li>
		</ul>
		<h4>@lang('messages.Subscription Transactions')</h4>
	</div>
</div><!-- media -->
</div><!-- pageheader -->
	<!-- will be used to show any messages -->
	@if (Session::has('message'))
		<div class="admin_sucess_common">
	<div class="admin_sucess">
		<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('message') }}</div></div></div>
	@endif
<div class="contentpanel">

 <table id="vendor-transactions" class="table table-striped table-bordered responsive">
    <thead>
        <tr class="headings">
			
            <th>@lang('messages.S.no')</th>
            <th>@lang('messages.Vendor Name')</th> 
            <th>@lang('messages.Plan Name')</th> 
			<th>@lang('messages.Transaction Id')</th> 
            <th>@lang('messages.Reference No')</th> 
            <th>@lang('messages.Auth Id')</th> 
            <th>@lang('messages.Created date')</th>
			<th>@lang('messages.Payment Status')</th> 
            <th>@lang('messages.Actions')</th>
        </tr>
    </thead>
</table>
</div>

<script>
$(function() {
    $('#vendor-transactions').DataTable({
		dom: 'Blfrtip',
		buttons: [
		],
        processing: true,
        serverSide: false,
		responsive: true,
		autoWidth:false,
        ajax: '{!! route('anyajaxVendorTransactions.data') !!}',
        "order": [],
		"columnDefs": [ {
		  "targets"  : 'no-sort',
		  "orderable": false,
		}],
        columns: [
			{ data: 'id', name: 'id',orderable: false },
			{ data: 'vendor_name', name: 'vendor_name',searchable:true },
			{ data: 'plan_name', name: 'plan_name',searchable:true },
			{ data: 'transaction_id', name: 'transaction_id',searchable:true },
			{ data: 'reference_no', name: 'reference_no',searchable:true },
			{ data: 'auth_id', name: 'auth_id',searchable:true },
            { data: 'created_at', name: 'created_at' },
			{ data: 'payment_status', name: 'payment_status' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ],
    });
});
</script>
@endsection

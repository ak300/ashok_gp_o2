@extends('layouts.vendors')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery-ui-1.10.3.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/bootstrap-datetimepicker.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/moment.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-datetimepicker.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/select2.min.js') }}"></script>

<script src="{{ URL::asset('/assets/admin/base/js/toastr.min.js') }}"></script> 

<link href="{{ URL::asset('/assets/admin/base/css/toastr.css') }}" media="all" rel="stylesheet" type="text/css" />

<!-- country code css -->
<link href="{{ URL::asset('/assets/front/otel2go/js/intlTelInput.css') }}" media="all" rel="stylesheet" type="text/css" />


<div class="row">
    <div class="col-md-12 ">
        <!-- Nav tabs -->
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-home"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
                        <li>@lang('messages.Booking')</li>
                    </ul>
                    <h4>@lang('messages.Add Booking')</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
        <div class="contentpanel">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li><?php echo trans('messages.'.$error); ?> </li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(Session::has('invalid-email'))
<?php /*            
                <script type="text/javascript">
                    toastr.error("{{  Session::get('invalid-email') }}");
                </script>
*/?>

                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
                    <ul>
                        <li>{{  Session::get('invalid-email') }}</li>
                    </ul>
                </div>                        

            @endif

            @if((Session::get('property_id')!="") && (count($room_type) > 0)  && (count($room) > 0))
            <ul class="nav nav-tabs"></ul>
            {!!Form::open(array('url' => 'booking/booking_user', 'method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'create_driver_form', 'files' => true, 'autocomplete'=> 'off'));!!} 
                <div class="tab-content mb30">
                    <div class="tab-pane active" id="home3">
                        <?php /*<div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Social Title') <span class="asterisk">*</span></label>
                            <div class="col-sm-1">
                                <select name="social_title" id="social_title" class="form-control">
                                    <option value="Mr.">@lang('messages.Mr.')</option>
                                    <option value="Mrs.">@lang('messages.Mrs.')</option>
                                    <option value="Ms.">@lang('messages.Ms.')</option>
                                </select>
                            </div>
                        </div>*/ ?>
                        
                        <div class="row">
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">@lang('messages.Booking type') <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                              <select id="booking_type" name="booking_type" class="form-control" >
                                <?php   /*  <option value="">@lang('messages.Select Booking Type')</option> */  ?>
                                @if ($order_status) > 0)
                                    @foreach ($order_status as $os)
                                        <option value="{{ $os->id }}" <?php echo (old('booking_type')==$os->id)?'selected="selected"':''; ?> >{{ $os->name }}</option>
                                    @endforeach
                                @endif
                              </select>
                            </div>
                        </div>
                        </div>
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">@lang('messages.Booking Source') <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                              <select id="booking_source" name="booking_source" class="form-control" >
                                <?php /*    <option value="">@lang('messages.Select Booking Source')</option>   */  ?>
                                @if (count(getBookingSources()) > 0)
                                    @foreach (getBookingSources() as $key => $type)
                                        <option value="{{ $key }}" <?php echo (old('booking_source')==$key)?'selected="selected"':''; ?> ><?php echo trans('messages.'.$type); ?></option>
                                    @endforeach
                                @endif
                              </select>
                            </div>
                        </div>
                        </div>
                        
                        </div>
                        <?php /*
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Customer Type')</label>
                            <div class="col-sm-6">
                              <select id="customer_type" name="customer_type" class="form-control" >
                                <?php   /*  <option value="">@lang('messages.Select Customer Type')</option>    */  ?>
                                <?php /*
                                @if (count(getCustomerTypes()) > 0)
                                    @foreach (getCustomerTypes() as $key => $type)
                                        <option value="{{ $key }}" <?php echo (old('customer_type')==$key)?'selected="selected"':''; */ ?><?php /* ><?php echo $type; */    ?><?php /*</option>
                                    @endforeach
                                @endif
                              </select>
                            </div>
                        </div> 
                        */  ?>
                        <div class="row">
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">@lang('messages.Email') <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" name="email" maxlength="100" placeholder="@lang('messages.Email')" class="form-control guest_email typeahead" value="{!! old('email') !!}" />
                            </div>
                        </div>
                        </div>
                        <input type="hidden" name="g_email" id="g_email" value="">
                        <div class="col-sm-6">
                        <div class="form-group new_cust">
                            <div class="col-sm-2">
                            <button type="button" class="btn btn-primary mr5" title="Save" id="add_new_cust">@lang('messages.Add New Customer')</button>
                            </div>
                        </div>
                        </div>
                        </div>
                        <?php   /*
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Guest Name') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <?php   /*
                                <div class="checkbox">
                                    <label class="checkbox"><input type="checkbox" id="guest" name="guest_check"></label>
                                </div>
                                */  ?>  <?php   /*
                                <input type="text" id="guest_name" name="guest_name" maxlength="50" placeholder="@lang('messages.Guest')" class="form-control" value="{!! old('guest_name') !!}" />
                                
                            </div>
                        </div>

                       
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Mobile') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                              <input type="text" id="mobile" name="mobile" maxlength="12" placeholder="@lang('messages.Mobile')" class="form-control" value="{!! old('mobile') !!}" />
                              <input id="phone_hidden" type="hidden" name="country_code" value="0">
                              <span class="help-block">@lang('messages.Add Phone number(s) in comma seperated. <br>For example: 9750550341,9791239324')</span>
                            </div>
                                <input type="hidden" name="country_short" id="country_short" value="<?php echo (Session::has('country_short')) ? 
                                Session::get('country_short'):'my'; */  ?><?php /*">                            
                        </div>                        

                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Address') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" name="address" maxlength="100" placeholder="@lang('messages.Address')" class="form-control" value="{!! old('address') !!}" id="address"/>
                            </div>
                        </div>

                        */  ?>

                        <input type="hidden" name="user_id" id="user_id" value="">
<?php   /*
                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.City') <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <select name="city" id="city_id" class="form-control" >
                                    <?php   /*  <option value="">@lang('messages.Select City')</option> */  ?>
                                    <?php /*
                                        @foreach(getCityList() as $list)
                                            <option name="city" value="{{$list->id}}" <?php echo (old('city')==$list->id)?'selected="selected"':''; */  ?><?php /* >{{$list->city_name}}</option>
                                        @endforeach
                                    }<?php /*
                                </select>
                            </div>
                       </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">@lang('messages.Postal Code')</label>
                            <div class="col-sm-6">
                              <input type="text" name="postal_code" maxlength="12" placeholder="@lang('messages.Postal Code')" class="form-control" value="{!! old('postal_code') !!}" />
                            </div>
                        </div> 
*/  ?>

                        <div class="row">
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">@lang('messages.Adult Count') <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                              <select id="adult_count" name="adult_count" class="form-control">
                                <?php   /*  <option value="">@lang('messages.Select Adult Count')</option>  */  ?>
                                @if (count(getAdultcount()) > 0)
                                    @foreach (getAdultcount() as $key => $type)
                                        <option value="{{ $key }}" <?php echo (old('adult_count')==$key)?'selected="selected"':''; ?> ><?php echo $type; ?></option>
                                    @endforeach
                                @endif
                              </select>
                            </div>
                        </div>
                        </div>
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">@lang('messages.Child Count') <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                              <select id="child_count" name="child_count" class="form-control">
                                <?php   /*  <option value="">@lang('messages.Select Child Count')</option>   */  ?>
                                @if (count(getChildCount()) > 0)
                                    @foreach (getChildCount() as $key => $type)
                                        <option value="{{ $key }}" <?php echo (old('child_count')==$key)?'selected="selected"':''; ?> ><?php echo $type; ?></option>
                                    @endforeach
                                @endif
                              </select>
                            </div>
                        </div>
                        </div>
                        </div>

                        <div class="row">
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">@lang('messages.Check-in date') <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php /*
                                    <input type="text" class="form-control" name="check_in" autocomplete="off" value="{!! old('check_in') !!}" placeholder="mm/dd/yyyy" id="check_in" onchange="checkin()">
                                    <span class="input-group-addon datepicker-trigger"><i class="glyphicon glyphicon-calendar" id="check_in"></i></span>
                                    */ ?>
                            <input type="text"  name="check_in" value="{{ old('check_in') }}" autocomplete="off" id="check_in" placeholder="mm/dd/yyyy" class="form-control" onchange="checkin()"/>                                    
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">@lang('messages.Check-out date') <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <?php /*
                                    <input type="text" class="form-control" name="check_out" autocomplete="off" value="{!! old('check_out') !!}" placeholder="mm/dd/yyyy" id="check_out">
                                    <span class="input-group-addon datepicker-triggere"><i class="glyphicon glyphicon-calendar" id="check_out"></i></span>

                                    */  ?>
                                <input type="text" name="check_out" value="{{ old('check_out') }}" autocomplete="off" id="check_out" placeholder="mm/dd/yyyy"  class="form-control"/>                                    
                                </div>
                            </div>
                        </div>                
                        </div>         
                        </div>
                        <div class="form-group" style="display: none;">
                            <label class="col-sm-2 control-label">@lang('messages.No Of Nights')</label>
                            <div class="col-sm-6">
                                <input type="text" name="nights" id="days" maxlength="50" placeholder="@lang('messages.No Of Nights')" class="form-control" value="{!! old('nights') !!}" readonly="" />
                                <input type="text" id="one_night_price" value="" name="one_night_price">
                                <input type="text" id="one_night_normal_price" value="" name="one_night_normal_price">
                            </div>
                        </div> 

                        <div class="row rooms_show">
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Room Types <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <select class="form-control" name="room_type_id" id="room_type">
                                    <?php   /*  <option value="">Select Room Type</option>  */  ?>
                                    @if(count($room_type)>0)
                                    <?php foreach($room_type as $value) {  ?>
                                    
                                        <option value="{!! $value->id !!}"  <?php if(!empty(old('room_type_id'))){ echo (old('room_type_id')==$value->id)?'selected="selected"':''; } ?>>{!! $value->room_type !!}</option>
                                    <?php }  ?>
                                    @endif
                                </select>
                            </div>
                        </div>
                        </div>
                       
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Rooms <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <!--    <select id = "room" name="room[]" data-placeholder="@lang('messages.Select Rooms')" class="width300"> -->
                                <select id = "room" name="room" data-placeholder="@lang('messages.Select Rooms')" class="form-control">
                                </select>
                            </div>
                        </div>  
                        </div>
                        </div>

                        <div class="row price_show">
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">@lang('messages.Price') <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" id="price" name="price" maxlength="100" placeholder="@lang('messages.Price')" class="form-control price" value="{!! old('price') !!}" />                                        
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" id="normal_price" name="normal_price" maxlength="100" placeholder="@lang('messages.Normal Price')" class="form-control normal_price" value="{!! old('normal_price') !!}" />
                                    </div>
                                </div>                                    
                            </div>
                        </div>
                        </div>
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">@lang('messages.Notes')</label>
                            <div class="col-sm-8">
                                <input type="text" name="notes" maxlength="100" placeholder="@lang('messages.Notes')" class="form-control" value="{!! old('notes') !!}" />
                            </div>
                        </div>
                        </div>
                        </div>                        

                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary mr5" title="Save">@lang('messages.Save')</button>
                        <button type="reset" title="Cancel" class="btn btn-default" onclick="window.location='{{ url('vendors/dashboard') }}'">@lang('messages.Cancel')</button>
                    </div>
                </div>


            {!!Form::close();!!}   

            @elseif(Session::get('property_id') == "")

                <script type="text/javascript">

                function pageRedirect() 
                {
                    redirect = '{{  url('vendor/outlets')   }}';    

                    window.location.replace(redirect);
                }   

                    toastr.options = {

                        "positionClass": "toast-top-left",

                    }
                    toastr.error('Please Add Property First');

                    setTimeout("pageRedirect()", 3000);
                </script>

            @elseif(count($room_type) == "" || count($room_type) == 0)
                <script type="text/javascript">

                function pageRedirect() 
                {
                    redirect = '{{  url('vendor/room_type')   }}';    

                    window.location.replace(redirect);
                }   

                    toastr.options = {

                        "positionClass": "toast-top-left",

                    }
                    toastr.error('Please Add Room Type First');

                    setTimeout("pageRedirect()", 3000);
                </script>       

            @elseif(count($room) == "" || count($room) == 0)
                <script type="text/javascript">

                function pageRedirect() 
                {
                    redirect = '{{  url('vendor/rooms')   }}';    

                    window.location.replace(redirect);
                }   

                    toastr.options = {

                        "positionClass": "toast-top-left",

                    }
                    toastr.error('Please Add Rooms First');

                    setTimeout("pageRedirect()", 3000);
                </script>  
            @endif  

            <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal_id">
              <div class="modal-dialog" role="document">
                <div class="modal-content">

                {!!Form::open(array( 'method' => 'post','class'=>'tab-form attribute_form','id'=>'userdetails','files' => true));!!}

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h5 class="modal-title" id="gridSystemModalLabel">Customer Information</h5>
                  </div>
                  <div class="modal-body">

                        <div class="form-group">
                            <label class="col-sm-3 control-label">@lang('messages.Guest Name') <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" id="guest_name" name="guest_name" maxlength="50" placeholder="@lang('messages.Guest Name')" class="form-control" value="{!! old('guest_name') !!}" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">@lang('messages.Email') <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" id="email_pop" name="email" maxlength="100" placeholder="@lang('messages.Email')" class="form-control" value="{!! old('email') !!}" />
                            </div>
                        </div>

                       <div class="form-group">
                            <label class="col-sm-3 control-label">@lang('messages.Mobile') <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                              <input type="text" id="mobile" name="mobile" maxlength="12" placeholder="@lang('messages.Mobile')" class="form-control" value="{!! old('mobile') !!}" />
                              <input id="phone_hidden" type="hidden" name="country_code" value="0">
                              <span class="help-block" style="display: none;">@lang('messages.Add Phone number(s) in comma seperated. <br>For example: 9750550341,9791239324')</span>
                            </div>
                                <input type="hidden" name="country_short" id="country_short" value="<?php echo (Session::has('country_short')) ? 
                                Session::get('country_short'):'my'; ?> ">                            
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">@lang('messages.Address') <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" id="address" name="address" maxlength="100" placeholder="@lang('messages.Address')" class="form-control" value="{!! old('address') !!}" />
                            </div>
                        </div>
<?php   /*
                        <div class="form-group">
                            <label class="col-sm-3 control-label">@lang('messages.City') <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <select name="city" id="city_id" class="form-control" >
                                    <?php   /*  <option value="">@lang('messages.Select City')</option> */  ?>
                                    <?php   /*
                                        @foreach(getCityList() as $list)
                                            <option name="city" value="{{$list->id}}" <?php echo (old('city')==$list->id)?'selected="selected"':''; */  ?> <?php    /*   >{{$list->city_name}}</option>
                                        @endforeach
                                    <?php /* } */ ?>    <?php   /*
                                </select>
                            </div>
                       </div>
                       

                        <div class="form-group">
                            <label class="col-sm-3 control-label">@lang('messages.Postal Code')</label>
                            <div class="col-sm-8">
                              <input type="text" name="postal_code" maxlength="12" placeholder="@lang('messages.Postal Code')" class="form-control" value="{!! old('postal_code') !!}" />
                            </div>
                        </div>                        
*/  ?>
                        
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" <?php /* onclick="submitForm()"   */  ?>  >Save</button>
                  </div>
                  {!!Form::close();!!} 
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->            

        </div>
    </div>
</div>

<script type="text/javascript">
    $(window).load(function(){


        $('#check_in').datetimepicker({
             format: 'DD/MM/YYYY',
             // minDate: new Date(),
        });

        $('#check_out').datetimepicker({
             format: 'DD/MM/YYYY',
             useCurrent: false //Important! See issue #1075
        });
                
        $("#check_in").on("dp.change", function (e) {
            $('#check_out').data("DateTimePicker").minDate(e.date);
                calc();
                roomShow();            
        });
        $("#check_out").on("dp.change", function (e) {

                calc();
                roomShowcout();            
            //  $('#datepicker').data("DateTimePicker").maxDate(e.date);
        });
        
    });
</script>        
<!-- country code js-->
<script type="text/javascript" src="{{ URL::asset('assets/front/otel2go/js/intlTelInput.min.js') }}"></script>
<script type="text/javascript">

        //  INITIALLY HIDING ROOMS & PRICE DIV
        $(".rooms_show").hide();
        $(".price_show").hide();

        var path = "{{ route('autocomplete') }}";
        $('input.typeahead').typeahead({
            source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        });

        $('#add_new_cust').on('click',function(){

            $("#modal_id").modal('show');

        });
</script>
<script type="text/javascript">

    $("#userdetails").submit(function() {
          var countryData = $("#mobile").intlTelInput("getSelectedCountryData");
          $("#phone_hidden").val('+'+countryData.dialCode);
          $('#country_short').val(countryData.iso2);
    });

    $("#adult_count").on("change",function(){

        if($("#check_in").val() !="" && $("#check_out").val() !="")
        {
            checkin();
        }

    });

    $("#child_count").on("change",function(){

        if($("#check_in").val() !="" && $("#check_out").val() !="")
        {
            checkin();
        }
        
    });    

function roomShow()
{

    //  IF INPUT GIVEN MEANS SHOWING ROOMS & PRICE DIV

    if($("#check_out").val() !="")
    {
         $(".rooms_show").show();
         $(".price_show").show();
         checkin();
    }
}

//  FOR OLD VALUES[IN CASE FORM FAILED MEANS] SHOWING ROOMS & PRICE DIV 

<?php if(old('check_in') && old('check_out')) { ?>

        $(".rooms_show").show();
        $(".price_show").show();
        checkin();

<?php  } ?>

function roomShowcout()
{
    if($("#check_in").val() =="")
    {
        toastr.error('Select Check-in Date First');
    }
    else
    {      
        $(".rooms_show").show();
        $(".price_show").show();
        checkin();
    }
}

function unCheckForm()
{
    //  alert('Ak');
    $('#guest').prop('checked', false);
    $("#modal_id").modal('hide');

}
function calc()
{
    if($("#check_in").val()!="" && $("#check_out").val()!=""){
 
 /*
            var From_date = new Date($("#check_in").val());
            var To_date = new Date($("#check_out").val());
            var diff_date =  To_date - From_date;
             
            var years = Math.floor(diff_date/31536000000);
            var months = Math.floor((diff_date % 31536000000)/2628000000);
            var days = Math.floor(((diff_date % 31536000000) % 2628000000)/86400000);
*/

            var From_date = $("#check_in").val();
            var To_date = $("#check_out").val();

            var a = moment(From_date, "DD/MM/YYYY");
            var b = moment(To_date, "DD/MM/YYYY");

            var days = b.diff(a,'days');
            $("#days").val(days);
    }
}

function roomPrice()  
{ 

        //  alert('Ak');

        var room_id,room_type;


        token = $("input[name=_token]").val();
        url = '{{   url('price_check')    }}';

        room_id = $("#room").val();
        room_type = $("#room_type").val();

        data = {room_id:room_id,room_type:room_type};

        $.ajax({

            url : url,
            headers: {'X-CSRF-TOKEN': token},
            data: data,
            type: 'POST',
            datatype: 'JSON',
            success: function (resp) {

                            if(resp.data!='')
                            {
                                $("#one_night_price").val(resp.data.discount_price);
                                $("#one_night_normal_price").val(resp.data.normal_price);
                                //  $("#price").val(resp.data.discount_price);
                                
                            }
                            else
                            {
                                toastr.error('No data');
                            }

                            var days = $("#days").val();

                            var onp = $("#one_night_price").val();
                            var onnp = $("#one_night_normal_price").val();

                            var op = days * onp ;

                            //  ROUND OF VALUES 

                            op = Math.round(op);

                            var opn = days * onnp ;

                            opn = Math.round(opn);

                            //  alert(op);
                            $("#price").val(op);
                            $("#price").attr('readonly',true);

                            $("#normal_price").val(opn);
                            $("#normal_price").attr('readonly',true);                            
                        }
        });            

}

function checkin()
{

    //  alert('Ak');
/*
    if($("#adult_count").val() == "")
    {
        toastr.remove();
        toastr.error('Adult Count should Not Empty');        
    }
    else if($("#child_count").val() == "")
    {
        toastr.remove();
        toastr.error('Child Count should Not Empty');        
    }
    else if($("#room_type").val() == "")
    {
        toastr.remove();
        toastr.error('Room Type should Not Empty');
    }    
    else if($("#check_in").val() == "")
    {
        toastr.remove();
        toastr.error('Child Count should Not Empty');        
    }  
*/
    //  event.preventDefault();
/*    
    if($("#check_in").val() == "")
    {
        toastr.remove();
        toastr.error('Check-in date should Not Empty');  
        return false;      
    }
    else if($("#check_out").val() == "")
    {
        toastr.remove();
        toastr.error('Check-out date should Not Empty');
        return false;
    }        
*/
    var token,url,cin,data,rtd,cc,ac,days;

    token = $("input[name=_token]").val();
    url = '{{   url('rooms_check')    }}';
    cin = $("#check_in").val();
    cout = $("#check_out").val();
    rtd = $("#room_type").val();
    cc  = $("#child_count").val();
    ac  = $("#adult_count").val();
    days = $("#days").val();

    data = {cin:cin,cout:cout,rtd:rtd,cc:cc,ac:ac,days:days};

    $.ajax({

        url : url,
        headers: {'X-CSRF-TOKEN': token},
        data: data,
        type: 'POST',
        datatype: 'JSON',
        success: function (resp) {
                        $('#room').empty();
                        toastr.remove();
                        //  $('#room').select2('data', id);
                        if(resp.data!='')
                        { 
                            //  $('#price').val(resp.data.discount_price);
                            $.each(resp.data, function(key, value)
                            {

                                //  alert(resp);
                                //  console.log(value['id']+'=='+value['room_name']);
                                //  console.log(value['id']+'=='+value['discount_price']);
                                // $('#room_type').append($("<option></option>").attr("value",value['room_type_id']).text(value['room_type']));
                                $('#room').append($("<option></option>").attr("value",value['id']).text(value['room_name']));
                                //  $('#price').val(discount_price);
                            });
                        }
                        else {
                            $('#room').text('No Matches Found');
                            toastr.remove();
                            toastr.warning('No Rooms found on Selected Dates');
                            $("#price").val("");
                            $("#normal_price").val("");
                        }
                    }
    });

    roomPrice();

}


function submitForm()
{
    if($("#guest_name").val() == "")
    {
        toastr.remove();
        toastr.error('Guest Name should Not Empty');
    }
    else if($("#email").val() == "")
    {
        toastr.remove();
        toastr.error('Email should Not Empty');        
    }
    else if($("#mobile").val() == "")
    {
        toastr.remove();
        toastr.error('Mobile Number should Not Empty');        
    }        
    else if($("#address").val() == "")
    {
        toastr.remove();
        toastr.error('Address should Not Empty');        
    }
    else
    {
        $("#modal_id").modal('hide');
    }  
}

</script>

<script type="text/javascript">

    $(document).ready(function(){

        $('#userdetails').on('submit',function(event){
         


        event.preventDefault();
/*
        if($("#guest_name").val() == "")
        {
            toastr.remove();
            toastr.error('Guest Name should Not Empty');
            return false;
        }
        else if($("#email_pop").val() == "")
        {
            toastr.remove();
            toastr.error('Email should Not Empty');        
            return false;
        }
        else if($("#mobile").val() == "")
        {
            toastr.remove();
            toastr.error('Mobile Number should Not Empty');        
            return false;
        }        
        else if($("#address").val() == "")
        {
            toastr.remove();
            toastr.error('Address should Not Empty');        
            return false;
        }              
*/       

            var token,url,data,new_email;

            token = $('input[name=_token]').val();
            new_email = $("#email_pop").val();
            url = '{{   url('booking/new_user')    }}';
            data = $("#userdetails").serialize();

            $.ajax({
                url:url,
                data: data,
                type: 'POST',
                datatype: 'JSON',

                success:function(resp){

                    if(resp.response.httpCode == 200)
                    {  
                        toastr.success('User Details Inserted Successfully');
                        $("#modal_id").modal('hide');
                        $("#userdetails").trigger('reset');
                        $(".guest_email").val(new_email);
                        $(".guest_email").attr('readonly',true);
                        $("#add_new_cust").hide();
                        $("#g_email").val(new_email);
                    }
                    else
                    {

                        //  alert('Ak');

                        //  alert(resp.response.httpCode);
                        if(resp.response.httpCode == 400)
                        {
                            
                            $.each(resp.response.Message,function(key,val){
                                toastr.warning(val);
                            });
                        } else {
                            
                            toastr.warning(resp.response.Message);
                        }
                      
                    }


/*
                    if(resp == 1)
                    {
                        //  alert('Ak');
                        toastr.success('User Details Inserted Successfully');
                        $("#modal_id").modal('hide');
                        $(".guest_email").val(new_email);
                        $(".guest_email").attr('readonly',true);
                        $("#add_new_cust").hide();
                        $("#g_email").val(new_email);

                    }
                    else if(resp == -1)
                    {
                        alert('No Output');
                    }

*/                    
                    
                },
                error:function(resp)
                {
                    //  $this.button('reset');
                    //$("#ajaxloading").hide();
                    console.log('out--'+resp); 
                    return false;
                }                

            });

        });
    });    
</script>
<script> 
    $(window).load(function(){

        //  $("#room").select2();

        //  $("#guest").select2();

    });

    $("#room").on('change',function(){

        roomPrice();

    });    


    $("#room_type").on('change',function(){

        checkin();
        roomPrice();

    }); 






/*

    $('#room_type').change(function(){

    if($("#adult_count").val() == "")
    {
        toastr.remove();
        toastr.error('Adult Count should Not Empty');        
    }
    else if($("#child_count").val() == "")
    {
        toastr.remove();
        toastr.error('Child Count should Not Empty');        
    }        
        var rid, cc, ac, token, url, data;
        token = $('input[name=_token]').val();
        rid   = $('#room_type').val();
        cc  = $("#child_count").val();
        ac  = $("#adult_count").val();        
        url   = '{{url('list/RoomList')}}';
        data  = {rid: rid,cc:cc,ac:ac};
        $.ajax({
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            data: data,
            type: 'POST',
            datatype: 'JSON',
            success: function (resp) {
                $('#room').empty();
                if(resp.data!='')
                { 
                    $('#room').html('Select Room');
                    $.each(resp.data, function(key, value)
                    {
                        //  console.log(value['id']+'=='+value['room_name']);
                        $('#room').append($("<option></option>").attr("value",value['id']).text(value['room_name'])); 
                    });
                }
                else {
                    $('#room').html('No Matches Found');
                }
            }
        });
        
    });
*/
    

    $('#country_id').change(function(){
        var cid, token, url, data;
        token = $('input[name=_token]').val();
        cid   = $('#country_id').val();
        url   = '{{url('list/CityList')}}';
        data  = {cid: cid};
        $.ajax({
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            data: data,
            type: 'POST',
            datatype: 'JSON',
            success: function (resp) {
                $('#city_id').empty();
                if(resp.data!='')
                { 
                    $('#select2-chosen-2').html('Select City');
                    $.each(resp.data, function(key, value)
                    {
                        //console.log(value['id']+'=='+value['city_name']);
                        $('#city_id').append($("<option></option>").attr("value",value['id']).text(value['city_name'])); 
                    });
                }
                else {
                    $('#select2-chosen-2').html('No Matches Found');
                }
            }
        });
    });
</script>

<script type="text/javascript">

    $(".guest_email").on('change',function(){
    //  alert($(".guest_email").val());

        //  $("#modal_id").modal('show');

        var /* guest_email, */  guest_name, token, url, data;
        token = $('input[name=_token]').val();
        guest_email  = $('.guest_email').val();
  //          guest_name   = $('#guest').val();
        url   = '{{url('guest_check')}}';
        data  = {guest_email:guest_email /* ,guest_name: guest_name */  };
        $.ajax({
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            data: data,
            type: 'POST',
            datatype: 'JSON',
            success: function (resp) {

                
                $('#guest_name').empty();
                if(resp.data!='')
                { 
                    //  alert('Data Coming');

                    toastr.success('Email Already Found');
 
                        $('#guest_name').val(resp.data.firstname);
                        $('#mobile').val(resp.data.mobile_number);
                        $('#address').val(resp.data.address);
                        $('#country_code').val(resp.data.country_code);
                        $('#user_id').val(resp.data.id);
                        $("#add_new_cust").hide();
                }
                else {
                    //  alert('Data Not Coming');
                    //  $('#select2-chosen-2').html('No Matches Found');
                        $("#add_new_cust").show();       
                        $('#guest_name').val('');
                        $('#mobile').val('');
                        $('#address').val('');
                        $('#user_id').val('');

                }

               
        
            }
        });   


/*
        if(this.checked){

            $("#modal_id").modal('show');
            
        }

*/        
       
    });
/*
    $("#days").change(function(){

 
            var date = new Date($("#check_in").val());

            days = parseInt($("#days").val(), 10);

            alert(days);

            alert(date.setDate(date.getDate() + days));

        if(!isNaN(date.getTime())){
            date.setDate(date.getDate() + days);
            
            $("#check_out").val(date.toInputFormat());
        } else {
            alert("Invalid Date");  
        }            

            var date = new Date();
            alert(date.addDays(5));

            var dr = $("#days").val();



            var date = new Date(<?php echo date("Y"); ?>,2,dr);

            alert(date);

            var diff_date = From_date - date;

            alert(diff_date);

            var days = Math.floor(((diff_date % 31536000000) % 2628000000)/86400000);  

            alert(days);

            var d = Math.floor(date / 86400000);
            alert(d);

            var date_add = From_date + dr;

            alert (date_add);
            var dds = Math.floor(((date_add % 31536000000) % 2628000000)/86400000);
alert(dds)

            alert(dr);

            var df = new Date(dds);

            alert(df);

              $("#check_out").val(Date(dds));

        //  alert(d);
        
    });
*/

//js
 $("#mobile").intlTelInput({
           initialCountry: "my",
           /*  geoIpLookup: function(callback) {
               $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                 var countryCode = (resp && resp.country) ? resp.country : "";
                 callback();
               });
             },*/
           nationalMode: false,
           separateDialCode: true,
           allowDropdown: true,
           //onlyCountries: ['in','us'],
           //setCountry: ['in'],
           utilsScript: '{{ URL::asset("/assets/admin/base/js/utils.js") }}' // just for formatting/placeholders etc
       });
$( document ).ready(function() {
         var country_short = $('#country_short').val();
         $("#mobile").intlTelInput("setCountry",country_short );
           setTimeout(function() {
                   $('.alert').fadeOut('fast');
           }, 7500);
           
           $('.BDC_CaptchaImageDiv a').addClass('test');
           $('.test').css('visibility', 'hidden');
           $('#ContactCaptcha_SoundIcon').css('visibility', 'hidden');
       });    


    
</script>
@endsection

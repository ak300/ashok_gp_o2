<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use App\Model\booking_user_details;
use App\Model\booking_detail;
use App\Model\booked_room_details;
use App\Model\payment;
use App\Model\admin_customers;
use App\Model\booking_charge;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;


class Reports extends Controller
{

                
                public function index()
                {

                    //  dd('Ak');
                    if(!has_permission('admin/settings/general'))
                    {
                        return view('errors.404');
                    }
                    if (Auth::guest()){
                        return redirect()->guest('admin/login');
                    }
                    else{

//     VENDORS COUNT 
                        $vendor =  DB::select('SELECT count(id) as vendor_id from vendors_view where vendor_type = 1 GROUP BY DATE(created_date) order by DATE(created_date) asc');

                        //  dd($vendor);

                        $vendors_count_array = array();

                        $i = 0;

                        foreach($vendor as $vc)
                        {
                            $vendors_count_array[][] = $vc->vendor_id;
                        }

                        $vendor_count_encoded = json_encode($vendors_count_array,JSON_NUMERIC_CHECK);

                        $vendor_count_encoded = str_replace("\"", "", $vendor_count_encoded);

//      USERS COUNT

                        $users = DB::select('SELECT count(id) as user_id from admin_customers GROUP BY DATE(created_at) order by DATE(created_at) asc');

                        $users_count_array = array();

                        $i = 0;

                        foreach($users as $user)
                        {
                            $users_count_array[][] = $user->user_id;
                        }

                        $user_count_encoded = json_encode($users_count_array,JSON_NUMERIC_CHECK);

                        //  dd($user_count_encoded);

//      MONTHS COUNT
                        for($i=0; $i<=11; $i++)
                        {
                            $months[] = date("M-Y", strtotime( date("Y-m-01")."-$i months"));

                            $mon = date('m', strtotime( date( 'Y-m-01' )." -$i months"));                       
                        }

                        $months_encoded = json_encode($months,JSON_NUMERIC_CHECK);

                        return view('admin.reports.reports')->with('months_encoded',$months_encoded)
                                                            ->with('vendor_count_encoded',$vendor_count_encoded)
                                                            ->with('user_count_encoded',$user_count_encoded);
                    }
                }

            public function order()
            {
                if (Auth::guest())
                {
                    return redirect()->guest('admin/login');
                }
                if(!has_permission('reports/order'))
                {
                    return view('errors.404');
                }                
                else{
                    /*
                    $order_status = DB::table('order_status')->select('id','name')->orderBy('name', 'asc')->get();
                    SEOMeta::setTitle('Orders Reports - '.$this->site_name);
                    SEOMeta::setDescription('Orders Reports - '.$this->site_name);
                    */
                    $date_data = DB::table('booking_details')
                                            ->select(DB::Raw('MIN(check_in_date) as min_date, 
                                                MAX(check_out_date) as max_date'))
                                            ->get();

                    $payment_amount = DB::select('SELECT DATE(created_date) as date , count(id) as booking_id from booking_details GROUP BY DATE(created_date) order by DATE(created_date)'); 

                    //  print_r($payment_amount); exit;


                    $payment_amount_array = array();

                    $j = 0;

                    foreach($payment_amount as $pa)
                    {
                        $date = strtotime($pa->date).'000';
                        $payment_amount_array[][] = $date.','.$pa->booking_id;
                        $j++;
                    }

                    $payment_amount_encoded = json_encode($payment_amount_array,JSON_NUMERIC_CHECK);
                    $payment_amount_encoded = str_replace("\"", "", $payment_amount_encoded);

                    return view('admin.reports.orders.list')->with('date_data',$date_data[0])
                                                            ->with('payment_amount_encoded',$payment_amount_encoded);
                                                           

                }
            }  

            public function anyAjaxReportOrderList(Request $request)
            {
                $post_data = $request->all();

                //  $this->order_charts($post_data);

                $booking_details  = DB::table('booking_details')
                                    ->select(DB::Raw('sum(booking_charges.price) as booking_charges_amount'),
                                     'booking_details.check_in_date', 'booking_details.check_out_date',
                                     'booking_details.booking_status','admin_customers.firstname',
                                     'booking_status.name')
                                    ->leftJoin('booking_charges','booking_charges.booking_id','=','booking_details.id')
                                    ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                    ->leftJoin('booking_status','booking_status.id','booking_details.booking_status')
                                    ->leftJoin('booked_room','booked_room.booking_id','booking_details.id')
                                    ->groupBy('booking_details.id','admin_customers.firstname','booking_status.name');
                                    //  ->get();

                      //    print_r($booking_details); exit;

                return Datatables::of($booking_details)->addColumn('check_in_date', function ($booking_details) {
                    $data = date("M-d-Y, l h:i:a", strtotime($booking_details->check_in_date));
                    return $data;
                })
                ->addColumn('check_out_date', function ($booking_details) {
                    $data = date("M-d-Y, l h:i:a", strtotime($booking_details->check_out_date));
                    return $data;
                })
                ->addColumn('booking_status', function ($booking_details) {
      
                        if($booking_details->booking_status == 1):
                            $data = $booking_details->name;
                        elseif($booking_details->booking_status == 2):
                            $data = $booking_details->name;
                        elseif($booking_details->booking_status == 3):
                            $data = $booking_details->name;
                        elseif($booking_details->booking_status == 4):
                            $data = $booking_details->name;
                        elseif($booking_details->booking_status == 5):
                            $data = $booking_details->name;
                        elseif($booking_details->booking_status == 6):
                            $data = $booking_details->name;                            
                        endif;
                        return $data;
                }) 
                ->addColumn('firstname', function ($booking_details) {
                    return wordwrap(ucfirst($booking_details->firstname),20,'<br>');
                })                           
                // ->addColumn('tax_total', function ($booking_details) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$booking_details->tax_total;
                //     }
                //     else {
                //         return $booking_details->tax_total.getCurrency();
                //     }
                // })
                ->addColumn('booking_charges_amount', function ($booking_details) {
                    if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$booking_details->booking_charges_amount;
                    }
                    else {
                        return $booking_details->booking_charges_amount.getCurrency();
                    }
                })
                ->filter(function ($query) use ($request){

                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 
                    
                    if(!empty($fr) && !empty($t))
                    {
                       
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                             $condition1 = $condition." and booked_room.date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);
                                               
                    } 
                    // if ($request->has('order_status') != '')
                    // {
                    //     $order_status = Input::get('order_status');
                    //     $condition2   = $condition." and orders.order_status = ".$order_status;
                        
                    //     $query->whereRaw($condition2);
                    // }
                })
                ->make(true);
            }    
/*
            public function order_charts($post_data)
            {

            }
*/            
            public function customer_report()
            {
                if (Auth::guest())
                {
                    return redirect()->guest('admin/login');
                }
                if(!has_permission('reports/order'))
                {
                    return view('errors.404');
                }                
                else{

                    $date_data = DB::table('admin_customers')
                                            ->select(DB::Raw('MIN(created_at) as min_date, 
                                                MAX(created_at) as max_date'))
                                            ->get();
                    //  dd($date_data);
                    return view('admin.reports.users.list')->with('date_data',$date_data[0]);
                }
            }  

            public function anyAjaxCustomerReportList(Request $request)
            {
                $post_data = $request->all();
                
                $booking_details  = DB::table('admin_customers')
                                    ->select(DB::Raw('count(booking_details.id) as booking_count, sum(payments.paid_amount) as payments_amount'), 'admin_customers.id', 'admin_customers.firstname', 'admin_customers.email')
                                    ->join('booking_details','booking_details.customer_id','=','admin_customers.id')
                                    ->leftJoin('payments','payments.booking_id','=','booking_details.id')
                                    ->groupBy('admin_customers.id');

                return Datatables::of($booking_details)->addColumn('payments_amount', function ($booking_details) {
                    if ($booking_details->payments_amount == "") {
                      return '-';
                    } else if(getCurrencyPosition()->currency_side == 1) {
                        return getCurrency().$booking_details->payments_amount;
                    } else {
                        return $booking_details->payments_amount.getCurrency();
                    }
                })
                // ->addColumn('tax_total', function ($booking_details) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$booking_details->tax_total;
                //     }
                //     else {
                //         return $booking_details->tax_total.getCurrency();
                //     }
                // })
                ->filter(function ($query) use ($request){
                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 
                    
                    if(!empty($fr) && !empty($t))
                    {
                       
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and admin_customers.created_at BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);
                                               
                    }                    
                })
                ->make(true);
            }

            public function vendor_report()
            {
                if (Auth::guest())
                {
                    return redirect()->guest('admin/login');
                }
                if(!has_permission('reports/order'))
                {
                    return view('errors.404');
                }                
                else{

                    $date_data = DB::table('vendors_view')
                                            ->select(DB::Raw('MIN(created_date) as min_date, 
                                                MAX(created_date) as max_date'))
                                            ->where('vendor_type','=','1')
                                            ->get();
                    //  dd($date_data);
                    return view('admin.reports.vendors.list')->with('date_data',$date_data[0]);
                }
            }  

            public function anyAjaxVendorReportList(Request $request)
            {
                $post_data = $request->all();

                //  print_r($post_data); exit;
                
                $booking_details  = DB::table('vendors_view')
                                    ->select(DB::Raw('count(booking_details.id) as booking_count,
                                                      count(outlets.id) as property_count,
                                                      sum(payments.paid_amount) as payments_amount'
                                                    ),
                                            'vendors_view.email',
                                            'vendors_view.id',
                                            'vendors_view.name')
                                    ->leftJoin('booking_details','booking_details.vendor_id','=','vendors_view.id')
                                    ->leftJoin('payments','payments.booking_id','=','booking_details.id')
                                    ->leftjoin('outlets','outlets.vendor_id','=','vendors_view.id')
                                    ->where('vendors_view.vendor_type','=',1)
                                    ->groupBy('vendors_view.id','outlets.id','vendors_view.email','vendors_view.name')
                                    ->distinct();
                                    //  ->get();

                //  $oc = DB::table('outlets')->where('vendor_id','=',2)->count();


                                  
                // echo "<pre>";
                // print_r($oc);exit;

                return Datatables::of($booking_details)->addColumn('payments_amount', function ($booking_details) {
                    if ($booking_details->payments_amount == "") {
                      return '-';
                    } else if(getCurrencyPosition()->currency_side == 1) {
                        return getCurrency().$booking_details->payments_amount;
                    } else {
                        return $booking_details->payments_amount.getCurrency();
                    }
                })
                // ->addColumn('tax_total', function ($booking_details) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$booking_details->tax_total;
                //     }
                //     else {
                //         return $booking_details->tax_total.getCurrency();
                //     }
                // })
                ->filter(function ($query) use ($request){

                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and vendors_view.created_date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                })
                ->make(true);
            }

            public function outlet_report()
            {
                if (Auth::guest())
                {
                    return redirect()->guest('admin/login');
                }
                if(!has_permission('reports/order'))
                {
                    return view('errors.404');
                }                
                else{

                    $date_data = DB::table('vendors_view')
                                            ->select(DB::Raw('MIN(created_date) as min_date, 
                                                MAX(created_date) as max_date'))
                                            ->where('vendor_type','=','1')
                                            ->get();
                    //  dd($date_data);
                    return view('admin.reports.outlets.list')->with('date_data',$date_data[0]);
                }
            }  

            public function anyAjaxOutletReportList(Request $request)
            {
                $post_data = $request->all();

                //  print_r($post_data); exit;
                
                $booking_details  = DB::table('vendors_view')
                                    ->select(DB::Raw('count(booking_details.id) as booking_count,
                                                      count(outlets.id) as property_count,
                                                      sum(payments.paid_amount) as payments_amount'
                                                    ),
                                            'vendors_view.email',
                                            'vendors_view.id',
                                            'vendors_view.name')
                                    ->leftjoin('outlets','outlets.vendor_id','=','vendors_view.id')
                                    ->leftJoin('booking_details','booking_details.vendor_id','=','vendors_view.id')
                                    ->leftJoin('payments','payments.booking_id','=','booking_details.id')
                                    ->where('vendors_view.vendor_type','=',1)
                                    ->groupBy('vendors_view.id','outlets.id','vendors_view.email','vendors_view.name')
                                    ->distinct();
                                    //  ->get();
                                  
                // echo "<pre>";
                // print_r($booking_details);exit;

                return Datatables::of($booking_details)->addColumn('payments_amount', function ($booking_details) {
                    if ($booking_details->payments_amount == "") {
                      return '-';
                    } else if(getCurrencyPosition()->currency_side == 1) {
                        return getCurrency().$booking_details->payments_amount;
                    } else {
                        return $booking_details->payments_amount.getCurrency();
                    }
                })
                // ->addColumn('tax_total', function ($booking_details) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$booking_details->tax_total;
                //     }
                //     else {
                //         return $booking_details->tax_total.getCurrency();
                //     }
                // })
                ->filter(function ($query) use ($request){

                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and vendors_view.created_date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                })
                ->make(true);
            }

            public function statistics_report(Request $request)
            {
                if (Auth::guest())
                {
                    return redirect()->guest('admin/login');
                }
                if(!has_permission('reports/order'))
                {
                    return view('errors.404');
                } 
                else{

                    //  dd('Al');

                if(count($request->all())>0){  
                       $validation = Validator::make($request->all(), array(
                           'from' => 'required',
                           'to' => 'required',
                       ));
                       // process the validation
                       if ($validation->fails()) { 
                           return Redirect::back()->withInput()->withErrors($validation);
                       }
                       else
                        { 

                            $st = $request->from;
                            $ed = $request->to;

                            $gs = getStatisticsSummary($st,$ed);
                            //      BOOKING COUNT

                            $bs1 = booking_detail::where('booking_source',1)->count();
                            $bs2 = booking_detail::where('booking_source',2)->count();

                            $bs1_array = array();
                            $bs2_array = array();
                            $j = 0;

                            foreach($gs as $pa)
                            {
                                $date = strtotime($pa['date']).'000';
                                $bs1_array[][] = $date.','.$pa['bs1'];
                                $bs2_array[][] = $date.','.$pa['bs2'];
                                $j++;
                            }

                            $bs1_encoded = json_encode($bs1_array,JSON_NUMERIC_CHECK);
                            $bs1_encoded = str_replace("\"", "", $bs1_encoded);
                            $bs2_encoded = json_encode($bs2_array,JSON_NUMERIC_CHECK);
                            $bs2_encoded = str_replace("\"", "", $bs2_encoded); 
                        }  
                    }

                else
                {

                    $st = date('Y-m-01',strtotime('this month'));
                    $ed = date('Y-m-t',strtotime('this month'));                    

                     $gs = getStatisticsSummary($st,$ed);

                     // dd($gs);

                    $bs1 = booking_detail::where('booking_source',1)->count();
                    $bs2 = booking_detail::where('booking_source',2)->count();

                    //  dd($bs1);


                    


                    //      BOOKING COUNT

                    $bs1_array = array();
                    $bs2_array = array();
                    $j = 0;

                    foreach($gs as $pa)
                    {
                        $date = strtotime($pa['date']).'000';
                        $bs1_array[][] = $date.','.$pa['bs1'];
                        $bs2_array[][] = $date.','.$pa['bs2'];
                        $j++;
                    }

                    $bs1_encoded = json_encode($bs1_array,JSON_NUMERIC_CHECK);
                    $bs1_encoded = str_replace("\"", "", $bs1_encoded);
                    $bs2_encoded = json_encode($bs2_array,JSON_NUMERIC_CHECK);
                    $bs2_encoded = str_replace("\"", "", $bs2_encoded);   

                    //  dd($bs1_encoded);                                     

                }


                    return view('admin.reports.statistics.list')
                                    ->with('bs1_encoded',$bs1_encoded)
                                    ->with('bs2_encoded',$bs2_encoded)
                                    ->with('bs1',$bs1)
                                    ->with('bs2',$bs2)
                                    ->with('gs',$gs)
                                    ->with('st',$st)
                                    ->with('ed',$ed);
                }
            } 



            public function transaction_report()
            {
                if (Auth::guest())
                {
                    return redirect()->guest('admin/login');
                }
                if(!has_permission('reports/order'))
                {
                    return view('errors.404');
                }  
                else{

                    //  dd('Ak');
                    $charges_amount = DB::select('SELECT DATE(created_date) as date , sum(price) as booking_charges_amount from booking_charges GROUP BY DATE(created_date) order by DATE(created_date)');

                    $charges_amount_array = array();
                    $i = 0;

                    foreach($charges_amount as $ch)
                    {
                        $date = strtotime($ch->date).'000';
                        $charges_amount_array[][] = $date.','.$ch->booking_charges_amount;

                        $i++;   
                    }

                    $charges_amount_encoded = json_encode($charges_amount_array,JSON_NUMERIC_CHECK);
                    $charges_amount_encoded = str_replace("\"", "", $charges_amount_encoded);

                    //  dd($charges_amount_encoded);


                    $payment_amount = DB::select('SELECT DATE(created_date) as date , sum(paid_amount) as paid_amount from payments GROUP BY DATE(created_date) order by DATE(created_date)');    

                    $payment_amount_array = array();

                    $j = 0;

                    foreach($payment_amount as $pa)
                    {
                        $date = strtotime($pa->date).'000';
                        $payment_amount_array[][] = $date.','.$pa->paid_amount;
                        $j++;
                    }

                    $payment_amount_encoded = json_encode($payment_amount_array,JSON_NUMERIC_CHECK);
                    $payment_amount_encoded = str_replace("\"", "", $payment_amount_encoded);

                    $date_data = DB::table('booking_details')
                                    ->select(DB::Raw('MIN(check_in_date) as min_date, 
                                                      MAX(check_out_date) as max_date'))
                                    ->get();

                    //  dd($date_data);

                    return view('admin.reports.transaction.list')
                                    ->with('charges_amount_encoded',$charges_amount_encoded)
                                    ->with('payment_amount_encoded',$payment_amount_encoded)
                                    ->with('date_data',$date_data[0]);
                }
            }                                    

            /** Report Orderds List for Admin **/
            public function anyAjaxTransactionReportList(Request $request)
            {
                $post_data = $request->all();

                //  print_r('Ak'); exit;

                $orders  = DB::table('booking_details')
                                    ->select(
                                            'booking_details.id',
                                            'booking_details.charges',
                                            'booking_details.payments',
                                            'booking_details.booking_status',
                                            'booking_details.booking_random_id',
                                            'booking_details.check_out_date',
                                            'booking_details.check_in_date',
                                            'vendors_view.name'
                                            //  'admin_customers.firstname'
                                            )
                                    //  ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                    ->leftJoin('booked_room','booked_room.booking_id','booking_details.id')
                                    ->leftJoin('vendors_view','vendors_view.id','booking_details.vendor_id')
                                    ->groupBy('booking_details.id','vendors_view.name'/*'admin_customers.firstname'*/);
                //                     ->get();
                // print_r($orders);
                // exit;

                return Datatables::of($orders)->addColumn('date_start', function ($orders) {
                    $data = date("M-d-Y, l h:i:a", strtotime($orders->check_in_date));
                    return $data;
                })

                ->addColumn('booking_random_id', function ($orders) {
                                    
                            $html='<a href="'.URL::to("admin/showbooking/".$orders->id).'">'.$orders->booking_random_id.'</a>';
                                    
                            return $html;
                                        
                                    })
                                   

                ->addColumn('date_end', function ($orders) {
                    $data = date("M-d-Y, l h:i:a", strtotime($orders->check_out_date));
                    return $data;
                })
/*                
                ->addColumn('tax_total', function ($orders) {
                    if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$orders->tax_total;
                    }
                    else {
                        return $orders->tax_total.getCurrency();
                    }
                })
*/                
                ->addColumn('total_charges', function ($orders) {
                    if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency()." ".$orders->charges;
                    }
                    else {
                        return $orders->charges." ".getCurrency();
                    }
                })
                ->addColumn('total_payments', function ($orders) {
                    if($orders->payments == 0)
                    {
                        return '-';
                    }
                    else if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency()." ".$orders->payments;
                    }
                    else {
                        return $orders->payments." ".getCurrency();
                    }
                })  
                ->addColumn('due_amount', function ($orders) {

                    $bcharges = $orders->charges;

                    $bpayments = $orders->payments;



                    if($bcharges >= $bpayments)
                    {
                        //  print_r("if"); exit;
                       $due_amount = $bcharges - $bpayments;

                    }
                    else 
                    {
                        $due_amount = "";
                    }
                    
                    if($due_amount == 0)
                    {
                        return '-';
                    }
                    else if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency()." ".$due_amount;
                    }
                    else {
                        return $due_amount." ".getCurrency();
                    }
                })    
                ->addColumn('balance', function ($orders) {

                    $bcharges = $orders->charges;

                    $bpayments = $orders->payments;


                    if($bcharges >= $bpayments)
                    {
                        //  print_r("if"); exit;
                       $due_amount = "";

                    }
                    if($bcharges < $bpayments)
                    {
                        $due_amount = $bpayments - $bcharges;
                    }

                    if($due_amount == 0)
                    {
                        return '-';
                    }
                    else if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency()." ".$due_amount;
                    }
                    else {
                        return $due_amount." ".getCurrency();
                    }
                })                                             
                ->filter(function ($query) use ($request){
                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and booked_room.date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                })
                ->make(true);
            }
            public function booked_room_report()
            {
                if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }              
                else{

                    $property_id = Session::get('property_id');

                    // $date_data = DB::table('rooms')
                    //                         ->select(DB::Raw('MIN(created_at) as min_date, 
                    //                             MAX(created_at) as max_date'))
                    //                         ->where('rooms.property_id','=',$property_id)
                    //                         ->get();

                    $date = date('Y-m-d H:i:s');

                    $date_show = date("l, M-d-Y", strtotime($date));

                    $order_status = DB::table('booking_status')
                                            ->select('id','name')->orderBy('id', 'asc')->get();

                    return view('vendors.reports.booked_room.list')->with('date',$date)
                                                                   ->with('date_show',$date_show)
                                                                   ->with('order_status',$order_status);
                }
            }  

            //Managers Booked Room Reports
            public function managers_booked_room_report()
            {
                 if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                        }              
                else{

                    $property_id = Session::get('manager_outlet');

                    // $date_data = DB::table('rooms')
                    //                         ->select(DB::Raw('MIN(created_at) as min_date, 
                    //                             MAX(created_at) as max_date'))
                    //                         ->where('rooms.property_id','=',$property_id)
                    //                         ->get();

                    $date = date('Y-m-d H:i:s');

                    $date_show = date("l, M-d-Y", strtotime($date));

                    $order_status = DB::table('booking_status')
                                            ->select('id','name')->orderBy('id', 'asc')->get();

                    return view('managers.managers_reports.booked_rooms.list')->with('date',$date)
                                                                   ->with('date_show',$date_show)
                                                                   ->with('order_status',$order_status);
                }
            }  

            public function anyAjaxBookedRoomReportList(Request $request)
            {
                $post_data = $request->all();

                //  print_r($post_data); exit;

                $fr = $request->from;

                $frs = date('Y-m-d');

                //  print_r($frs);exit;


                 if (Session::get('manager_id')) {

                        $property_id = Session::get('manager_outlet');
                        $vendor_id = Session::get('manager_vendor');
                        
                    }                     

                $property_id = Session::get('property_id');
                $vendor_id = Session::get('vendor_id');                

                
                
                $booking_details = DB::table('booked_room')
                                ->select('booked_room.booking_status as order_status','booked_room.*',
                                    'rooms.id','booking_details.booking_status','admin_customers.firstname',
                                    'rooms.room_clean_status','rooms_infos.room_name','booking_status.name')
                                ->leftJoin('rooms','rooms.id','booked_room.room_id')
                                ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                ->leftJoin('booking_details','booking_details.id','=','booked_room.booking_id')
                                ->leftJoin('admin_customers','admin_customers.id','=','booking_details.customer_id')
                                ->leftJoin('booking_status','booking_status.id','booked_room.booking_status')
                                ->where('rooms.property_id','=',$property_id)
                                ->where('booked_room.date','=',$frs)
                                //  ->groupBy('rooms.id','rooms_infos.room_name')
                                ->distinct();
                                
                                //  ->get();
                //  print_r($booking_details);exit;
/*
                $booking_details  = DB::table('rooms')
                                    ->select('rooms.id','rooms_infos.room_name',
                                             'rooms.room_clean_status',
                                             'rooms.check_in_date',
                                             'rooms.check_out_date',
                                             'admin_customers.firstname',
                                             'booked_room.booking_status as order_status',
                                             'booking_status.name',
                                             'booking_status.color_code'
                                            )
                                    ->leftJoin('admin_customers','admin_customers.id','=','rooms.customer_id')
                                    ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                                    ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                    ->leftJoin('booking_info','booking_info.room_id','=','rooms.id')
                                    ->leftJoin('booking_status','booking_status.id','booked_room.booking_status')
                                    ->where('rooms.property_id','=',$property_id)
                                    ->orwhere('booked_room.date','=',$frs)
                                    // ->groupBy('rooms.id','booked_room.check_in_date','order_status',
                                    //           'booked_room.check_out_date','rooms_infos.room_name')
                                    ->distinct('rooms.id');
                                    //  ->get();
*/                                    
/*                                  
                 echo "<pre>";
                 print_r($booking_details);exit;
*/
                return Datatables::of($booking_details)
                ->addColumn('room_clean_status', function ($booking_details) {
      
                        if($booking_details->room_clean_status == 0):
                            $data = trans("messages.Need To Clean / Dirty");
                        elseif($booking_details->room_clean_status == 1):
                            $data = trans("messages.Cleaning Inprogress");  
                        elseif($booking_details->room_clean_status == 2):
                            $data = trans("messages.Clean");                            
                        endif;
                        return $data;
                })
                ->addColumn('order_status', function ($booking_details) {
      
                        if($booking_details->order_status == ""):
                            $data = $booking_details->name;     
                        elseif($booking_details->order_status == 1):
                            $data = $booking_details->name;
                        elseif($booking_details->order_status == 2):
                            $data = $booking_details->name; 
                        elseif($booking_details->order_status == 3):
                            $data = $booking_details->name;
                        elseif($booking_details->order_status == 4):
                            $data =$booking_details->name;                                               
                        elseif($booking_details->order_status == 5):
                            $data = $booking_details->name;                                                
                        endif;                        
                        return $data;
                })                               
                ->addColumn('firstname', function ($booking_details) {
                    return wordwrap(ucfirst($booking_details->firstname),20,'<br>');
                })                
                // ->addColumn('tax_total', function ($booking_details) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$booking_details->tax_total;
                //     }
                //     else {
                //         return $booking_details->tax_total.getCurrency();
                //     }
                // })
                 ->filter(function ($query) use ($request){
//  ->where('booked_room.date','=',$frs)
                    $condition = '1=1';
                    $fr = $request->from;
                    $frs = date('Y-m-d', strtotime($fr));
                    $order_status =  $request->order_status; 

                    $date = date('Y-m-d');
/*
                    $date_show = date("l, M-d-Y", strtotime($date));
                    if(!empty($fr))
                    {   
                            //  $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            //  $condition1 = $condition." and booked_room.date = ".$frs;
                            $query->where('booked_room.date','<=',$frs);                           
                    }
                    else
                    {
                            $query->where('booked_room.date','<=',$date);  
                    }
*/                    
                    if(!empty($order_status))                    
                    {
                        $order_status = Input::get('order_status');
                        $condition2   = $condition." and booking_details.booking_status = ".$order_status;

                        $query->whereRaw($condition2);
                    }
                 })
                ->make(true);
            }


            //Managers Booked room report list.
            public function anyManagersAjaxBookedRoomReportList(Request $request)
            {
                $post_data = $request->all();

                //  print_r($post_data); exit;

                $fr = $request->from;

                $frs = date('Y-m-d', strtotime($fr));

                //  print_r($frs);exit;

                $property_id = Session::get('property_id');
/*                
                $booking_details = DB::table('booked_room')
                                ->select('booked_room.booking_status as order_status','booked_room.*','rooms.id','booking_details.booking_status',
                                'admin_customers.firstname','rooms.room_clean_status','rooms_infos.room_name')
                                ->leftJoin('rooms','rooms.id','booked_room.room_id')
                                ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                ->leftJoin('booking_details','booking_details.id','=','booked_room.booking_id')
                                ->leftJoin('admin_customers','admin_customers.id','=','booking_details.customer_id')
                                ->where('rooms.property_id','=',$property_id)
                                //  ->where('booked_room.date','=',$frs)
                                // ->groupBy('rooms.id','rooms_infos.room_name'
                                //         ,'room_type.normal_price','room_type.discount_price')
                                //  ->distinct();
                                
                                ->get();
*/                                

                $booking_details  = DB::table('rooms')
                                    ->select('rooms.id','rooms_infos.room_name',
                                             'rooms.room_clean_status',
                                             'rooms.check_in_date',
                                             'rooms.check_out_date',
                                             'admin_customers.firstname',
                                             'booked_room.booking_status as order_status'
                                            )
                                    ->leftJoin('admin_customers','admin_customers.id','=','rooms.customer_id')
                                    ->leftJoin('booked_room','booked_room.room_id','=','rooms.id')
                                    ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                    ->leftJoin('booking_details','booking_details.rooms','=','rooms.id')
                                    ->where('rooms.property_id','=',$property_id)
                                    // ->groupBy('rooms.id','booked_room.check_in_date','order_status',
                                    //           'booked_room.check_out_date','rooms_infos.room_name')
                                    ->distinct('rooms.id');
                                      //    ->get();
                                  
                // echo "<pre>";
                // print_r($booking_details);exit;

                return Datatables::of($booking_details)
                ->addColumn('room_clean_status', function ($booking_details) {
      
                        if($booking_details->room_clean_status == 0):
                            $data = trans("messages.Need To Clean / Dirty");
                        elseif($booking_details->room_clean_status == 1):
                            $data = trans("messages.Cleaning Inprogress");  
                        elseif($booking_details->room_clean_status == 2):
                            $data = trans("messages.Clean");                            
                        endif;
                        return $data;
                })   
                ->addColumn('order_status', function ($booking_details) {
      
                        if($booking_details->order_status == ""):
                            $data = trans("messages.Empty");      
                        elseif($booking_details->order_status == 1):
                            $data = trans("messages.Walk In");
                        elseif($booking_details->order_status == 2):
                            $data = trans("messages.Check In");  
                        elseif($booking_details->order_status == 3):
                            $data = trans("messages.Checked Out");
                        elseif($booking_details->order_status == 4):
                            $data = trans("messages.Cancelled");                                                 
                        endif;
                        return $data;
                })                               
                ->addColumn('firstname', function ($booking_details) {
                    return wordwrap(ucfirst($booking_details->firstname),20,'<br>');
                })                
                // ->addColumn('tax_total', function ($booking_details) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$booking_details->tax_total;
                //     }
                //     else {
                //         return $booking_details->tax_total.getCurrency();
                //     }
                // })
                 ->filter(function ($query) use ($request){
//  ->where('booked_room.date','=',$frs)
                    $condition = '1=1';
                    $fr = $request->from;
                    $frs = date('Y-m-d', strtotime($fr));
                    $order_status =  $request->order_status; 

                    $date = date('Y-m-d');
/*
                    $date_show = date("l, M-d-Y", strtotime($date));
                    if(!empty($fr))
                    {   
                            //  $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            //  $condition1 = $condition." and booked_room.date = ".$frs;
                            $query->where('booked_room.date','<=',$frs);                           
                    }
                    else
                    {
                            $query->where('booked_room.date','<=',$date);  
                    }
*/                    
                    if(!empty($order_status))                    
                    {
                        $order_status = Input::get('order_status');
                        $condition2   = $condition." and booking_details.booking_status = ".$order_status;

                        $query->whereRaw($condition2);
                    }
                 })
                ->make(true);
            }

            public function room_report()
            {
                if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }              
                else{

                    $property_id = Session::get('property_id');

                    $date_data = DB::table('rooms')
                                            ->select(DB::Raw('MIN(created_at) as min_date, 
                                                MAX(created_at) as max_date'))
                                            ->where('rooms.property_id','=',$property_id)
                                            ->get();
                    //  dd($date_data);
                    return view('vendors.reports.rooms.list')->with('date_data',$date_data[0]);
                }
            }  


            //Managers room reports
            public function managers_room_report()
            {
                // print_r('expression');exit;
                 if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    }

                else{

                    $property_id = Session::get('manager_outlet');

                    $date_data = DB::table('rooms')
                                            ->select(DB::Raw('MIN(created_at) as min_date, 
                                                MAX(created_at) as max_date'))
                                            ->where('rooms.property_id','=',$property_id)
                                            ->get();
                      // dd($date_data[0]);
                    return view('managers.managers_reports.rooms.list')->with('date_data',$date_data[0]);
                }
            }

            public function anyAjaxRoomReportList(Request $request)
            {
                $post_data = $request->all();

                //  print_r($post_data); exit;

                $property_id = Session::get('property_id');
                
                $booking_details  = DB::table('rooms')
                                    ->select(DB::Raw('count(booking_details.id) as booking_count,
                                                      sum(payments.paid_amount) as payments_amount,
                                                      sum(booking_details.no_of_days) as stay_length'
                                                    ),
                                            'rooms.id',
                                            'room_type.normal_price',
                                            'room_type.discount_price',
                                            'rooms_infos.room_name')
                                    ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                    ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                    ->leftJoin('booking_details','booking_details.rooms','=','rooms.id')
                                    ->leftJoin('payments','payments.booking_id','=','booking_details.id')
                                    ->where('rooms.property_id','=',$property_id)
                                    ->groupBy('rooms.id','rooms_infos.room_name'
                                        ,'room_type.normal_price','room_type.discount_price');
                                    //  ->distinct();
                                    //  ->get();
                                  
                // echo "<pre>";
                // print_r($booking_details);exit;

                return Datatables::of($booking_details)->addColumn('payments_amount', function ($booking_details) {
                    if ($booking_details->payments_amount == "") {
                      return '-';
                    } else if(getCurrencyPosition()->currency_side == 1) {
                        return getCurrency().$booking_details->payments_amount;
                    } else {
                        return $booking_details->payments_amount.getCurrency();
                    }
                })
                ->addColumn('stay_length', function ($booking_details) {
                                    if ($booking_details->stay_length == "") {
                                      return '-';
                                    }
                                    else if($booking_details->stay_length == 1){
                                        return $booking_details->stay_length." Day";
                                    }                                    
                                     else {
                                        return $booking_details->stay_length." Days";
                                    }
                                })                
                // ->addColumn('tax_total', function ($booking_details) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$booking_details->tax_total;
                //     }
                //     else {
                //         return $booking_details->tax_total.getCurrency();
                //     }
                // })
                ->filter(function ($query) use ($request){

                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and rooms.created_at BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                })
                ->make(true);
            }


            // Managers Room Reports
            public function anyAjaxManagersRoomReportList(Request $request)
            {
                $post_data = $request->all();

                //  print_r($post_data); exit;

               $property_id = Session::get('manager_outlet');
                
                $booking_details  = DB::table('rooms')
                                    ->select(DB::Raw('count(booking_details.id) as booking_count,
                                                      sum(payments.paid_amount) as payments_amount,
                                                      sum(booking_details.no_of_days) as stay_length'
                                                    ),
                                            'rooms.id',
                                            'room_type.normal_price',
                                            'room_type.discount_price',
                                            'rooms_infos.room_name')
                                    ->leftJoin('room_type','room_type.id','rooms.room_type_id')
                                    ->leftJoin('rooms_infos','rooms_infos.id','rooms.id')
                                    ->leftJoin('booking_details','booking_details.rooms','=','rooms.id')
                                    ->leftJoin('payments','payments.booking_id','=','booking_details.id')
                                    ->where('rooms.property_id','=',$property_id)
                                    ->groupBy('rooms.id','rooms_infos.room_name'
                                        ,'room_type.normal_price','room_type.discount_price');
                                    //  ->distinct();
                                    //  ->get();
                                  
                // echo "<pre>";
                // print_r($booking_details);exit;

                return Datatables::of($booking_details)->addColumn('payments_amount', function ($booking_details) {
                    if ($booking_details->payments_amount == "") {
                      return '-';
                    } else if(getCurrencyPosition()->currency_side == 1) {
                        return getCurrency().$booking_details->payments_amount;
                    } else {
                        return $booking_details->payments_amount.getCurrency();
                    }
                })
                ->addColumn('stay_length', function ($booking_details) {
                                    if ($booking_details->stay_length == "") {
                                      return '-';
                                    }
                                    else if($booking_details->stay_length == 1){
                                        return $booking_details->stay_length." Day";
                                    }                                    
                                     else {
                                        return $booking_details->stay_length." Days";
                                    }
                                })                
                // ->addColumn('tax_total', function ($booking_details) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$booking_details->tax_total;
                //     }
                //     else {
                //         return $booking_details->tax_total.getCurrency();
                //     }
                // })
                ->filter(function ($query) use ($request){

                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and rooms.created_at BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                })
                ->make(true);
            }


            public function vendors_staff(Request $request)
            {
                if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }
                else{


                if(count($request->all())>0){  
                       $validation = Validator::make($request->all(), array(
                           'from' => 'required',
                           'to' => 'required',
                       ));
                       // process the validation
                       if ($validation->fails()) { 
                           return Redirect::back()->withInput()->withErrors($validation);
                       }
                       else
                        { 

                            $st = $request->from;
                            $ed = $request->to;
                            $manager_id = $request->outlet_managers;

                            //  dd($request->all());

                            $gs = getStaffReportSummary($st,$ed,$manager_id);                           

                            //      BOOKING COUNT

                            $booking_count_array = array();

                            $j = 0;

                            foreach($gs as $pa)
                            {
                                $date = strtotime($pa['date']).'000';
                                $booking_count_array[][] = $date.','.$pa['booking_count'];
                                $j++;
                            }

                            $booking_count_encoded = json_encode($booking_count_array,JSON_NUMERIC_CHECK);
                            $booking_count_encoded = str_replace("\"", "", $booking_count_encoded);

                            //      CHARGES 

                            $charges_amount_array = array();
                            $i = 0;
                            foreach($gs as $rch)
                            {
                                $date = strtotime($rch['date']).'000';
                                $charges_amount_array[][] = $date.','.$rch['booking_charges'];

                                $i++;   
                            }

                            $charges_amount_encoded = json_encode($charges_amount_array,JSON_NUMERIC_CHECK);
                            $charges_amount_encoded = str_replace("\"", "", $charges_amount_encoded);

                            //  dd($charges_amount_encoded);

                            //      PAYMENTS 

                            $payment_amount_array = array();

                            $j = 0;

                            foreach($gs as $pa)
                            {
                                $date = strtotime($pa['date']).'000';
                                $payment_amount_array[][] = $date.','.$pa['payments'];
                                $j++;
                            }

                            $payment_amount_encoded = json_encode($payment_amount_array,JSON_NUMERIC_CHECK);
                            $payment_amount_encoded = str_replace("\"", "", $payment_amount_encoded);

                            //  dd($payment_amount_encoded);
                        }
                    }

                else
                {

                    $st = date('Y-m-01',strtotime('this month'));
                    $ed = date('Y-m-t',strtotime('this month'));

                    $vendor_id = Session::get('vendor_id');

                    $property_id = Session::get('property_id');                    

                    $outlet_managers = DB::table('outlet_managers')
                                            ->select('outlet_managers.id','outlet_managers.first_name')
                                            ->leftJoin('vendors_infos','vendors_infos.vendors_view_id','=','outlet_managers.vendor_id')
                                            ->leftJoin('outlets','outlets.id','=','outlet_managers.outlet_id')
                                            ->leftJoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                            ->where('outlet_managers.outlet_id','=',$property_id)
                                            ->where('outlet_managers.vendor_id','=',$vendor_id)
                                            ->first();

                    //  dd($outlet_managers);

                    if($outlet_managers == "")
                    {
                        //  dd('Ak');

                        Session::flash('managers','Add Staffs First');

                        //  Session::flash('message', trans('messages.Invalid Request'));

                        return Redirect::to('vendor/outletmanagers');
                    }

                    $manager_id = $outlet_managers->id;
                    //  dd($outlet_managers);
                    $gs = getStaffReportSummary($st,$ed,$manager_id);

                    //   dd($gs);

                    //      BOOKING COUNT

                    $booking_count_array = array();
                    $checked_in_array = array();
                    $pending_array = array();

                    $j = 0;

                    foreach($gs as $pa)
                    {
                        $date = strtotime($pa['date']).'000';
                        $booking_count_array[][] = $date.','.$pa['booking_count'];
                        $checked_in_array[][] = $date.','.$pa['checked_in'];
                        $pending_array[][] = $date.','.$pa['pending'];
                        $j++;
                    }

                    $booking_count_encoded = json_encode($booking_count_array,JSON_NUMERIC_CHECK);
                    $booking_count_encoded = str_replace("\"", "", $booking_count_encoded);
                    $checked_in_encoded = json_encode($checked_in_array,JSON_NUMERIC_CHECK);
                    $checked_in_encoded = str_replace("\"", "", $checked_in_encoded);
                    $pending_encoded = json_encode($pending_array,JSON_NUMERIC_CHECK);
                    $pending_encoded = str_replace("\"", "", $pending_encoded);                                        

                    //      CHARGES 

                    $charges_amount_array = array();
                    $i = 0;
                    foreach($gs as $rch)
                    {
                        $date = strtotime($rch['date']).'000';
                        $charges_amount_array[][] = $date.','.$rch['booking_charges'];

                        $i++;   
                    }

                    $charges_amount_encoded = json_encode($charges_amount_array,JSON_NUMERIC_CHECK);
                    $charges_amount_encoded = str_replace("\"", "", $charges_amount_encoded);

                    //  dd($charges_amount_encoded);

                    //      PAYMENTS 

                    $payment_amount_array = array();

                    $j = 0;

                    foreach($gs as $pa)
                    {
                        $date = strtotime($pa['date']).'000';
                        $payment_amount_array[][] = $date.','.$pa['payments'];
                        $j++;
                    }

                    $payment_amount_encoded = json_encode($payment_amount_array,JSON_NUMERIC_CHECK);
                    $payment_amount_encoded = str_replace("\"", "", $payment_amount_encoded);

                    //  dd($payment_amount_encoded);

                }

                    $vendor_id = Session::get('vendor_id');

                    $property_id = Session::get('property_id');                 

                    $outlet_managers = DB::table('outlet_managers')
                                            ->select('outlet_managers.id','outlet_managers.first_name')
                                            ->leftJoin('vendors_infos','vendors_infos.vendors_view_id','=','outlet_managers.vendor_id')
                                            ->leftJoin('outlets','outlets.id','=','outlet_managers.outlet_id')
                                            ->where('outlet_managers.outlet_id','=',$property_id)
                                            ->where('outlet_managers.vendor_id','=',$vendor_id)
                                            ->get();                

                    return view('vendors.reports.staffs.list')
                                    ->with('booking_count_encoded',$booking_count_encoded)
                                    ->with('charges_amount_encoded',$charges_amount_encoded)
                                    ->with('payment_amount_encoded',$payment_amount_encoded)
                                    ->with('outlet_managers',$outlet_managers)
                                    ->with('manager_id',$manager_id)
                                    //  ->with('checked_in_encoded',$checked_in_encoded)
                                    //  ->with('pending_encoded',$pending_encoded)
                                    ->with('gs',$gs)
                                    ->with('st',$st)
                                    ->with('ed',$ed);
                }
            } 


            public function staff_date_reports($d,$manager_id)
            {
                //  dd($d);

                if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }
                else
                {
                
                    $date = $d;

                    //dd($date);

                 if (Session::get('manager_id')) {

                        $property_id = Session::get('manager_outlet');
                        $vendor_id = Session::get('manager_vendor');
                        
                    }                     

                    $property_id = Session::get('property_id');
                    $vendor_id = Session::get('vendor_id');

                    //  dd($vendor_id);

                    $booking_details = DB::table('booking_details')
                                        ->select(DB::RAW('count(booking_details.id) as booking_count'), 'admin_customers.firstname','booking_details.*','booking_info.*','booking_status.*')
                                        ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                                        ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                        ->leftJoin('booking_status','booking_status.id','booking_details.booking_status')
                                        //  ->where('booking_details.vendor_id','=',$vendor_id)
                                        ->where('booking_details.created_date',$d)
                                        ->where('booking_details.outlet_id',$property_id)
                                        ->where('booking_details.manager_id',$manager_id)
                                        ->groupBy('booking_details.id','admin_customers.firstname',
                                            'booking_info.id','booking_status.id')
                                        ->get();

                          //    dd($booking_details);
                    

                    $booking_charges = DB::table('booking_charges')
                                         ->selectRaw('sum(price) as booking_charge')
                                         ->where('created_date',$d)
                                         ->where('property_id',$property_id)
                                         ->where('manager_id',$manager_id)
                                         // ->where('created_by',$vendor_id)
                                         ->get();
                    //  dd($booking_charges);

                    $payments = DB::table('payments')
                                         ->selectRaw('sum(paid_amount) as paid_amount')
                                         ->where('created_date',$d)
                                         ->where('manager_id',$manager_id)
                                         // ->where('created_by',$vendor_id)
                                         ->where('property_id',$property_id)
                                         ->get();

                    $payment_list = DB::table('payments')
                                    ->select('payments.id as payment_id','payments.active_status as payment_status','payments.*','admin_customers.*','booking_details.booking_random_id')
                                    ->leftJoin('admin_customers','admin_customers.id','payments.customer_id')
                                    ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                    //  ->where('payments.vendor_id','=',$vendor_id)
                                    ->where('payments.property_id','=',$property_id)
                                    ->where('payments.manager_id',$manager_id)
                                    ->where('payments.created_date',$d)
                                    ->orderBy('payments.id', 'asc')
                                    ->get(); 

                    //  dd($payment_list);

                    $charges_list = DB::table('booking_charges')
                                                ->select('booking_charges.id as bc_id','booking_charges.notes as bs_notes','booking_charges.active_status as booking_status','booking_charges.*','admin_customers.*','booking_details.booking_random_id')                                       
                                                ->leftJoin('booking_details','booking_details.id','booking_charges.booking_id')
                                                ->leftJoin('admin_customers','admin_customers.id','booking_charges.customer_id')
                                                //  ->where('booking_charges.created_by','=',$vendor_id)
                                                ->where('booking_charges.property_id','=',$property_id)    
                                                ->where('booking_charges.created_date',$d)
                                                ->where('booking_charges.manager_id',$manager_id)
                                                ->orderBy('booking_charges.id','asc')
                                                ->get();

                      //    dd($charges_list);


                    return view('vendors.reports.staffs.show')->with('booking_details',$booking_details)
                                                              ->with('charges_list',$charges_list)
                                                              ->with('payment_list',$payment_list)
                                                              ->with('booking_charges',$booking_charges[0])
                                                              ->with('payments',$payments[0])
                                                              ->with('date',$date);
                }
                //  dd($booking_charges);
            }


            public function vendors_order(Request $request)
            {
                if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }
                else{


                if(count($request->all())>0){  
                       $validation = Validator::make($request->all(), array(
                           'from' => 'required',
                           'to' => 'required',
                       ));
                       // process the validation
                       if ($validation->fails()) { 
                           return Redirect::back()->withInput()->withErrors($validation);
                       }
                       else
                        { 

                            $st = $request->from;
                            $ed = $request->to;

                            $gs = getReportSummary($st,$ed);

                            //      BOOKING COUNT

                            $booking_count_array = array();

                            $j = 0;

                            foreach($gs as $pa)
                            {
                                $date = strtotime($pa['date']).'000';
                                $booking_count_array[][] = $date.','.$pa['booking_count'];
                                $j++;
                            }

                            $booking_count_encoded = json_encode($booking_count_array,JSON_NUMERIC_CHECK);
                            $booking_count_encoded = str_replace("\"", "", $booking_count_encoded);

                            //      CHARGES 

                            $charges_amount_array = array();
                            $i = 0;
                            foreach($gs as $rch)
                            {
                                $date = strtotime($rch['date']).'000';
                                $charges_amount_array[][] = $date.','.$rch['booking_charges'];

                                $i++;   
                            }

                            $charges_amount_encoded = json_encode($charges_amount_array,JSON_NUMERIC_CHECK);
                            $charges_amount_encoded = str_replace("\"", "", $charges_amount_encoded);

                            //  dd($charges_amount_encoded);

                            //      PAYMENTS 

                            $payment_amount_array = array();

                            $j = 0;

                            foreach($gs as $pa)
                            {
                                $date = strtotime($pa['date']).'000';
                                $payment_amount_array[][] = $date.','.$pa['payments'];
                                $j++;
                            }

                            $payment_amount_encoded = json_encode($payment_amount_array,JSON_NUMERIC_CHECK);
                            $payment_amount_encoded = str_replace("\"", "", $payment_amount_encoded);

                            //  dd($payment_amount_encoded);
                        }
                    }

                else
                {

                    $st = date('Y-m-01',strtotime('this month'));
                    $ed = date('Y-m-t',strtotime('this month'));                    

                    $gs = getReportSummary($st,$ed);

                    //   dd($gs);


                    //      BOOKING COUNT

                    $booking_count_array = array();
                    $checked_in_array = array();
                    $pending_array = array();

                    $j = 0;

                    foreach($gs as $pa)
                    {
                        $date = strtotime($pa['date']).'000';
                        $booking_count_array[][] = $date.','.$pa['booking_count'];
                        $checked_in_array[][] = $date.','.$pa['checked_in'];
                        $pending_array[][] = $date.','.$pa['pending'];
                        $j++;
                    }

                    $booking_count_encoded = json_encode($booking_count_array,JSON_NUMERIC_CHECK);
                    $booking_count_encoded = str_replace("\"", "", $booking_count_encoded);
                    $checked_in_encoded = json_encode($checked_in_array,JSON_NUMERIC_CHECK);
                    $checked_in_encoded = str_replace("\"", "", $checked_in_encoded);
                    $pending_encoded = json_encode($pending_array,JSON_NUMERIC_CHECK);
                    $pending_encoded = str_replace("\"", "", $pending_encoded);                                        

                    //      CHARGES 

                    $charges_amount_array = array();
                    $i = 0;
                    foreach($gs as $rch)
                    {
                        $date = strtotime($rch['date']).'000';
                        $charges_amount_array[][] = $date.','.$rch['booking_charges'];

                        $i++;   
                    }

                    $charges_amount_encoded = json_encode($charges_amount_array,JSON_NUMERIC_CHECK);
                    $charges_amount_encoded = str_replace("\"", "", $charges_amount_encoded);

                    //  dd($charges_amount_encoded);

                    //      PAYMENTS 

                    $payment_amount_array = array();

                    $j = 0;

                    foreach($gs as $pa)
                    {
                        $date = strtotime($pa['date']).'000';
                        $payment_amount_array[][] = $date.','.$pa['payments'];
                        $j++;
                    }

                    $payment_amount_encoded = json_encode($payment_amount_array,JSON_NUMERIC_CHECK);
                    $payment_amount_encoded = str_replace("\"", "", $payment_amount_encoded);

                    //  dd($payment_amount_encoded);

                }

                    return view('vendors.reports.orders.list')
                                    ->with('booking_count_encoded',$booking_count_encoded)
                                    ->with('charges_amount_encoded',$charges_amount_encoded)
                                    ->with('payment_amount_encoded',$payment_amount_encoded)
                                    //  ->with('checked_in_encoded',$checked_in_encoded)
                                    //  ->with('pending_encoded',$pending_encoded)
                                    ->with('gs',$gs)
                                    ->with('st',$st)
                                    ->with('ed',$ed);
                }
            } 



            public function managers_order(Request $request)
            {
                // print_r('expression');exit;
                if (!Session::get('manager_id')) {
                        return redirect()->guest('managers/login');
                    } 

                else{
                     // print_r($request->all());exit;

                if(count($request->all())>0){  
                       $validation = Validator::make($request->all(), array(
                           'from' => 'required',
                           'to' => 'required',
                       ));

                       // process the validation
                       if ($validation->fails()) { 
                           return Redirect::back()->withInput()->withErrors($validation);
                       }
                       else
                        { 

                            $st = $request->from;
                            $ed = $request->to;

                            $gs = getReportSummary($st,$ed);

                            //  dd($gs);

                            //      BOOKING COUNT

                            $booking_count_array = array();

                            $j = 0;

                            foreach($gs as $pa)
                            {
                                $date = strtotime($pa['date']).'000';
                                $booking_count_array[][] = $date.','.$pa['booking_count'];
                                $j++;
                            }

                            $booking_count_encoded = json_encode($booking_count_array,JSON_NUMERIC_CHECK);
                            $booking_count_encoded = str_replace("\"", "", $booking_count_encoded);

                            //      CHARGES 

                            $charges_amount_array = array();
                            $i = 0;
                            foreach($gs as $rch)
                            {
                                $date = strtotime($rch['date']).'000';
                                $charges_amount_array[][] = $date.','.$rch['booking_charges'];

                                $i++;   
                            }

                            $charges_amount_encoded = json_encode($charges_amount_array,JSON_NUMERIC_CHECK);
                            $charges_amount_encoded = str_replace("\"", "", $charges_amount_encoded);

                            //  dd($charges_amount_encoded);

                            //      PAYMENTS 

                            $payment_amount_array = array();

                            $j = 0;

                            foreach($gs as $pa)
                            {
                                $date = strtotime($pa['date']).'000';
                                $payment_amount_array[][] = $date.','.$pa['payments'];
                                $j++;
                            }

                            $payment_amount_encoded = json_encode($payment_amount_array,JSON_NUMERIC_CHECK);
                            $payment_amount_encoded = str_replace("\"", "", $payment_amount_encoded);

                            //  dd($payment_amount_encoded);
                        }
                    }

                else
                {

                    $st = date('Y-m-01',strtotime('this month'));
                    $ed = date('Y-m-t',strtotime('this month'));                    

                    $gs = getReportSummary($st,$ed);


                    //      BOOKING COUNT

                    $booking_count_array = array();

                    $j = 0;

                    foreach($gs as $pa)
                    {
                        $date = strtotime($pa['date']).'000';
                        $booking_count_array[][] = $date.','.$pa['booking_count'];
                        $j++;
                    }

                    $booking_count_encoded = json_encode($booking_count_array,JSON_NUMERIC_CHECK);
                    $booking_count_encoded = str_replace("\"", "", $booking_count_encoded);

                    //      CHARGES 

                    $charges_amount_array = array();
                    $i = 0;
                    foreach($gs as $rch)
                    {
                        $date = strtotime($rch['date']).'000';
                        $charges_amount_array[][] = $date.','.$rch['booking_charges'];

                        $i++;   
                    }

                    $charges_amount_encoded = json_encode($charges_amount_array,JSON_NUMERIC_CHECK);
                    $charges_amount_encoded = str_replace("\"", "", $charges_amount_encoded);

                    //  dd($charges_amount_encoded);

                    //      PAYMENTS 

                    $payment_amount_array = array();

                    $j = 0;

                    foreach($gs as $pa)
                    {
                        $date = strtotime($pa['date']).'000';
                        $payment_amount_array[][] = $date.','.$pa['payments'];
                        $j++;
                    }

                    $payment_amount_encoded = json_encode($payment_amount_array,JSON_NUMERIC_CHECK);
                    $payment_amount_encoded = str_replace("\"", "", $payment_amount_encoded);

                    //  dd($payment_amount_encoded);

                }

                    return view('managers.managers_reports.list')
                                    ->with('booking_count_encoded',$booking_count_encoded)
                                    ->with('charges_amount_encoded',$charges_amount_encoded)
                                    ->with('payment_amount_encoded',$payment_amount_encoded)
                                    ->with('gs',$gs)
                                    ->with('st',$st)
                                    ->with('ed',$ed);
                }
            }                                   

/*
            public function vendors_reports_order(Request $request)
            {
                if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }
                else{

                    $property_id = Session::get('property_id');

                    $charges_amount = DB::select('SELECT DATE(created_date) as date , sum(price) as booking_charges_amount from booking_charges where property_id = '.$property_id.' GROUP BY DATE(created_date) order by DATE(created_date)');

                    $charges_amount_array = array();
                    $i = 0;

                    foreach($charges_amount as $ch)
                    {
                        $date = strtotime($ch->date).'000';
                        $charges_amount_array[][] = $date.','.$ch->booking_charges_amount;

                        $i++;   
                    }

                    $charges_amount_encoded = json_encode($charges_amount_array,JSON_NUMERIC_CHECK);
                    $charges_amount_encoded = str_replace("\"", "", $charges_amount_encoded);

                    //  dd($charges_amount_encoded);


                    $payment_amount = DB::select('SELECT DATE(created_date) as date , sum(paid_amount) as paid_amount from payments where property_id = '.$property_id.' GROUP BY DATE(created_date) order by DATE(created_date)');    

                    $payment_amount_array = array();

                    $j = 0;

                    foreach($payment_amount as $pa)
                    {
                        $date = strtotime($pa->date).'000';
                        $payment_amount_array[][] = $date.','.$pa->paid_amount;
                        $j++;
                    }

                    $payment_amount_encoded = json_encode($payment_amount_array,JSON_NUMERIC_CHECK);
                    $payment_amount_encoded = str_replace("\"", "", $payment_amount_encoded);

                    $vendor_id = Session::get('vendor_id');

                    $st = $request->from;
                    $ed = $request->to;

                    $gs = getReportSummary($st,$ed);

                    // print_r($gs);
                    // exit;
                    
                    return view('vendors.reports.orders.list')
                                    ->with('charges_amount_encoded',$charges_amount_encoded)
                                    ->with('payment_amount_encoded',$payment_amount_encoded)
                                    ->with('gs',$gs);
                }
            }   
*/

            /** Report Orderds List for Vendors **/
            public function anyAjaxVendorReportOrderList(Request $request)
            {
                // if(Session::get('vendor_type') == 2)
                // {
                //     $vendor_id = Session::get('staffs_under_by');
                // }
                // else
                // {
                //     $vendor_id = Session::get('vendor_id');
                // }
                $post_data = $request->all();

                $property_id = Session::get('property_id');

                $vendor_id = Session::get('vendor_id');


                $orders  = DB::table('booking_details')
                                    ->select(DB::Raw('sum(payments.paid_amount) as payments_amount,
                                                      sum(booking_charges.price) as booking_charges_amount'
                                                    ),
                                            'booking_details.booking_random_id',
                                            'booking_details.check_out_date',
                                            'booking_details.check_in_date',
                                            'admin_customers.firstname')
                                    ->leftJoin('booking_charges','booking_charges.booking_id','=','booking_details.id')
                                    ->leftJoin('payments','payments.booking_id','=','booking_details.id')
                                    ->leftJoin('booked_room','booked_room.booking_id','booking_details.id')
                                    ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                    ->where('booking_details.outlet_id','=',$property_id)
                                    ->where('booking_details.vendor_id','=',$vendor_id)
                                    ->groupBy('booking_details.id','admin_customers.firstname');
                                    //  ->get();

                                    // print_r($orders);
                                    // exit;

                return Datatables::of($orders)->addColumn('date_start', function ($orders) {
                    $data = date("M-d-Y, l h:i:a", strtotime($orders->check_in_date));
                    return $data;
                })
                ->addColumn('date_end', function ($orders) {
                    $data = date("M-d-Y, l h:i:a", strtotime($orders->check_out_date));
                    return $data;
                })
/*                
                ->addColumn('tax_total', function ($orders) {
                    if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$orders->tax_total;
                    }
                    else {
                        return $orders->tax_total.getCurrency();
                    }
                })
*/                
                ->addColumn('total_charges', function ($orders) {
                    if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$orders->booking_charges_amount;
                    }
                    else {
                        return $orders->booking_charges_amount.getCurrency();
                    }
                })
                ->addColumn('total_payments', function ($orders) {
                    if($orders->payments_amount == "")
                    {
                        return '';
                    }
                    else if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$orders->payments_amount;
                    }
                    else {
                        return $orders->payments_amount.getCurrency();
                    }
                })  
                ->addColumn('due_amount', function ($orders) {

                    $due_amount = $orders->booking_charges_amount - $orders->payments_amount;

                    if($due_amount == 0)
                    {
                        return '';
                    }
                    else if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$due_amount;
                    }
                    else {
                        return $due_amount.getCurrency();
                    }
                })                               
                ->filter(function ($query) use ($request){
                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and booked_room.date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                
                    /*
                    if ($request->has('from') != '' && $request->has('to') != '')
                    {
                        $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                        $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                        $condition1 = $condition." and orders.created_date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";

                        $query->whereRaw($condition1);
                    }
                    if ($request->has('order_status') != '')
                    {
                        $order_status = Input::get('order_status');
                        $condition2   = $condition." and orders.order_status = ".$order_status;

                        $query->whereRaw($condition2);
                    }
                    if ($request->has('group_by'))
                    {
                        $group_by = ($request->get('group_by') != '')?$request->get('group_by'):1;
                        if($group_by == 1)
                            $start_date = " date_trunc('day', orders.created_date) AS date_start, ";
                        else if($group_by == 2)
                            $start_date = " date_trunc('week', orders.created_date) AS date_start, ";
                        else if($group_by == 3)
                            $start_date = " date_trunc('month', orders.created_date) AS date_start, ";
                        else if($group_by == 4)
                            $start_date = " date_trunc('year', orders.created_date) AS date_start, ";
                        $query->selectRaw($start_date.' MAX(orders.created_date) AS date_end')->groupBy('date_start');
                    }
                    */
                })
                ->make(true);
            }

                        /** Report Orderds List for Vendors **/
            public function anyAjaxManagerReportOrderList(Request $request)
            {
                // if(Session::get('vendor_type') == 2)
                // {
                //     $vendor_id = Session::get('staffs_under_by');
                // }
                // else
                // {
                //     $vendor_id = Session::get('vendor_id');
                // }
                $post_data = $request->all();

                $property_id = Session::get('manager_outlet');

                $vendor_id = Session::get('manager_vendor');


                $orders  = DB::table('booking_details')
                                    ->select(DB::Raw('sum(payments.paid_amount) as payments_amount,
                                                      sum(booking_charges.price) as booking_charges_amount'
                                                    ),
                                            'booking_details.booking_random_id',
                                            'booking_details.check_out_date',
                                            'booking_details.check_in_date',
                                            'admin_customers.firstname')
                                    ->leftJoin('booking_charges','booking_charges.booking_id','=','booking_details.id')
                                    ->leftJoin('payments','payments.booking_id','=','booking_details.id')
                                    ->leftJoin('booked_room','booked_room.booking_id','booking_details.id')
                                    ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                    ->where('booking_details.outlet_id','=',$property_id)
                                    ->where('booking_details.vendor_id','=',$vendor_id)
                                    ->groupBy('booking_details.id','admin_customers.firstname');
                                    //  ->get();

                                    // print_r($orders);
                                    // exit;

                return Datatables::of($orders)->addColumn('date_start', function ($orders) {
                    $data = date("M-d-Y, l h:i:a", strtotime($orders->check_in_date));
                    return $data;
                })
                ->addColumn('date_end', function ($orders) {
                    $data = date("M-d-Y, l h:i:a", strtotime($orders->check_out_date));
                    return $data;
                })
/*                
                ->addColumn('tax_total', function ($orders) {
                    if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$orders->tax_total;
                    }
                    else {
                        return $orders->tax_total.getCurrency();
                    }
                })
*/                
                ->addColumn('total_charges', function ($orders) {
                    if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$orders->booking_charges_amount;
                    }
                    else {
                        return $orders->booking_charges_amount.getCurrency();
                    }
                })
                ->addColumn('total_payments', function ($orders) {
                    if($orders->payments_amount == "")
                    {
                        return '';
                    }
                    else if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$orders->payments_amount;
                    }
                    else {
                        return $orders->payments_amount.getCurrency();
                    }
                })  
                ->addColumn('due_amount', function ($orders) {

                    $due_amount = $orders->booking_charges_amount - $orders->payments_amount;

                    if($due_amount == 0)
                    {
                        return '';
                    }
                    else if(getCurrencyPosition()->currency_side == 1)
                    {
                        return getCurrency().$due_amount;
                    }
                    else {
                        return $due_amount.getCurrency();
                    }
                })                               
                ->filter(function ($query) use ($request){
                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and booked_room.date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                
                    /*
                    if ($request->has('from') != '' && $request->has('to') != '')
                    {
                        $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                        $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                        $condition1 = $condition." and orders.created_date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";

                        $query->whereRaw($condition1);
                    }
                    if ($request->has('order_status') != '')
                    {
                        $order_status = Input::get('order_status');
                        $condition2   = $condition." and orders.order_status = ".$order_status;

                        $query->whereRaw($condition2);
                    }
                    if ($request->has('group_by'))
                    {
                        $group_by = ($request->get('group_by') != '')?$request->get('group_by'):1;
                        if($group_by == 1)
                            $start_date = " date_trunc('day', orders.created_date) AS date_start, ";
                        else if($group_by == 2)
                            $start_date = " date_trunc('week', orders.created_date) AS date_start, ";
                        else if($group_by == 3)
                            $start_date = " date_trunc('month', orders.created_date) AS date_start, ";
                        else if($group_by == 4)
                            $start_date = " date_trunc('year', orders.created_date) AS date_start, ";
                        $query->selectRaw($start_date.' MAX(orders.created_date) AS date_end')->groupBy('date_start');
                    }
                    */
                })
                ->make(true);
            }

            public function date_reports($d)
            {
                //  dd($d);

                if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }
                else
                {
                
                    $date = $d;

                    //dd($date);

                 if (Session::get('manager_id')) {

                        $property_id = Session::get('manager_outlet');
                        $vendor_id = Session::get('manager_vendor');
                        
                    }                     

                    $property_id = Session::get('property_id');
                    $vendor_id = Session::get('vendor_id');

                    //  dd($vendor_id);

                    $booking_details = DB::table('booking_details')
                                        ->select(DB::RAW('count(booking_details.id) as booking_count'), 'admin_customers.firstname','booking_details.*','booking_info.*','booking_status.*')
                                        ->leftJoin('booking_info','booking_info.booking_id','booking_details.id')
                                        ->leftJoin('admin_customers','admin_customers.id','booking_details.customer_id')
                                        ->leftJoin('booking_status','booking_status.id','booking_details.booking_status')
                                        //  ->where('booking_details.vendor_id','=',$vendor_id)
                                        ->where('booking_details.created_date',$d)
                                        ->where('booking_details.outlet_id',$property_id)
                                        ->groupBy('booking_details.id','admin_customers.firstname',
                                            'booking_info.id','booking_status.id')
                                        ->get();

                          //    dd($booking_details);
                    

                    $booking_charges = DB::table('booking_charges')
                                         ->selectRaw('sum(price) as booking_charge')
                                         ->where('created_date',$d)
                                         ->where('property_id',$property_id)
                                         // ->where('created_by',$vendor_id)
                                         ->get();
                    //  dd($booking_charges);

                    $payments = DB::table('payments')
                                         ->selectRaw('sum(paid_amount) as paid_amount')
                                         ->where('created_date',$d)
                                         // ->where('created_by',$vendor_id)
                                         ->where('property_id',$property_id)
                                         ->get();

                    $payment_list = DB::table('payments')
                                    ->select('payments.id as payment_id','payments.active_status as payment_status','payments.*','admin_customers.*','booking_details.booking_random_id')
                                    ->leftJoin('admin_customers','admin_customers.id','payments.customer_id')
                                    ->leftJoin('booking_details','booking_details.id','payments.booking_id')
                                    //  ->where('payments.vendor_id','=',$vendor_id)
                                    ->where('payments.property_id','=',$property_id)
                                    ->where('payments.created_date',$d)
                                    ->orderBy('payments.id', 'asc')
                                    ->get(); 

                    //  dd($payment_list);

                    $charges_list = DB::table('booking_charges')
                                                ->select('booking_charges.id as bc_id','booking_charges.notes as bs_notes','booking_charges.active_status as booking_status','booking_charges.*','admin_customers.*','booking_details.booking_random_id')                                       
                                                ->leftJoin('booking_details','booking_details.id','booking_charges.booking_id')
                                                ->leftJoin('admin_customers','admin_customers.id','booking_charges.customer_id')
                                                //  ->where('booking_charges.created_by','=',$vendor_id)
                                                ->where('booking_charges.property_id','=',$property_id)    
                                                ->where('booking_charges.created_date',$d)
                                                ->orderBy('booking_charges.id','asc')
                                                ->get();

                      //    dd($charges_list);


                    return view('vendors.reports.orders.show')->with('booking_details',$booking_details)
                                                              ->with('charges_list',$charges_list)
                                                              ->with('payment_list',$payment_list)
                                                              ->with('booking_charges',$booking_charges[0])
                                                              ->with('payments',$payments[0])
                                                              ->with('date',$date);
                }
                //  dd($booking_charges);
            }

            public function outlets_report()
            {

                if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }              
                else{

                    $property_id = Session::get('property_id');

                    $vendor_id = Session::get('vendor_id');

                    $date = date('Y-m-d H:i:s');

                    $date_show = date("l, M-d-Y", strtotime($date));


                    $date_data = DB::table('booking_details')
                                            ->select(DB::Raw('MIN(created_date) as min_date, 
                                                MAX(created_date) as max_date'))
                                            ->get();

                    return view('vendors.reports.outlets.list')->with('date',$date)
                                                                   ->with('date_show',$date_show)
                                                                   ->with('date_data',$date_data[0]);
                                                                   
                }            
            }


            public function anyAjaxOutletsReportList(Request $request)
            {
                $post_data = $request->all();

                //  print_r($post_data); exit;

                $vendor_id = Session::get('vendor_id');
                
                $booking_details  = DB::table('outlets')
                                    ->select(DB::Raw('count(booking_details.id) as booking_count,
                                                      sum(booking_details.charges) as charges_amount,
                                                      sum(booking_details.payments) as payments_amount'
                                                    ),
                                            'outlets.id',
                                            'outlet_infos.outlet_name')
                                    ->leftJoin('outlet_infos','outlet_infos.id','outlets.id')
                                    ->leftJoin('booking_details','booking_details.outlet_id','=','outlets.id')
                                    ->groupBy('outlets.id','outlet_infos.outlet_name')
                                    ->where('outlets.vendor_id','=',$vendor_id)
                                    ->distinct();
                                      //    ->get();

                 // print_r($booking_details);exit;

                //  $oc = DB::table('outlets')->where('vendor_id','=',2)->count();


                                  
                // echo "<pre>";
                // print_r($oc);exit;

                return Datatables::of($booking_details)->addColumn('payments_amount', function ($booking_details) {
                    if ($booking_details->payments_amount == 0) {
                      return '-';
                    } else if(getCurrencyPosition()->currency_side == 1) {
                        return getCurrency()." ".$booking_details->payments_amount;
                    } else {
                        return $booking_details->payments_amount." ".getCurrency();
                    }
                })
                    ->addColumn('charges_amount', function ($booking_details) {
                                        if ($booking_details->charges_amount == "") {
                                          return '-';
                                        } else if(getCurrencyPosition()->currency_side == 1) {
                                            return getCurrency()." ".$booking_details->charges_amount;
                                        } else {
                                            return $booking_details->charges_amount." ".getCurrency();
                                        }
                   })                
                // ->addColumn('tax_total', function ($booking_details) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$booking_details->tax_total;
                //     }
                //     else {
                //         return $booking_details->tax_total.getCurrency();
                //     }
                // })
                ->filter(function ($query) use ($request){

                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and booking_details.created_date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                })
                ->make(true);
            }

            public function staff_report()
            {

                if (!Session::get('vendor_id'))
                    {
                        return redirect()->guest('vendors/login');
                    }              
                else{

                    $property_id = Session::get('property_id');

                    $vendor_id = Session::get('vendor_id');

                    $date = date('Y-m-d H:i:s');

                    $date_show = date("l, M-d-Y", strtotime($date));


                    $date_data = DB::table('booking_details')
                                            ->select(DB::Raw('MIN(created_date) as min_date, 
                                                MAX(created_date) as max_date'))
                                            ->get();
                    //  dd($outlet_managers);

                    return view('vendors.reports.staff.list')->with('date',$date)
                                                                   ->with('date_show',$date_show)
                                                                   ->with('date_data',$date_data[0]);
                                                                   
                }            
            }


            public function anyAjaxStaffsReportList(Request $request)
            {
                $post_data = $request->all();

                //  print_r($post_data); exit;

                $vendor_id = Session::get('vendor_id');

                $property_id = Session::get('property_id');
                
                $outlet_managers = DB::table('outlet_managers')
                                        ->select('outlet_managers.id','outlet_managers.first_name')
                                        ->leftJoin('vendors_infos','vendors_infos.vendors_view_id','=','outlet_managers.vendor_id')
                                        ->leftJoin('outlets','outlets.id','=','outlet_managers.outlet_id')
                                        ->leftJoin('outlet_infos','outlet_infos.id','=','outlets.id')
                                        ->where('outlet_managers.outlet_id','=',$property_id)
                                        ->where('outlet_managers.vendor_id','=',$vendor_id)
                                        ->orderBy('outlet_managers.id', 'desc');
                                          //    ->get();

                  //    print_r($outlet_managers);exit;

                //  $oc = DB::table('outlets')->where('vendor_id','=',2)->count();


                                  
                // echo "<pre>";
                // print_r($oc);exit;

                return Datatables::of($outlet_managers)->addColumn('payments_amount', function ($outlet_managers) {
                    if ($outlet_managers->payments_amount == 0) {
                      return '-';
                    } else if(getCurrencyPosition()->currency_side == 1) {
                        return getCurrency()." ".$outlet_managers->payments_amount;
                    } else {
                        return $outlet_managers->payments_amount." ".getCurrency();
                    }
                })
                    ->addColumn('charges_amount', function ($outlet_managers) {
                                        if ($outlet_managers->charges_amount == "") {
                                          return '-';
                                        } else if(getCurrencyPosition()->currency_side == 1) {
                                            return getCurrency()." ".$outlet_managers->charges_amount;
                                        } else {
                                            return $outlet_managers->charges_amount." ".getCurrency();
                                        }
                   })                
                // ->addColumn('tax_total', function ($outlet_managers) {
                //     if(getCurrencyPosition()->currency_side == 1)
                //     {
                //         return getCurrency().$outlet_managers->tax_total;
                //     }
                //     else {
                //         return $outlet_managers->tax_total.getCurrency();
                //     }
                // })
                ->filter(function ($query) use ($request){

                    $condition = '1=1';
                    $fr = $request->from;
                    $t =  $request->to; 

                    if(!empty($fr) && !empty($t))
                    {   
                            $from = date('Y-m-d H:i:s', strtotime($request->get('from')));
                            $to   = date('Y-m-d H:i:s', strtotime($request->get('to')));
                            $condition1 = $condition." and outlet_managers.created_date BETWEEN '".$from."'::timestamp and '".$to."'::timestamp";
                            $query->whereRaw($condition1);                           
                    }
                })
                ->make(true);
            }                         

}
